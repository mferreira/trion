_ = require('lib/vendor/underscore-min')
URI = require('lib/vendor/jsuri.min')
cacheLib = require('lib/cache')
hawk = require('lib/vendor/hawk/hawk')
utils = require 'lib/utils'

appName = GLOBAL.appName
timeout = 15 # in seconds

tentMimeType = 'application/vnd.tent.post.v0+json; charset=utf-8'

uri =
	redirect: 'trion://tentauth'
	tent_notification_url: 'http://apn.tentrapp.com'
	app_url: "https://itunes.apple.com/us/app/trion/id589295052"
	url_template: /\{(.+?)\}/g

content_types =
	Post: "application/vnd.tent.post.v0+json"
	Mentions: "application/vnd.tent.post-mentions.v0+json"
	Children: "application/vnd.tent.post-children.v0+json"
	Feed: "application/vnd.tent.posts-feed.v0+json"
	Versions: "application/vnd.tent.post-versions.v0+json"
	All: "*/*"

post_types =
	"App": "https://tent.io/types/app/v0"
	"AppAuthorization": "https://tent.io/types/app-auth/v0"
	"Credentials": "https://tent.io/types/credentials/v0"
	"Status": "https://tent.io/types/status/v0"
	"Essay": "https://tent.io/types/essay/v0"
	"Photo": "https://tent.io/types/photo/v0"
	"Album": "https://tent.io/types/album/v0"
	"Repost": "https://tent.io/types/repost/v0"
	"Profile": "https://tent.io/types/profile/v0" # TODO: IMPLEMENT API
	"Delete": "https://tent.io/types/delete/v0"
	"Follower": "https://tent.io/types/follower/v0" # TODO: IMPLEMENT API
	"Following": "https://tent.io/types/following/v0" # TODO: IMPLEMENT API
	"Group": "https://tent.io/types/group/v0" # TODO: IMPLEMENT API
	"Favorite": "https://tent.io/types/favorite/v0"
	"Tag": "https://tent.io/types/tag/v0"
	"Cursor": "https://tent.io/types/cursor/v0"
	"Relationship": "https://tent.io/types/relationship/v0"
	"Subscription": "https://tent.io/types/subscription/v0"
	"DeliveryFailure": "https://tent.io/types/delivery-failure/v0"

post_types.postTypeProperties = {}
post_types.postTypeProperties[post_types.Status] =
	textLimit: 256

app_supported_post_types =
	"read": [
		post_types.App
		post_types.App
		post_types.Follower
	]
	"write": [
		post_types.Profile
		post_types.Status
		post_types.Repost
		post_types.Photo
		post_types.Following
		post_types.Delete
		post_types.Group
	]

app_notification_post_types = [
	post_types.Status
	post_types.Repost
	post_types.Photo
	post_types.Follower
]

app_required_scopes = [
	"permissions"
	# "read_posts"
	# "write_posts"
	# "read_profile"
	# "write_profile"
	# "read_followers"
	# "read_followings"
	# "write_followings"
	# "read_groups"
	# "write_groups"
	# "read_permissions"
	# "read_apps"
]

cachePropName = 'apiCache'
defaultCache =
	discover: {}
	avatars: {}
	uris: []
	registerApp: null
	appRegistered: false
	oauth: false
	access_token: null
	token_type: "https://tent.io/oauth/hawk-token"
	credentials: {}
	# posts related stuff
	posts:
		last_id: null
		last_time: null
		last_id_entity: null
	# mentions related stuff
	mentions:
		last_id: null
		last_time: null
		last_id_entity: null

cache = _.defaults( cacheLib.loadCache(cachePropName), defaultCache)
tempCache =
	conQ:
		id: 0
		q: {}


saveCache = ->
	cacheLib.saveCache cachePropName, cache

checkNetwork = (callback) ->
	Ti.API.debug "Checking if network is available ..."
	if (Ti.Network.online)
		Ti.API.debug "... device is online"
		callback.online.apply(callback.this)
	else
		Ti.API.warn "... device is offline"
		if callback.offline
			callback.offline.apply(callback.this)

XHR = (method, url, args) ->
	tempCache.conQ.id += 1
	myId = tempCache.conQ.id
	xhr = Ti.Network.createHTTPClient
		conId: myId
		timeout: timeout *1000 # in milliseconds
		tlsVersion: Ti.Network.TLS_VERSION_1_2
		withCredentials: true
		# called when the response data is available
		onload: (e) ->
			# clearTimeout(timeoutTimer);
			Ti.API.log "xhr::#{this.conId}", this.statusText
			# Ti.API.log('xhr', 'conQ -> '+JSON.stringify(tempCache.conQ.q));
			if tempCache.conQ.q[this.conId]
				tempCache.conQ.q[this.conId] = null
				delete tempCache.conQ.q[this.conId]
				Ti.API.log "xhr::#{this.conId}", "HTTP Server-Authorization: #{this.getResponseHeader( 'Server-Authorization' )}"
				Ti.API.log "xhr::#{this.conId}", "Received text: #{this.responseText}" #if method != 'HEAD'
				# Ti.API.log 'xhr',"Received Data: " + this.responseData
				responseParsed = if args.noJson then '' else JSON.parse(this.responseText or '{}')
				args.onload.call(args.this or this, {json: responseParsed, raw: this.responseText, data: this.responseData, status: this.status}) if args.onload
			args.onload = null
		# function called when an error occurs, including a timeout
		onerror: (e) ->
			# clearTimeout(timeoutTimer);
			Ti.API.log "xhr::#{this.conId}", this.statusText
			Ti.API.error "#{e.status} #{this.responseText}"
			# Ti.API.log('xhr', 'conQ -> '+JSON.stringify(tempCache.conQ.q));
			if (tempCache.conQ.q[this.conId])
				tempCache.conQ.q[this.conId] = null
				delete tempCache.conQ.q[this.conId]
				# alert('ERROR: '+e.error);
				args.onerror.call( args.this or this, {status: this.status, error: this.responseText} ) if (args.onerror)
			args.onerror = null
		onreadystatechange: ->
			Ti.API.log "xhr::#{this.conId}", "Ready state: #{['UNSENT', 'OPENED', 'HEADERS_RECEIVED', 'LOADING', 'DONE'][this.readyState]}"
		
	xhr.open( method, url, args.async or true )

	xhr.setRequestHeader( header, value ) for own header, value of args.headers if args.headers

	tempCache.conQ.q[myId] = xhr

	Ti.API.log "xhr::#{xhr.conId}", "#{method}: #{url}"
	Ti.API.log "xhr::#{xhr.conId}", "HEADER: #{JSON.stringify(args.headers)}"

	tempCache.conQ.q[myId]

cancelXHR = (id) ->
	Ti.API.log 'api', "cancelXHR <- #{JSON.stringify(id)}"
	if id
		if (typeof id == 'number' && tempCache.conQ.q[id])
			Ti.API.warn('Terminating XHR with ID: '+id)
			tempCache.conQ.q[id].abort()
			delete tempCache.conQ.q[id]
		else
			for _id in id
				if (tempCache.conQ.q[_id])
					Ti.API.warn('Terminating XHR with IDs: '+_id)
					tempCache.conQ.q[_id].abort()
					delete tempCache.conQ.q[_id]


class API

	XHR: XHR
	cancelXHR: cancelXHR
	checkNetwork: checkNetwork

	execIfOnline: (execThis) ->
		checkNetwork
			online: execThis
			offline: -> alert 'Looks like you are offline.'

	tempCache: ->
		return tempCache

	getCache: (req) ->
		# Ti.API.log('api', 'getCache called: '+ req +' > '+JSON.stringify(cache[req]));
		cache[req]

	clearAuthCache: ->
		cache.registerApp = null
		cache.credentials = {}
		cache.secret_code = null

	isAuthenticated: ->
		# Ti.API.debug('cache:  '+JSON.stringify(cache));
		# Ti.API.debug('isAuthenticated: '+!!(cache.access_token && cache.registerApp.mac_key_id && cache.registerApp.mac_key && cache.registerApp.id));
		if cache.registerApp
			if cache.access_token && cache.appRegistered && cache.oauth && cache.registerApp.id
				return true
			else if cache.access_token && cache.registerApp.mac_key_id && cache.registerApp.mac_key && cache.registerApp.id
				cache.appRegistered = true
				cache.oauth = true
				saveCache()
				return true
		return false

	getHawkCredentials: ->
		ret = 
			id: cache.credentials.id
			key: cache.credentials.content.hawk_key
			algorithm: cache.credentials.content.hawk_algorithm
		# Ti.API.log 'api', 'getHawkCredentials -> '+JSON.stringify(ret)
		ret

	generateAuthHeader: (method, url, contentType = null, payload = null) =>
		hawk.client.header( url, method,
			credentials: @getHawkCredentials()
			contentType: contentType
			payload: payload
			app: cache.registerApp.id
		).field

	postType: (type) ->
		post_types[type]

	activePostTypes: ->
		# TODO: add support for more post types
		[post_types.Status, post_types.Repost, post_types.Photo]

	getAppInfo: ->
		type: post_types.App+"#"
		content:
			name: appName,
			description: "Lets you shoot amazing posts from your iOS device to the Tent community."
			url: uri.app_url
			types: app_supported_post_types
			redirect_uri: uri.redirect
			notification_url: uri.tent_notification_url
			notification_types: app_notification_post_types,
			scopes: app_required_scopes
		permissions:
			public: false

	getApiRootUrls: (entity) ->
		entity = entity || GLOBAL.entity;
		# Ti.API.log 'api',"getApiRootUrls() <- #{entity}"
		# Ti.API.log 'api',"getApiRootUrls() -- cache.discover[entity] -> #{JSON.stringify(cache.discover[entity])}"
		ret = if cache.discover[entity] and cache.discover[entity].servers then cache.discover[entity].servers[0].urls else {}
		# Ti.API.log 'api',"getApiRootUrls() -> #{JSON.stringify(ret)}"
		ret;

	clearDiscoveryCache: ->
		Ti.API.log 'api', 'Clearing discovery cache.'
		# Ti.API.log('cache.discover: '+JSON.stringify(cache.discover));
		me = cache.discover[GLOBAL.entity]
		cache.discover = {}
		cache.discover[GLOBAL.entity] = me
		saveCache()
		Ti.App.Properties.setString('discover', JSON.stringify(cache.discover))
		Ti.API.debug "cache.discover: #{JSON.stringify(cache.discover)}"

	doHeaderDiscovery: (defined_uri, headerLink, callback) =>
		Ti.API.log 'api', "doHeaderDiscovery <- #{defined_uri}, #{headerLink}"
		# cache all uri
		re_match = /<([^>]*)>\; rel\=\"https\:\/\/tent.io\/rels\/meta-post\"/ig
		while (match = re_match.exec(headerLink))
			if match[1].match /(https?:\/\/[^>]*)/i
				cache.uris[defined_uri].push( match[1] )
			else
				cache.uris[defined_uri].push( defined_uri + match[1] )

		saveCache()

		url = cache.uris[defined_uri][0];

		# grab the profile api roots
		xhr = new XHR 'GET', url,
			onload: (resp) ->
				Ti.API.debug 'Discovery finish successfuly. Saving info.'

				discoveredData = resp.json.post.content
				attachments = resp.json.post.attachments
				# cache.discover[defined_uri] = discoveredData
				cache.discover[discoveredData.entity] = discoveredData
				if attachments and attachments.length
					cache.avatars[discoveredData.entity] = _.findWhere attachments, {category: 'avatar'}
				saveCache()

				Ti.API.debug 'firing callback / event'
				# Ti.API.info('cache: '+JSON.stringify(cache));
				if (callback)
					callback.onload.apply(callback.thiz || this, if callback.data then [discoveredData, callback.data] else [discoveredData] )
				else
					Ti.App.fireEvent 'api:discover:success', discoveredData

			onerror: (resp) ->
				if (callback)
					callback.onerror.apply(callback.thiz || this, if callback.data then [resp, callback.data] else [resp] )
				else
					Ti.App.fireEvent 'api:discover:failed'

			headers:
				'Accept': tentMimeType

		xhr.send()
		return

	discover: (defined_uri, callback, noCache) =>
		Ti.API.log 'api', "discover <- #{defined_uri}"
		return if not defined_uri
		defined_uri = defined_uri.replace(/\/$/g, "")
		Ti.API.debug('Starting discovery on URI: '+defined_uri)
		if cache.discover[defined_uri] && !noCache
			Ti.API.debug 'Discovery data found in cache. Firing success.'
			# Ti.API.debug 'cache.discover ['+defined_uri+']: '+JSON.stringify(cache.discover[defined_uri])
			if callback
				callback.onload.apply(callback.thiz || this, if callback.data then [cache.discover[defined_uri], callback.data] else [cache.discover[defined_uri]] )
			else
				Ti.App.fireEvent('api:discover:success', cache.discover[defined_uri] )

		else
			cache.uris[defined_uri] = [];
			xhr = new XHR "HEAD", defined_uri,
				onload: (resp) =>
					# grab HEAD Link containing the profile uri's
					headerLink = xhr.getResponseHeader 'Link'
					Ti.API.debug 'HEADER Link: '+headerLink
					@doHeaderDiscovery(defined_uri, headerLink, callback)

				onerror: (resp) =>
					headerLink = xhr.getResponseHeader 'Link'
					Ti.API.debug 'HEADER Link: '+headerLink
					if headerLink
						@doHeaderDiscovery(defined_uri, headerLink, callback)
					else if callback
						callback.onerror.apply(callback.thiz || this, if callback.data then [resp, callback.data] else [resp] )
					else
						Ti.App.fireEvent 'api:discover:failed'

				noJson: true

			Ti.API.debug('HEAD: '+defined_uri)
			# Send the request.
			xhr.send()

	registerApp: (forceRegister) =>
		Ti.API.debug 'Starting app registration proccess.'
		checkNetwork
			this: @
			online: =>
				Ti.API.debug JSON.stringify(cache)
				if cache.access_token is null || forceRegister
					Ti.API.debug 'No access token found.'

					getAppCredentials = (url) =>
						xhr = new XHR 'GET', url,
							onload: (resp) =>
								cache.credentials = resp.json.post
								saveCache()
								@oauthApp.call(@, cache)
							headers:
								'Accept': tentMimeType

						xhr.clearCookies url
						xhr.send()

					regFunc = =>
						Ti.API.info 'Registering app to tent service...'
						url = @getApiRootUrls().new_post
						httpData = JSON.stringify(@getAppInfo())

						xhr = new XHR 'POST', url,
							onload: (resp) ->
								Ti.API.debug 'Data recieved from app register: '+resp.raw
								cache.registerApp_headerLink = xhr.getResponseHeader('Link').match(/<([^>]*)>\; rel\=\"https?\:\/\/[\S\/]*"/i)[1]
								cache.registerApp = resp.json.post
								cache.appRegistered = true
								getAppCredentials(cache.registerApp_headerLink)
							noJson: false
							headers:
								'Content-Type': "#{tentMimeType}; type=\"#{post_types.App}#\""
								'Accept': tentMimeType

						Ti.API.debug 'POST: '+url
						Ti.API.debug 'Data: '+httpData
						# Clears any cookies stored for the host
						xhr.clearCookies url
						xhr.send httpData

					if cache.registerApp is null or forceRegister
						regFunc()
					else
						Ti.API.debug 'cache.registerApp: '+JSON.stringify(cache.registerApp)
						Ti.API.info 'App should be already registered with the tent service. Checking...'

						@apps_GET
							callback: (result) =>
								switch result
									when "success"
										Ti.API.debug 'Server accepts this app.'
										Ti.API.debug 'OAuth proccess starting.'
										@oauthApp()
									when "unauthorized"
										Ti.API.warn 'Server did not accept this app. Start registration.'
										cache.access_token = null
										saveCache()
										regFunc()
									else
										Ti.API.debug 'Server error. Failing.'
										Ti.App.fireEvent 'api:registerApp:error'

				else
					Ti.API.debug 'Access token found. Skipping app auth and registration.'

			offline: => alert 'Looks like you are offline.'

	oauthApp: (cacheData) =>
		@cacheData = cacheData || cache;

		_cache = cacheData || cache

		checkNetwork
			this: @
			online: =>
				if _cache.registerApp is null
					@registerApp()

				else if _cache.secret_code and cache.credentials.content
					Ti.API.info 'Secret code found in cache. Skipping OAuth.'
					# exchange the code for the access token
					@grabToken _cache.secret_code

				else
					Ti.API.info 'OAuth app to tent service...'
					tempCache.oauthState = hawk.utils.nonce(32)
					url = "#{@getApiRootUrls().oauth_auth}?client_id=#{cache.registerApp.id}&state=#{tempCache.oauthState}"
					Ti.API.info "Opening browser with url: "+url
					Ti.Platform.openURL url

			offline: => alert 'Looks like you are offline.'

	grabToken: (secret_code) =>
		Ti.API.debug('Exchanging secret code for permanet token.')
		# Ti.API.debug('Using for authHeader: '+JSON.stringify(cache.app_auth))
		cache.access_token = null
		url = @getApiRootUrls().oauth_token
		httpMethod = 'POST'
		contentType = 'application/json'

		httpData =
			code: secret_code
			token_type: cache.token_type

		authHeader = @generateAuthHeader(httpMethod, url, contentType, httpData)

		xhr = new XHR httpMethod, url,
			onload: (resp) =>
				if !resp.json
					Ti.API.debug('Exchanging secrets failed.')
				else
					Ti.API.debug('Updating app auth keys.')
					cache.access_token = resp.json.access_token # TODO: deprecate this in favor of cache.credentials data
					# cache.app_auth = _.extend cache.app_auth, # TODO: deprecate this in favor of cache.credentials data
					# 	mac_key_id: resp.json.access_token
					# 	mac_key: resp.json.mac_key

					# update hawk credentials
					cache.credentials.id = resp.json.access_token
					cache.credentials.content.hawk_key = resp.json.hawk_key
					saveCache()

					Ti.API.debug('We have the OAuth key now: '+cache.credentials.id)
					Ti.App.fireEvent('api:grabToken:success')

			onerror: (resp) =>
				Ti.API.debug('Exchanging secrets failed.')
				if resp.status == 404
					# not found? maybe the auth process was interrupted?
					@cacheData.secret_code = null
					@oauthApp(@cacheData)
				else
					Ti.App.fireEvent('api:grabToken:failed')

			noJson: false

			headers:
				'Content-Type': contentType
				'Accept': contentType
				'Authorization': authHeader

		Ti.API.debug "#{httpMethod}: #{url}"
		Ti.API.debug 'Authorization: '+authHeader
		Ti.API.debug 'Data: '+ JSON.stringify httpData
		xhr.clearCookies url # Clears any cookies stored for the host
		xhr.send JSON.stringify httpData

	apps_GET: (args) =>
		Ti.API.debug 'Starting GET /apps'
		checkNetwork
			this: this
			online: =>
				if !cache.registerApp
					Ti.API.warn 'cache.registerApp is no set. Stopping apps_GET api call.'
					return null

				url = "#{@getApiRootUrls().posts_feed}?types=#{encodeURIComponent(post_types.App)}&limit=1"
				httpMethod = 'GET'
				contentType = "#{content_types.Feed}"
				authHeader = @generateAuthHeader(httpMethod, url)

				callback = if args and args.callback then args.callback else null

				xhr = new XHR httpMethod, url,
					onload: (resp) =>
						if resp.status is 200
							if !resp.json.posts.length
								if (callback) callback('unauthorized');
								else Ti.App.fireEvent('api:apps_GET:unauthorized');
							else
								try
									serverAppAuth = resp.json.posts[0].content
									localAppAuth = @getAppInfo().content

									appIsOK = true

									for auth in localAppAuth.types.read
										appIsOK = false if auth not in serverAppAuth.types.read
									for auth in localAppAuth.types.write
										appIsOK = false if auth not in serverAppAuth.types.write
									for scope in localAppAuth.scopes
										appIsOK = false if scope not in serverAppAuth.scopes
									for scope in localAppAuth.notification_types
										appIsOK = false if scope not in serverAppAuth.notification_types
									appIsOK = false if localAppAuth.name != serverAppAuth.name
									appIsOK = false if localAppAuth.description != serverAppAuth.description
									appIsOK = false if localAppAuth.url != serverAppAuth.url
									appIsOK = false if localAppAuth.redirect_uri != serverAppAuth.redirect_uri
									appIsOK = false if localAppAuth.notification_url != serverAppAuth.notification_url

									Ti.API.info 'Matching app authorization info...'

									if appIsOK
										Ti.API.info "Authorization seems to be ok. Calling :success: ..."
										if callback
											Ti.API.debug "Executing success callback ..."
											callback('success')
										else
											Ti.API.debug "Firing success event ..."
											Ti.App.fireEvent('api:apps_GET:success', resp.json)

									else
										Ti.API.info "Authorization is not ok. Calling :reauth: ..."
										if callback then callback('reauth')
										else Ti.App.fireEvent('api:apps_GET:reauth', resp.json)

								catch err
									Ti.API.error(err)

						else if resp.status in [403, 401]
							Ti.App.fireEvent('api:apps_GET:unauthorized')
							callback('unauthorized') if callback

						else
							Ti.App.fireEvent('api:apps_GET:error', resp)
							callback('error') if callback

					onerror: (resp) ->
						if resp.status in [403, 401]
							Ti.App.fireEvent('api:apps_GET:unauthorized')
							callback('unauthorized') if callback
						else
							Ti.App.fireEvent('api:apps_GET:error', resp)
							callback('error') if callback

					noJson: false

					headers:
						'Accept': contentType
						'Authorization': authHeader

				Ti.API.debug "#{httpMethod}: #{url}"
				Ti.API.debug 'Accept: '+contentType
				Ti.API.debug 'Authorization: '+authHeader
				xhr.clearCookies url # Clears any cookies stored for the host
				xhr.send()
				
				xhr.conId

			offline: => alert('Looks like you are offline.')

	posts_COUNT: (filters, callback) =>
		Ti.API.log 'api', 'posts_COUNT <- '+JSON.stringify(filters)
		checkNetwork
			this: this
			offline: =>
				Ti.API.log 'api', 'posts_COUNT -- Looks like you are offline'
				alert 'Looks like you are offline.'
			online: =>
				url = new URI @getApiRootUrls( if filters && filters.entity then filters.entity else null).posts_feed
				httpMethod = 'HEAD'
				checkingMe = if (filters && filters.entity && filters.entity != GLOBAL.entity) then false else true

				filters = _.defaults filters || {},
					types: @activePostTypes().join(',')

				Ti.API.log 'api', 'posts_GET -- Encoding filters into the url.'
				for own filter of _.omit(filters, 'entity')
					url.addQueryParam filter, encodeURIComponent(filters[filter])

				url = url.toString()

				xhrArgs =
					this: this
					onload: (resp) ->
						_count = xhr.getResponseHeader( 'Count' );
						Ti.App.Properties.setString('posts_count', _count);
						if callback and callback.onload then callback.onload( _count )
						else Ti.App.fireEvent('api:posts_COUNT:success', {count: _count})
					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror( resp )
						else Ti.App.fireEvent('api:posts_COUNT:error', resp)
					headers:
						'Accept': "#{content_types.Feed}"

				if checkingMe
					authHeader = @generateAuthHeader(httpMethod, url)
					xhrArgs.Authorization = authHeader
				
				xhr = new XHR httpMethod, url, xhrArgs

				Ti.API.debug "#{httpMethod}: #{url}"
				Ti.API.debug 'Filters: '+JSON.stringify(filters) if filters
				Ti.API.debug 'Authorization: '+authHeader if authHeader
				
				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send()
				xhr.conId
		return

	posts_GET: (args) =>
		Ti.API.log 'api', 'posts_GET <- '+JSON.stringify(args)
		checkNetwork
			this: this
			online: =>

				url = new URI @getApiRootUrls().posts_feed
				httpMethod = 'GET'

				filters = _.defaults args.filters || {},
					limit: 10
					types: @activePostTypes().join(',')

				Ti.API.log 'api', 'posts_GET -- Encoding filters into the url.'
				for own filter of filters
					# Ti.API.debug filter +' > '+ encodeURIComponent(filters[filter])
					url.addQueryParam filter, encodeURIComponent(filters[filter])

				url = url.toString()

				xhrArgs =
					this: args.this
					onload: args.onload
					onerror: args.onerror
					headers:
						'Accept': "#{content_types.Feed}"

				if args.auth
					authHeader = @generateAuthHeader(httpMethod, url)
					xhrArgs.headers.Authorization = authHeader

				xhr = new XHR httpMethod, url, xhrArgs

				Ti.API.debug "#{httpMethod}: #{url}"
				Ti.API.debug 'Filters: '+JSON.stringify(filters) if filters
				Ti.API.debug 'Authorization: '+authHeader if args.auth
				
				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send()
				xhr.conId
			
			offline: =>
				alert('Looks like you are offline.');

	posts_OLD: (filters, callback) ->
		Ti.API.log 'api', 'posts_OLD <- '+JSON.stringify(filters)
		checkNetwork
			online: =>
				@stream_posts_GET filters, callback

				# # OLD METHOD
				# apiRootUrls = this.getApiRootUrls(),
				# url = apiRootUrls[i]+'/posts';

				# var xhr = new XHR({
				# 	onload: function(resp){
				# 		var postsLen = resp.json.length;
				# 		Ti.API.debug('Received '+ postsLen +' posts.');
				# 		if (callback && callback.onload) callback.onload( resp.json );
				# 		else Ti.App.fireEvent('api:posts_OLD:success', {posts: resp.json});
				# 	},
				# 	onerror: function(resp) {
				# 		if (callback && callback.onerror) callback.onerror( resp );
				# 		else Ti.App.fireEvent('api:posts_OLD:error', resp);
				# 	},
				# 	noJson: false
				# });

				# this.posts_GET({
				# 	xhr: xhr,
				# 	filters: _.extend({
				# 		limit: 10
				# 	}, filters),
				# 	url: url,
				# 	auth: true
				# });
				
				# xhr.conId

			offline: ->
				alert 'Looks like you are offline.'

	posts_POST: (payload, callback) ->
		Ti.API.log 'api', 'posts_POST <- '+JSON.stringify(payload)
		checkNetwork
			online: =>
				payload.type += "#"
				payload.type += "reply" if payload.isReply
				# remove 'isReply'
				payload = _.omit(payload, 'isReply') if payload.isReply
				# 'permissions' Should be omitted if public member is true.
				payload.permissions = _.omit(payload.permissions, 'entities') if not payload.permissions.entities.length
				# since its optional, lets remove it if empty
				payload = _.omit(payload, 'mentions') if not payload.mentions.length

				# url = "http://posttestserver.com/post.php?dir=example"
				url = @getApiRootUrls().new_post
				httpMethod = 'POST'

				if payload.attachments
					Ti.API.log 'api', 'posts_POST -- Payload has attachments. Creating multipart head...'
					attachments = payload.attachments
					payload = _.omit(payload, 'attachments')
					httpData = JSON.stringify(payload)

					boundary = "-----------------------#{utils.randomString(15)}"
					contentType = "multipart/form-data; boundary=\"#{boundary}\""

					content = '--'+ boundary + '\r\n'
					content += 'Content-Disposition: form-data; name="post"; filename="post.json"\r\n'
					content += "Content-Type: #{content_types.Post}; type=\"#{payload.type}\"\r\n"
					content += "Content-Length: #{httpData.length}\r\n"
					content += '\r\n'
					content += "#{httpData}\r\n"
					full_content = Ti.createBuffer({value: content})

					# Ti.API.log 'api', "posts_POST -- multipart head BEGIN"
					# Ti.API.debug "#{content}"
					# Ti.API.log 'api', "posts_POST -- multipart head END"
					# Ti.API.log 'api', "posts_POST -- Starting multipart body..."

					for blob, i in attachments
						# create a temp file containing the image blob
						file = Titanium.Filesystem.createTempFile();
						file.write(blob)

						content = '--'+ boundary + '\r\n'
						content += "Content-Disposition: form-data; name=\"photo[#{i}]\"; filename=\"#{file.name}\"\r\n"
						content += "Content-Type: #{blob.mimeType}\r\n"
						content += "Content-Length: #{file.size}\r\n"
						content += "\r\n"
						full_content.append(Ti.createBuffer({value: content}))
						full_content.append(utils.toBuffer(blob))
						full_content.append(Ti.createBuffer({value: "\r\n"}))

						# Ti.API.log 'api', "posts_POST -- Appended File#{i} Size: #{file.size}"

					content = '--'+ boundary + '--\r\n'
					full_content.append(Ti.createBuffer({value: content}))
					content = full_content.toBlob()
					# Ti.API.log 'api', "posts_POST -- Finished composing multipart:"

				else
					content = JSON.stringify(payload)
					contentType = "#{content_types.Post}; type=\"#{payload.type}\""


				xhr = new XHR httpMethod, url,
					onload: (resp) ->
						last_post = resp.json.post;
						if callback and callback.onload then callback.onload({post: last_post})
						else Ti.App.fireEvent('api:posts_POST:success', {post: last_post});
					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror(resp);
						else Ti.App.fireEvent('api:posts_POST:error', resp);
					noJson: false
					headers:
						'Content-Type': contentType
						'Authorization': @generateAuthHeader(httpMethod, url, contentType)

				Ti.API.debug 'Data: '+content
				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send content

			offline: =>
				alert 'Looks like you are offline.'

	posts_DELETE: (args, callback) ->
		Ti.API.log 'api', "posts_DELETE <- #{JSON.stringify(args)}"
		checkNetwork
			offline: => alert 'Looks like you are offline.'
			online: =>
				url = _.template(@getApiRootUrls().post, {entity: encodeURIComponent(GLOBAL.entity), post: args.id}, {interpolate: uri.url_template})
				httpMethod = 'DELETE'
				contentType = "#{content_types.Post}"

				eventData = args.eventData

				xhr = new XHR httpMethod, url,
					onload: (resp) ->
						if callback and callback.onload then callback.onload eventData
						else Ti.App.fireEvent 'api:posts_DELETE', _.extend(eventData, {callback: 'success'})
					onerror: (resp) ->
						if resp.status == 404
							if callback and callback.onload then callback.onload eventData
							else Ti.App.fireEvent 'api:posts_DELETE', _.extend(eventData, {callback: 'success'})
						else
							if callback and callback.onerror then callback.onerror resp
							else Ti.App.fireEvent 'api:posts_DELETE', _.extend(resp, {callback: 'error'})
					noJson: false
					headers:
						'Content-Type': contentType
						'Authorization': @generateAuthHeader(httpMethod, url, contentType)

				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send()

	stream_posts_GET: (filters, callback) =>
		# accepted filters:
		# since_id		Posts made since a specific post identifier.
		# before_id		Posts made before a specific post identifier.
		# since_time	Posts with published_at greater than the specified unix epoch.
		# before_time	Posts with published_at less than the specified unix epoch.
		# entities		Selects posts published by one or more comma-separated entities.
		# types			Posts that match specific comma-separated type URIs.
		# limit			The number of posts to return (defaults to the maximum of 200).
		# mentions 		Each mention is a entity url optionally followed by a space and a post ID (comma-separated)
		#
		Ti.API.log 'api', "stream_posts_GET <- #{JSON.stringify(filters)}"
		checkNetwork
			this: this
			offline: => alert 'Looks like you are offline.'
			online: =>
				apiRootUrls = @getApiRootUrls()
				isMentions = (filters && filters.mentions)

				if apiRootUrls == {}
					Ti.API.warn "stream_posts_GET -- Entity root URLs are not set. Stopping API call."
					Ti.App.fireEvent 'api:posts_GET:success', {posts: []}
					return;

				if isMentions and cache.mentions.last_id and !filters.since_id
					newFilters = {since_id: cache.mentions.last_id, since_id_entity: cache.mentions.last_id_entity}
					filters = if filters then _.extend(newFilters, _.omit(filters, 'mentions')) else newFilters

				else if !isMentions and cache.posts.last_id and !filters.since_id
					newFilters = {since_id: cache.posts.last_id, since_id_entity: cache.posts.last_id_entity}
					filters = if filters then _.extend(newFilters, filters) else newFilters

				@posts_GET
					this: @
					onload: (resp) ->
						postsLen = resp.json.posts.length
						Ti.API.log 'api', "stream_posts_GET -- Received #{postsLen} posts."
						if callback and callback.onload then callback.onload({posts: resp.json.posts, pages: resp.json.pages})
						else
							eventName = if isMentions then 'api:mentions_GET:success' else 'api:posts_GET:success'
							Ti.App.fireEvent eventName, {posts: resp.json.posts, pages: resp.json.pages}

					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror(resp)
						else
							eventName = if isMentions then 'api:mentions_GET:error' else 'api:posts_GET:error'
							Ti.App.fireEvent eventName, resp

					filters: filters
					auth: true
		return

	mentions_GET: (filters, callback) =>
		Ti.API.log 'api', "mentions_GET <- #{JSON.stringify(filters)}"
		checkNetwork
			this: this
			offline: => alert 'Looks like you are offline.'
			online: =>
				entity = if filters and filters.entity then filters.entity else GLOBAL.entity
				@discover entity,
					onload: (discoverData) =>
						Ti.API.debug 'cache.discover: '+JSON.stringify(discoverData)
						mentionsFilters =
							mentions: entity
							eventName: 'mentions_GET'
						
						_.extend(mentionsFilters, filters) if filters
						@stream_posts_GET(mentionsFilters, callback);
		return

	deleted_post_GET: (filters, callback) =>
		Ti.API.log 'api', "deleted_post_GET <- #{JSON.stringify(filters)}"
		checkNetwork
			this: this
			offline: => alert 'Looks like you are offline.'
			online: =>
				@stream_posts_GET _.extend({types: post_types.Delete}, filters), callback

	entity_posts_GET: (filters, callback) ->
		Ti.API.log 'api', "entity_posts_GET <- #{JSON.stringify(filters)}"
		checkNetwork
			offline: =>
				Ti.API.log 'api', 'entity_posts_GET -- Looks like you are offline'
				alert 'Looks like you are offline.'
			online: =>
				api_posts_GET = @posts_GET

				@discover filters.entities,
					onload: =>
						@posts_GET
							this: @
							onload: (resp) ->
								if callback and callback.onload
									callback.onload({posts: resp.json.posts, pages: resp.json.pages})
								else
									Ti.App.fireEvent 'api:entity_posts_GET:success', {posts: resp.json}

							onerror: (resp) ->
								if callback and callback.onerror
									callback.onerror(resp)
								else
									Ti.App.fireEvent 'api:entity_posts_GET:error', resp

							filters: filters
							auth: true

					onerror: (resp) ->
						if callback
							callback.onerror(resp)
						else
							Ti.App.fireEvent 'api:entity_posts_GET:error', resp
		return

	entity_post_GET: (args) ->
		Ti.API.log 'api', "entity_post_GET <- #{JSON.stringify(args)}"
		checkNetwork
			offline: =>
				Ti.API.log 'api', 'entity_post_GET -- Looks like you are offline'
				alert 'Looks like you are offline.'
			online: =>
				entity = args.entity
				post_id = args.post_id
				entity_profile = null
				callback = args.callback

				@discover entity,
					onload: =>
						entity_profile = cache.discover[entity]
						Ti.API.debug "entity_profile: #{JSON.stringify entity_profile}"

						url = _.template(@getApiRootUrls().post, {entity: encodeURIComponent(entity), post: post_id}, {interpolate: uri.url_template})
						httpMethod = 'GET'
						authHeader = @generateAuthHeader(httpMethod, url)

						xhrArgs =
							this: @
							onload: (resp) =>
								if callback and callback.onload
									callback.onload(resp.json.post)
								else
									Ti.App.fireEvent 'api:entity_post_GET:success', resp.json.post
							onerror: (resp) =>
								if callback and callback.onerror
									callback.onerror({status: 404})
								else
									Ti.App.fireEvent 'api:entity_post_GET:error', {status: 404}
							headers:
								Accept: content_types.Post
								Authorization: authHeader

						xhr = new XHR httpMethod, url, xhrArgs
						xhr.clearCookies url
						xhr.send()

					onerror: (resp) ->
						if callback and callback.onerror
							callback.onerror(resp)
						else
							Ti.App.fireEvent 'api:entity_post_GET:error', resp

	followers_COUNT: (entity, callback) =>
		Ti.API.log 'api', 'followers_COUNT <- '+entity
		checkNetwork
			this: @
			offline: =>
				Ti.API.log 'api', 'followers_COUNT -- Looks like you are offline'
				alert 'Looks like you are offline.'
			online: =>
				url = @getApiRootUrls(entity || null).posts_feed+"?types=#{encodeURIComponent(post_types.Follower)}"
				httpMethod = 'HEAD'

				xhr = new XHR httpMethod, url,
					onload: (resp) ->
						_count = xhr.getResponseHeader 'Count'
						Ti.App.Properties.setString('followers_count', _count) if entity == GLOBAL.entity
						if callback and callback.onload then callback.onload( _count )
						else Ti.App.fireEvent 'api:followers_COUNT:success', {count: _count}
					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror( resp )
						else Ti.App.fireEvent 'api:followers_COUNT:error', resp
					headers:
						'Accept': "#{content_types.Feed}"
				
				Ti.API.debug "#{httpMethod}: #{url}"
				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send()
				xhr.conId
		return

	followers_GET: (callback, entity = GLOBAL.entity, filters=null) =>
		# accepted filters:
		# entity		Posts published by the specified entity.
		#
		Ti.API.log 'api', "followers_GET <- #{entity}"
		checkNetwork
			this: @
			offline: => alert 'Looks like you are offline.'
			online: =>
				@posts_GET
					this: @
					onload: (resp) ->
						postsLen = resp.json.posts.length
						Ti.API.log 'api', "followers_GET -- Received #{postsLen} posts."
						if callback and callback.onload
							callback.onload({posts: resp.json.posts, pages: resp.json.pages})
							return
						else
							Ti.App.fireEvent 'api:followers_GET:success', {posts: resp.json.posts, pages: resp.json.pages}
							return

					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror(resp)
						else Ti.App.fireEvent 'api:followers_GET:error', resp

					filters: _.extend({types: post_types.Follower, entities: entity}, filters)
					auth: entity is GLOBAL.entity
		return

	followings_COUNT: (entity, callback) =>
		Ti.API.log 'api', 'followings_COUNT <- '+entity
		checkNetwork
			this: @
			offline: =>
				Ti.API.log 'api', 'followings_COUNT -- Looks like you are offline'
				alert 'Looks like you are offline.'
			online: =>
				url = @getApiRootUrls(entity || null).posts_feed+"?types=#{encodeURIComponent(post_types.Following)}"
				httpMethod = 'HEAD'

				xhr = new XHR httpMethod, url,
					onload: (resp) ->
						_count = xhr.getResponseHeader 'Count'
						Ti.App.Properties.setString('followings_count', _count) if entity == GLOBAL.entity
						if callback and callback.onload then callback.onload( _count )
						else Ti.App.fireEvent 'api:followings_COUNT:success', {count: _count}
					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror( resp )
						else Ti.App.fireEvent 'api:followings_COUNT:error', resp
					headers:
						'Accept': "#{content_types.Feed}"
				
				Ti.API.debug "#{httpMethod}: #{url}"
				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send()
				xhr.conId
		return

	followings_GET: (callback, entity = GLOBAL.entity, filters=null) =>
		# accepted filters:
		# entity		Posts published by the specified entity.
		#
		Ti.API.log 'api', "followings_GET <- #{entity}"
		checkNetwork
			this: @
			offline: => alert 'Looks like you are offline.'
			online: =>
				@posts_GET
					this: @
					onload: (resp) ->
						postsLen = resp.json.posts.length
						Ti.API.log 'api', "followings_GET -- Received #{postsLen} posts."
						if callback and callback.onload
							callback.onload({posts: resp.json.posts, pages: resp.json.pages})
							return
						else
							Ti.App.fireEvent 'api:followings_GET:success', {posts: resp.json.posts, pages: resp.json.pages}
							return

					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror(resp)
						else Ti.App.fireEvent 'api:followings_GET:error', resp

					filters: _.extend({types: post_types.Following, entities: entity}, filters)
					auth: entity is GLOBAL.entity
		return

	getFollowing: (uri) ->
		Ti.API.debug 'Getting following from cache: '+uri
		
		if cache.following.indexOf(uri) > -1 and cache.following_ext.hasOwnProperty(uri)
			return cache.following_ext[uri]
		else if not cache.following_ext.hasOwnProperty(uri)
			Ti.API.warn('No cache data for "'+uri+'" in following_ext.');
		
		Ti.API.debug JSON.stringify(cache.following_ext)

	addFollowing: (followData) ->
		Ti.API.debug('Adding following to cache: '+followData.entity);
		if cache.following.indexOf(followData.entity) > -1
			return

		cache.following.push followData.entity
		Ti.App.Properties.setList 'bonds:following', cache.following
		cache.following_ext[followData.entity] = followData
		Ti.App.Properties.setString 'bonds:following_ext', JSON.stringify(cache.following_ext)

	removeFollowing: (uri) ->
		Ti.API.debug('Removing following from cache: '+uri);
		index = cache.following.indexOf(uri)
		if index > -1
			cache.following.splice(index, 1);
			Ti.App.Properties.setList('bonds:following', cache.following)
			delete cache.following_ext[uri]
			Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext))

	isFollowing: (uri, checkOnline, callback) ->
		Ti.API.log 'api', "isFollowing <- #{uri}"
		if not checkOnline
			ret = (cache.following.indexOf(uri) > -1)
			Ti.API.debug('Following entity '+uri +' > '+ret)
			return ret
		
		else
			checkNetwork
				online: =>
					@posts_GET
						this: @
						onload: (resp) ->
							postsLen = resp.json.posts.length
							isFollowing = (postsLen > 0)
							Ti.API.log 'api', "isFollowing -- Received #{postsLen} posts."
							if callback and callback.onload then callback.onload(isFollowing)
							else Ti.App.fireEvent 'api:isFollowing:success', isFollowing

						onerror: (resp) ->
							if callback and callback.onerror then callback.onerror(resp)
							else Ti.App.fireEvent 'api:isFollowing:error', resp

						filters:
							types: encodeURIComponent(post_types.Following)
							entity: encodeURIComponent(uri)
						auth: true

				offline: ->
					alert 'Looks like you are offline.'
					if callback and callback.offline then callback.offline()

	isFollowed: (uri) ->
		res = false
		allFollowers = @getCache('followers')
		for f in allFollowers
			if uri == f.entity
				res = true
				break
		return res

	favorites_GET: (entity, filters, callback) ->
		Ti.API.log 'api', "favorites_GET <- #{JSON.stringify(filters)}"
		checkNetwork
			this: @
			offline: => alert 'Looks like you are offline.'
			online: =>
				@posts_GET
					this: @
					onload: (resp) ->
						postsLen = resp.json.posts.length
						Ti.API.log 'api', "favorites_GET -- Received #{postsLen} posts."
						if callback and callback.onload
							callback.onload({posts: resp.json.posts, pages: resp.json.pages})
							return
						else
							Ti.App.fireEvent 'api:favorites_GET:success', {posts: resp.json.posts, pages: resp.json.pages}
							return

					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror(resp)
						else Ti.App.fireEvent 'api:favorites_GET:error', resp

					filters: _.extend({types: post_types.Favorite, entities: entity}, filters)
					auth: entity is GLOBAL.entity
		return

	favorites_POST: (args, callback) ->
		# A favorite references another post.
		# The post must include a ref may include a mention of the original post. The type fragment must be set to the type of the original post.
		# post	 Required	 Object	 The post to be referenced by this favorite.
		# note	 Optional	 String	 Text describing the favorite.

		Ti.API.log 'api', 'favorites_POST <- '+JSON.stringify(args)
		checkNetwork
			online: =>

				ref =
					post: args.post.id

				# Should be set unless the referenced post is private.
				ref.type = args.post.type if not args.post.permissions

				postType = post_types.Favorite+"#"

				url = @getApiRootUrls().new_post
				httpMethod = 'POST'
				contentType = "#{content_types.Post}; type=\"#{postType}\""

				payload =
					refs: [ ref ]
				payload.note = args.note if args.note

				httpData = JSON.stringify(payload)

				xhr = new XHR httpMethod, url,
					onload: (resp) ->
						if callback and callback.onload then callback.onload({post: last_post})
						else Ti.App.fireEvent('api:favorites_POST:success', {post: last_post});
					onerror: (resp) ->
						if callback and callback.onerror then callback.onerror(resp);
						else Ti.App.fireEvent('api:favorites_POST:error', resp);
					noJson: false
					headers:
						'Content-Type': contentType
						'Authorization': @generateAuthHeader(httpMethod, url, contentType, payload)

				Ti.API.debug 'Data: '+httpData
				# Clears any cookies stored for the host
				xhr.clearCookies url
				xhr.send httpData

			offline: =>
				alert 'Looks like you are offline.'

		return

	isFavorite: (postId) ->
		@favorites_GET(GLOBAL.entity)

	hasProfile: (entity_uri) ->
		cache.discover.hasOwnProperty(entity_uri)

	getMyProfile: ->
		@getProfile GLOBAL.entity

	saveMyProfile: (profileData) ->
		if @hasProfile GLOBAL.entity
			cache.discover[GLOBAL.entity] = profileData
			Ti.App.Properties.setString 'discover', JSON.stringify(cache.discover)
		return

	getProfile: (entity_uri) ->
		cache.discover[entity_uri] or null

	saveProfile: (entity_uri, profileData, dontStore) ->
		Ti.API.log('api', "saveProfile() <- #{entity_uri}, #{JSON.stringify profileData}")
		if _.isEmpty(profileData)
			Ti.API.log 'api', 'saveProfile() -- Empty profile. Skipping.'
			return
		cache.discover[entity_uri] = profileData
		Ti.App.Properties.setString('discover', JSON.stringify(cache.discover)) if not dontStore
		return

	profile_PUT: (data, callback) ->
		Ti.API.log('api', "profile_PUT <- #{JSON.stringify data}")
		Ti.API.log 'api', "isFollowing -- NOT IMPLEMENTET YET"
		# type = uri[data.type];
		# apiRootUrls = this.getApiRootUrls(),
		# url = new URI(apiRootUrls[i]+'/profile/'+encodeURIComponent(type)),

		# cache_discover = cache.discover;

		# xhr = new XHR({
		# 	onload: function(resp){
		# 		// update local cache
		# 		cache_discover[GLOBAL.entity] = {basic: resp.json[uri.basic], core: resp.json[uri.core]};
		# 		Ti.App.Properties.setString('discover', JSON.stringify(cache_discover));
		# 		// fire event and callback
		# 		if (callback && callback.onload) callback.onload( resp.json );
		# 		else Ti.App.fireEvent('api:profile_PUT:success', {profile: resp.json});
		# 	},
		# 	onerror: function(resp){
		# 		if (callback && callback.onerror) callback.onerror( resp );
		# 		else Ti.App.fireEvent('api:profile_PUT:error', resp);
		# 	},
		# 	noJson: false
		# });
		
		# Ti.API.debug('PUT: '+url.toString());
		# Ti.API.debug('Authorization: '+authHeader);
		# Ti.API.debug('Data: '+JSON.stringify(data.payload));
		
		# // Clears any cookies stored for the host
		# xhr.clearCookies(apiRootUrls[i]);
		# xhr.open('PUT', url.toString(), true);
		# xhr.setRequestHeader('Content-Type', tentMimeType);
		# xhr.setRequestHeader('Accept', tentMimeType);
		# xhr.setRequestHeader('Authorization', authHeader);
		# xhr.send(JSON.stringify(data.payload));

	attachment_GET: (args, callback) ->
		Ti.API.log 'api', "attachment_GET <- #{JSON.stringify args}"

		url = _.template(@getApiRootUrls().attachment, {entity: encodeURIComponent(args.entity), digest: args.attachments[0].digest}, {interpolate: uri.url_template})
		httpMethod = 'GET'
		xhr = new XHR httpMethod, url,
			onload: (resp) ->
				if callback and callback.onload then callback.onload(resp)
				else Ti.App.fireEvent('api:attachment_GET:success', resp);
			onerror: (resp) ->
				if callback and callback.onerror then callback.onerror(resp);
				else Ti.App.fireEvent('api:attachment_GET:error', resp);
			noJson: true
			headers:
				'Authorization': @generateAuthHeader(httpMethod, url)
		return

	attachment_URL: (args) ->
		Ti.API.log 'api', "attachment_URL <- #{JSON.stringify args}"
		digest = if args.attachment then args.attachment.digest else args.attachments[0].digest
		url = _.template(@getApiRootUrls().attachment, {entity: encodeURIComponent(args.entity), digest: digest}, {interpolate: uri.url_template})
		# add the bewit url param to proxy the auth
		bewitOpts =
			credentials: @getHawkCredentials()
			ttlSec: 120
		url += "?bewit=#{hawk.client.getBewit(url, bewitOpts)}"
		Ti.API.log 'api', "attachment_URL -> #{url}"
		url

module.exports = new API()
