defaultCacheProp = 'mainCache'

exports.loadCache = (cacheProp, defaultCache) ->
	if Ti.App.Properties.hasProperty(cacheProp or defaultCacheProp) then JSON.parse( Ti.App.Properties.getString(cacheProp or defaultCacheProp, '{}') ) else defaultCache or {}

exports.saveCache = (cacheProp, json) ->
	Ti.App.Properties.setString( cacheProp or defaultCacheProp, JSON.stringify(json) )

exports.appendCache = (cacheProp, json) ->
	cacheProp = cacheProp or defaultCacheProp
	cache = exports.loadCache cacheProp
	for own p of json
		cache[p] = json[p];
	exports.saveCache cache, cacheProp