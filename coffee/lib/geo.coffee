Ti.Geolocation.setPurpose 'Service is needed to attach location to your posts.'
Ti.Geolocation.distanceFilter = 10

# We need high accuracy
if GLOBAL.iOS
	Ti.Geolocation.setAccuracy Ti.Geolocation.ACCURACY_BEST
else
	Ti.Geolocation.setAccuracy Ti.Geolocation.ACCURACY_HIGH

geolocCallback = (e) ->
	if not e.success or e.error
		Ti.API.error JSON.stringify(e.error)
		Ti.App.fireEvent 'geo:getLocation:failed', e
	else
		Ti.API.debug 'Geolocation changed. Saving.'
		geo.lastLocation =
			longitude: e.coords.longitude
			latitude: e.coords.latitude
			altitude: e.coords.altitude
			heading: e.coords.heading
			accuracy: e.coords.accuracy
			speed: e.coords.speed
			timestamp: e.coords.timestamp
			altitudeAccuracy: e.coords.altitudeAccuracy
			address: L('no_address_found',"No address found.")

		# Ti.Geolocation.reverseGeocoder(geo.lastLocation.latitude, geo.lastLocation.longitude, function(e){
		# 	if (e.success) {
		# 		if (e.places && e.places.length) {geo.lastLocation.address = e.places[0].address;}
		# 		Ti.API.debug("reverse geolocation result = "+JSON.stringify(e));
		# 	}
		# 	Ti.App.fireEvent('geo:getLocation:success', geo.lastLocation);
		# });

geo =
	started: false
	askForAuth: (callback, force) ->
		# Ti.Geolocation.setLocationServicesAuthorization( Ti.Geolocation.AUTHORIZATION_AUTHORIZED );
		if Ti.Geolocation.getLocationServicesAuthorization() not in [Ti.Geolocation.AUTHORIZATION_AUTHORIZED, Ti.Geolocation.AUTHORIZATION_RESTRICTED] or force
			Ti.Geolocation.setLocationServicesAuthorization( Ti.Geolocation.AUTHORIZATION_AUTHORIZED )
		Ti.API.debug 'Geolocation after setting Auth: '+
			['AUTHORIZATION_UNKNOWN','AUTHORIZATION_RESTRICTED','AUTHORIZATION_DENIED','AUTHORIZATION_AUTHORIZED'][Ti.Geolocation.getLocationServicesAuthorization()]
		if callback
			if this.isAvailable()
				 callback.onload() if callback.onload
			else if callback.onerror
				callback.onerror()

	isAuthorized: ->
		return Ti.Geolocation.getLocationServicesAuthorization() == Ti.Geolocation.AUTHORIZATION_AUTHORIZED

	isAvailable: (callback) ->
		Ti.API.log 'geo', "Checking if location service is available... #{this.isEnabled()}"
		Ti.API.debug 'Geolocation is enabled: '+ this.isEnabled()
		Ti.API.debug 'Geolocation is authorized: '+ this.isAuthorized()
		if this.isEnabled() and this.isAuthorized()
			Ti.API.debug 'Geolocation started. Obtaining device geo position.'
			return true
		else
			Ti.API.warn 'Geolocation will not be started. Device location service needs to be enabled first.'
			return false

	isEnabled: ->
		return Ti.Geolocation.getLocationServicesEnabled()
	
	purpose: Ti.Geolocation.getPurpose()
	lastLocation: {}

	grabCountry: (ip) ->
		ip = ip or Ti.Platform.address
		xhr = Ti.Network.createHTTPClient
			onload : (e) ->
				Ti.API.info("Received text: " + this.responseText)
				Ti.App.fireEvent('grabCountry:load')
			onerror : (e) ->
				Ti.API.debug(e.error)
				Ti.App.fireEvent('grabCountry:error')
			timeout : 5000  # in milliseconds

		api_key = '00716d625212cd6324979faa4c49b9512ee4f3b71ac2a43a5c8b1ce36813c5bb';
		xhr.open('http://api.ipinfodb.com/v3/ip-country/?key='+api_key+'&ip='+ip+'&format=json')
		xhr.send()

	start: (callback) ->
		if not this.started
			if this.isAvailable()
				this.started = true
				callback.onload() if callback and callback.onload
			else if not this.isAuthorized()
				this.askForAuth()
				Ti.Geolocation.getCurrentPosition (e) ->
					Ti.API.debug JSON.stringify(e)
					callback.onload() if callback and callback.onload

	getCurrentPosition: (callback) ->
		Ti.API.info 'Grabbing current device geo location.'
		Ti.Geolocation.getCurrentPosition (e) ->
			Ti.API.debug JSON.stringify(e)
			geolocCallback(e)
			if not !e.success or e.error
				callback.onerror()
			else
				callback.onload {latitude: e.coords.latitude, longitude: e.coords.longitude, altitude: e.coords.altitude}

# Ask for authorization if we dont have it
# geo.askForAuth()

module.exports = geo