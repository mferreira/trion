drafts = if Ti.App.Properties.hasProperty('drafts') then JSON.parse(Ti.App.Properties.getString('drafts'))  else []

exports.load = (i) ->
	drafts[i]

exports.save = (data) ->
	drafts.push data
	Ti.App.Properties.setString 'drafts', JSON.stringify(drafts)

exports.remove = (i) ->
	if drafts.length > i
		delete drafts[i]
		drafts.splice(i, 1)
		Ti.App.Properties.setString 'drafts', JSON.stringify(drafts)

exports.all = ->
	drafts