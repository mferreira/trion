cache =
	favorites_id: if Ti.App.Properties.hasProperty('favorite_posts_id') then JSON.parse(Ti.App.Properties.getString('favorite_posts_id')) else []
	favorites: if Ti.App.Properties.hasProperty('favorite_posts') then JSON.parse(Ti.App.Properties.getString('favorite_posts')) else []

exports.isFavorite = (id) ->
	cache.favorites_id.indexOf(id) > -1

exports.addFavorite = (postData) ->
	if !exports.isFavorite(postData.id)
		cache.favorites.push(postData)
		cache.favorites_id.push(postData.id)
		Ti.App.Properties.setString 'favorite_posts', JSON.stringify(cache.favorites)
		Ti.App.Properties.setString 'favorite_posts_id', JSON.stringify(cache.favorites_id)

exports.removeFavorite = (id) ->
	if exports.isFavorite(id)
		fav_index = cache.favorites_id.indexOf id
		cache.favorites.splice fav_index, 1
		cache.favorites_id.splice fav_index, 1
		Ti.App.Properties.setString 'favorite_posts', JSON.stringify(cache.favorites)
		Ti.App.Properties.setString 'favorite_posts_id', JSON.stringify(cache.favorites_id)

exports.getFavorites = ->
	cache.favorites