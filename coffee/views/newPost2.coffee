ui = require 'lib/ui_sugar'
api = require 'lib/tent.api2'
drafts = require 'lib/drafts'
url_short = require 'lib/url_shortener'
_ = require 'lib/vendor/underscore-min'


statusPostTextLimit = api.postType('postTypeProperties')[api.postType('Status')].textLimit
isPortrait = null

createView = (newPostData, callback, args) ->
	Ti.API.debug "Creating 'New Post' view with data: #{JSON.stringify(newPostData)}"

	newPost_args = newPostData
	newPost_callback = callback

	if newPostData
		cur_postData = newPostData.originalPost
		prefill = newPostData.prefill
		isReply = newPostData.reply or false
		replyAll = newPostData.replyAll or false
	else
		cur_postData = null
		prefill = null
		isReply = false
		replyAll = false

	pmSwitch_value = if newPost_args then newPost_args.pmSwitch or false else false

	isPosting = false
	flagPrivate = false
	flagLocation = false
	location_latlon = null
	postInput_focus = true
	isRepost = if newPostData and newPostData.originalPost then (newPostData.originalPost.type is api.postType('Repost')) else false

	discoverData = api.getCache('discover')
	allBonds = _.keys(discoverData).join(' ')
	allBonds_split = allBonds.split(' ')

	selectedPosttype = api.postType('Status')

	#////////////////////////////////////////////

	getTopBarHeight = ->
		20 + (if isPortrait then 44 else 32)

	updatePostInput = (e) ->
		# we dont care about anything but portrait and landscape
		return if e.orientation not in [Ti.UI.FACE_UP, Ti.UI.FACE_DOWN, Ti.UI.UNKNOWN]

		isPortrait = e.orientation in [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]

		topBar.applyProperties {height: getTopBarHeight()}

		mentionSelector.applyProperties {top: topBar.height + 46}

		postInput.applyProperties {top: topBar.height}

		toolbar.bottom = GLOBAL.keyboardHeight()

		# portrait mode
		if isPortrait
			if not GLOBAL.isTablet
				Ti.API.debug 'Updating navbar buttons.'
				self.setLeftNavButton navLeftButton
				self.setRightNavButton navRightButton

			if not mentionSelector_visible
				postInput.applyProperties
					height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 64)
					width: Ti.UI.FILL
			else
				postInput.applyProperties
					height: 45
					width: Ti.UI.FILL

			mentionSelector.applyProperties
				width: Ti.UI.FILL
				top: 46
				bottom: GLOBAL.keyboardHeight() + 34

		# landscape mode
		else
			if not GLOBAL.isTablet
				Ti.API.debug 'Updating navbar buttons.'
				self.setLeftNavButton navLeftButton2
				self.setRightNavButton navRightButton2

			mentionSelector.applyProperties
				width: 200
				top: 0
				bottom: GLOBAL.keyboardHeight() + 34

			if not mentionSelector_visible
				postInput.applyProperties
					height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 52)
					width: Ti.UI.FILL
			else
				postInput.applyProperties
					height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 52)
					width: Ti.Platform.displayCaps.platformWidth - mentionSelector.width

		photoFrame.applyProperties
			height: GLOBAL.keyboardHeight()


	countPostChars = (text) ->
		re = /(?:[^\S]|^)(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*)/ig
		urls = 0
		stripText = text
		while match = re.exec(text)
			stripText = stripText.replace( match[1], '' )
			urls += 1
		stripText.length + (urls * 18)

	entityLookup = (searchFor) ->
		Ti.API.debug "entityLookup: '#{searchFor}'"
		re = new RegExp('(\\S*'+searchFor+'\\S*)', 'ig')
		matches = []
		while match = re.exec(allBonds)
			matches.push( match[1].toLowerCase() )
		matches

	get_name_or_entity = (uri) ->
		d = discoverData[uri]
		return d.profile.name if d and d.profile and d.profile.name
		uri

	get_uri_avatar = (uri) ->
		d = discoverData[uri]

	showPossibleMentions = (uris, callback) ->
		Ti.API.debug 'showPossibleMentions() called'
		Ti.API.debug "isPortrait -> #{isPortrait}"
		mentionSelector_callback = callback
		mentionSelector_table.data = (->
			rows = []
			for uri in uris
				ent = discoverData[uri]
				rows.push ui.row
					properties: {height: 35}
					content: [
						Ti.UI.createImageView
							width: 30
							height: 30
							left: 5
							borderRadius: 3
							defaultImage: 'images/avatar.png'
							image: if ent.basic and ent.basic.avatar_url then ent.basic.avatar_url else ''
						Ti.UI.createLabel
							text: if ent.basic and ent.basic.name then ent.basic.name else uri
							left: 40
							right: 5
							color: '#313339'
							font:
								fontSize: 14
								fontFamily: 'Helvetica'
					]
			rows
		)()

		mentionSelector_visible = true
		userButton.image = 'images/post_user_icon_active.png'
		if isPortrait
			postInput.applyProperties {height: 45}
		else
			if mentionSelector.width isnt 200
				mentionSelector.applyProperties
					width: 200
					top: 0
					bottom: GLOBAL.keyboardHeight() + 34
			postInput.applyProperties
				width: Ti.Platform.displayCaps.platformWidth - mentionSelector.width
		# postInput.blur()

	hidePossibleMentions = () ->
		Ti.API.debug 'hidePossibleMentions() called'
		if isPortrait
			postInput.applyProperties
				height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 64)
		else
			postInput.applyProperties {width: Ti.UI.FILL}

		# postInput.focus()
		userButton.image = 'images/post_user_icon.png'
		mentionSelector_visible = false

	cancelButton_onclick = (e) ->
		if postInput.value.length
			ui.optionDialog
				properties:
					options: [L('dont_save', "Don't Save"), L('save_draft', "Save Draft"), L('cancel', "Cancel")]
					cancel: 2
					destructive: 0
				events: [
					['click', (e) ->
						if e.index is e.destructive
							self.close()
						else if e.index is 1
							# TODO: support more post types
							# save the post to drafts and close
							drafts.save
								type: api.postType('Status')
								content: {text: postInput.value}
							self.close()
					]
				]
			.show()
		else
			self.close()

	postButton_onclick = (e) ->
		isPosting = true
		ui.showIndicator()

		getUrls = (text) ->
			# var exp = /(\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])))/ig;
			re = /(?:[^\S]|^)(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*)/ig
			urls = []
			while match = re.exec(text)
				urls.push match[1]
			urls

		getMentions = (text) ->
			re = /(\^(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*))/ig
			urls = []
			while match = re.exec(text)
				urls.push match[2].toLowerCase()
			urls

		_urls = getUrls(postInput.value)
		urls = {}
		output = postInput.value
		cur_url = null

		urls[_url] = null for _url in _urls

		checkAllUrls = ->
			Ti.API.debug 'checkAllUrls...'
			for url in _urls
				shortified = urls[url]
				if shortified is null
					if url.length > url_short.sites[url_short.choice].chars
						return shortifyURL(url)
					else
						urls[url] = url
				else
					# make some dark magic regex stuff - gotta love it
					rep_url = url.replace(/\//g, '\\/').replace(':', '\\:').replace(/\./g, '\\.')
					rep_regex = new RegExp("(?:[^\\S]|^)(#{rep_url})", 'gi')
					match = rep_regex.exec(output)
					output = output.replace(match[1], shortified)
			postIt()

		shortifyURL = (uri) ->
			Ti.API.debug "shortifying: #{uri}"
			cur_url = uri
			xhr = Ti.Network.createHTTPClient
				# timeout : timeout *1000,  // in milliseconds
				# function called when the response data is available
				onload : (e) ->
					Ti.API.debug "Shortified url: #{this.responseText}"
					urls[cur_url] = this.responseText
					checkAllUrls()
				# function called when an error occurs, including a timeout
				onerror : (e) ->
					Ti.API.error "Something went wrong shortifying url: #{uri}"
					checkAllUrls()

			xhr.open('GET', url_short.sites[url_short.choice].uri + encodeURIComponent(uri), false)
			xhr.send()

		postIt = ->
			Ti.API.debug "post shortified: #{output}"

			payload =
				permissions:
					public: !flagPrivate
					entities: []
				# choose the post type
				type: if photoArray.length then api.postType('Photo') else api.postType('Status')
				content: if photoArray.length then {caption: output} else {text: output }
				mentions: []

			payload.content.location = location_latlon if flagLocation and location_latlon

			input_mentions = getMentions(output)
			if input_mentions.length
				mentionIndex = 0
				mentionDone = []
				for m in input_mentions
					continue if mentionDone.indexOf(m) >= 0 or (isReply and cur_postData.entity is m)

					mentionDone.push m
					payload.mentions.push {entity: m}

					# build inline mentions
					output = output.replace( '^'+m, '^['+get_name_or_entity(m)+']('+mentionIndex+')')
					mentionIndex += 1

			# set post content text based on post type
			if payload.type is api.postType('Photo')
				payload.content.caption = output
				payload.attachments = photoArray
			else if payload.type is api.postType('Status')
				payload.content.text = output

			# TODO: do we really want to do this? The code above already takes care of mentions from the input...
			if prefill and prefill.mentions
				payload.mentions.push({entity: cur_entity}) for cur_entity in prefill.mentions

			if cur_postData and isReply
				mentions_post_id = if isRepost then cur_postData.original_post.post_content.id else cur_postData.id
				mentions_post_entity = if isRepost then cur_postData.original_post.post_content.entity else cur_postData.entity
				payload.mentions.push { entity: mentions_post_entity, post: mentions_post_id }
				if replyAll
					for cur_entity in cur_postData.mentions
						# don't mention me or the entity i'm replying to
						continue if cur_entity in [cur_postData.entity, GLOBAL.entity]
						payload.mentions.push {entity: cur_entity}

			if flagPrivate
				if payload.mentions.length
					payload.permissions.entities.push(mention.entity) for mention in payload.mentions
				else if cur_postData
					payload.permissions.entities.push(cur_postData.entity)

			postItNow = ->
				api.posts_POST payload,
					onload: (e) ->
						newPost_callback(newPost_args.callbackArgs) if newPost_callback
						self.close()
						cur_postData = null
						isPosting = false
						ui.hideIndicator()
						Ti.App.fireEvent 'ui:myStream:addPost', {post: e.post}
					onerror: (e) ->
						isPosting = false
						ui.hideIndicator()
						ui.alert
							properties:
								cancel: 1
								buttonNames: [L('yes', 'Yes'), L('no', 'No')]
								message: 'Sorry, your post could not be sent. Would you like to try again?'
								title: 'Error'
							events: [
								['click', (e) ->
									postItNow() if e.index isnt e.cancel
								]
							]
						.show()

			Ti.API.debug "Posting new post with: #{JSON.stringify(payload)}"
			postItNow()

		# start the proccess
		checkAllUrls()

	#////////////////////////////////////////////

	self = ui.win
		config: 'windowProperties'
		properties:
			title: 'New Post'
			orientationModes: (->
				if Ti.Platform.osname is "iphone"
					return [
						Ti.UI.PORTRAIT
						Ti.UI.LANDSCAPE_LEFT
						Ti.UI.LANDSCAPE_RIGHT
					]
				else
					return [
						Ti.UI.PORTRAIT
						Ti.UI.UPSIDE_PORTRAIT
						Ti.UI.LANDSCAPE_LEFT
						Ti.UI.LANDSCAPE_RIGHT
					]
			)()
		events: [
			['focus', (e) ->
				isPortrait = e.orientation in [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
				topBar.applyProperties {height: getTopBarHeight()}
				postInput.applyProperties {top: topBar.height}

				Ti.API.debug "e.orientation in [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]: #{isPortrait}"
				Ti.API.debug "topBar.height: #{topBar.height}"
				Ti.API.debug "postInput.top: #{postInput.top}"

				# if not GLOBAL.isTablet
				# 	if isPortrait
				# 		self.setLeftNavButton navLeftButton
				# 		self.setRightNavButton navRightButton
				# 	else
				# 		self.setLeftNavButton navLeftButton2
				# 		self.setRightNavButton navRightButton2

				if pmSwitch_value
					flagPrivate = true
					pmSwitch.image = 'images/post_private_active_icon.png'
				else if isReply and cur_postData
					flagPrivate = cur_postData.permissions
					pmSwitch.image = if flagPrivate then 'images/post_private_active_icon.png' else 'images/post_private_icon.png'

				# pmSwitch.value = pmSwitch_value ? pmSwitch_value : (isReply && cur_postData) ? !cur_postData.permissions['public'] : false; // private is off by default
				postInput.value = if postInput.value then postInput.value else (->
					postInput_value = ''
					if isReply
						postInput_value = "^#{cur_postData.entity} " if cur_postData.entity isnt GLOBAL.entity
						if replyAll
							for mention in cur_postData.mentions
								cur_entity = mention.entity;
								continue if [cur_postData.entity, GLOBAL.entity].indexOf(cur_entity) > -1
								postInput_value += "^#{cur_entity} "
					else if prefill
						postInput_value = prefill.content.text
					postInput_value
				)()

				postInput_length = postInput.value.length
				postInput.focus()

				# push cursor position to the end
				postInput.setSelection(postInput_length, postInput_length)

				content = postInput.value
				if photoArray.length
					inputCharsLeft = "∞"
					canPost = true
				else
					inputCharsLeft = statusPostTextLimit - countPostChars(content)
					canPost = (Ti.Network.online and inputCharsLeft > -1 and inputCharsLeft < statusPostTextLimit)

				charCounter.setText( inputCharsLeft )
				navRightButton.enabled = canPost
				navRightButton2.enabled = canPost
			],
			['open', (e) ->
				isPortrait = (e.source.orientation is Ti.UI.PORTRAIT)
				toolbar.bottom = GLOBAL.keyboardHeight()
				postInput.height = Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 35 + (if isPortrait then 64 else 52))
				Ti.Gesture.addEventListener('orientationchange', updatePostInput)
				args.button.enabled = true if args and args.button
			],
			['close', (e) ->
				Ti.Gesture.removeEventListener('orientationchange', updatePostInput)
				lastKeyboardHeight = GLOBAL.keyboardHeight()
			]
		]

	navLeftButton = ui.button
		config: 'cancelButton'
		events: [
			['click', cancelButton_onclick]
		]

	navRightButton = ui.button
		config: 'postButton'
		events: [
			['click', postButton_onclick]
		]

	navLeftButton2 = ui.button
		properties:
			systemButton: Ti.UI.iPhone.SystemButton.STOP
			tintColor: 'white'
		events: [
			['click', cancelButton_onclick]
		]

	navRightButton2 = ui.button
		config: 'postButton2'
		events: [
			['click', postButton_onclick]
		]

	topBar_buttons = ui.toolBar
		properties:
			top: 12
			width: Ti.UI.FILL
			backgroundColor: "#009fdd"
			barColor: "#009fdd"
			items: [navLeftButton, ui.FLEXSPACE, navRightButton]
			borderBottom: false
			borderTop: false
			tintColor: 'white'

	topBar = ui.view
		properties:
			zIndex: 2
			top: 0
			height: getTopBarHeight()
			width: Ti.UI.FILL
			backgroundColor: '#009fdd'
		content: [ topBar_buttons ]

	# self.setLeftNavButton(navLeftButton)
	# self.setRightNavButton(navRightButton)

	pmLabel = Ti.UI.createLabel
		text: 'Private'
		width: Ti.UI.SIZE
		height: Ti.UI.SZIE
		textAlign: 'left'
		left: 0

	pmSwitch = ui.button
		properties:
			image: 'images/post_private_icon.png'
			style: Ti.UI.iPhone.SystemButtonStyle.PLAIN
			left: 0
			width: 40
			height: 25
		events: [['singletap', (e) ->
			flagPrivate = !flagPrivate
			e.source.image = if flagPrivate then 'images/post_private_active_icon.png' else 'images/post_private_icon.png'
			Ti.API.debug "Private switch is: #{if flagPrivate then 'on' else 'off'}"
		]]

	locationSwitch_wrap = Ti.UI.createView
		left: 0
		width: 40
		height: 25

	locationActInd = Ti.UI.createActivityIndicator
		color: '#31333b'
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK
		width: 40
		height: 25

	locationSwitch = ui.button
		properties:
			image: 'images/post_loc_icon.png'
			style: Ti.UI.iPhone.SystemButtonStyle.PLAIN
			width: 40
			height: 25
		events: [['singletap', (e) ->
			flagLocation = !flagLocation
			# e.source.image = flagLocation ? 'images/kb_toolbar_location_active.png' : 'images/kb_toolbar_location.png';
			Ti.API.debug "Location switch is: #{if flagLocation then 'on' else 'off'}"
			if flagLocation
				geo = require('lib/geo')
				grabDeviceLocation = ->
					geo.getCurrentPosition
						onload: (latlon) ->
							location_latlon = latlon
							locationSwitch.image = 'images/post_loc_icon_active.png'
							locationActInd.hide()
							locationSwitch.show()
						onerror: (e) ->
							locationSwitch.image = 'images/post_loc_icon.png'
							locationActInd.hide()
							locationSwitch.show()
							flagLocation = false

				locationSwitch.hide()
				locationActInd.show()

				if geo.isAvailable()
					if not geo.started
						geo.start
							onload: ->
								grabDeviceLocation()
							onerror: ->
								locationSwitch.image = 'images/post_loc_icon.png'
								locationActInd.hide()
								locationSwitch.show()
								flagLocation = false
					else
						grabDeviceLocation()

				else if geo.isEnabled()
						geo.start
							onload: ->
								grabDeviceLocation()
							onerror: ->
								locationSwitch.image = 'images/post_loc_icon.png'
								locationActInd.hide()
								locationSwitch.show()
								flagLocation = false
								alert 'Unable to get your location. Please, check your device privacy settings.'
				else
					geo.start
						onload: ->
							grabDeviceLocation()
						onerror: ->
							locationSwitch.image = 'images/post_loc_icon.png'
							locationActInd.hide()
							locationSwitch.show()
							flagLocation = false
			else
				location_latlon = null
				e.source.image = 'images/post_loc_icon.png'
		]]

	userButton = ui.button
		properties:
			image: 'images/post_user_icon.png'
			style: Ti.UI.iPhone.SystemButtonStyle.PLAIN
			left: 0
			width: 40
			height: 25
		events: [['singletap', (e) ->
			if mentionSelector_visible
				hidePossibleMentions()
			else
				if allBonds_split.length
					postInput_length = postInput.value.length

					# only adds mentions to the end
					postInput.value += ' ' if postInput_length and postInput.value[postInput_length - 1] isnt '\n'
					postInput.value += '^'

					# adds mentions to where cursor is positioned - DOES NOT WORK YET
					# ///////////////////////
					# if (!textSelected || (textSelected.location === postInput_length-1 && !textSelected.length)) {
					# 	if (postInput_length && postInput.value[postInput_length-1] !== '\n')
					# 		postInput.value += ' ';
					# 	postInput.value += '^';
					# }
					# else if (textSelected.location !== postInput_length-1) {
					# 	var newCursorPosition = textSelected.location,
					# 	postInput_value = postInput.value;
					# 	postInput_value = postInput_value.slice(0, textSelected.location) +
					# 		'^' + postInput_value.slice(textSelected.location+textSelected.length);

					# 	postInput.value = postInput_value;
					# 	Ti.API.debug('repositioning text cursor to position: '+newCursorPosition);
					# 	setTimeout(function(){ postInput.setSelection(newCursorPosition, newCursorPosition); }, 100);
					# }

					showPossibleMentions allBonds_split, (index) ->
						postInput.value = "#{postInput.value}#{allBonds_split[index]} "
				else
					hidePossibleMentions()

			Ti.API.debug "Location switch is: #{if mentionSelector_visible then 'on' else 'off'}"
		]]

	photoArray = []
	photoButton = ui.button
		properties:
			image: 'images/post_photos_icon.png'
			style: Ti.UI.iPhone.SystemButtonStyle.PLAIN
			left: 0
			width: 40
			height: 25
		events: [['click', (e) ->
			if photoArray.length
				if postInput_focus
					postInput.blur()
					postInput_focus = false
				else
					postInput.focus()
					postInput_focus = true
			else
				onMediaSuccess = (media) ->
					postInput.focus()
					postInput_focus = true
					photoButton.image = 'images/post_photos_active_icon.png'
					photoArray.push(media.media)
					# set out photo frame image
					photoFrame.applyProperties
						image: media.media
						height: GLOBAL.keyboardHeight()
					# set char counter value to infinite
					charCounter.setText("∞")

				ui.optionDialog
					properties:
						options: [L('from_camera', "From Camera"), L('from_library', "From Library"), L('cancel', "Cancel")]
						cancel: 2
					events: [
						['click', (e) ->
							if e.index is 0
								Ti.Media.showCamera
									mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
									allowEditing: true
									success: onMediaSuccess
							else if e.index is 1
								Ti.Media.openPhotoGallery
									mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
									allowEditing: true
									success: onMediaSuccess
						]
					]
				.show()
		]]

	photoFrame = ui.image
		properties:
			height: GLOBAL.keyboardHeight()
			bottom: 0
		events: [['click', (e) ->
			ui.optionDialog
				properties:
					options: [L('remove_image', "Remove Image"), L('cancel', "Cancel")]
					destructive: 0
					cancel: 1
				events: [
					['click', (e) ->
						if e.index is e.destructive
							postInput.focus()
							postInput_focus = true
							photoButton.image = 'images/post_photos_icon.png'
							photoFrame.image = null
							photoArray.pop()
							# set char counter value to status post remaining chars
							charCounter.setText(statusPostTextLimit - countPostChars(postInput.value))
					]
				]
			.show()
		]]

	charCounter = Ti.UI.createLabel
		width: Ti.UI.SIZE
		height: Ti.UI.SZIE
		textAlign: 'right'
		right: 20
		text: if photoArray.length then "∞" else "#{statusPostTextLimit}"
		font:
			fontSize: 13
			fontWeight: 'bold'
		color: '#31333b'

	toolbar = ui.view
		properties:
			zIndex: 2
			bottom: GLOBAL.keyboardHeight()
			left: 0
			right: 0
			height: 34
			backgroundImage: 'images/kb_toolbar_bg.png'
			backgroundTopCap: 5
		content: [
			ui.view
				properties:
					width: Ti.UI.SZIE
					height: Ti.UI.SZIE
					layout: 'horizontal'
					left: 10
					top: 6
				content: [ pmSwitch, locationSwitch_wrap, userButton, photoButton ]
			charCounter
		]

	postInput = ui.textArea
		properties:
			zIndex: 2
			left: 0
			top: topBar.height
			height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 64)
			width: Ti.UI.FILL
			keyboardType: Ti.UI.KEYBOARD_ASCII
			suppressReturn: false
			autocorrect: true
			autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES
			color: '#313339'
			font:
				fontSize: 14
				fontFamily: 'Helvetica'
		events: [
			['selected', (e) ->
				textSelected = e.range
			],
			['change', (e) ->
				contentLength = e.source.value.length
				content = e.source.value

				inputCharsLeft = if photoArray.length then "∞" else (statusPostTextLimit - countPostChars(content))
				charCounter.setText inputCharsLeft

				if Ti.Network.online
					canPost = if photoArray.length then true else (inputCharsLeft > -1 and inputCharsLeft < statusPostTextLimit)
					navRightButton.enabled = canPost
					navRightButton2.enabled = canPost

				# check if user is going to mention someone
				if contentLength > 1
					curChar = content[contentLength-1]
					previousChar = content[contentLength-2]

					if curChar not in [' ', '\n']
						if previousChar is '^'
							mentionStart = contentLength - 1
							uris = entityLookup(curChar)

							Ti.API.debug "Possible mentions: #{JSON.stringify(uris)}"

							if uris.length
								showPossibleMentions uris, (index) ->
									postInput.value = postInput.value.slice(0, mentionStart) + uris[index]
							else
								hidePossibleMentions()

						else if mentionStart isnt null and contentLength > mentionStart
							uris = entityLookup(content.slice(mentionStart, contentLength))
							Ti.API.debug "Possible mentions: #{JSON.stringify(uris)}"
							if uris.length
								showPossibleMentions uris, (index) ->
									postInput.value = postInput.value.slice(0, mentionStart) + uris[index]
							else
								hidePossibleMentions()
					else
						mentionStart = null
						hidePossibleMentions()
				else
					mentionStart = null
					hidePossibleMentions()

			],
			['focus', (e) ->
				userButton.image = 'images/post_user_icon.png'
				mentionSelector_visible = false
			]
		]

	# mention selection view
	mentionSelector_visible = false
	mentionSelector_callback = null
	mentionSelector_table = ui.table
			properties:
				width: Ti.UI.FILL
			events: [['click', (e) ->
				mentionSelector_callback(e.index) if mentionSelector_callback
				hidePossibleMentions()
			]]

	mentionSelector = ui.view
		properties:
			zIndex: 1
			right: 0
			width: Ti.UI.FILL
			top: topBar.height + 46
			bottom: GLOBAL.keyboardHeight() + 34
			backgroundImage: 'images/kb_toolbar_bg.png'
			backgroundTopCap: 5
		content: [mentionSelector_table]

	locationSwitch_wrap.add locationActInd
	locationSwitch_wrap.add locationSwitch

	self.add topBar
	self.add mentionSelector
	self.add postInput
	self.add toolbar
	self.add photoFrame

	self

module.exports = createView
