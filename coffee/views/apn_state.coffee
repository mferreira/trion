apn = require 'lib/apn'
ui = require 'lib/ui_sugar'

exports.createView = ->

	self = ui.win
		properties:
			backgroundColor: '#fff'
			layout: 'vertical'
		content: [
			ui.label({text: 'remoteNotificationsEnabled: '+Ti.Network.remoteNotificationsEnabled, top: 0, width: Ti.UI.SIZE, height: Ti.UI.SIZE})
			ui.label({text: 'remoteDeviceUUID: '+Ti.Network.remoteDeviceUUID, top: 10, width: Ti.UI.SIZE, height: Ti.UI.SIZE})
		]


	channelLabel = ui.label({text: 'My channel: '+GLOBAL.apn_channel, top: 15, width: Ti.UI.SIZE, height: Ti.UI.SIZE})
	self.add channelLabel

	checkInstallation = ui.label({text: 'checkInstallation: ...', top: 10, width: Ti.UI.SIZE, height: Ti.UI.SIZE})
	self.add checkInstallation

	apn.checkInstallation
		onload: (installationData) ->
			alert installationData
			if installationData.channels.indexOf(GLOBAL.apn_channel) > -1
				checkInstallation.text = 'checkInstallation: OK'
			else
				checkInstallation.text = 'checkInstallation: NO CHANNEL'
				reregisterapn = ui.button
					properties: {title: 'Subscribe APN channel', top: 15, width: Ti.UI.SIZE, height: Ti.UI.SIZE}
					events: [
						['click', ->
							ui.alert(
								properties:
									cancel: 1
									buttonNames: [L('yes', 'Yes'), L('no', 'No')]
									title: 'Are you sure?'
								events: [
									['click', (e) ->
										if e.index != e.cancel
											try 
												apn.subscribeChannel GLOBAL.apn_channel
											catch err
												alert err
									]
								]
							).show()
						]
					]
				
				self.add reregisterapn

		onerror: (e) ->
			checkInstallation.text = 'checkInstallation: ERROR'
			alert e

			reregisterapn = ui.button
				properties: {title: 'Re-register for APN', top: 15, width: Ti.UI.SIZE, height: Ti.UI.SIZE}
				events: [
					['click', (e) ->
						ui.alert(
							properties:
								cancel: 1
								buttonNames: [L('yes', 'Yes'), L('no', 'No')]
								title: 'Are you sure?'
							events: [
								['click', (e) ->
									if e.index != e.cancel
										try
											apn.unregisterForPushNotifications()
											apn.registerForPushNotifications()
										catch err
											alert err
								]
							]
						).show()
					]
				]

			self.add reregisterapn

	self