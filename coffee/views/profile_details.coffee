ui = require 'lib/ui_sugar'
api = require 'lib/tent.api'
URI = require 'lib/vendor/jsuri.min'
_f = require 'views/func_repo'
NewPost = require 'views/newPost2'

flickr_key = "189da0233649d27aab6e562c610ae060"

module.exports = class ProfileEdit
	constructor: (@profile, @viewOptions) ->
		if typeof @profile == 'string'
			@profile = api.getCache('discover')[@profile]

		@self = ui.win
			config: 'windowProperties'
			properties:
				title: 'Details'
				rightNavButton: ui.button
					config: 'newPostButton'
					events: [['click', ->
						e.source.enabled = false
						(new NewPost(null. null, {button: e.source})).open modal: true
					]]

				leftNavButton: (=>
					if @viewOptions and @viewOptions.backButton
						ui.button
							config: 'backButton'
							properties:
								backgroundImage: 'images/back_profile.png', width: 64
							events: [['click', =>
								@tableView.setData()
								GLOBAL.tabGroup.activeTab.close(@self, animated: true)
							]]
				)()

				# layout: 'vertical'

		if viewOptions and viewOptions.backButton
			@self.leftNavButton = ui.button
				config: 'backButton',
				events: [['click', (e) => GLOBAL.tabGroup.activeTab.close(@self, {animated: true}) ]]

		hasHeaderPhoto = @profile.avatar_url

		@tableView = ui.table
			config: 'tableView'
			properties: {top: if hasHeaderPhoto then (if GLOBAL.isTablet then 250 else 160) else 0}

		@self.add @tableView

		if hasHeaderPhoto
			Ti.API.debug "Generating profile header avatar"
			headerBg = Ti.UI.createImageView
				width: Ti.UI.FILL
				height: if GLOBAL.isTablet then 250 else 160
				top: 0

			loadingImageIndicator = Ti.UI.createActivityIndicator
				color: '#2f3238'
				style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK
				height:Ti.UI.SIZE, width:Ti.UI.SIZE,

			headerWrap = ui.view
				properties:
					top: 0
					width: if GLOBAL.isTablet then 600 else Ti.UI.FILL
					height: if GLOBAL.isTablet then 268 else 165
					# backgroundColor: 'green'
				content: [
					headerBg
				]

			Ti.API.debug "Grabbing image from flickr api"
			flickr_image_url = ""
			flickr_query = "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=#{flickr_key}&text=#{encodeURIComponent(@profile.location or 'deep blue sea')}&has_geo=1&content_type=1&safe_search=1&accuracy=#{encodeURIComponent('~11')}&privacy_filter=1&extras=o_dims&per_page=100&page=1&format=json&nojsoncallback=1"
			xhr = new api.XHR
				onload: (e) ->
					data = e.json
					if parseInt(data.photos.total, 10) > 0
						Ti.API.debug "Got data from Flickr"
						Ti.API.debug "Selecting photo from data"
						for photo in data.photos.photo
							if parseInt(photo.o_width, 10) > parseInt(photo.o_height, 10)
								Ti.API.debug JSON.stringify(photo)
								Ti.API.debug "------------- selected -------------"
								flickr_image_url = "http://farm#{photo.farm}.staticflickr.com/#{photo.server}/#{photo.id}_#{photo.secret}.jpg"
								Ti.API.debug flickr_image_url
								headerBg.applyProperties {image: flickr_image_url}

								headerWrap.add Ti.UI.createView
									bottom: 0, width: Ti.UI.FILL, height: if GLOBAL.isTablet then 18 else 5
									backgroundImage: if GLOBAL.isTablet then 'images/dropshadow_ipad_profile.png' else 'images/dropshadow_vert.png'
									backgroundRepeat: if GLOBAL.isTablet then false else true

								loadingImageIndicator.hide()
								break
					else
						flickr_query = "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=#{flickr_key}&text=#{encodeURIComponent('deep blue sea')}&has_geo=1&content_type=1&safe_search=1&accuracy=#{encodeURIComponent('~11')}&privacy_filter=1&extras=o_dims&per_page=100&page=1&format=json&nojsoncallback=1"
						xhr = new api.XHR
							onload: (e) ->
								data = e.json
								Ti.API.debug "Got data from Flickr"
								Ti.API.debug "Selecting photo from data"
								for photo in data.photos.photo
									if parseInt(photo.o_width, 10) > parseInt(photo.o_height, 10)
										Ti.API.debug JSON.stringify(photo)
										Ti.API.debug "------------- selected -------------"
										flickr_image_url = "http://farm#{photo.farm}.staticflickr.com/#{photo.server}/#{photo.id}_#{photo.secret}.jpg"
										Ti.API.debug flickr_image_url
										headerBg.applyProperties {image: flickr_image_url}

										headerWrap.add Ti.UI.createView
											bottom: 0, width: Ti.UI.FILL, height: if GLOBAL.isTablet then 18 else 5
											backgroundImage: if GLOBAL.isTablet then 'images/dropshadow_ipad_profile.png' else 'images/dropshadow_vert.png'
											backgroundRepeat: if GLOBAL.isTablet then false else true

										loadingImageIndicator.hide()
										break

						xhr.open 'GET', flickr_query, true
						xhr.setRequestHeader 'Content-Type', 'application/json'
						xhr.send()

			xhr.open 'GET', flickr_query, true
			xhr.setRequestHeader 'Content-Type', 'application/json'
			xhr.send()

			headerWrap.add loadingImageIndicator
			headerWrap.add _f.createAvatar( @profile.avatar_url, {width: 100, height: 100, left: 10, bottom: if GLOBAL.isTablet then 25 else 12}, 13)
			@self.add headerWrap
			loadingImageIndicator.show()


		nameEntry = ui.textField
			config: 'profileTextField'
			properties: {editable: false, hintText: 'Not set', value: @profile.name}

		urlEntry = ui.textField
			config: 'profileTextField'
			properties: {editable: false, hintText: 'Not set', value: @profile.website}

		birthdateEntry = ui.textField
			config: 'profileTextField'
			properties: {editable: false, hintText: 'Not set', value: @profile.birthdate}

		genderEntry = ui.textField
			config: 'profileTextField'
			properties: {editable: false, hintText: 'Not set', value: @profile.gender}

		locationEntry = ui.textField
			config: 'profileTextField'
			properties: {editable: false, hintText: 'Not set', value: @profile.location}

		bioEntry = ui.textArea
			properties:
				top: 4, bottom: 4, left: 5, right: 5
				hintText: 'No Bio'
				value: @profile.bio
				editable: false
				autoLink: Ti.UI.AUTOLINK_ALL
				color: '#45474c'
				font: {fontSize:13, fontFamily:'Helvetica'}

		@tableData = [
			_f.createTableSection
				headerView: Ti.UI.createView (height:7)
				footerView: Ti.UI.createView (height:2)
			, [
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Name'} }
						nameEntry
					]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Gender'} }
						genderEntry
					]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Birthdate'} }
						birthdateEntry
					]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'URL'} }
						urlEntry
					]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Location'} }
						locationEntry
					]
			]
		,
			ui.section
				properties:
					headerView: Ti.UI.createView (height:3)
					footerView: Ti.UI.createView (height:5)
				content: [ui.row
					properties:
						height: 104
						backgroundImage: 'images/row_bg_full.png', backgroundTopCap: 8, backgroundLeftCap: 20
						selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
					content: [ bioEntry ]
					events: [['click', (e) -> nameEntry.focus() ]]
				]
		]

		@tableView.setData @tableData

	getView: ->
		@self
