ui = require 'lib/ui_sugar'
api = require 'lib/tent.api'
_f = require 'views/func_repo'


createView = (settings, callback) ->

	Ti.API.debug 'settings.updateFreq: '+JSON.stringify(settings)

	self = ui.win
		config: 'windowProperties'
		properties:
			title: 'Update Posts'
			leftNavButton: ui.button
				config: 'backButton'
				events: [[
					'click', (e) ->
						GLOBAL.tabGroup.activeTab.close self, {animated: true}
						if callback and callback.onclose
							callback.onclose settings
				]]

	tableView = ui.table { config: 'tableView' }

	settings_frequently = Ti.UI.createImageView
		image: if settings.frequently then 'images/checkbox_active.png' else 'images/checkbox.png'
		width: 25
		right: 0

	settings_rarely = Ti.UI.createImageView
		image: if settings.rarely then 'images/checkbox_active.png' else 'images/checkbox.png'
		width: 25
		right: 0

	settings_never = Ti.UI.createImageView
		image: if settings.never then 'images/checkbox_active.png' else 'images/checkbox.png'
		width: 25
		right: 0

	section = _f.createTableSection
			headerView: Ti.UI.createView {height:9}
			footerView: Ti.UI.createView {height:5}
		, [
			{
				config: 'rowWith2labels'
				properties: {className: 'row_labelandimage'}
				content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Frequently'} }), settings_frequently ]
				events: [['click', (e) ->
					settings.frequently = true
					settings.rarely = false
					settings.never = false
					updateCheckboxes()
				]]
			},
			{
				config: 'rowWith2labels'
				properties: {className: 'row_labelandimage'}
				content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Rarely'} }), settings_rarely ]
				events: [['click', (e) ->
					settings.frequently = false
					settings.rarely = true
					settings.never = false
					updateCheckboxes()
				]]
			},
			{
				config: 'rowWith2labels'
				properties: {className: 'row_labelandimage'}
				content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Never'} }), settings_never ]
				events: [['click', (e) ->
					settings.frequently = false
					settings.rarely = false
					settings.never = true
					updateCheckboxes()
				]]
			}
		]

	tableView.setData [section]
	self.add tableView

	updateCheckboxes = ->
		settings_frequently.image = if settings.frequently then 'images/checkbox_active.png' else 'images/checkbox.png'
		settings_rarely.image = if settings.rarely then 'images/checkbox_active.png' else 'images/checkbox.png'
		settings_never.image = if settings.never then 'images/checkbox_active.png' else 'images/checkbox.png'

	self

module.exports = createView