ui = require 'lib/ui_sugar'
api = require 'lib/tent.api2'
URI = require 'lib/vendor/jsuri.min'
_f = require 'views/func_repo'
NewPost = require 'views/newPost2'
ProfileEdit = require 'views/profile_edit'
ProfileDetails = require 'views/profile_details'


createView = (profile, viewOptions) ->
	Ti.API.debug "Creating a profile view for: #{JSON.stringify(profile)}"

	try

		profile = api.getCache('discover')[profile] if typeof profile == 'string'

		# for some error we might not have the profile
		return if not profile

		isMe = (profile.entity == GLOBAL.entity)
		canViewFollowing = false
		canViewFollowers = false
		hasProfile = !!profile.basic
		openCons = []

		updateCounters = ->
			# give 10sec between count updates
			if Ti.Network.online and not GLOBAL.loggedOut and ((new Date()).getTime() - (lastCountUpdate or 0)) > 10000
				postCountfilters = null
				# if profile.entity != GLOBAL.entity
				postCountfilters = {entity: profile.entity}

				openCons.push(
					api.posts_COUNT postCountfilters,
						onload: (count) ->
							Ti.App.Properties.setInt(GLOBAL.entity+'profile_postCounter', count) if isMe
							postCountLabel.text = count
						onerror: (e) ->
							Ti.API.error "posts_COUNT - #{e.status} - #{e.error}"
				)

				lastCountUpdate = (new Date()).getTime()
				openCons.push(
					api.followers_COUNT profile.entity,
						onload: (count) ->
							Ti.App.Properties.setInt(GLOBAL.entity+'profile_followersCounter', count) if isMe
							followersCountLabel.text = count
							canViewFollowers = true
						onerror: (e) ->
							Ti.API.error('followers_COUNT - '+e.status+' - '+e.error)
				)

				openCons.push(
					api.followings_COUNT profile.entity,
						onload: (count) ->
							Ti.App.Properties.setInt(GLOBAL.entity+'profile_followingCounter', count) if isMe
							followingCountLabel.text = count
							canViewFollowing = true
						onerror: (e) ->
							Ti.API.error('followings_COUNT - '+e.status+' - '+e.error)
				)

			canViewFollowing = true
			canViewFollowers = true


		postCounter = if isMe then Ti.App.Properties.getInt("#{GLOBAL.entity}profile_postCounter", '-') else '-'
		followingCounter = if isMe then Ti.App.Properties.getInt("#{GLOBAL.entity}profile_followingCounter", '-') else '-'
		followersCounter = if isMe then Ti.App.Properties.getInt("#{GLOBAL.entity}profile_followersCounter", '-') else '-'

		ignore_clicks = false
		lastCountUpdate = null
		myProfileData = null
		updateCountersTimer = null

		self = ui.win
			config: 'windowProperties'
			properties:
				title: 'Profile'
				rightNavButton: ui.button
					config: 'newPostButton'
					events: [
						['click', (e) ->
							e.source.enabled = false
							(new NewPost(null, null, {button: e.source})).open({modal: true})
						]
					]
				leftNavButton: (->
					if viewOptions and viewOptions.backButton
						return ui.button
							config: 'backButton'
							events: [['click', (e) -> GLOBAL.tabGroup.activeTab.close(self, {animated: true}) ]]
				)()
				layout: 'vertical'
			events: [
				['open', (e) ->
					updateCounters()
					# update every 5 minutes
					updateCountersTimer = setInterval(updateCounters, 300000) if isMe
				]
				['close', (e) ->
					# cancel all open connections
					api.cancelXHR(openCons)
					openCons.length = 0
					tableView.setData()
					self.remove( profileHeader )
					self.remove( tableView )
					clearInterval(updateCountersTimer)
				]
				['focus', (e) -> tableView.scrollsToTop = true ]
				['blur', (e) -> tableView.scrollsToTop = false ]
			]


		if viewOptions and viewOptions.backButton
			self.leftNavButton = ui.button
				config: 'backButton'
				events: [['click', (e) -> GLOBAL.tabGroup.activeTab.close(self, {animated: true}) ]]

		profileHeader = ui.view
			properties:
				width: Ti.UI.FILL
				height: 67
				backgroundImage: 'images/profile_top_bg.png'
			content: [
				# profile avatar
				(->
					avatar_url = profile.avatar_url or 'images/avatar.png'
					avatar = _f.createAvatar avatar_url,
						width: 48
						height: 48
						left: 10

					avatar.addEventListener 'click', (e) ->
						ignore_clicks = true
						profileDetailsView = new ProfileDetails( api.getProfile(profile.entity), {backButton: 'Back'})
						GLOBAL.tabGroup.activeTab.open(profileDetailsView.getView())
						ignore_clicks = false

					avatar
				)()

				# profile real name
				ui.label
					config: 'defaultLabel'
					properties:
						left: 66
						top: 14
						text: if hasProfile then profile.basic.name else 'No Profile'
						font: { fontSize: 16, fontWeight: 'bold' }
						color: if hasProfile then '#fff' else '#c4c4c4'
						shadowColor: '#000'
						shadowOffset: {x: 0, y: 1}

				# profile entity name
				ui.label
					config: 'defaultLabel'
					properties:
						left: 66
						top: 34
						text: (new URI(profile.entity)).host()
						font: { fontSize: 14}
						color: '#d4d5d7'
						shadowColor: '#000'
						shadowOffset: {x: 0, y: 1}
			]

		self.add( profileHeader )

		tableView = ui.table({ config: 'tableView' })
		tableData = []


		# if we're looking at our own profile
		if isMe
			# config button
			profileHeader.add ui.button
				properties:
					right: 2
					backgroundImage: 'images/cog.png'
					width: 44
					height: 44
					style: Ti.UI.iPhone.SystemButtonStyle.PLAIN
				events: [['click', (e) ->
					settings = require('views/settings')
					GLOBAL.tabGroup.activeTab.open( settings( {backButton: self.title} ))
				]]

			tableData = [
				_f.createTableSection({
					headerView: Ti.UI.createView({height:7})
					footerView: Ti.UI.createView({height:5})
				}, [
					{
						config: 'rowWithChild',
						content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Edit Profile'} }), Ti.UI.createImageView({image: 'images/arrow_child.png', right: 0, width: 11, height: 11}) ],
						events: [['click', (e) ->
							if not Ti.Network.online
								alert('Looks like you are offline.')
							else if not ignore_clicks
								ignore_clicks = true
								profileEditView = new ProfileEdit( api.getProfile(GLOBAL.entity), {backButton: 'Back'})
								GLOBAL.tabGroup.activeTab.open(profileEditView.getView())
								ignore_clicks = false
						]]
					}
				]),

				_f.createTableSection({
					headerView: Ti.UI.createView({height:5})
					footerView: Ti.UI.createView({height:5})
				}, [
					{
						config: 'rowWithChild',
						content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Favorites'} }), Ti.UI.createImageView({image: 'images/arrow_child.png', right: 0, width: 11, height: 11}) ],
						events: [['click', (e) ->
							if not Ti.Network.online
								alert('Looks like you are offline.')
							else if not ignore_clicks
								ignore_clicks = true
								postFavorites = require('views/postFavorites')
								GLOBAL.tabGroup.activeTab.open(postFavorites({hasBackButton: true}))
								ignore_clicks = false
						]]
					},
					{
						config: 'rowWithChild',
						content:  [ ui.label({config: 'defaultRowLabel', properties: {text: 'Drafts'} }), Ti.UI.createImageView({image: 'images/arrow_child.png', right: 0, width: 11, height: 11}) ],
						events: [['click', (e) ->
							if not Ti.Network.online
								alert('Looks like you are offline.')
							else if not ignore_clicks
								ignore_clicks = true
								postDrafts = require('views/postDrafts')
								GLOBAL.tabGroup.activeTab.open(postDrafts({hasBackButton: true}))
								ignore_clicks = false
						]]
					}
				])
			]

		else
			addFollowButton = (isFollowing) ->
				# follow button
				follow_button = ui.button
					config: if isFollowing then 'followingButton_profile' else 'followButton_profile'
					properties:
						right: 15
						isFollowing: isFollowing
						actInd: Ti.UI.createActivityIndicator
							style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN
							height:Ti.UI.SIZE
							width:Ti.UI.SIZE
							right: 15
					events: [['click', (e) ->
						if not Ti.Network.online
							alert('Looks like you are offline.')
						else
							source = e.source
							source.hide()
							source.actInd.show()
							if not source.isFollowing
								openCons.push api.followings_POST
									entity: profile.entity
								,
									onload: (newFollow) ->
										api.addFollowing(newFollow)
										source.applyProperties(ui.defaultConfig['followingButton_profile'])
										source.actInd.hide()
										source.show()
									onerror: (e) ->
										if e.status == 404
											api.removeFollowing(profile.entity)
											source.applyProperties(ui.defaultConfig['followButton_profile'])
											source.actInd.hide()
											source.show()
										# already following error
										else if e.status == 409
											api.followings_GET_ENTITY profile.entity,
												onload: (followData) ->
													api.addFollowing(followData)
													source.applyProperties(ui.defaultConfig['followingButton_profile'])
													source.actInd.hide()
													source.show()
												onerror: (e) ->
													source.applyProperties(ui.defaultConfig['followingButton_profile'])
													source.actInd.hide()
													source.show()
										else
											source.actInd.hide()
											source.show()
							else
								openCons.push( api.followings_DELETE
									id: api.getFollowing(profile.entity).id
								,
									onload: (e) ->
										api.removeFollowing(profile.entity)
										source.applyProperties(ui.defaultConfig['followButton_profile'])
										source.actInd.hide()
										source.show()
									onerror: (e) ->
										if e.status == 404
											api.followings_GET_ENTITY profile.entity,
												onload: (followData) ->
													api.followings_DELETE({
														id: followData.id
													}, {
														onload: (e) ->
															source.applyProperties(ui.defaultConfig['followButton_profile'])
															source.actInd.hide()
															source.show()
														onerror: (e) ->
															source.actInd.hide()
															source.show()
													})
										else
											source.actInd.hide()
											source.show()
								)
					]]

				profileHeader.add(follow_button)
				profileHeader.add(follow_button.actInd)


			api.isFollowing profile.entity, Ti.Network.online,
				onload: (isFollowing) ->
					addFollowButton(isFollowing)

			tableData = [
				_f.createTableSection({
					headerView: Ti.UI.createView({height:7})
					footerView: Ti.UI.createView({height:5})
				}, [
					{
						config: 'rowWithoutChild'
						content: [
							ui.label
								config: 'defaultRowLabel'
								properties: {text: 'New Message'}
						]
						events: [['click', (e) ->
							if not Ti.Network.online
								alert('Looks like you are offline.')
							else if not ignore_clicks
								ignore_clicks = true
								newPost = new NewPost
									pmSwitch: true
									prefill:
										content:
											text: "^#{profile.entity} "
										mentions: [profile.entity]

								newPost.open({modal: true})
								ignore_clicks = false
						]]
					}
				])
			]


		postCountLabel = ui.label
			text: postCounter
			textAlign: 'right'
			color: '#8f9194'
			right: 0
			width: 200
			height: Ti.UI.SIZE
			font: {fontSize: 14, fontFamily: 'Helvetica Neue'}

		followingCountLabel = ui.label
			text: followingCounter
			textAlign: 'right'
			color: '#8f9194'
			right: 0
			width: 200
			height: Ti.UI.SIZE
			font: {fontSize: 14, fontFamily: 'Helvetica Neue'}

		followersCountLabel = ui.label
			text: followersCounter
			textAlign: 'right'
			color: '#8f9194'
			right: 0
			width: 200
			height: Ti.UI.SIZE
			font: {fontSize: 14, fontFamily: 'Helvetica Neue'}

		tableData.push(
			_f.createTableSection({
				headerView: Ti.UI.createView({height:5})
				footerView: Ti.UI.createView({height:5})
			}, [
				{
					config: 'rowWith2labels'
					content: [
						ui.label
							config: 'defaultRowLabel'
							properties: {text: 'All Posts'}
						postCountLabel
					]
					events: [['click', (e) ->
						if not Ti.Network.online
							alert 'Looks like you are offline.'
						else if not ignore_clicks
							ignore_clicks = true
							Posts = require('views/userPosts')
							GLOBAL.tabGroup.activeTab.open(new Posts
								title: (new URI(profile.entity)).host()
								entity: profile.entity
							)
							ignore_clicks = false
					]]
				},
				{
					config: 'rowWith2labels'
					content: [
						ui.label
							config: 'defaultRowLabel'
							properties: {text: 'Following'}
						followingCountLabel
					]
					events: [['click', (e) ->
						if not Ti.Network.online
							alert('Looks like you are offline.')
						else if not ignore_clicks and (canViewFollowing or followingCounter)
							ignore_clicks = true
							Bonds = require('views/bonds')
							GLOBAL.tabGroup.activeTab.open(new Bonds('following', profile, { backButton: self.title, maxBonds: followingCountLabel.text }))
							ignore_clicks = false
					]]
				},
				{
					config: 'rowWith2labels'
					content: [
						ui.label
							config: 'defaultRowLabel'
							properties: {text: 'Followers'}
						followersCountLabel
					]
					events: [['click', (e) ->
						if not Ti.Network.online
							alert('Looks like you are offline.')
						else if not ignore_clicks and (canViewFollowers or followersCounter)
							ignore_clicks = true
							Bonds = require('views/bonds')
							GLOBAL.tabGroup.activeTab.open(new Bonds('followers', profile, { backButton: self.title, maxBonds: followersCountLabel.text }))
							ignore_clicks = false
					]]
				}
			])
		)

		tableView.setData( tableData )
		self.add(tableView)

		self

	catch er
		Ti.API.error(er)


module.exports = createView
