moment = require 'lib/vendor/moment.min'
StreamView = require 'views/streamView'

# change moment lang to a shorter format
moment.lang 'timeline'

createView = (conf) ->
	self = new StreamView
		title: conf.title
		api_GET: 'entity_posts_GET'
		api_posts_OLD: 'posts_OLD'
		entity: conf.entity
		noEvents: true
		noCache: true
		hasBackButton: true

	self

module.exports = createView