ui = require 'lib/ui_sugar'
URI = require 'lib/vendor/jsuri.min'
api = require 'lib/tent.api2'

class Main

	constructor: ->

		@authenticating = false

		@addAuthEvents()

		@self = ui.win
			properties: { navBarHidden: true, backgroundColor: '#029cd2' }
			events: [
				['open', (e) =>
					api.clearAuthCache()
					if GLOBAL.reAuthApp
						@entity_input.value = GLOBAL.entity
						@reAuthApp()
					@entity_input.focus() if !api.isAuthenticated()
					GLOBAL.hasAuthorizeOpen = true
					return
				],
				['focus', (e) =>
					Ti.API.debug 'Authorize has focus.'
					if !@authenticating
						@resetAuthButton()
						@entity_input.value = ''
						@entity_input.focus()
					else if api.cacheData and !api.cacheData.oauth
						@resetAuthButton()
					return
				]
			]

		@scrollView = Ti.UI.createScrollView
			contentWidth: 'auto'
			contentHeight: 'auto'
			height: Ti.UI.FILL
			width: Ti.UI.FILL
			showVerticalScrollIndicator: false

		@wrap = Ti.UI.createView
			height: Ti.UI.SIZE
			width: Ti.UI.FILL
			layout: 'vertical'
			top: 53

		@appLogo = Ti.UI.createImageView
			image: 'images/authorize_logo.png'
			width: 117
			height: 117
			top: 0

		@entity_input = ui.textField
			properties:
				hintText: 'Entity URI'
				value: ''
				color: '#009BD4'
				textAlign: 'center'
				top: 40, height: 40, width: 280
				borderRadius: 2, borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE
				autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
				autocorrect: false
				keyboardType: Ti.UI.KEYBOARD_URL
				returnKeyType: Ti.UI.RETURNKEY_NEXT
				backgroundColor: '#E6F5FB'
			events: [
				['change', (e) =>
					@loginButton.enabled = !!e.source.value.length
				],
				['return', (e) =>
					e.source.blur()
					url = new URI(e.source.value)
					e.source.value = "https://#{e.source.value}" if !url.protocol()
					@authEntity() if e.source.value
					return
				]
			]

		@loginButton = ui.button
			properties:
				title: L('authenticate', 'Authenticate')
				top: 15
				height: 40
				width: 280
				enabled: false
				backgroundImage: 'images/ipad-button-green.png'
				backgroundSelectedImage: 'images/ipad-button-green-pressed.png'
				style: Ti.UI.iPhone.SystemButtonStyle.PLAIN
				color: 'white'
				shadowColor: 'black'
				shadowOffset: {x:0, y:1}
				font: {fontWeight:'bold', fontSize:18}

			events: [
				['click', (e) =>
					@entity_input.blur()
					url = new URI(@entity_input.value)
					@entity_input.value = "https://#{@entity_input.value}" if !url.protocol()
					@authEntity() if @entity_input.value
					return
				]
			]

		@wrap.add @appLogo
		@wrap.add @entity_input
		@wrap.add @loginButton

		@scrollView.add @wrap
		@self.add @scrollView

	# FUNCTIONS
	authEntity: =>
		@authenticating = true
		@entity_input.blur()
		@loginButton.applyProperties
			title: 'Discovering service...'
			enabled: false
			backgroundImage:'images/ipad-button-grey.png'
			backgroundSelectedImage: 'images/ipad-button-grey-pressed.png'
		
		api.discover @entity_input.value, 
			onload: (e) =>
				Ti.API.debug 'Setting profile avatar and other stuff here.'

				# store entity uri
				GLOBAL.entity = e.entity
				GLOBAL.apn_channel = GLOBAL.entity.replace('://','_').replace(/\./g,'_0_').replace(/\:/g,'_00_')
				Ti.App.Properties.setString('entity', GLOBAL.entity)

				# only registar app if no access_token is present
				@loginButton.title = 'Registering app...'
				Ti.API.debug 'Will start app registration now.'
				api.registerApp(true); # forceRegister = true

			onerror: =>
				@resetAuthButton()
				alert('Service discovery failed.\nPlease try again.')

			thiz: this
		, true
		return

	reAuthApp: =>
		@authenticating = true
		@entity_input.blur()
		@loginButton.applyProperties
			title: 'Re-Authenticating...'
			enabled: false
			backgroundImage: 'images/ipad-button-grey.png'
			backgroundSelectedImage: 'images/ipad-button-grey-pressed.png'
		api.registerApp(true)

	grabTokenSuccess: (e) =>
		Ti.API.debug 'Clearing all cache before starting...'
		api.clearDiscoveryCache()
		Ti.App.Properties.removeProperty 'myStream_cache'
		Ti.App.Properties.removeProperty 'myMentions_cache'
		@syncFollowsAndStart()

	grabTokenFailed: =>
		alert 'Authentication failed.\nPlease try again.'
		@resetAuthButton()

	registerAppError: (e) =>
		@resetAuthButton()
		alert 'App registration failed.\nPlease try again.'

	registerAppCanceled: (e) =>
		@resetAuthButton()
		alert 'App registration canceled.'

	addAuthEvents: =>
		Ti.App.addEventListener 'api:grabToken:success', @grabTokenSuccess
		Ti.App.addEventListener 'api:grabToken:failed', @grabTokenFailed
		Ti.App.addEventListener 'api:registerApp:error', @registerAppError
		Ti.App.addEventListener 'api:registerApp:canceled', @registerAppCanceled

	removeAuthEvents: =>
		Ti.App.removeEventListener 'api:registerApp:error', @registerAppError
		Ti.App.removeEventListener 'api:grabToken:success', @grabTokenSuccess
		Ti.App.removeEventListener 'api:grabToken:failed', @grabTokenFailed

	resetAuthButton: =>
		@loginButton.applyProperties
			title: L('authenticate', 'Authenticate')
			enabled: true
			backgroundImage:'images/ipad-button-green.png'
			backgroundSelectedImage: 'images/ipad-button-green-pressed.png'

	syncFollowsAndStart: =>
		@loginButton.title = 'Updating followings...'
		api.followings_GET
			onload: =>
				@loginButton.title = 'Updating followers...'
				api.followers_GET
					onload: =>
						@loginButton.title = 'All done. Welcome!'
						@openMain()
					onerror: =>
						@loginButton.title = 'Server having problems.'
						@openMain()
			onerror: =>
				@loginButton.title = 'Server having problems.'
				@openMain()

	openMain: =>
		@authenticating = false
		GLOBAL.loggedOut = false
		main = require 'views/main'

		setTimeout =>
			# flip the view and show main app window
			main.getView().open
				transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
			@self.close()
		, 1000

		# remove event listeners
		@removeAuthEvents()

module.exports = (new Main()).self