ui = require 'lib/ui_sugar'
api = require 'lib/tent.api'
_f = require 'views/func_repo'
apn = require 'lib/apn'

settings = if Ti.App.Properties.hasProperty('settings') then JSON.parse(Ti.App.Properties.getString('settings')) else {}

createView = (viewOptions) ->

	if typeof profile == 'string'
		profile = api.getCache('discover')[profile]

	ignore_clicks = false
	
	self = ui.win
		config: 'windowProperties'
		properties:
			title: 'Settings'

	if viewOptions and viewOptions.backButton
		self.leftNavButton = ui.button
			config: 'backButton'
			events: [[
				'click', (e) ->
					GLOBAL.tabGroup.activeTab.close self, {animated: true}
					return
			]]


	tableView = ui.table
		config: 'tableView'

	section1 = _f.createTableSection
			headerView: Ti.UI.createView {height:9}
			footerView: Ti.UI.createView {height:5}
		, [
			config: 'rowWithoutChild',
			content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Clear cache'} }) ]
			events: [[
				'click', (e) ->
					ui.alert(
						properties:
							cancel: 1
							buttonNames: [L('ok', 'Ok'), L('cancel', 'Cancel')]
							message: L('clear_cache_warning',"Some user data might be unavailable until #{Ti.App.name}\ndownloads it again")+'.'
							title: L('confirm','Confirm')

						events: [
							['click', (e) ->
								if e.index != e.cancel
									api.clearDiscoveryCache()
									Ti.App.Properties.removeProperty 'myStream_cache'
									Ti.App.Properties.removeProperty 'myMentions_cache'
									Ti.App.Properties.removeProperty 'settings'
									Ti.App.Properties.removeProperty 'bonds:following'
									Ti.App.Properties.removeProperty 'bonds:following_ext'
									Ti.App.Properties.removeProperty 'bonds:followers'
								return
							]
						]
					).show()
					return
			]]
		]

	section2 = _f.createTableSection
			headerView: Ti.UI.createView {height:5}
			footerView: Ti.UI.createView {height:5}
		, [
			config: 'rowWithChild'
			content: [
				ui.label
					config: 'defaultRowLabel'
					properties: {text: 'Update Frequency'}
				Ti.UI.createImageView {image: 'images/arrow_child.png', right: 0, width: 11, height: 11}
			]
			events: [[
				'click', (e) ->
					UpdateFreq = require 'views/settings_updateFreq'
					updateFreqWin = UpdateFreq settings.updateFreq or {frequently: true, rarely: false, never: false},
						onclose: (updateFreq) ->
							Ti.API.debug "settings.updateFreq: #{JSON.stringify(updateFreq)}"
							
							settings.updateFreq = updateFreq
							settings.updateFreq.minutes_stream = if updateFreq.frequently then 1.7 else if updateFreq.rarely then 10.7 else 0
							settings.updateFreq.minutes_mentions = if updateFreq.frequently then 1.2 else if updateFreq.rarely then 10.2 else 0
							
							Ti.App.Properties.setString 'settings', JSON.stringify(settings)
							
							GLOBAL.appConfig.bgCheck.posts_timeout = settings.updateFreq.minutes_stream*60000
							GLOBAL.appConfig.bgCheck.mentions_timeout = settings.updateFreq.minutes_mentions*60000
							
							Ti.App.fireEvent 'startBgJob'
							return

					GLOBAL.tabGroup.activeTab.open updateFreqWin
					return
			]]
		]

	section3 = _f.createTableSection
			headerView: Ti.UI.createView {height:5}
			footerView: Ti.UI.createView {height:5}
		, [
			config: 'rowWithoutChild'
			content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Check APN state'} }) ]
			events: [[
				'click', (e) ->
					apn_state = require('views/apn_state').createView()
					GLOBAL.tabGroup.activeTab.open apn_state
					return
			]]
		]

	tableData = [ section1, section2, section3 ]

	tableView.setData tableData
	self.add tableView

	self

module.exports = createView