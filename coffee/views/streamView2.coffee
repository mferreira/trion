ui = require 'lib/ui_sugar'

module.exports = class ProfileEdit
	constructor: (@conf) ->
		Ti.API.debug "streamView <- #{JSON.stringify(@conf)}"
		@self = ui.win
			config: 'windowProperties'
			properties:
				title: @conf.title or 'Title Here'
				navBarHidden: false
				rightNavButton: ui.button
					config: 'newPostButton',
					events: [['click', (e) ->
						e.source.enabled = false;
						(new NewPost()).open({modal: true});
						setTimeout (->
							e.source.enabled = true
						), 1000
					]]
		if @conf.hasBackButton
			@self.leftNavButton = ui.button
				config: 'backButton'
				events: [['click', (e) -> GLOBAL.tabGroup.activeTab.close(@self, animated: true) ]]

	showPullView: (text) ->
		if text
			tablePullView.message.text = text
			tablePullView.hide tablePullView.loadingIndicator
			tablePullView.show tablePullView.message
		else
			tablePullView.show tablePullView.loadingIndicator
			tablePullView.hide tablePullView.message
		tablePullView.show()

	getView: ->
		@self
