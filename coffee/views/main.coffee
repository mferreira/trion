GLOBAL = require 'globals'
api = require 'lib/tent.api2'
_f = require 'views/func_repo'

myStreamWindow = require 'views/myStream'
mentionsWindow = require 'views/mentions'
Profile = require 'views/profile'

png_name_ext = if GLOBAL.isTablet then '_76' else ''

tab_home = Ti.UI.createTab
	backgroundImage: "images/stream_tab#{png_name_ext}.png"
	selectedBackgroundImage: "images/stream_tab_active#{png_name_ext}.png"
	backgroundImage2: "images/stream_tab2#{png_name_ext}.png"
	selectedBackgroundImage2: "images/stream_tab_active2#{png_name_ext}.png"
	tabName: 'Stream'
	window: myStreamWindow

# myStreamWindow.defineTab tab_home

tab_mentions = Ti.UI.createTab
	backgroundImage: "images/mentions_tab#{png_name_ext}.png"
	selectedBackgroundImage: "images/mentions_tab_active#{png_name_ext}.png"
	backgroundImage2: "images/mentions_tab2#{png_name_ext}.png"
	selectedBackgroundImage2: "images/mentions_tab_active2#{png_name_ext}.png"
	tabName: 'Mentions'
	window: mentionsWindow

# mentionsWindow.defineTab tab_mentions

tab_me = Ti.UI.createTab
	backgroundImage: "images/profile_tab#{png_name_ext}.png"
	selectedBackgroundImage: "images/profile_tab_active#{png_name_ext}.png"
	backgroundImage2: "images/profile_tab2#{png_name_ext}.png"
	selectedBackgroundImage2: "images/profile_tab_active2#{png_name_ext}.png"
	tabName: 'Me',
	window: new Profile GLOBAL.entity

# create tab group
tabGroup = Ti.UI.createTabGroup()

# add tabs
tabGroup.addTab tab_home
tabGroup.addTab tab_mentions
tabGroup.addTab tab_me

tabGroup = _f.overrideTabs tabGroup,
	{backgroundImage: 'images/tabbar.png', backgroundTopCap: 5, height: 49},
	{},
	{}

GLOBAL.tabGroup = tabGroup

####################################

exports.getView = ->

	# pre-discover our profile data
	api.discover GLOBAL.entity

	# background jobs
	Ti.API.debug 'Starting post timestamp updater job.'
	setInterval ->
		if GLOBAL.timestampCache.length
			for tsCache in GLOBAL.timestampCache
				tsLabel = tsCache[0]
				ts = tsCache[1]
				tsLabel.text = moment(ts).fromNow()
	, 60000 # TODO: put this in the config screen
	
	# Ti.App.fireEvent 'startBgJob'

	if Ti.Platform.model isnt "Simulator"
		apn = require 'lib/apn'
		try
			if Ti.Network.remoteNotificationsEnabled
				if !Ti.Network.remoteDeviceUUID
					Ti.API.warn 'remoteDeviceUUID is not set. Re-registering for Push Notifications.'
					apn.unregisterForPushNotifications()
					apn.registerForPushNotifications()
				else
					apn.checkInstallation
						onload: (e) ->
							if e.channels.indexOf(GLOBAL.apn_channel) == -1
								# subsribe entity to APN channel
								apn.subscribeChannel GLOBAL.apn_channel
						onerror: (e) ->
							Ti.API.warn 'Parse installation check error. Uploading new installation.'
							apn.uploadParseInstallation()
			else
				apn.registerForPushNotifications()

		catch err
			Ti.API.error err
			alert err

	return tabGroup


exports.updateFollows = (callback) ->
	if api.isAuthenticated() and Ti.Network.online
		Ti.API.info 'Updating followings...'
		api.followings_GET
			onload: ->
				Ti.API.info 'Updating followers...'
				api.followers_GET
					onload: ->
						callback()
					onerror: ->
						Ti.API.warn 'Searching followings - Server having problems'
						callback()
			onerror: ->
				Ti.API.warn 'Searching followers - Server having problems'
				callback()


exports.addTabGroupEvents = ->
	Ti.App.addEventListener 'notify:newPosts', (e) ->
		if tabGroup.activeTab.tabName != 'Stream' and e.postsLength
			tabGroup.hasNotification tabGroup.tabs[0]

	Ti.App.addEventListener 'notify:newMentions', (e) ->
		if tabGroup.activeTab.tabName != 'Mentions' and e.postsLength
			tabGroup.hasNotification tabGroup.tabs[1]
