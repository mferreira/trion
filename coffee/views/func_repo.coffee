ui = require('lib/ui_sugar')
GLOBAL = require('globals')
api = require('lib/tent.api2')
uri = require('lib/vendor/jsuri.min')
moment = require('lib/vendor/moment.min')
_ = require('lib/vendor/underscore-min')
utils = require 'lib/utils'
Map = require 'ti.map'


RE_mention = /\^\[([^\[\]]+)\]\(\d+\)/gi

createRowFromPost = (post, viewProperties) ->
	Ti.API.debug "Creating row post for: #{post.type}"
	# Ti.API.debug "Creating row from post data: #{JSON.stringify post}"

	fillEntityInfo = (profile) ->
		Ti.API.debug('post.entity: ', post.entity);
		Ti.API.debug('profile: ', JSON.stringify(profile))

		entityName.setText profile.profile.name if profile.profile.name.length
		if api.getCache('avatars')[post.entity]
			entityAvatar.setImage api.attachment_URL
				entity: postEntity
				attachment: api.getCache('avatars')[post.entity]

		# else {
		# 	entityName.setText( (new uri(post.entity)).host() );
		# }
		# entityLabel.setText( (new URI(profile.core.entity)).host() );
		# bio.setText( profile.basic.bio );
		# location.setText( profile.basic.location || 'Unknown' );

	postType = post.type.split('#')[0]
	isBottom = (viewProperties && (viewProperties.backgroundImage == 'images/row_bg_bottom.png' || viewProperties.backgroundImage == 'images/row_bg_full.png'))
	isRepost = (postType == api.postType('Repost'))
	hasLocation = !!post.content.location
	postEntity = if isRepost then post.original_post.profile.entity else post.entity

	entityProfile = api.getProfile(post.entity)
	repostEntityProfile = null

	wrapView = ui.view
		properties:
			touchEnabled: true
			top: 11, bottom: 12, left: 71
			width: if GLOBAL.isTablet then 510 else 210

	entityName = ui.label
		touchEnabled: false
		text: (new uri(postEntity)).host()
		color: '#2E3238'
		top: 0, left: 0, width: 225, height: 13
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
		font: { fontSize: 13, fontWeight: 'bold' }

	entityAvatar = ui.image
		properties:
			bubbleParent: false
			defaultImage: 'images/avatar.png'
			image: 'images/avatar.png'
			width: 46, height: 46, top: 13, left: 14
			borderRadius: 3
		events: [['click', (e) ->
			if postEntity == GLOBAL.entity
				GLOBAL.tabGroup.setActiveTab(2);
			else
				Profile = require('views/profile')
				profileView = new Profile(postEntity, { backButton: 'back' })

				if not profileView
					alert('Error discovering this entity.')
				else
					GLOBAL.tabGroup.activeTab.open( profileView )
		]]


	postTimestampLabel = ui.label
		touchEnabled: false
		text: moment(post.published_at).fromNow()
		top: 0, right: 0, width: 90, height: 10
		textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, color: '#909AA2', font: { fontSize: 10 }

	# we'll use this to update every timestamp
	GLOBAL.timestampCache.push([postTimestampLabel, post.published_at]);

	postInfoView = ui.view
		properties:
			touchEnabled: false, bottom: if isBottom then 4 else 0
			height: 28, left: 2, right: 2, layout: 'horizontal'
			backgroundImage: if isBottom then 'images/postinfo_bottom_bg.png' else 'images/postinfo_bg.png'

	rowConfig = 'genericPostRow'
	className = ''
	hasDetails = true

	if hasLocation
		postInfoView.add ui.image
			properties:
				widht: 8, height: 8, top: 11
				left: if postInfoView.children.length then 3 else 69
				image: 'images/postinfo_location.png', touchEnabled: false


	# TODO: implement diferent row layout for post types
	switch postType

		when api.postType('Status') # TODO: remove this ugly tail. Use REGEX instead
			className = 'status';
			# replace inline mention for display text
			postContent = post.content.text.replace(RE_mention, "$1")

		when api.postType('Repost')
			className = 'repost';
			# replace inline mention for display text
			postContent = post.content.text.replace(RE_mention, "$1")
			# title shows entity of the original post
			repostEntityProfile = post.original_post.profile

			# add symbol and information about the repost
			postInfoView.add ui.image
				properties:
					widht: 9, height: 8, top: 12
					left: if postInfoView.children.length then 3 else 69
					image: 'images/repost_icon.png', touchEnabled: false

			repostedLabel = ui.label
				touchEnabled: false
				text: "Repost by #{if entityProfile then entityProfile.profile.name else (new uri(post.entity)).host()}"
				left: 5, height: 13, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
				color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x: 0, y: 1}

			if not entityProfile
				# try to grab the entity name
				api.discover post.entity,
					onload: (profileData) ->
						repostedLabel.text = "Repost by #{profileData.profile.name}"

			postInfoView.add repostedLabel


		when api.postType('Follower')
			className = 'follower';
			postContent = 'You have a new follower';
			hasDetails = false;

		when api.postType('Photo')
			className = 'photo';
			# replace inline mention for display text
			postContent = post.content.caption.replace(RE_mention, "$1")
			Ti.API.debug "postContent is: #{postContent}"

			imageURL = api.attachment_URL
				entity: if isRepost then post.original_post.profile.entity else post.entity
				attachments: if isRepost then post.original_post.attachments else post.attachments

			postImage = Ti.UI.createImageView
				image: utils.cropImageURL(imageURL, 210, 110)
				width: 210, height: 110
				top: 5, left: 0, right: 0

			isPhotoPost = true


	# check if we have the entity profile
	if repostEntityProfile
		fillEntityInfo repostEntityProfile
	# discover the profile if we dont know it yet
	else if entityProfile
		fillEntityInfo entityProfile
	else
		api.discover post.entity,
			onload: (profileData) ->
				fillEntityInfo profileData
			onerror: (resp) ->
				Ti.API.warn 'Failed entity profile discovery. Retrying...'
				api.discover post.entity,
					onload: (profileData) ->
						fillEntityInfo profileData
					onerror: (resp) ->
						Ti.API.warn 'Failed entity profile discovery. Giving up.'


	if viewProperties and viewProperties.autoLink
		postContentText = ui.textArea
			properties:
				value: postContent
				color: '#2F3238', font: {fontFamily: 'Helvetica', fontSize: 13}
				textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
				autoLink: Ti.UI.AUTOLINK_ALL
				editable: false, scrollable: false
				backgroundColor: 'white'
				bubbleParent: true
				left: -8, right: -9, height: Ti.UI.SIZE

		if isPhotoPost
			postContentText.applyProperties({top: 0})
		else
			postContentText.applyProperties({top: 7, bottom: -1})

	else
		postContentText = ui.label
			touchEnabled: false
			text: postContent
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
			color: '#2F3238', font: { fontSize: 13 }
			left: 0, right: 0, height: Ti.UI.SIZE

		if isPhotoPost
			postContentText.applyProperties({top: 0})
		else
			postContentText.applyProperties({top: 16, bottom: 0})

	if isPhotoPost
		postContentText = ui.view
			properties:
				layout: 'vertical'
				top: 16, bottom: 0, left: 0, right: 0, height: Ti.UI.SIZE
			content: [ postContentText, postImage ]

	postContentView = ui.view
		properties: { touchEnabled: true, top: 0, left: 0, right: 0 }
		content: [
			# post content
			postContentText
			# entity name
			entityName
			# time 'ago'
			postTimestampLabel
		]
	wrapView.add postContentView


	if post.permissions and not post.permissions['public']
		className += 'Private'
		# add symbol and information about privacy
		postInfoView.add ui.label
			touchEnabled: false
			text: '$', height: 11, top: 10
			left: if postInfoView.children.length then 3 else 69
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
			color: '#5E6165', font: { fontSize: 10, fontFamily: 'Tentr-symbols' }

		postInfoView.add ui.label
			touchEnabled: false
			text: 'Private', left: 2, height: Ti.UI.SIZE, top: 9
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165'
			font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x: 0, y: 1}


	reply = null
	# run through the mentions to check for any post - it's a reply
	if postType != api.postType('Repost') && post.mentions
		for mention in post.mentions
			if mention.post
				className += 'Reply'
				reply = mention
				replyEntityProfile = api.getProfile(reply.entity)

				# its a reply - add visual info about it
				postInfoView.add ui.image
					properties:
						widht: 9
						height: 8
						top: 11
						left: if postInfoView.children.length then 3 else 69
						image: 'images/reply_icon.png'
						touchEnabled: false

				replyedLabel = ui.label
					touchEnabled: false
					text: "Reply to #{if replyEntityProfile then replyEntityProfile.profile.name else (new uri(reply.entity)).host()}"
					left: 5, height: 13, top: 9
					textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165'
					font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x: 0, y: 1}

				if not entityProfile
					# try to grab the entity name
					api.discover reply.entity,
						onload: (profileData) ->
							replyedLabel.text = "Repost by #{profileData.profile.name}"

				postInfoView.add replyedLabel

				break


	# hack to set the height to acomodate content
	postContentView.height = Ti.UI.SIZE
	wrapView.height = Ti.UI.SIZE

	rowProperties =
		className: className
		postData: post
		height: Ti.UI.SIZE
		hasDetails: hasDetails

	_.extend(rowProperties, viewProperties) if viewProperties

	rowContent = [ entityAvatar, wrapView ]

	if postInfoView.children.length
		# give some bottom space
		postContentView.bottom = 30
		rowContent.push(postInfoView)
		entityAvatar.bottom = 42
	else
		entityAvatar.bottom = if isBottom then 18 else 13

	row = ui.row
		config: rowConfig
		properties: rowProperties
		content: rowContent

	row



createRowDetailsFromPost = (post, viewProperties) ->
	Ti.API.debug('Creating row from post data: '+JSON.stringify(post));

	fillEntityInfo = (profile) ->
		# Ti.API.debug('post.entity: ', post.entity);
		Ti.API.debug 'fillEntityInfo <- ', JSON.stringify(profile)

		entityName.setText profile.profile.name if profile.profile.name.length
		if api.getCache('avatars')[post.entity]
			entityAvatar.setImage api.attachment_URL
				entity: postEntity
				attachment: api.getCache('avatars')[post.entity]

		entityURI.setText( (new uri(profile.entity)).host() )

	postType = post.type.split('#')[0]
	isBottom = (viewProperties && (viewProperties.backgroundImage == 'images/row_bg_bottom.png' || viewProperties.backgroundImage == 'images/row_bg_full.png'))
	isRepost = (postType is api.postType('Repost'))
	hasLocation = !!post.content.location
	postEntity = if isRepost then post.original_post.profile.entity else post.entity

	_entityURI = new uri(postEntity)

	entityProfile = api.getProfile(post.entity)
	repostEntityProfile = null

	wrapView = ui.view
		properties:
			touchEnabled: true
			top: 11
			width: if GLOBAL.isTablet then 510 else 210
			left: 71

	entityName = ui.label
		touchEnabled: false, text: _entityURI.host(), color: '#2E3238'
		top: 7, left: 0, width: 225, height: 15
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: { fontSize: 15, fontWeight: 'bold' }

	entityURI = ui.label
		touchEnabled: false, text: '^'+_entityURI.host(), color: '#97989b'
		top: 29, left: 0, width: 225, height: 13
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: { fontSize: 13 }

	entityAvatar = ui.image
		properties:
			bubbleParent: false, width: 46, height: 46, top: 13, left: 14, borderRadius: 3
			defaultImage: 'images/avatar.png', image: 'images/avatar.png'
		events: [['click', (e) ->
			if postEntity == GLOBAL.entity
				GLOBAL.tabGroup.setActiveTab(2)
			else
				Profile = require('views/profile')
				GLOBAL.tabGroup.activeTab.open( new Profile(postEntity, { backButton: 'back' }) )
		]]

	postTimestampLabel = ui.label
		touchEnabled: false, text: moment(post.published_at).fromNow()
		top: 8, right: 0, width: 90, height: 10
		textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, color: '#909AA2', font: { fontSize: 10 }

	# we'll use this to update every timestamp
	GLOBAL.timestampCache.push([postTimestampLabel, post.published_at])

	postContent = post.content.text
	postInfoView = ui.view
		properties:
			touchEnabled: false, bottom: if isBottom then 4 else 0
			height: 28, left: 2, right: 2, layout: 'horizontal'
			backgroundImage: if isBottom then 'images/postinfo_bottom_bg.png' else 'images/postinfo_bg.png'

	rowConfig = 'genericPostRow'
	className = ''
	hasDetails = true

	# TODO: implement diferent row layout for post types
	switch postType
		when api.postType('Status')
			className = 'status'
			# replace inline mention for display text
			postContent = post.content.text.replace(RE_mention, "$1")

		when api.postType('Repost')
			className = 'repost'
			# replace inline mention for display text
			postContent = post.content.text.replace(RE_mention, "$1")
			# title shows entity of the original post
			repostEntityProfile = post.original_post.profile

			# add symbol and information about the repost
			postInfoView.add ui.image
				properties:
					widht: 9, height: 8, top: 12
					left: if postInfoView.children.length then 3 else 69
					image: 'images/repost_icon.png', touchEnabled: false

			repostedLabel = ui.label
				touchEnabled: false
				text: "Repost by #{if entityProfile then entityProfile.profile.name else (new uri(post.entity)).host()}"
				left: 5, height: 13, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
				color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x: 0, y: 1}

			if not entityProfile
				# try to grab the entity name
				api.discover post.entity
					onload: (profileData) ->
						repostedLabel.text = "Repost by #{profileData.profile.name}"

			postInfoView.add repostedLabel

		when api.postType('Follower')
			className = 'follower'
			postContent = 'You have a new follower'
			hasDetails = false

		when api.postType('Photo')
			className = 'photo';
			# replace inline mention for display text
			postContent = post.content.caption.replace(RE_mention, "$1")

			imageURL = api.attachment_URL
				entity: if isRepost then post.original_post.profile.entity else post.entity
				attachments: if isRepost then post.original_post.attachments else post.attachments

			postImage = Ti.UI.createImageView
				image: utils.cropImageURL(imageURL, 268)
				height: Ti.UI.SIZE
				top: 5, left: 0, right: 0

			isPhotoPost = true


	# check if we have the entity profile
	if repostEntityProfile
		fillEntityInfo( repostEntityProfile )
	# discover the profile if we dont know it yet
	else if entityProfile
		fillEntityInfo( entityProfile )
	else
		api.discover post.entity,
			onload: (profileData) ->
				fillEntityInfo( profileData )
			onerror: (resp) ->
				Ti.API.warn('Failed entity profile discovery. Retrying...')
				api.discover post.entity,
					onload: (profileData) ->
						fillEntityInfo( profileData )
					onerror: (resp) ->
						Ti.API.warn('Failed entity profile discovery. Giving up.')


	# should we style the label ?
	if viewProperties and viewProperties.autoLink
		StyledLabel = require('ti.styledlabel')
		postContentText = StyledLabel.createLabel _.extend
			html: post2HTML(post)
			height: Ti.UI.SIZE

		if isPhotoPost
			postContentText.applyProperties({top: 0, left: 0, right: 0})
		else
			postContentText.applyProperties({top: 68, bottom: 13, left: 14, right: 14})

		postContentText.addEventListener 'click', (e) ->
			if e.url
				# alert('You clicked ' + e.url);
				# Ti.API.debug('You clicked ' + e.url);
				entity = e.url.replace(/\/+$/, '')
				if entity == GLOBAL.entity
					GLOBAL.tabGroup.setActiveTab(2)
				else
					# make sure we have entity discover data in cache
					api.discover entity,
						thiz: this
						onload: (data) ->
							Profile = require('views/profile')
							GLOBAL.tabGroup.activeTab.open new Profile(entity, { backButton: 'back' })
						onerror: (e) ->
							alert('Error retrieving profile data.')

	else
		postContentText = ui.label
			properties: _.extend
				touchEnabled: false
				text: postContent
				textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#2F3238', font: { fontSize: 15 }
				height: Ti.UI.SIZE

		if isPhotoPost
			postContentText.applyProperties({top: 0, left: 0, right: 0})
		else
			postContentText.applyProperties({top: 68, bottom: 13, left: 14, right: 14})

	if isPhotoPost
		postContentText = ui.view
			properties:
				layout: 'vertical'
				top: 68, bottom: 13, left: 14, right: 14, height: Ti.UI.SIZE
			content: [ postContentText, postImage ]


	postContentView = ui.view
		properties: { touchEnabled: true, top: 0, left: 0, right: 0 }
		content: [
			# entity name
			entityName
			# entity uri
			entityURI
			# time 'ago'
			postTimestampLabel
		]

	wrapView.add postContentView

	if post.permissions and not post.permissions['public']
		className += 'Private'
		# add symbol and information about privacy
		postInfoView.add ui.label
			touchEnabled: false
			text: '$', left: if postInfoView.children.length then 3 else 69
			height: 11, top: 10, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
			color: '#5E6165', font: { fontSize: 10, fontFamily: 'Tentr-symbols' }

		postInfoView.add ui.label
			touchEnabled: false,
			text: 'Private', left: 2, height: Ti.UI.SIZE, top: 9
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165'
			font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x: 0, y: 1}

	rowContent = [ entityAvatar, wrapView, postContentText ]

	if postInfoView.children.length
		# postContentView.bottom = 30; // give some bottom space
		postContentText.applyProperties
			bottom: if viewProperties and viewProperties.autoLink then 29 else 30
			height: Ti.UI.SIZE
		rowContent.push(postInfoView)
		entityAvatar.bottom = 42
	else
		entityAvatar.bottom = if isBottom then 18 else 13


	if hasLocation
		entityLocation = Map.createAnnotation
			latitude: post.content.location.latitude
			longitude: post.content.location.longitude
			title: _entityURI.host()
			# subtitle:'Mountain View, CA',
			pincolor: Map.ANNOTATION_RED
			animate: true
			myid:1 # Custom property to uniquely identify this annotation.

		Ti.API.debug "postInfoView.children.length -> #{postInfoView.children.length}"
		postInfoView_padding = if postInfoView.children.length then 13 else 0
		mapView = ui.view
			properties:
				left: 14, right: 14, height: 60
				bottom: (if postContent and viewProperties and viewProperties.autoLink then 15 else 16) + postInfoView_padding
				location:
					latitude: post.content.location.latitude
					longitude: post.content.location.longitude
					title: _entityURI.host()
			content: [
				Map.createView
					left: 0, right: 0, height: 60, touchEnabled: false
					mapType: Map.STANDARD_TYPE
					region:
						latitude: post.content.location.latitude
						longitude: post.content.location.longitude
						latitudeDelta: 1
						longitudeDelta: 1
					animate: true
					regionFit: true
					userLocation: false
					annotations: [entityLocation]
			]
			events: [['click', (e) ->
				location = e.source.location
				Ti.API.debug "Location: #{JSON.stringify(location)}"
				entityLocation = Map.createAnnotation
					latitude: location.latitude
					longitude: location.longitude
					title: location.title
					# subtitle:'Mountain View, CA'
					pincolor:Map.ANNOTATION_RED
					animate:true
					myid:1 # Custom property to uniquely identify this annotation.

				mapViewWin = ui.win
					config: 'windowProperties'
					properties:
						leftNavButton: ui.button
							config: 'backButton'
							events: [['click', (e) -> GLOBAL.tabGroup.activeTab.close(mapViewWin, {animated: true}) ]]
					content: [Map.createView
						left: 0, right: 0, top: 0, bottom: 0
						mapType: Map.STANDARD_TYPE
						region:
							latitude: location.latitude
							longitude: location.longitude
							latitudeDelta: 0.01
							longitudeDelta: 0.01
						animate: true
						regionFit: true
						userLocation: false
						annotations: [entityLocation]
					]

				GLOBAL.tabGroup.activeTab.open(mapViewWin)
			]]

		rowContent.push(mapView);
		postContentText.bottom = postContentText.bottom + 68


	# hack to set the height to acomodate content
	postContentView.height = Ti.UI.SIZE
	wrapView.height = Ti.UI.SIZE

	rowProperties =
		className: className
		postData: post
		height: Ti.UI.SIZE
		hasDetails: hasDetails

	_.extend(rowProperties, viewProperties) if viewProperties

	row = ui.row
		config: rowConfig
		properties: rowProperties
		content: rowContent

	row

getInlineMentions = (postText) ->
	re = /\^\[(.+)\]\((\d+)\)/ig
	# TODO: make sure this works and replace the old code
	# [{url: match[1], index: match[2]} while match = re.exec(postText)]
	mentions = []
	while match = re.exec(postText)
		mentions.push {url: match[1], index: match[2]}
	mentions

getInlineLinks = (postText) ->
	re = /\[(.+)\]\((\d+)\)/ig
	[{url: match[1], index: match[2]} while match = re.exec(postText)]

post2HTML = (postData) ->
	postText = postData.content.text or postData.content.caption
	postMentions = postData.mentions
	regex2anchor = (foo, p1, p2) -> "<a href='#{postMentions[parseInt(p2,10)].entity}'>#{p1}</a>"
	postText.replace /\^\[([^\[\]]+)\]\((\d+)\)/ig, regex2anchor
	# linkify urls
	# postText.replace ///
	# 	\[(.+)\] 				# url name
	# 	\(
	# 	(
	# 	(ht|f)tp(s)?:// # protocol
	# 	[\d\w\.\-/]+	# url
	# 	)
	# 	\)
	# 	///ig, (foo, p1, p2) ->
	# 	"<a href='#{p2}'>#{p1}</a>"

createWindowTitle = (title) ->
	Ti.UI.createLabel
		color: '#2e89c4'
		width: 210
		height: 18
		top: 10
		text: title
		textAlign: 'center'
		font:
			fontFamily: 'HelveticaNeue-Medium'
			fontSize: 18
		shadowColor: '#fff'
		shadowOffset: {x:0,y:1}


#
# Override a tab group's tab bar on iOS.
#
# NOTE: Call this function on a tabGroup AFTER you have added all of your tabs to it! We'll look at the tabs that exist
# to generate the overriding tab bar view. If your tabs change, call this function again and we'll update the display.
#
# @param tabGroup The tab group to override
# @param backgroundOptions The options for the background view; use properties like backgroundColor, or backgroundImage.
# @param selectedOptions The options for a selected tab button.
# @param deselectedOptions The options for a deselected tab button.
#
exports.overrideTabs = (tabGroup, backgroundOptions, selectedOptions, deselectedOptions) ->
	# a bunch of our options need to default to 0 for everything to position correctly; we'll do it en mass:
	backgroundOptions = _.extend({bottom: 0, height: 49, width: Ti.UI.FILL}, backgroundOptions)
	selectedOptions = _.extend({bottom: 0, height: 49}, selectedOptions)

	# create the overriding tab bar using the passed in background options
	# backgroundOptions.height = backgroundOptions.height || 49;
	background = Ti.UI.createView backgroundOptions

	# pass all touch events through to the tabs beneath our background
	background.touchEnabled = false

	# create our individual tab buttons
	increment = if GLOBAL.isTablet then 76 else 100 / tabGroup.tabs.length
	selectedOptions.width = if GLOBAL.isTablet then increment else increment + '%'

	buttonWrap = Ti.UI.createView {left: (Ti.Platform.displayCaps.platformWidth-296) /2}
	background.add buttonWrap

	for tab, i in tabGroup.tabs
	# for (var i = 0, l = tabGroup.tabs.length; i < l; i++) {
		# position our views over the tab.
		selectedOptions.left = if GLOBAL.isTablet then (increment*i) + (34*i) else increment * i + '%'

		# customize the selected and deselected based on properties in the tab.
		selectedOptions.title = tab.title
		if tab.backgroundImage
			selectedOptions.image = tab.backgroundImage;

		tab.ct = Ti.UI.createImageView selectedOptions

		if GLOBAL.isTablet
			buttonWrap.add(tab.ct)
		else
			background.add(tab.ct)

	tabGroup.selectTab = (tab) ->
		tab.ct.image = tab.selectedBackgroundImage
		return
	tabGroup.unselectTab = (tab) ->
		tab.ct.image = tab.backgroundImage
		return
	tabGroup.hasNotification = (tab) ->
		tab.ct.image = tab.backgroundImage2
		return

	# update the tab group, removing any old overrides
	if tabGroup.overrideTabs
		tabGroup.remove(tabGroup.overrideTabs)
	else
		tabGroup.addEventListener('focus', overrideFocusTab)

	tabGroup.add background
	tabGroup.overrideTabs = background

	Titanium.Gesture.addEventListener 'orientationchange', ->
		buttonWrap.left = (Ti.Platform.displayCaps.platformWidth-296) /2
		return

	tabGroup


overrideFocusTab = (e) ->
	Ti.API.debug('tab focus - overrideFocusTab called');
	e.source.unselectTab(e.source.tabs[e.previousIndex]) if e.previousIndex >= 0
	e.source.selectTab e.tab

exports.createAvatar = (image, properties, borderWidth) ->
	ui.view
		properties: _.extend({backgroundImage: 'images/avatar_bg.png', width: 46, height: 46}, properties)
		content: [
			ui.image
				properties:
					image: image
					borderRadius: parseInt( (if borderWidth then borderWidth/2 else 3), 10)
					top: parseInt( (if borderWidth then borderWidth/2 else 4), 10)
					width: properties.width- (borderWidth or 9)
					height: properties.height- (borderWidth or 9)
		]

exports.createTableSection = (properties, data) ->
	section = Ti.UI.createTableViewSection properties

	data_length = data.length

	if data_length == 1
		_data = data[0]
		section.add ui.row
			config: _data.config+'_full'
			properties: _data.properties
			content: [
				ui.view
					config: 'rowContainerView'
					properties: {height: 40, top: 1}
					content: _data.content
			]
			events: _data.events

	else
		for cur_data, i in data
			if i == 0
				config = cur_data.config+'_top'
				properties = {height: 40, bottom: 0}
			else if i == data_length-1
				config = cur_data.config+'_bottom'
				properties = {height: 40, top: 0}
			else
				config = cur_data.config
				properties = {height: 40, top: 0}

			section.add ui.row
				config: config
				properties: cur_data.properties
				content: [
					ui.view
						config: 'rowContainerView'
						properties: properties
						content: cur_data.content
				]
				events: cur_data.events

	section

exports.createPostQuickMenu = (eventData) ->

	NewPost = require('views/newPost2')
	favoritePosts = require('lib/favoritePosts')

	rowIndex = eventData.index
	postData = eventData.rowData.postData
	postType = postData.type.split('#')[0]
	myEntity = GLOBAL.entity

	replyAll = false
	myPost = false
	isRepost = (postType == api.postType('Repost'))
	isFavorite = favoritePosts.isFavorite(postData.id)

	# no one was yet mentioned or only I was mentioned = no option to reply everyone
	if postData.mentions and postData.mentions.length < 2
		replyAll = true if postData.mentions.length and postData.mentions[0].entity != myEntity
	# entities found in mentions = show option to reply everyone
	else
		replyAll = true

	myPost = true if postData.entity == myEntity


	wrapperWin = ui.win
		properties: {backgroundColor: 'rgba(0,0,0, 0.3)'}
		events: [
			['click', (e) -> closeMenu() ]
			['open', (e) -> menuWrap.animate(slide_menu_in) ]
		]


	menuWrap = ui.view
		properties:
			backgroundImage: if GLOBAL.isTablet then 'images/postMenu_bg_ipad.png' else 'images/postMenu_bg.png'
			backgroundLeftCap: if GLOBAL.isTablet then 6 else 0
			backgroundTopCap: if GLOBAL.isTablet then 6 else 0
			backgroundRepeat: if GLOBAL.isTablet then false else true
			height: if GLOBAL.isTablet then 246 else 240 # shadow is 6px high
			bottom: if GLOBAL.isTablet then -246 else -240
			width: if GLOBAL.isTablet then 320 else Ti.UI.FILL
		content: [
			# Ti.UI.createView({backgroundImage: 'images/postMenu_dropshadow.png', height: 12, width: Ti.UI.FILL, top: 0}),
			ui.view
				properties:
					height: 234
					bottom: if GLOBAL.isTablet then 6 else 0
					bubbleParent: false
				content: [
					# buttons div
					Ti.UI.createView({backgroundImage: 'images/postMenu_div.png', height: 150, width: 150, top: 46})

					ui.button
						properties: {top: 23, left: 46, backgroundImage: 'images/postMenu_reply.png', width: 90, height: 70}
						events: [['click', (e) ->
							(new NewPost({ originalPost: postData, reply: true })).open({modal: true})
							closeMenu()
						]]

					ui.button
						properties: {enabled: replyAll, top: 23, right: 46, backgroundImage: 'images/postMenu_replyall.png', width: 90, height: 70},
						events: [['click', (e) ->
							(new NewPost({originalPost: postData, reply: true, replyAll: true})).open({modal: true})
							closeMenu()
						]]

					(->
						if myPost
							return ui.button
								properties: {bottom: 23, left: 46, backgroundImage: 'images/postMenu_delete.png', width: 90, height: 70},
								events: [['click', (e) ->
									# only delete with user confirmation
									ui.alert(
										properties:
											cancel: 1
											buttonNames: [L('yes', 'Yes'), L('no', 'No')]
											message: L('delete_the_post','Delete the post')+'?'
											title: L('confirm','Confirm')
										events: [
											['click', (e) ->
												if e.index != e.cancel
													ui.showIndicator()
													api.posts_DELETE
														id: postData.id
														eventData: { rowIndex: rowIndex }
											]
										]
									).show()
									closeMenu()
								]]
						else
							return ui.button
								properties: {bottom: 23, left: 46, backgroundImage: 'images/postMenu_repost.png', width: 90, height: 70}
								events: [['click', (e) ->
									# only repost with user confirmation
									ui.alert(
										properties:
											cancel: 1
											buttonNames: [L('yes', 'Yes'), L('no', 'No')]
											message: L('repost','Repost')+' '+(new uri(if isRepost then postData.original_post.post_content.entity else postData.entity)).host() +'?'
											title: L('confirm','Confirm')
										events: [
											['click', (e) ->
												if e.index != e.cancel
													ui.showIndicator()
													api.posts_POST
														"permissions": {"public":true}
														"type": api.postType('Repost')
														"content":
															"entity": if isRepost then postData.original_post.post_content.entity else postData.entity
															"id": if isRepost then postData.original_post.post_content.id else postData.id
														"mentions":[
															"entity": if isRepost then postData.original_post.post_content.entity else postData.entity
															"post": if isRepost then postData.original_post.post_content.id else postData.id
														]
											]
										]
									).show()
									closeMenu()
								]]
					)()

					ui.button
						properties:
							backgroundImage: if isFavorite then 'images/postMenu_unfav.png' else 'images/postMenu_fav.png'
							bottom: 23
							right: 46
							width: 90
							height: 70
						events: [['click', (e) ->
							if isFavorite
								favoritePosts.removeFavorite postData.id
								e.source.backgroundImage = 'images/postMenu_unfav.png'
							else
								favoritePosts.addFavorite postData
								e.source.backgroundImage = 'images/postMenu_fav.png'
							closeMenu()
						]]
				]
		]

	slide_menu_in =  Ti.UI.createAnimation
		bottom: if GLOBAL.isTablet then (Ti.Platform.displayCaps.platformHeught / 2) - 123 else 0
		duration: 200
	slide_menu_out =  Ti.UI.createAnimation
		bottom: -246
		duration: 200

	cancelButton = ui.button
		properties:
			title: 'Cancel'
			width: 280
			height: 44
			bottom: 20
		events: [
			['click', (e) ->
				setTimeout(->
					wrapperWin.close()
				, 205)
				menuWrap.animate(slide_menu_out)
			]
		]

	# menu.add(cancelButton)
	wrapperWin.add(menuWrap)

	closeMenu = ->
		setTimeout(->
			wrapperWin.close()
		, 205)
		menuWrap.animate(slide_menu_out)

	return wrapperWin


exports.cropImage = (original, w, h) ->
	# Here's our base (full-size) image.
	# It's never displayed as-is.
	baseImage = Ti.UI.createImageView
		image: original
		width: w
		# height: 'auto'

	# Here's the view we'll use to do the cropping.
	# It's never displayed either.
	cropView = Ti.UI.createView
		width: w
		height: h

	# Add the image to the crop-view.
	# Position it left and above origin.
	cropView.add baseImage
	baseImage.left= -(baseImage.size.width/2 - cropView.width/2)
	baseImage.top= -(baseImage.size.height/2 - cropView.height/2)

	# now convert the crop-view to an image Blob and return an imageView
	Ti.UI.createImageView
		image: cropView.toImage()
		width: w
		height: h


exports.createRowFromPost = createRowFromPost
exports.createRowDetailsFromPost = createRowDetailsFromPost
exports.createWindowTitle = createWindowTitle
