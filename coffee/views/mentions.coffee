moment = require 'lib/vendor/moment.min'
StreamView = require 'views/streamView'

# change moment lang to a shorter format
moment.lang 'timeline'

self = new StreamView
	title: 'Mentions'
	api_GET: 'mentions_GET'
	api_setLastKnown: 'setLastKnownMention'
	api_posts_OLD: 'mentions_OLD'
	api_events: {GET: 'mentions_GET', POST: 'mentions_POST', DELETE: 'mentions_DELETE'}
	appProperties_cache: 'myMentions_cache'
	bgCheck: 'mentions_timeout'
	notificationEvent: 'notify:newMentions'
	noEvents: GLOBAL.Mentions_postsBgJobs || false
	clearNotificationCenterOnFocus: true

module.exports = self