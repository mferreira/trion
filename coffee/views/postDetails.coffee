ui = require 'lib/ui_sugar'
api = require 'lib/tent.api2'
URI = require 'lib/vendor/jsuri.min'
_f = require 'views/func_repo'
moment = require 'lib/vendor/moment.min'
favoritePosts = require 'lib/favoritePosts'
NewPost = require 'views/newPost2'


generatorReplyRows = (postData, tableView, replySection, replyRows, rowCount, windowClosed) ->
	return if windowClosed

	rowCount = rowCount or 0
	hasReply = false

	entity_post_GET_onload  = (post) ->
		Ti.API.debug('Entity post seems to exist. Updating table content.')
		row = new _f.createRowFromPost(post, {})
		# row.addEventListener('postlayout', function(e){
		# 	Ti.API.debug('row postlayout: '+JSON.stringify(e));
		# 	if (e.source.rect.height<75) e.source.height = 75;
		# });
		if not rowCount
			row.applyProperties({ backgroundImage: 'images/row_bg_full.png', backgroundTopCap: 8, backgroundLeftCap: 20 })
		else if rowCount == 1
			replyRows[0].applyProperties({ backgroundImage: 'images/row_bg_top.png', backgroundTopCap: 8, backgroundLeftCap: 20 })

		tableView.appendRow(replyRows[replyRows.length-1]) if replyRows.length # add previous generated row
		rowCount += 1
		replyRows.push(row)
		generatorReplyRows( post, tableView, replySection, replyRows, rowCount )

	entity_post_GET_onerror = (resp) ->
		post_text = 'Error grabbing post.'
		if resp.status == 404
			Ti.API.warn('Entity post does not exist anymore (404).')
			post_text = 'Post no longer exists.'

	if postData.mentions
		for reply in postData.mentions
			if reply.post
				Ti.API.debug "generatorReplyRows rowCount: #{rowCount}"
				hasReply = true
				if not replySection # create the new section to hold the replied posts
					replySection = ui.section
						properties:
							headerView: Ti.UI.createView {height:1}
							footerView: Ti.UI.createView {height:5}
					tableView.appendSection(replySection)

				api.entity_post_GET
					entity: reply.entity or (if postData.original_post then postData.original_post.post_content.entity else postData.entity)
					post_id: reply.post
					callback:
						onload: entity_post_GET_onload
						onerror: entity_post_GET_onerror
				break

	if replyRows.length and not hasReply
		lastRow = replyRows[replyRows.length-1]
		lastRow.applyProperties
			backgroundImage: if replyRows.length == 1 then 'images/row_bg_full.png' else 'images/row_bg_bottom.png'
			backgroundTopCap: 8
			backgroundLeftCap: 20
		tableView.appendRow(lastRow) # add previous generated row


createView = (eventData, viewOptions) ->

	Social = require('dk.napp.social')

	isActivityViewSupported = Social.isActivityViewSupported()
	rowIndex = eventData.index
	postData = eventData.rowData.postData
	hasReplyAll = if postData.mentions then postData.mentions.length > 1 or (postData.mentions.length == 1 and postData.mentions[0].entity != GLOBAL.entity) else false
	replySection = null
	replyRows = []
	windowClosed = false
	isRepost = (postData.type == api.postType('Repost'))
	isPublic = postData.permissions and postData.permissions['public']

	postActionsButtons = [
		# reply
		ui.button
			properties: {backgroundImage: 'images/post_reply_icon.png', left: 0, width: 40, height: 40}
			events: [['click', (e) ->
				(new NewPost({ originalPost: postData, reply: true })).open({modal: true})
			]]

		# reply all
		ui.button
			properties:
				enabled: hasReplyAll
				backgroundImage: 'images/post_replyall_icon.png'
				left: if isActivityViewSupported then 15 else 19
				width: 40
				height: 40
			events: [['click', (e) ->
				(new NewPost(
					originalPost: postData
					reply: true
					replyAll: true
				)).open( {modal: true} )
			]]

		# favorite
		ui.button
			properties:
				backgroundImage: if favoritePosts.isFavorite(postData.id) then 'images/post_fav_active_icon.png' else 'images/post_fav_icon.png'
				left: if isActivityViewSupported then 15 else 19
				width: 40
				height: 40
			events: [['click', (e) ->
				if favoritePosts.isFavorite(postData.id)
					favoritePosts.removeFavorite( postData.id )
					e.source.backgroundImage = 'images/post_fav_icon.png'
				else
					favoritePosts.addFavorite( postData )
					e.source.backgroundImage = 'images/post_fav_active_icon.png'
				# else { api.favorites_POST( {post: postData} ); e.source.backgroundImage = 'images/post_fav_active_icon.png'; }
			]]

		(->
			if postData.entity == GLOBAL.entity
				# delete
				return ui.button
					properties:
						backgroundImage: 'images/post_delete_icon.png'
						left: if isActivityViewSupported then 15 else 19
						width: 40
						height: 40
					events: [['click', (e) ->
						# only delete with user confirmation
						ui.alert(
							properties:
								cancel: 1
								buttonNames: [L('yes', 'Yes'), L('no', 'No')]
								message: L('delete_the_post','Delete the post')+'?'
								title: L('confirm','Confirm')
							events: [
								['click', (e) ->
									if e.index != e.cancel
										ui.showIndicator()
										api.posts_DELETE
											id: postData.id
											eventData: { rowIndex: rowIndex }
										self.close()
								]
							]
						).show()
					]]
			else
				# repost
				return ui.button
					properties:
						backgroundImage: 'images/post_repost_icon.png'
						left: if isActivityViewSupported then 15 else 19
						width: 40
						height: 40
					events: [['click', (e) ->
						ui.alert(
							properties:
								cancel: 1
								buttonNames: [L('yes', 'Yes'), L('no', 'No')]
								message: L('repost','Repost')+' '+(new URI(if isRepost then postData.original_post.post_content.entity else postData.entity)).host() +'?'
								title: L('confirm','Confirm')
							events: [
								['click', (e) ->
									if e.index != e.cancel
										ui.showIndicator()
										api.posts_POST
											"permissions": {"public": true}
											"type": api.postType('Repost')
											"content":
												"entity": if isRepost then postData.original_post.post_content.entity else postData.entity
												"id": if isRepost then postData.original_post.post_content.id else postData.id
											"mentions": [
												"entity": if isRepost then postData.original_post.post_content.entity else postData.entity
												"post": if isRepost then postData.original_post.post_content.id else postData.id
											]
								]
							]
						).show()
					]]
		)()
	]

	Ti.API.debug "isActivityViewSupported: #{isActivityViewSupported}"
	if isActivityViewSupported
		# more - activity view
		postActionsButtons.push ui.button
			properties:
				backgroundImage: 'images/post_more_icon.png'
				left: 15
				width: 40
				height: 40
			events: [['click', (e) ->
				Social.activityView
					text: if isRepost then postData.original_post.post_content.content.text else postData.content.text
					# image:"pin.png",
					removeIcons: "print,contact,camera"
			]]

	self = ui.win
		config: 'windowProperties'
		properties:
			title:'Post'
			rightNavButton: ui.button
				config: 'newPostButton'
				events: [
					['click', (e) ->
						e.source.enabled = false
						(new NewPost(null, null, {button: e.source})).open({modal: true})
					]
				]
			leftNavButton: (->
				if viewOptions and viewOptions.backButton
					return ui.button
						config: 'backButton'
						events: [['click', (e) ->
							try
								GLOBAL.tabGroup.activeTab.close(self, {animated: true})
								windowClosed = true
							catch er
								Ti.API.error(er)
						]]
			)()
		events: [
			['open', (e) ->
				generatorReplyRows(postData, tableView, replySection, replyRows, windowClosed)
			],
			['close', (e) ->
				postActionsButtons.length = replyRows.length = 0
				tableView.setData();
				thisPostRow = thisPostActions = thisPostSection = null
			]
		]

	thisPostRow = new _f.createRowDetailsFromPost postData,
		backgroundImage: 'images/row_bg_top.png'
		backgroundTopCap: 8
		backgroundLeftCap: 20
		autoLink: true

	thisPostActions = ui.row
		config: 'postActions'
		content: [ui.view
			properties:
				left: 2
				right: 2
				height: 39
				top: 0
				backgroundImage: 'images/post_bottom_bg.png'
				backgroundTopCap: 8
				backgroundLeftCap: 20
			content: [ui.view
				properties:
					width: if isActivityViewSupported then 260 else 220
					height: 39
					top: 0
					layout: 'horizontal'
				content: postActionsButtons
			]
		]

	tableView_clicking = false

	tableView = ui.table
		config: 'tableView'
		events: [
			['longpress', (e) ->
				if e.index >= 2
					menu = _f.createPostQuickMenu(e)
					menu.open()
			],
			['singletap', (e) ->
				Ti.API.debug('Row click event fired.')
				if e.index >= 2
					rowIndex = e.index
					postData = e.rowData.postData
					myEntity = GLOBAL.entity
					if not tableView_clicking
						tableView_clicking = true
						# Ti.API.debug('Row post data: '+JSON.stringify(postData));
						PostDetails = require('views/postDetails')
						GLOBAL.tabGroup.activeTab.open( new PostDetails(e, { backButton: self.title }) )
						setTimeout(
							-> tableView_clicking = false
						, 1000)
			]
		]

	thisPostSection = ui.section
		properties:
			headerView: Ti.UI.createView({height:9})
			footerView: Ti.UI.createView({height:5})
		content: [ thisPostRow, thisPostActions ]

	tableView.setData([thisPostSection])

	self.add(tableView)

	self


module.exports = createView;
