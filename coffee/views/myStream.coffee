moment = require 'lib/vendor/moment.min'
StreamView = require 'views/streamView'

# change moment lang to a shorter format
moment.lang 'timeline'

self = new StreamView
	title: 'Stream'
	api_GET: 'stream_posts_GET'
	api_setLastKnown: 'setLastKnownPost'
	api_posts_OLD: 'posts_OLD'
	api_events: {GET: 'posts_GET', POST: 'posts_POST', DELETE: 'posts_DELETE'}
	appProperties_cache: 'myStream_cache'
	bgCheck: 'posts_timeout'
	notificationEvent: 'notify:newPosts'
	noEvents: GLOBAL.Stream_postsBgJobs or false

module.exports = self;