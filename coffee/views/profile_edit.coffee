ui = require 'lib/ui_sugar'
api = require 'lib/tent.api2'
URI = require 'lib/vendor/jsuri.min'
_f = require 'views/func_repo'
NewPost = require 'views/newPost2'

module.exports = class ProfileEdit
	constructor: (@profile, @viewOptions) ->
		if typeof @profile == 'string'
			@profile = api.getCache('discover')[@profile]

		@self = ui.win
			config: 'windowProperties'
			properties:
				title: 'Edit Profile'
				rightNavButton: ui.button
					config: 'newPostButton'
					events: [['click', ->
						e.source.enabled = false
						(new NewPost(null. null, {button: e.source})).open modal: true
					]]
				leftNavButton: (=>
					if @viewOptions and @viewOptions.backButton
						ui.button
							config: 'backButton'
							properties:
								backgroundImage: 'images/back_profile.png', width: 64
							events: [['click', (e) =>
								saveProfile 'basic'
								tableView.setData()
								GLOBAL.tabGroup.activeTab.close(@self, animated: true)
							]]
				)()
				layout: 'vertical'

		profileHeader = ui.view
			properties:
				width: Ti.UI.FILL
				height: 66
				backgroundImage: 'images/profile_top_bg.png'
			content: [
				# @profile avatar
				_f.createAvatar (@profile.avatar or "images/avatar.png"),
					width: 48
					height: 48
					left: 10

				# @profile real name
				ui.label
					config: 'defaultLabel'
					properties: { left: 66, top: 14, text: @profile.name, font: { fontSize: 16, fontWeight:'bold' }, color: '#fff', shadowColor: '#000', shadowOffset: {x:0,y:1} }

				# @profile entity name
				ui.label
					config: 'defaultLabel',
					properties: { left: 66, top: 34, text: (new URI(@profile.entity)).host(), font: { fontSize: 14}, color: '#d4d5d7', shadowColor: '#000', shadowOffset: {x:0,y:1} }
			]

		# self.add( profileHeader );

		tableView = ui.table(config: 'tableView')

		nameEntry = ui.textField
			config: 'profileTextField'
			properties: {hintText: 'Type Your Name', returnKeyType: Ti.UI.RETURNKEY_DONE, value: @profile.name}
			# events: [['return', (e) -> saveProfile 'basic']]

		urlEntry = ui.textField
			config: 'profileTextField'
			properties:
				hintText: 'URL'
				value: @profile.website
				autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
				autocorrect: false
				returnKeyType: Ti.UI.RETURNKEY_DONE
				keyboardType: Ti.UI.KEYBOARD_URL
			# events: [['return', (e) -> saveProfile 'basic']]

		birthdateEntry = ui.textField
			config: 'profileTextField'
			properties: {hintText: 'Not set', returnKeyType: Ti.UI.RETURNKEY_DONE, value: @profile.birthdate}
			# events: [['return', (e) -> saveProfile 'basic']]

		genderEntry = ui.textField
			config: 'profileTextField'
			properties: {hintText: 'Not set', returnKeyType: Ti.UI.RETURNKEY_DONE, value: @profile.gender}
			# events: [['return', (e) -> saveProfile 'basic']]

		locationEntry = ui.textField
			config: 'profileTextField'
			properties: {hintText: 'Your Location', returnKeyType: Ti.UI.RETURNKEY_DONE, value: @profile.location}
			# events: [['return', (e) -> saveProfile 'basic']]

		bioEntry = ui.textArea
			properties:
				top: 4, bottom: 4, left: 5, right: 5
				value: @profile.bio
				returnKeyType: Ti.UI.RETURNKEY_DONE
				autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES
				color: '#45474c'
				font:
					fontSize:13,
					fontFamily:'Helvetica'
			# events: [['return', (e) -> saveProfile 'basic']]


		tableData = [
			# _f.createTableSection({
			# 	headerView: Ti.UI.createView({height:9}),
			# 	footerView: Ti.UI.createView({height:5})
			# }, [
			# 	{
			# 		config: 'rowWithChild',
			# 		content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Upload Photo'} }) ]
			# 	}
			# ]),

			_f.createTableSection
				headerView: Ti.UI.createView (height:7)
				footerView: Ti.UI.createView (height:2)
			, [
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Name'} }
						nameEntry
					]
					events: [['click', (e) -> nameEntry.focus() ]]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Gender'} }
						genderEntry
					]
					events: [['click', (e) -> genderEntry.focus() ]]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Birthdate'} }
						birthdateEntry
					]
					events: [['click', (e) -> birthdateEntry.focus() ]]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'URL'} }
						urlEntry
					]
					events: [['click', (e) -> urlEntry.focus() ]]
				,
					config: 'profileEditableRow'
					content: [
						ui.label {config: 'defaultRowLabel', properties: {text: 'Location'} }
						locationEntry
					]
					events: [['click', (e) -> locationEntry.focus() ]]
			]
		,
			ui.section
				properties:
					headerView: Ti.UI.createView (height:3)
					footerView: Ti.UI.createView (height:5)
				content: [ui.row
					properties:
						height: 104
						backgroundImage: 'images/row_bg_full.png', backgroundTopCap: 8, backgroundLeftCap: 20
						selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
					content: [ bioEntry ]
					events: [['click', (e) -> nameEntry.focus() ]]
				]
		]

		tableView.setData tableData
		@self.add tableView

		saveProfile = (type) =>
			api.profile_PUT
				type: type
				payload:
					name: nameEntry.value
					avatar: @profile.avatar
					birthdate: birthdateEntry.value
					location: locationEntry.value
					gender: genderEntry.value
					bio: bioEntry.value
					website: urlEntry.value

	getView: ->
		@self
