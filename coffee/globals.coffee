settings = (if Ti.App.Properties.hasProperty("settings") then JSON.parse(Ti.App.Properties.getString("settings")) else {})

globals =
    app_firstRun: Ti.App.Properties.getBool("app_firstRun", true)
    entity: Ti.App.Properties.getString("entity", null)

    appConfig: # TODO: put this in a config screen
        bgCheck:
            posts_timeout: ((if settings and settings.updateFreq then settings.updateFreq.minutes_stream else 1.7)) * 60000
            mentions_timeout: ((if settings and settings.updateFreq then settings.updateFreq.minutes_mentions else 1.2)) * 60000

    newPostData: null
    dontProccessNewPosts: false

    appName: "#{Ti.App.name} on "+ (->
        return "Simulator"  if Ti.Platform.model is "Simulator"
        osname = Ti.Platform.osname
        ( if osname.charAt(0) != 'i' then osname.charAt(0).toUpperCase() + osname.slice(1) else osname.charAt(0) + osname.charAt(1).toUpperCase() + osname.slice(2) )
    )(),
    isTablet: Ti.Platform.osname is 'ipad' or (Ti.Platform.displayCaps.platformWidth > 899 or Ti.Platform.displayCaps.platformHeight > 899)

    # get keyboard height depending on the device - iphone or ipad
    keyboardHeight: ->
        if @isTablet
            (if (Ti.Platform.displayCaps.platformWidth is 1024) then 352 else 264)
        else
            (if (Ti.Platform.displayCaps.platformWidth is 320) then 216 else 162)

    timestampCache: []
    cur_tab: null

    appArguments: null

module.exports = globals