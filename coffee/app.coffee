Ti.API.info "Starting app on #{Ti.Platform.model} ..."

require('ti.newrelic').start "AA9d9d7fc562863a8b01db43125a379fb338e81506"
GLOBAL = require 'globals'

# global new "setTimeout" function
delay = (ms, func) ->
	setTimeout(func, ms)

require 'main'