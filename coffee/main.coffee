testflight = require 'lib/testflight'
api = require 'lib/tent.api2'

Ti.App.Properties.setBool "isResumed", false
Ti.App.Properties.setBool "isPaused", false

# this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor '#000'

GLOBAL.appArguments = Ti.App.getArguments()

Ti.API.info 'App name is: '+GLOBAL.appName
Ti.API.info 'App arguments: '+JSON.stringify(GLOBAL.appArguments)
Ti.API.info 'App launchURL: '+GLOBAL.appArguments.url

openAuthScreen = ->
	auth = require 'views/authorize'
	auth.open()

openMainScreen = ->
	Ti.API.debug "Opening main screen ..."
	if Ti.Platform.model isnt "Simulator"
		apn = require 'lib/apn'
		# start APN
		apn.registerForPushNotifications()
	try
		api.checkNetwork
			this: @
			online: ->
				updateFollows = (callback) ->
					if api.isAuthenticated() and Ti.Network.online
						Ti.API.info 'Updating followings...'
						api.followings_GET
							onload: ->
								Ti.API.info 'Updating followers...'
								api.followers_GET
									onload: -> callback()
									onerror: ->
										Ti.API.warn 'Searching followings - Server having problems';
										callback()
							onerror: ->
								Ti.API.warn 'Searching followers - Server having problems'
								callback()
				openMain = ->
					# updateFollows ->
					main = require 'views/main'
					main.addTabGroupEvents()
					mainWin = main.getView()
					mainWin.open()

				api.discover GLOBAL.entity,
					onload: openMain,
					onerror: openMain
				, true

			offline: ->
				main = require 'views/main'
				main.addTabGroupEvents()
				mainWin = main.getView()
				mainWin.open()
	catch er
		Ti.API.error er

checkAppAuth = (callback) ->
	api.apps_GET
		callback: (result) ->
			if result == 'success'
				callback.ok()
			else if result == 'reauth'
				Ti.API.warn 'Server accepted this app, but a new auth is needed to change some information.'
				GLOBAL.reAuthApp = true
				callback.nok()
				# alert 'Our apologies, but due to some internal changes, you need to re-authorize this app.'
			else if result == 'error'
				Ti.API.warn 'Server responded with an error. Rechecking app authorization...'
				api.apps_GET
					callback: (result) ->
						if result == 'success'
							callback.ok()
						else if result == 'error'
							Ti.API.warn 'Server responded with an error. Continuing to cached data.'
							callback.ok()
						else
							Ti.API.warn 'Server did not accept this app. Opening auth screen.'
							callback.nok()
			else
				Ti.API.warn 'Server did not accept this app. Opening auth screen.'
				callback.nok()

stopAuthChecker = -> clearInterval(authChecker)

if GLOBAL.app_firstRun or !api.isAuthenticated()
	Ti.API.info 'App is running for the first time or not authenticated.'
	Ti.App.Properties.setBool 'app_firstRun', false
	openAuthScreen()
else
	api.checkNetwork
		online: ->
			checkAppAuth
				ok: -> openMainScreen()
				nok: -> openAuthScreen()
		offline: ->
			openMainScreen()
			delay 1000, -> alert 'Looks like you are offline.'
			# check app auth when device comes back online
			authChecker = setInterval ->
				api.checkNetwork
					online: ->
						stopAuthChecker()
						checkAppAuth
							ok: ->
							nok: ->
								auth = require 'views/authorize'
								auth.open transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
				, 2000

Ti.App.addEventListener 'resumed', ->
	if Ti.App.Properties.getBool "isPaused", false
		Ti.App.Properties.setBool "isResumed", true
	else
		Ti.App.Properties.setBool "isResumed", false

	if GLOBAL.proccessingData
		GLOBAL.proccessingData = false

	Ti.API.info "App resuming with arguments: #{JSON.stringify Ti.App.getArguments()}"
	if Ti.App.getArguments().url
		URI = require 'lib/vendor/jsuri.min'
		NewPost = require 'views/newPost2'
		uri =  new URI Ti.App.getArguments().url

		if uri.protocol() == 'tentstatus'
			(new NewPost
				prefill: content: text: decodeURIComponent uri.host()
			).open modal: true

		# authenticating the app with the service
		else if uri.host() == 'tentauth'
			if uri.query().getParamValue 'error'
				Ti.UI.createAlertDialog(
					# message: "Error: #{uri.query().getParamValue 'error'}\nDescription: #{uri.query().getParamValue 'error_description'}"
					message: uri.query().split('&').join('\n')
					title: 'Auth Failed'
				).show()
				Ti.App.fireEvent 'api:grabToken:failed'

			else if uri.query().getParamValue('state') is api.tempCache.oauthState
				Ti.API.error "Auth 'state' does not match internal value: #{api.tempCache.oauthState}"
				Ti.App.fireEvent 'api:grabToken:failed'

			else
				api.cacheData.secret_code = uri.query().getParamValue 'code'
				Ti.API.debug "Grabbed secret code: #{api.cacheData.secret_code}"
				Ti.App.Properties.getBool 'oauth', true
				api.cacheData.oauth = true
				# exchange the code for the access token
				api.grabToken.call api, api.cacheData.secret_code

Ti.App.addEventListener 'pause', ->
	Ti.API.info 'Pause is fired now'
	# Ti.App.Properties.setString "isResumed", 0
	Ti.App.Properties.setBool "isPaused", true
