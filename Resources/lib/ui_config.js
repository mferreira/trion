var moment = require('lib/vendor/moment.min'),
	_ = require('lib/vendor/underscore-min'),
	GLOBAL = require('globals');

moment.lang('timeline', {
	relativeTime : {
		future: "in %s",
		past: "%s",
		s: "now",
		m: "1m",
		mm: "%dm",
		h: "1h",
		hh: "%dh",
		d: "1d",
		dd: "%dd",
		M: "1mon",
		MM: "%dmon",
		y: "1y",
		yy: "%dy"
	}
});

var
topRow = { height: 42, backgroundImage: 'images/row_bg_top.png', backgroundTopCap: 8, backgroundLeftCap: 20 },
midRow = { height: 41, backgroundImage: 'images/row_bg_mid.png', backgroundLeftCap: 20 },
bottomRow = { height: 45, backgroundImage: 'images/row_bg_bottom.png',  backgroundTopCap: 10, backgroundLeftCap: 20 },
fullRow = {backgroundImage: 'images/row_bg_full.png', backgroundTopCap: 8, backgroundLeftCap: 20, height: 46};

var config = {
	defaultProperties: {
		barColor: '#1d68a0'
	},
	windowProperties: {
		navTintColor: 'white',
		barColor: '#1382b9',
		barImage: 'images/title_bar.png',
		backgroundImage: 'images/bg.png',
		backgroundRepeat: true
		// top: 0, bottom: 49,
		// left: 0, right: 0
		// barColor: '#1d68a0',
		// barColor: '#00a7c5',
		// backgroundColor: '#e3ded8'
		// backgroundColor:'#fff'
	},
	navbarButton: {
		backgroundImage: 'images/menubar-button.png',
		height: 30,
		color: 'white',
		font: { fontSize: 14 }
	},
	newPostButton: {
		tintColor: 'white',
		width: 35, height: 31,
		systemButton: Ti.UI.iPhone.SystemButton.COMPOSE
	},
	postButton: {
		tintColor: 'white',
		right: 8,
		width: 47, height: 31,
		systemButton: Ti.UI.iPhone.SystemButton.ADD
	},
	cancelButton: {
		backgroundColor: 'white',
		tintColor: '#fff',
		left: 8, top: 0,
		width: 61, height: 31,
		systemButton: Ti.UI.iPhone.SystemButton.STOP
	},
	postButton2: {
		tintColor: 'white',
		right: 8,
		width: 47, height: 31,
		systemButton: Ti.UI.iPhone.SystemButton.ADD
	},
	cancelButton2: {
		tintColor: 'white',
		left: 8,
		width: 61, height: 31,
		systemButton: Ti.UI.iPhone.SystemButton.STOP
	},
	backButton: {
		tintColor: 'white',
		left: 8,
		width: 57, height: 31,
		systemButton: Ti.UI.iPhone.SystemButton.STOP
	},

	buttonLabel: {
		width: 60, height: 31,
		font:{fontFamily:'Helvetica Neue', fontWeight:'bold', fontSize:12}, color: '#0077ac',
		shadowColor: 'white', shadowOffset:{x:0, y:1},
		right: 0
	},

	observationTextField: {
		textAlign: 'left',
		left: 240,
		width: 560,
		height: 40,
		font: { fontSize: 18 },
		paddingLeft: 8, paddingRight: 3,
		backgroundImage:'images/ipad-text-input.png',
		borderStyle:Titanium.UI.INPUT_BORDERSTYLE_NONE
	},
	tableView: {
		top: 0, bottom: 0,
		width: GLOBAL.isTablet ? 680 : Ti.UI.FILL,
		minRowHeight: 30,
		separatorStyle:Titanium.UI.iPhone.TableViewSeparatorStyle.SINGLE_LINE,
		separatorColor: '#e2e4e6',
		headerView: Ti.UI.createView({height:1}),
		rowBackgroundColor: '#fff',
		backgroundColor: 'transparent',
		backgroundImage: 'images/bg.png', backgroundRepeat: true,
		style: Titanium.UI.iPhone.TableViewStyle.GROUPED
	},

	defaultRowLabel: {
		width: Ti.UI.SIZE, height: Ti.UI.SIZE,
		font: {fontSize:14,fontFamily:'Helvetica Neue', fontWeight:'bold'},
		color: '#474b50',
		left: 0
	},

	rowContainerView: {
		width: GLOBAL.isTablet ? 565 : 268,
		left: 15
	},

	rowHasDetailImage: {
		width: Ti.UI.SIZE, height: Ti.UI.SIZE,
		image: 'images/arrow_child.png',
		right: 15
	},

	defaultRowProperties: {
		className: 'defaultRow',
		// backgroundImage:'images/list-item-bg.png',
		// backgroundTopCap: 2,
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	},
	moreTableRowProperties: {
		// backgroundImage:'images/list-item-bg.png',
		// backgroundTopCap: 2,
		className: 'moreTableRowProperties',
		editable:false,
		moveable:false,
		height: 35
	},
	genericPostRow: {
		className: 'statusPostRow',
		backgroundImage: 'images/row_bg_mid.png',
		backgroundTopCap: 8, backgroundLeftCap: 20,
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	},
	statusPostRow: {
		className: 'statusPostRow',
		backgroundImage: 'images/row_bg_mid.png',
		backgroundTopCap: 8, backgroundLeftCap: 20,
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	},
	replyPostRow: {
		className: 'replyPostRow',
		backgroundImage: 'images/row_bg_mid.png',
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	},
	repostPostRow: {
		className: 'repostPostRow',
		backgroundImage: 'images/row_bg_mid.png',
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	},
	entityRow: {
		className: 'entityRow',
		height: 50,
		hasChild: false,
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE,
		backgroundColor: '#fff'
	},

	//////////////////////////////////////////////////////////////////////////////////////////////////
	topRow: topRow,
	midRow: midRow,
	bottomRow: bottomRow,
	fullRow: fullRow,

	postActions: {
		backgroundImage: 'images/row_bg_bottom.png',
		backgroundTopCap: 8,
		backgroundLeftCap: 20,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE,
		height: 43
	},

	rowWithChild: _.extend({
		className: 'rowWithChild',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
		// rightImage: 'images/arrow_child.png'
	}, midRow),

	rowWithChild_top: _.extend({
		className: 'rowWithChild_top',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
		// rightImage: 'images/arrow_child.png'
	}, topRow),

	rowWithChild_bottom: _.extend({
		className: 'rowWithChild_bottom',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
		// rightImage: 'images/arrow_child.png'
	}, bottomRow),

	rowWithChild_full: _.extend({
		className: 'rowWithChild_full',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
		// rightImage: 'images/arrow_child.png'
	}, fullRow),

	rowWithoutChild: _.extend({
		className: 'rowWithoutChild',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, midRow),

	rowWithoutChild_top: _.extend({
		className: 'rowWithoutChild_top',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, topRow),

	rowWithoutChild_bottom: _.extend({
		className: 'rowWithoutChild_bottom',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, bottomRow),

	rowWithoutChild_full: _.extend({
		className: 'rowWithoutChild_full',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, fullRow),

	profileEditableRow:  _.extend({
		className: 'profileEditableRow',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, midRow),

	profileEditableRow_top: _.extend({
		className: 'profileEditableRow_top',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, topRow),

	profileEditableRow_bottom: _.extend({
		className: 'profileEditableRow_bottom',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, bottomRow),

	profileEditableRow_full: _.extend({
		className: 'profileEditableRow_full',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, fullRow),

	rowWith2labels: _.extend({
		className: 'rowWith2labels',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, midRow),

	rowWith2labels_top: _.extend({
		className: 'rowWith2labels_top',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, topRow),

	rowWith2labels_bottom: _.extend({
		className: 'rowWith2labels_bottom',
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
	}, bottomRow),

	rowWith2labels_full: _.extend({
		className: 'rowWith2labels_full'
	}, fullRow),

	profileTextField: {
		left: 103, right: 15, height: 32,
		font: {fontSize:14, fontFamily:'Helvetica'},
		textAlign: 'left', editable: true, color: '#484b50',
		autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_WORDS,
		autocorrect: false,
		clearButtonMode: Ti.UI.INPUT_BUTTONMODE_ONFOCUS
	},

	sideMenuSeparatorRow: {
		className: 'sideMenuSeparatorRow',
		height: 20,
		hasChild: false,
		editable:false,
		moveable:false,
		selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE,
		touchEnabled: false,
		backgroundColor: '#e1e4e9'
	},
	// sideMenuRow: {
	// 	className: 'sideMenuRow',
	// 	height: 40,
	// 	hasChild: false,
	// 	editable:false,
	// 	moveable:false,
	// 	selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE,
	// 	backgroundGradient: {
	// 		type: 'linear',
	// 		startPoint: { x: '50%', y: '0%' },
	// 		endPoint: { x: '50%', y: '100%' },
	// 		colors: [
	// 			{ color: '#fefefe', offset: 0.0}, { color: '#fefefe', offset: 0.025 },
	// 			{ color: '#e1e4e9', offset: 0.025 }, { color: '#e1e4e9', offset: 0.975 },
	// 			{ color: '#c5c6ca', offset: 0.975 }, { color: '#c5c6ca', offset: 1.0 }
	// 		]
	// 	}
	// },
	sideMenuLabel: {
		left: 20,
		width: Ti.UI.SIZE, height: Ti.UI.SIZE,
		textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
		font: { fontSize: 16 }
	},

	defaultLabel: {
		width: Ti.UI.SIZE, height: Ti.UI.SIZE,
		font: {fontSize:16,fontFamily:'Helvetica Neue'},
		color: '#474b50'
	},

	activityButton: {
		systemButton: Ti.UI.iPhone.SystemButton.ACTIVITY,
		height: 40
	},

	followingButton: {
		width: 68, height: 24,
		backgroundImage: 'images/following_btn.png',
		style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
		isFollowing: true
	},
	followButton: {
		width: 52, height: 24,
		backgroundImage: 'images/follow_btn.png',
		style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
		isFollowing: false
	},
	followingButton_profile: {
		width: 68, height: 24,
		backgroundImage: 'images/following_profile_btn.png',
		style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
		isFollowing: true
	},
	followButton_profile: {
		width: 52, height: 24,
		backgroundImage: 'images/follow_profile_btn.png',
		style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
		isFollowing: false
	}
};

module.exports = config;
