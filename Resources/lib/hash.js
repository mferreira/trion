var hash = {
	randomString: function (length, chars) {
		chars = chars || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var result = '';
		for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
		return result;
	},
	chars_from_hex: function (inputstr, delimiter) {
		var outputstr = '';
		inputstr = inputstr.replace(/^(0x)?/g, '');
		inputstr = inputstr.replace(/[^A-Fa-f0-9]/g, '');
		inputstr = inputstr.split('');
		for(var i=0, j=inputstr.length; i<j; i+=2) {
			outputstr += String.fromCharCode(parseInt(inputstr[i]+''+inputstr[i+1], 16));
		}
		return outputstr;
	},
	encode64: function (input) {

		var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		//remove null characters from end of output
		input = input.replace(/\0*$/g, '');
		
		var output = "";
		var chr1, chr2, chr3 = "";
		var enc1, enc2, enc3, enc4 = "";
		var i = 0;

		do {
			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output +
				keyStr.charAt(enc1) +
				keyStr.charAt(enc2) +
				keyStr.charAt(enc3) +
				keyStr.charAt(enc4);
			chr1 = chr2 = chr3 = "";
			enc1 = enc2 = enc3 = enc4 = "";
		} while (i < input.length);

		var strout = "";
		output = output.split('');
		for(var c=0; c<output.length; c++) {
			if(c % 64 === 0 && c > 0) strout += '\n';
			strout += output[c];
		}
		output = output.join();
		
		var nulls = strout % 4;
		for(var i=0; i<nulls; i++)
			strout += '=';
		
		return strout;
	},

	createHash: function(inputstr, method) {
		var sha = Ti.Utils[method]( inputstr );
		return this.encode64( this.chars_from_hex( sha ) );
	}
};

module.exports = hash;