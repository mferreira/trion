var options = {

	// TODO: Add more URL shorteners sites
	sites: {
		"V": {
			host: 'v.gd',
			uri: 'http://v.gd/create.php?format=simple&url=',
			chars: 18
		}
	},

	choice: Ti.App.Properties.getString('url_shortener_service', 'V')

};

module.exports = options;