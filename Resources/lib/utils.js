exports.equals = function(o1, o2)
{
	var p;
	for(p in o1) {
		if(typeof(o2[p])=='undefined') {return false;}
	}

	for(p in o1) {
		if (o1[p]) {
			switch(typeof(o1[p])) {
				case 'object':
					if (!o1[p].equals(o2[p])) { return false; } break;
				case 'function':
					if (typeof(o2[p])=='undefined' ||
						(p != 'equals' && o1[p].toString() != o2[p].toString()))
						return false;
					break;
				default:
					if (o1[p] != o2[p]) { return false; }
			}
		} else {
			if (o2[p])
				return false;
		}
	}

	for(p in o2) {
		if(typeof(o1[p])=='undefined') {return false;}
	}

	return true;
};

exports.randomString = function (length) {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', result = '';
	for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
	return result;
};

exports.toBin = function (str) {
	var st,i,j,d;
	var arr = [];
	var len = str.length;
	for (i = 1; i<=len; i++){
		//reverse so its like a stack
		d = str.charCodeAt(len-i);
		for (j = 0; j < 8; j++) {
			arr.push(d%2);
			d = Math.floor(d/2);
		}
	}
	//reverse all bits again.
	return arr.reverse().join("");
}

exports.toBuffer = function(foo) {
    switch (typeof foo) {
        case 'string':
            return Ti.createBuffer({
                value : foo
            });
            break;
        case 'object':
        	Ti.API.debug('Converting '+foo+' into a buffer...');
            var instream = Ti.Stream.createStream({
                mode : Titanium.Stream.MODE_READ,
                source : foo // e.media is a Blob
            });
            var buffer = Ti.createBuffer({
                length : foo.length
            });
            while ((instream.read(buffer)) > 0) {}
            instream.close();
            return buffer;
            break;
    }
}

exports.imageAsCropped = function (image, options, imageIsBlob) {
	if (!imageIsBlob)
		image = Ti.UI.createImageView({ image: image, width: 'auto', height: 'auto' }).toImage();

	var blob = image.imageAsCropped({
		width: options.width,
		height: options.height,
		x: ((image.size.width - options.width) /2),
		y: ((image.size.height - options.height) /2)
	});
	image.applyProperties(options);
	return image;
};

exports.cropImageHeight = function (imageView, newHeight) {
	var width = imageView.size.width;
	var cropView = Ti.UI.createView({width: width, height: newHeight});
	cropView.add(imageView);
	imageView.top = (imageView.size.height - newHeight) /2
	return Ti.UI.createImageView({ image: cropView.toImage(), width: width, height: newHeight});
};

exports.imageAsCropped2 = function (blob, newWidth, newHeight) {
	var ImageFactory = require('ti.imagefactory');
    return ImageFactory.imageAsCropped(blob, {
    	width: newWidth,
    	height: newHeight
    });
};

exports.cropImageURL = function (originalURL, w, h) {
	var url = "http://rco.cc/thumbnail/?src="+originalURL+"&zc=1"
	if (w) url += '&w='+w
	if (h) url += '&h='+h
	return url
};

/*
Copyright Vassilis Petroulias [DRDigit]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
exports.B64 = {
    alphabet: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
    lookup: null,
    encode: function (s) {
        var buffer = this.toUtf8(s),
            position = -1,
            len = buffer.length,
            nan1, nan2, enc = [, , , ];
        result = '';
        while (++position < len) {
            nan1 = buffer[position + 1], nan2 = buffer[position + 2];
            enc[0] = buffer[position] >> 2;
            enc[1] = ((buffer[position] & 3) << 4) | (buffer[++position] >> 4);
            if (isNaN(nan1)) enc[2] = enc[3] = 64;
            else {
                enc[2] = ((buffer[position] & 15) << 2) | (buffer[++position] >> 6);
                enc[3] = (isNaN(nan2)) ? 64 : buffer[position] & 63;
            }
            result += this.alphabet[enc[0]] + this.alphabet[enc[1]] + this.alphabet[enc[2]] + this.alphabet[enc[3]];
        }
        return result;
    },
    decode: function (s) {
        var buffer = this.fromUtf8(s),
            position = 0,
            len = buffer.length;
        result = '';
        while (position < len) {
            if (buffer[position] < 128) result += String.fromCharCode(buffer[position++]);
            else if (buffer[position] > 191 && buffer[position] < 224) result += String.fromCharCode(((buffer[position++] & 31) << 6) | (buffer[position++] & 63));
            else result += String.fromCharCode(((buffer[position++] & 15) << 12) | ((buffer[position++] & 63) << 6) | (buffer[position++] & 63));
        }
        return result;
    },
    toUtf8: function (s) {
        var position = -1,
            len = s.length,
            chr, buffer = [];
        if (/^[\x00-\x7f]*$/.test(s)) while (++position < len)
        buffer.push(s.charCodeAt(position));
        else while (++position < len) {
            chr = s.charCodeAt(position);
            if (chr < 128) buffer.push(chr);
            else if (chr < 2048) buffer.push((chr >> 6) | 192, (chr & 63) | 128);
            else buffer.push((chr >> 12) | 224, ((chr >> 6) & 63) | 128, (chr & 63) | 128);
        }
        return buffer;
    },
    fromUtf8: function (s) {
        var position = -1,
            len, buffer = [],
            enc = [, , , ];
        if (!this.lookup) {
            len = this.alphabet.length;
            this.lookup = {};
            while (++position < len)
            this.lookup[this.alphabet[position]] = position;
            position = -1;
        }
        len = s.length;
        while (position < len) {
            enc[0] = this.lookup[s.charAt(++position)];
            enc[1] = this.lookup[s.charAt(++position)];
            buffer.push((enc[0] << 2) | (enc[1] >> 4));
            enc[2] = this.lookup[s.charAt(++position)];
            if (enc[2] == 64) break;
            buffer.push(((enc[1] & 15) << 4) | (enc[2] >> 2));
            enc[3] = this.lookup[s.charAt(++position)];
            if (enc[3] == 64) break;
            buffer.push(((enc[2] & 3) << 6) | enc[3]);
        }
        return buffer;
    }
};