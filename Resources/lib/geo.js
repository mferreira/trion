var geo, geolocCallback;

Ti.Geolocation.setPurpose('Service is needed to attach location to your posts.');

Ti.Geolocation.distanceFilter = 10;

if (GLOBAL.iOS) {
  Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_BEST);
} else {
  Ti.Geolocation.setAccuracy(Ti.Geolocation.ACCURACY_HIGH);
}

geolocCallback = function(e) {
  if (!e.success || e.error) {
    Ti.API.error(JSON.stringify(e.error));
    return Ti.App.fireEvent('geo:getLocation:failed', e);
  } else {
    Ti.API.debug('Geolocation changed. Saving.');
    return geo.lastLocation = {
      longitude: e.coords.longitude,
      latitude: e.coords.latitude,
      altitude: e.coords.altitude,
      heading: e.coords.heading,
      accuracy: e.coords.accuracy,
      speed: e.coords.speed,
      timestamp: e.coords.timestamp,
      altitudeAccuracy: e.coords.altitudeAccuracy,
      address: L('no_address_found', "No address found.")
    };
  }
};

geo = {
  started: false,
  askForAuth: function(callback, force) {
    var _ref;
    if (((_ref = Ti.Geolocation.getLocationServicesAuthorization()) !== Ti.Geolocation.AUTHORIZATION_AUTHORIZED && _ref !== Ti.Geolocation.AUTHORIZATION_RESTRICTED) || force) {
      Ti.Geolocation.setLocationServicesAuthorization(Ti.Geolocation.AUTHORIZATION_AUTHORIZED);
    }
    Ti.API.debug('Geolocation after setting Auth: ' + ['AUTHORIZATION_UNKNOWN', 'AUTHORIZATION_RESTRICTED', 'AUTHORIZATION_DENIED', 'AUTHORIZATION_AUTHORIZED'][Ti.Geolocation.getLocationServicesAuthorization()]);
    if (callback) {
      if (this.isAvailable()) {
        if (callback.onload) {
          return callback.onload();
        }
      } else if (callback.onerror) {
        return callback.onerror();
      }
    }
  },
  isAuthorized: function() {
    return Ti.Geolocation.getLocationServicesAuthorization() === Ti.Geolocation.AUTHORIZATION_AUTHORIZED;
  },
  isAvailable: function(callback) {
    Ti.API.log('geo', "Checking if location service is available... " + (this.isEnabled()));
    Ti.API.debug('Geolocation is enabled: ' + this.isEnabled());
    Ti.API.debug('Geolocation is authorized: ' + this.isAuthorized());
    if (this.isEnabled() && this.isAuthorized()) {
      Ti.API.debug('Geolocation started. Obtaining device geo position.');
      return true;
    } else {
      Ti.API.warn('Geolocation will not be started. Device location service needs to be enabled first.');
      return false;
    }
  },
  isEnabled: function() {
    return Ti.Geolocation.getLocationServicesEnabled();
  },
  purpose: Ti.Geolocation.getPurpose(),
  lastLocation: {},
  grabCountry: function(ip) {
    var api_key, xhr;
    ip = ip || Ti.Platform.address;
    xhr = Ti.Network.createHTTPClient({
      onload: function(e) {
        Ti.API.info("Received text: " + this.responseText);
        return Ti.App.fireEvent('grabCountry:load');
      },
      onerror: function(e) {
        Ti.API.debug(e.error);
        return Ti.App.fireEvent('grabCountry:error');
      },
      timeout: 5000
    });
    api_key = '00716d625212cd6324979faa4c49b9512ee4f3b71ac2a43a5c8b1ce36813c5bb';
    xhr.open('http://api.ipinfodb.com/v3/ip-country/?key=' + api_key + '&ip=' + ip + '&format=json');
    return xhr.send();
  },
  start: function(callback) {
    if (!this.started) {
      if (this.isAvailable()) {
        this.started = true;
        if (callback && callback.onload) {
          return callback.onload();
        }
      } else if (!this.isAuthorized()) {
        this.askForAuth();
        return Ti.Geolocation.getCurrentPosition(function(e) {
          Ti.API.debug(JSON.stringify(e));
          if (callback && callback.onload) {
            return callback.onload();
          }
        });
      }
    }
  },
  getCurrentPosition: function(callback) {
    Ti.API.info('Grabbing current device geo location.');
    return Ti.Geolocation.getCurrentPosition(function(e) {
      Ti.API.debug(JSON.stringify(e));
      geolocCallback(e);
      if (!!e.success || e.error) {
        return callback.onerror();
      } else {
        return callback.onload({
          latitude: e.coords.latitude,
          longitude: e.coords.longitude,
          altitude: e.coords.altitude
        });
      }
    });
  }
};

module.exports = geo;
