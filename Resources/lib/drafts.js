var drafts;

drafts = Ti.App.Properties.hasProperty('drafts') ? JSON.parse(Ti.App.Properties.getString('drafts')) : [];

exports.load = function(i) {
  return drafts[i];
};

exports.save = function(data) {
  drafts.push(data);
  return Ti.App.Properties.setString('drafts', JSON.stringify(drafts));
};

exports.remove = function(i) {
  if (drafts.length > i) {
    delete drafts[i];
    drafts.splice(i, 1);
    return Ti.App.Properties.setString('drafts', JSON.stringify(drafts));
  }
};

exports.all = function() {
  return drafts;
};
