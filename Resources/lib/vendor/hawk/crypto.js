// Load modules

var Crypto = require('lib/vendor/sha_dev');
// var Crypto = require('crypto');
// var Url = require('url');
var Utils = require('lib/vendor/hawk/utils');
var myUtils = require('lib/utils');


// Declare internals

var internals = {};


// MAC normalization format version

exports.headerVersion = '1';                        // Prevent comparison of mac values generated with different normalized string formats


// Supported HMAC algorithms

exports.algorithms = ['sha1', 'sha256'];


// Calculate the request MAC

/*
    type: 'header',                                 // 'header', 'bewit', 'response'
    credentials: {
        key: 'aoijedoaijsdlaksjdl',
        algorithm: 'sha256'                         // 'sha1', 'sha256'
    },
    options: {
        method: 'GET',
        resource: '/resource?a=1&b=2',
        host: 'example.com',
        port: 8080,
        ts: 1357718381034,
        nonce: 'd3d345f',
        hash: 'U4MKKSmiVxk37JCCrAVIjV/OhB3y+NdwoCr6RShbVkE=',
        ext: 'app-specific-data',
        app: 'hf48hd83qwkj',                        // Application id (Oz)
        dlg: 'd8djwekds9cj'                         // Delegated by application id (Oz), requires options.app
    }
*/

exports.calculateMac = function (type, credentials, options) {

    // Ti.API.log('hawk', "crypto.calculateMac <- "+type+", "+JSON.stringify(credentials)+", "+JSON.stringify(options));
    var normalized = exports.generateNormalizedString(type, options);

    // var hmac = Crypto.createHmac(credentials.algorithm, credentials.key).update(normalized);
    // var digest = hmac.digest('base64');
    // var digest = new Crypto(normalized, "TEXT").getHash(credentials.algorithm, "B64");
    var digest = (new Crypto(normalized, "TEXT")).getHMAC(credentials.key, "TEXT", credentials.algorithm, "B64");

    // Ti.API.log('hawk', "crypto.calculateMac -> "+digest);
    return digest;
};


exports.generateNormalizedString = function (type, options) {

    // Ti.API.log('hawk', "crypto.generateNormalizedString <- "+type+", "+JSON.stringify(options));
    var normalized = 'hawk.' + exports.headerVersion + '.' + type + '\n' +
                     options.ts + '\n' +
                     options.nonce + '\n' +
                     options.method + '\n' +
                     options.resource + '\n' +
                     options.host + '\n' +
                     options.port + '\n' +
                     (options.hash || '') + '\n';

    if (options.ext) {
        normalized += options.ext.replace('\\', '\\\\').replace('\n', '\\n');
    }

    normalized += '\n';

    if (options.app) {
        normalized += options.app + '\n' +
                      (options.dlg || '') + '\n';
    }

    // Ti.API.log('hawk', "crypto.generateNormalizedString -> "+JSON.stringify(normalized));
    return normalized;
};


exports.calculatePayloadHash = function (payload, algorithm, contentType) {

    // Ti.API.log('hawk', "crypto.calculatePayloadHash <- "+JSON.stringify(payload)+", "+algorithm+", "+contentType);

    // if (''+payload === "[object TiBlob]") {
    //     var buffer = Ti.createBuffer({value: 'hawk.' + exports.headerVersion + '.payload\n' +contentType.split(';')[0].trim().toLowerCase()+ '\n'});
    //     buffer.append(myUtils.toBuffer(payload));
    //     buffer.append(Ti.createBuffer({value: '\n'}));
    //     var newBlob = buffer.toBlob();
    //     try {
    //         return Ti.Utils.base64encode(Ti.Utils[algorithm](newBlob));
    //     }
    //     catch (err) {
    //         Ti.API.error(err);
    //         Ti.API.error('This algorithm is unsupported to hash a Blob: '+algorithm);
    //     }
    // }

    if (typeof payload !== 'string')
        payload = JSON.stringify(payload);

    var hashThis = 'hawk.' + exports.headerVersion + '.payload\n' +contentType.split(';')[0].trim().toLowerCase()+ '\n' + (payload || '');
    hashThis += '\n';
    // Ti.API.log('hawk', "crypto.calculatePayloadHash -- "+JSON.stringify(hashThis));

    var hash = new Crypto(hashThis, "TEXT").getHash(algorithm, "B64");
    // hash.update(payload || '');
    // return exports.finalizePayloadHash(hash);
    return hash;
};


exports.initializePayloadHash = function (algorithm, contentType) {

    // var hash = Crypto.createHash(algorithm);
    // hash.update('hawk.' + exports.headerVersion + '.payload\n');
    // hash.update(Utils.parseContentType(contentType) + '\n');
    var hash = new Crypto('hawk.' + exports.headerVersion + '.payload\n' +contentType.split(';')[0].trim().toLowerCase()+ '\n', "TEXT").getHash(algorithm, "B64");
    return hash;
};


exports.finalizePayloadHash = function (hash) {

    hash.update('\n');
    return hash.digest('base64');
};


exports.calculateTsMac = function (ts, credentials) {

    var hmac = Crypto.createHmac(credentials.algorithm, credentials.key);
    hmac.update('hawk.' + exports.headerVersion + '.ts\n' + ts + '\n');
    return hmac.digest('base64');
};

