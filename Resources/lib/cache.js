var defaultCacheProp,
  __hasProp = {}.hasOwnProperty;

defaultCacheProp = 'mainCache';

exports.loadCache = function(cacheProp, defaultCache) {
  if (Ti.App.Properties.hasProperty(cacheProp || defaultCacheProp)) {
    return JSON.parse(Ti.App.Properties.getString(cacheProp || defaultCacheProp, '{}'));
  } else {
    return defaultCache || {};
  }
};

exports.saveCache = function(cacheProp, json) {
  return Ti.App.Properties.setString(cacheProp || defaultCacheProp, JSON.stringify(json));
};

exports.appendCache = function(cacheProp, json) {
  var cache, p;
  cacheProp = cacheProp || defaultCacheProp;
  cache = exports.loadCache(cacheProp);
  for (p in json) {
    if (!__hasProp.call(json, p)) continue;
    cache[p] = json[p];
  }
  return exports.saveCache(cache, cacheProp);
};
