var _ = require('lib/vendor/underscore-min'),
	URI = require('lib/vendor/jsuri.min'),
	macauth = require('lib/vendor/macauth'),
	moment = require('lib/vendor/moment.min');


var appName = GLOBAL.appName,

timeout = 15, // in seconds
timeoutTimer = null,
conId = 0,
conQ = {},

uri = {
	basic: 'https://tent.io/types/info/basic/v0.1.0',
	core: 'https://tent.io/types/info/core/v0.1.0',
	redirect: 'trion://tentauth',
	tent_notification_url: 'http://apn.tentrapp.com'
},

tentMimeType = 'application/vnd.tent.v0+json; charset=utf-8',
multipartMimeType = 'multipart/form-data; boundary=-----------TentAttachment; charset=utf-8',
// TODO: support user defined licenses
licenses = ["http://creativecommons.org/licenses/by/3.0/"],

post_types = {
	Status: "https://tent.io/types/post/status/v0.1.0",
	Essay: "https://tent.io/types/post/essay/v0.1.0",
	Photo: "https://tent.io/types/post/photo/v0.1.0",
	Album: "https://tent.io/types/post/album/v0.1.0",
	Repost: "https://tent.io/types/post/repost/v0.1.0",
	Profile: "https://tent.io/types/post/profile/v0.1.0", // TODO: IMPLEMENT API
	Delete: "https://tent.io/types/post/delete/v0.1.0",
	Follower: "https://tent.io/types/post/follower/v0.1.0", // TODO: IMPLEMENT API
	Following: "https://tent.io/types/post/following/v0.1.0", // TODO: IMPLEMENT API
	Group: "https://tent.io/types/post/group/v0.1.0" // TODO: IMPLEMENT API
},

scopes = {
	read_posts: 'x',
	write_posts: 'x',
	// import_posts: 'x',
	read_profile: 'x',
	write_profile: 'x',
	read_followers: 'x',
	// write_followers: 'x',
	read_followings: 'x',
	write_followings: 'x',
	read_groups: 'x',
	write_groups: 'x',
	read_permissions: 'x',
	// write_permissions: 'x',
	read_apps: 'x'
	// write_apps: 'x',
	// follow_ui: 'x',
	// read_secrets: 'x',
	// write_secrets: 'x'
},

tent_profile_info_types = [
	// 'all'
	"https://tent.io/types/info/core/v0.1.0",
	"https://tent.io/types/info/basic/v0.1.0"
],
tent_post_types = [
	// "all"
	post_types.Status,
	post_types.Repost,
	post_types.Follower,
	post_types.Delete
],

//////////////////////////////////////
//
cache = {
	access_token: Ti.App.Properties.getString('access_token', null),
	secret_code: Ti.App.Properties.getString('secret_code', null),
	uris: [],
	profile: JSON.parse(Ti.App.Properties.getString('profile')) || null,
	app_auth: Ti.App.Properties.getObject('app_auth', null),
	appRegistered: Ti.App.Properties.getBool('appRegistered', false),
	oauth: Ti.App.Properties.getBool('oauth', false),
	apiRootUrls: null,
	discover: ( Ti.App.Properties.hasProperty('discover')? JSON.parse(Ti.App.Properties.getString('discover')) : {} ),

	// follows
	following: (Ti.App.Properties.hasProperty('bonds:following')? Ti.App.Properties.getList('bonds:following') : []),
	following_ext: (Ti.App.Properties.hasProperty('bonds:following_ext')? JSON.parse(Ti.App.Properties.getString('bonds:following_ext')) : {}),
	followers: (Ti.App.Properties.hasProperty('bonds:followers')? JSON.parse(Ti.App.Properties.getString('bonds:followers')) : []),

	// posts related stuff
	posts: {
		last_id: null,
		last_time: null,
		last_id_entity: null
	},
	// mentions related stuff
	mentions: {
		last_id: null,
		last_time: null,
		last_id_entity: null
	},

	getAppAuth: Ti.App.Properties.getObject('getAppAuth', null)
};
cache.app_authorizations = (this.app_auth? JSON.parse(this.app_auth.authorizations) : null);
//
//////////////////////////////////////
//

Ti.API.debug('cache.app_auth -> '+JSON.stringify(cache.app_auth));

function checkNetwork(callback) {
	if (Ti.Network.online) {
		callback.online.apply(api);
	}
	else {
		Ti.API.warn('Device is not online.');
		if (callback.offline) callback.offline.apply(api);
	}
}

function XHR(callbacks) {
	conId += 1;
	var myId = conId;
	conQ[myId] = Ti.Network.createHTTPClient({
		conId: myId,
		timeout: timeout *1000,  // in milliseconds
		tlsVersion: Ti.Network.TLS_VERSION_1_2,
		withCredentials: true,
		// function called when the response data is available
		onload : function() {
			clearTimeout(timeoutTimer);
			Ti.API.log('api',"HTTP Code: " + this.status);
			// Ti.API.log('api', 'conQ -> '+JSON.stringify(conQ));
			if (conQ[this.conId]){
				conQ[this.conId] = null;
				delete conQ[this.conId];
				// Ti.API.log('api',"HTTP HEADER: " + this.getResponseHeader( 'Link' ));
				// Ti.API.log('api',"Received text: " + this.responseText);
				// Ti.API.log('api',"Received Data: " + this.responseData);
				var responseParsed = (callbacks.noJson? '': JSON.parse(this.responseText));
				if (callbacks.onload) {
					callbacks.onload( {json: responseParsed, raw: this.responseText, data: this.responseData, status: this.status} );
				}
			}
			callbacks.onload = null;
		},
		// function called when an error occurs, including a timeout
		onerror : function(e) {
			clearTimeout(timeoutTimer);
			Ti.API.log('api',"HTTP Code: " + this.status);
			Ti.API.error(e.status+' '+this.responseText);
			// Ti.API.log('api', 'conQ -> '+JSON.stringify(conQ));
			if (conQ[this.conId]){
				conQ[this.conId] = null;
				delete conQ[this.conId];
				// alert('ERROR: '+e.error);
				if (callbacks.onerror) {
					callbacks.onerror( {status: this.status, error: this.responseText} );
				}
			}
			callbacks.onerror = null;
		},
		onreadystatechange: function() {
			Ti.API.log('api',"Ready state: " + ['UNSENT', 'OPENED', 'HEADERS_RECEIVED', 'LOADING', 'DONE'][this.readyState]);
		}
	});
	return conQ[myId];
}

var api = {

	XHR: XHR,

	cancelXHR: function(id){
		Ti.API.log('api', 'cancelXHR <- '+JSON.stringify(id));
		if (id) {
			if (typeof id === 'number' && conQ[id]) {
				Ti.API.warn('Terminating XHR with ID: '+id);
				conQ[id].abort();
				delete conQ[id];
			}
			else {
				for (var i=id.length; i--;){
					if (conQ[id[i]]) {
						Ti.API.warn('Terminating XHR with IDs: '+id[i]);
						conQ[id[i]].abort();
						delete conQ[id[i]];
					}
				}
			}
		}
	},

	cacheData: null,

	checkNetwork: checkNetwork,

	getAppInfo: function(){
		return {
			"name": appName,
			"description": "Lets you shoot amazing posts from your iOS device to the Tent community.",
			"url": "http://tentrapp.com",
			"icon": "http://rco.cc/tentrapp/icon.png",
			"redirect_uris": [ uri.redirect ],
			"scopes": scopes
		};
	},

	matchAppInfo: function(appData){
		var appInfo = this.getAppInfo();
		if (appInfo.name !== appData.name || appInfo.description !== appData.description ||
			appInfo.url !== appData.url || appInfo.icon !== appData.icon || !_.isEqual(appInfo.redirect_uris, appData.redirect_uris))
			return false;
		return true;
	},

	getAppAuth: function(_cache){
		cache = _cache || cache;
		cache.getAppAuth = {
			'client_id': cache.app_auth.id,
			'redirect_uri': uri.redirect,
			'state': macauth.getNonce(32),
			'scope': _.keys(scopes).join(','),
			'tent_profile_info_types': tent_profile_info_types.join(','),
			'tent_post_types': tent_post_types.join(','),
			'tent_notification_url': uri.tent_notification_url +'/'+GLOBAL.apn_channel
		};
		Ti.App.Properties.setObject('getAppAuth', cache.getAppAuth);
		return cache.getAppAuth;
	},

	execIfOnline: function(execThis) {
		checkNetwork({
			online: execThis,
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	isAuthenticated: function(){
		// Ti.API.debug('cache:  '+JSON.stringify(cache));
		// Ti.API.debug('isAuthenticated: '+!!(cache.access_token && cache.app_auth.mac_key_id && cache.app_auth.mac_key && cache.app_auth.id));
		if (cache.app_auth) {
			if (cache.access_token && cache.appRegistered && cache.oauth && cache.app_auth.id) {
				return true;
			}
			else if (cache.access_token && cache.app_auth.mac_key_id && cache.app_auth.mac_key && cache.app_auth.id) {
				Ti.App.Properties.setBool('appRegistered', true);
				Ti.App.Properties.setBool('oauth', true);
				return true;
			}
		}
		return false;
	},

	getCache: function(req) {
		// Ti.API.log('api', 'getCache called: '+ req +' > '+JSON.stringify(cache[req]));
		return cache[req];
	},

	clearAuthCache: function() {
		cache.app_auth = null;
	},

	getFollowing: function(uri) {
		Ti.API.debug('Getting following from cache: '+uri);
		if (cache.following.indexOf(uri) > -1 && cache.following_ext.hasOwnProperty(uri)) return cache.following_ext[uri];
		else if (!cache.following_ext.hasOwnProperty(uri)) Ti.API.warn('No cache data for "'+uri+'" in following_ext.');
		Ti.API.debug(JSON.stringify(cache.following_ext));
	},

	addFollowing: function(followData) {
		Ti.API.debug('Adding following to cache: '+followData.entity);
		if (cache.following.indexOf(followData.entity) > -1) return;
		cache.following.push(followData.entity);
		Ti.App.Properties.setList('bonds:following', cache.following);
		cache.following_ext[followData.entity] = followData;
		Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext));
	},

	removeFollowing: function(uri){
		Ti.API.debug('Removing following from cache: '+uri);
		var index = cache.following.indexOf(uri);
		if (index > -1) {
			cache.following.splice(index, 1);
			Ti.App.Properties.setList('bonds:following', cache.following);
			delete cache.following_ext[uri];
			Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext));
		}
	},

	isFollowing: function(uri, checkOnline, callback) {
		Ti.API.debug('Checking if we are following: "'+uri+'"');
		if (!checkOnline) {
			var ret = (cache.following.indexOf(uri) > -1);
			Ti.API.debug('Following entity '+uri +' > '+ret);
			return ret;
		}
		else {
			checkNetwork({
				online: function(){
					var i = 0,  // index of server to hit
						apiRootUrls = this.getApiRootUrls(),
						url = new URI(apiRootUrls[i]+'/followings'),
						authHeader = null;
						// authHeader = (cache.app_auth? macauth.getAuthorizationHeader('GET', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						// 	id: cache.app_auth.mac_key_id,
						// 	key: cache.app_auth.mac_key,
						// 	algorithm: cache.app_auth.mac_algorithm,
						// 	issued: (new Date()).getTime()
						// }) : '');

					url.addQueryParam( 'entity', encodeURIComponent(uri) );

					var xhr = new XHR({
						onload: function(resp){
							var isFollowing = (resp.json.length > 0);
							if (callback && callback.onload) callback.onload( isFollowing );
							else Ti.App.fireEvent('api:isFollowing:success', isFollowing);
						},
						onerror: function(resp) {
							if (callback && callback.onerror) callback.onerror( resp );
							else Ti.App.fireEvent('api:isFollowing:error', resp);
						},
						noJson: false
					});

					Ti.API.debug('GET: '+url.toString());
					Ti.API.debug('Authorization: '+authHeader);
					
					// Clears any cookies stored for the host
					xhr.clearCookies(url.toString());
					xhr.open('GET', url.toString(), true);
					xhr.setRequestHeader('Content-Type', tentMimeType);
					xhr.setRequestHeader('Accept', tentMimeType);
					if (authHeader) xhr.setRequestHeader('Authorization', authHeader);
					xhr.send();
				},
				offline: function(){ alert('Looks like you are offline.'); if (callback && callback.offline) callback.offline(); }
			});
		}
	},

	isFollowed: function(uri) {
		var res = false;
		var allFollowers = this.getCache('followers');
		for (var i=allFollowers.length; i--;){
			var cur_entity = allFollowers[i].entity;
			if (uri === cur_entity) {
				res = true;
				break;
			}
		}
		return res;
	},

	clearDiscoveryCache: function(){
		Ti.API.log('api', 'Clearing discovery cache.');
		// Ti.API.log('cache.discover: '+JSON.stringify(cache.discover));
		var me = cache.discover[GLOBAL.entity];
		cache.discover = {};
		cache.discover[GLOBAL.entity] = me;
		Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
		Ti.API.log('cache.discover: '+JSON.stringify(cache.discover));
	},

	getUri: function(req) {
		return uri[req];
	},

	postType: function (type) {
		return post_types[type];
	},

	activePostTypes: function(){
		return [post_types.Status, post_types.Repost];
	},

	getApiRootUrls: function(entity){
		entity = entity || GLOBAL.entity;
		Ti.API.log('api', 'getApiRootUrls() <- '+entity);
		Ti.API.log('api', 'getApiRootUrls() -- cache.discover[entity] -> '+JSON.stringify(cache.discover[entity]));
		var ret = cache.discover[entity] ? cache.discover[entity].core.servers : [];
		Ti.API.log('api', 'getApiRootUrls() -> '+ret);
		return ret;
	},

	hasProfile: function(entity_uri) {
		return cache.discover.hasOwnProperty(entity_uri);
	},

	getMyProfile: function() {
		return cache.discover[GLOBAL.entity] || null;
	},

	saveMyProfile: function(profileData) {
		if (this.hasProfile(GLOBAL.entity)) {
			cache.discover[GLOBAL.entity] = profileData;
			Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
		}
	},

	getProfile: function(entity_uri) {
		return cache.discover[entity_uri] || null;
	},

	saveProfile: function(entity_uri, profileData, dontStore) {
		Ti.API.log('api', 'saveProfile() <- '+entity_uri+' <- '+JSON.stringify(profileData));
		if (_.isEmpty(profileData)) {
			Ti.API.log('api', 'saveProfile() -- Empty profile. Skipping.');
			return;
		}
		cache.discover[entity_uri] = profileData;
		if (!dontStore) Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
	},

	profile_PUT: function(data, callback) {
		Ti.API.debug('Starting PUT /profile');
		var type =uri[data.type];
		var i = 0,  // index of server to hit
			apiRootUrls = this.getApiRootUrls(),
			url = new URI(apiRootUrls[i]+'/profile/'+encodeURIComponent(type)),
			authHeader = (cache.app_auth? macauth.getAuthorizationHeader('PUT', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
				id: cache.app_auth.mac_key_id,
				key: cache.app_auth.mac_key,
				algorithm: cache.app_auth.mac_algorithm,
				issued: (new Date()).getTime()
			}) : ''),

			cache_discover = cache.discover;

		var xhr = new XHR({
			onload: function(resp){
				// update local cache
				cache_discover[GLOBAL.entity] = {basic: resp.json[uri.basic], core: resp.json[uri.core]};
				Ti.App.Properties.setString('discover', JSON.stringify(cache_discover));
				// fire event and callback
				if (callback && callback.onload) callback.onload( resp.json );
				else Ti.App.fireEvent('api:profile_PUT:success', {profile: resp.json});
			},
			onerror: function(resp){
				if (callback && callback.onerror) callback.onerror( resp );
				else Ti.App.fireEvent('api:profile_PUT:error', resp);
			},
			noJson: false
		});
		
		Ti.API.debug('PUT: '+url.toString());
		Ti.API.debug('Authorization: '+authHeader);
		Ti.API.debug('Data: '+JSON.stringify(data.payload));
		
		// Clears any cookies stored for the host
		xhr.clearCookies(apiRootUrls[i]);
		xhr.open('PUT', url.toString(), true);
		xhr.setRequestHeader('Content-Type', tentMimeType);
		xhr.setRequestHeader('Accept', tentMimeType);
		xhr.setRequestHeader('Authorization', authHeader);
		xhr.send(JSON.stringify(data.payload));
	},

	discover: function(defined_uri, callback, noCache) {
		defined_uri = defined_uri.replace(/\/$/g, "");
		Ti.API.debug('Starting discovery on URI: '+defined_uri);
		if (cache.discover[defined_uri] && !noCache) {
			Ti.API.debug('Discovery data found in cache. Firing success.');
			Ti.API.debug('cache.discover ['+defined_uri+']: '+JSON.stringify(cache.discover[defined_uri]));
			if (callback) callback.onload.apply(this, callback.data ? [cache.discover[defined_uri], callback.data] : [cache.discover[defined_uri]] );
			else Ti.App.fireEvent('api:discover:success', cache.discover[defined_uri] );
		}
		else {
			var doHeaderDiscovery = function(headerLink) {
				// cache all uri
				var re_match = /<([^>]*)>\; rel\=\"https\:\/\/tent.io\/rels\/profile\"/ig;
				while (match = re_match.exec(headerLink)) {
					if (match[1].match(/https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*/i))
						cache.uris[defined_uri].push( match[1] );
					else {
						cache.uris[defined_uri].push( defined_uri + match[1] );
					}
				}
				// for (var i=uris.length; i--;) {
				// 	cache.uris[defined_uri].push(uris[i].split(';')[0].replace(/^<|>$/g, ''));
				// }

				setTimeout(function() {
					// grab the profile api roots
					var xhr = new XHR({
						onload: function(resp){
							Ti.API.debug('Discovery finish successfuly. Saving info.');

							var discoveredData = {basic: resp.json[uri.basic], core: resp.json[uri.core]};
							cache.discover[defined_uri] = discoveredData;
							Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));

							Ti.API.debug('firing callback / event');
							// Ti.API.info('cache: '+JSON.stringify(cache));
							if (callback) callback.onload.apply(this, callback.data ? [discoveredData, callback.data] : [discoveredData] );
							else Ti.App.fireEvent('api:discover:success', discoveredData );
						},
						onerror: function(resp){
							if (callback) callback.onerror.apply(this, callback.data ? [resp, callback.data] : [resp] );
							else Ti.App.fireEvent('api:discover:failed');
						}
					});
					Ti.API.debug('GET: '+cache.uris[defined_uri][0]);
					xhr.open('GET', cache.uris[defined_uri][0]);
					xhr.setRequestHeader('Accept', tentMimeType);
					xhr.send();
				}, 400);
			};

			cache.uris[defined_uri] = [];
			var xhr = new XHR({
				onload: function(resp){
					// grab HEAD Link containing the profile uri's
					var headerLink = xhr.getResponseHeader( 'Link' );
					Ti.API.debug('HEADER Link: '+headerLink);
					doHeaderDiscovery(headerLink);
				},
				onerror: function(resp){
					var headerLink = xhr.getResponseHeader( 'Link' );
					Ti.API.debug('HEADER Link: '+headerLink);
					if (headerLink) doHeaderDiscovery(headerLink);
					
					else if (callback) callback.onerror.apply(this, callback.data ? [resp, callback.data] : [resp] );
					else Ti.App.fireEvent('api:discover:failed');
				},
				noJson: false
			});

			Ti.API.debug('HEAD: '+defined_uri);
			// Prepare the connection.
			xhr.open("HEAD", defined_uri);
			// Send the request.
			xhr.send();
		}
	},

	registerApp: function (forceRegister) {
		Ti.API.debug('Starting app registration proccess.');
		var oauthApp = this.oauthApp,
			that = this;
		checkNetwork({
			online: function(){
				if (cache.access_token === null || forceRegister) {
					Ti.API.debug('No access token found.');
					var getApiRootUrls = this.getApiRootUrls;
					var regFunc = function(){
						Ti.API.info('Registering app to tent service...');
						var i = 0,  // index of server to hit
							apiRootUrls = getApiRootUrls(),
							url = apiRootUrls[i]+'/apps',
							httpData = JSON.stringify(that.getAppInfo());

						var xhr = new XHR({
							onload: function(resp){
								Ti.API.debug('Data recieved from app register: '+resp.raw);
								cache.app_auth = {
									id: resp.json.id,
									mac_key_id: resp.json.mac_key_id,
									mac_key: resp.json.mac_key,
									mac_algorithm: resp.json.mac_algorithm,
									authorizations: JSON.stringify(resp.json.authorizations)
								};
								cache.app_authorizations = resp.json.authorizations;
								cache.appRegistered = true;
								Ti.App.Properties.setObject('app_auth', cache.app_auth);
								Ti.App.Properties.setObject('appRegistered', cache.appRegistered);
								oauthApp.call(that, cache);
							},
							noJson: false
						});

						Ti.API.debug('POST: '+url);
						Ti.API.debug('Data: '+httpData);
						// Clears any cookies stored for the host
						xhr.clearCookies(apiRootUrls[i]);
						xhr.open('POST', url, true);
						xhr.setRequestHeader('Content-Type', tentMimeType);
						xhr.setRequestHeader('Accept', tentMimeType);
						xhr.send(httpData);
					};

					if (cache.app_auth === null || forceRegister) { regFunc(); }
					else {
						Ti.API.debug('cache.app_auth: '+JSON.stringify(cache.app_auth));
						Ti.API.info('App should be already registered with the tent service. Checking...');

						this.apps_GET({
							callback: function(result){
								switch (result) {
									case 'success':
										Ti.API.debug('Server accepts this app.');
										Ti.API.debug('OAuth proccess starting.');
										oauthApp();
										break;
									case 'unauthorized':
										Ti.API.warn('Server did not accept this app. Start registration.');
										cache.access_token = null;
										regFunc();
										break;
									default:
										Ti.API.debug('Server error. Failing.');
										Ti.App.fireEvent('api:registerApp:error');
								}
							}
						});
					}
				}
				else {
					Ti.API.debug('Access token found. Skipping app auth and registration.');
				}
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	oauthApp: function (cacheData){
		this.cacheData = cacheData || cache;

		var grabToken = this.grabToken,
			that = this,
			_cache = cacheData || cache;
		checkNetwork({
			online: function(){
				if (_cache.app_auth === null) {
					this.registerApp();
				}
				else if (_cache.secret_code) {
					Ti.API.info('Secret code found in cache. Skipping OAuth.');
					// exchange the code for the access token
					grabToken(_cache.secret_code);
				}
				else {
					Ti.API.info('OAuth app to tent service...');
					var i = 0,  // index of server to hit
						apiRootUrls = this.getApiRootUrls(),
						url = apiRootUrls[i]+'/oauth/authorize',
						httpData = that.getAppAuth(_cache);

					// construct url query based on httpData
					var tail = [];
					for (var p in httpData) {
						if (httpData.hasOwnProperty(p)) {
							tail.push(p + "=" + encodeURIComponent(httpData[p]));
						}
					}
					httpData = tail.join("&");
					var openUrl = url + '?' + httpData;
					Ti.API.warn(openUrl);
					Ti.Platform.openURL(openUrl);
				}
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	grabToken: function(secret_code){
		Ti.API.debug('Exchanging secret code for permanet token.');
		Ti.API.debug('Using for authHeader: '+JSON.stringify(cache.app_auth));
		cache.access_token = null;
		var i = 0,  // index of server to hit
			apiRootUrls = this.getApiRootUrls(),
			url = new URI(apiRootUrls[i]+'/apps/'+cache.app_auth.id+'/authorizations'),
			httpData = JSON.stringify({code: secret_code, token_type: 'mac'}),
			authHeader = macauth.getAuthorizationHeader('POST', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
				id: cache.app_auth.mac_key_id,
				key: cache.app_auth.mac_key,
				algorithm: cache.app_auth.mac_algorithm,
				issued: (new Date()).getTime()
			});

			Ti.API.debug('URL: '+url.host()+' : '+(url.port() || (url.protocol() === 'https'? 443:80)));

		var xhr = new XHR({
			onload: function(resp){
				if (!resp.json) { Ti.API.debug('Exchanging secrets failed.'); }
				else {
					Ti.API.debug('Updating app auth keys.');
					cache.access_token = resp.json.access_token;
					Ti.App.Properties.setString('access_token', resp.json.access_token);
					cache.app_auth = _.extend(cache.app_auth, {
						mac_key_id: resp.json.access_token,
						mac_key: resp.json.mac_key
					});
					Ti.App.Properties.setObject('app_auth', cache.app_auth);
					Ti.API.debug('We have the OAuth key now: '+cache.app_auth.mac_key_id);
					Ti.App.fireEvent('api:grabToken:success');
				}
			},
			onerror: function(resp) {
				Ti.API.debug('Exchanging secrets failed.');
				Ti.App.fireEvent('api:grabToken:failed');
			},
			noJson: false
		});
		
		Ti.API.debug('POST: '+url.toString());
		Ti.API.debug('Authorization: '+authHeader);
		Ti.API.debug('Data: '+httpData);
		// Clears any cookies stored for the host
		xhr.clearCookies(apiRootUrls[i]);
		xhr.open('POST', url.toString(), true);
		xhr.setRequestHeader('Content-Type', tentMimeType);
		xhr.setRequestHeader('Accept', tentMimeType);
		xhr.setRequestHeader('Authorization', authHeader);
		xhr.send(httpData);
	},

	setLastKnownPost: function(postData) {
		if (postData) {
			Ti.API.log('api', 'Setting last known post to: '+JSON.stringify(postData));
			cache.posts.last_id = postData.id;
			cache.posts.last_time = postData.published_at;
			cache.posts.last_id_entity = postData.entity;
		}
	},

	setLastKnownMention: function(postData) {
		if (postData) {
			Ti.API.log('api', 'Setting last known mention to: '+JSON.stringify(postData));
			cache.mentions.last_id = postData.id;
			cache.mentions.last_time = postData.published_at;
			cache.mentions.last_id_entity = postData.entity;
		}
	},

	posts_GET: function(args) {
		Ti.API.debug('Starting GET /posts');
		checkNetwork({
			online: function(){
				Ti.API.debug('posts_GET args: '+JSON.stringify(args));
				if (!cache.app_auth) {
					Ti.API.warn("App auth keys are not set yet. Will return user's own public posts.");
				}

				var xhr = args.xhr,
					url =  new URI(args.url),
					filters = _.extend({limit: 10}, (args.filters || {})),
					i = 0,  // index of server to hit
					isMentions = false,
					authHeader = null;

				// TODO: add support for more post types
				var posttypes = api.activePostTypes();
				filters = _.extend({
					post_types: posttypes.join(',')
				}, filters);

				if (filters !== {}) {
					Ti.API.debug('Filter found. Encoding them into the url.');
					for (var filter in filters) {
						if (filters.hasOwnProperty(filter)) {
							Ti.API.debug(filter +' > '+ encodeURIComponent(filters[filter]));
							url.addQueryParam(filter, encodeURIComponent(filters[filter]));
						}
					}
				}

				if (args.auth) {
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('GET', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');
				}

				Ti.API.debug('GET: '+url.toString());
				if (filters) Ti.API.debug('Filters: '+JSON.stringify(filters));
				if (args.auth) Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(url.toString());
				xhr.open('GET', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				if (args.auth) xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	stream_posts_GET: function(filters, callback) {
		// accepted filters:
		// since_id		Posts made since a specific post identifier.
		// before_id	Posts made before a specific post identifier.
		// since_time	Posts with published_at greater than the specified unix epoch.
		// before_time	Posts with published_at less than the specified unix epoch.
		// entity		Posts published by the specified entity.
		// post_types	Posts that match specific comma-separated type URIs.
		// limit		The number of posts to return (defaults to the maximum of 200).
		// mentioned_entity
		//
		Ti.API.debug('Starting GET STREAM /posts');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = apiRootUrls[i]+'/posts',
					isMentions = (filters && filters.mentions);

				// if (filters && filters.mentions) {
				// 	isMentions = filters.mentions;
				// 	delete filters.mentions;
				// }

				if (this.getApiRootUrls().length === 0) {
					Ti.API.warn("Entity root URLs are not set. Stopping API call.");
					Ti.App.fireEvent('api:posts_GET:success', {posts: []});
					return;
				}

				var newFilters;
				if (isMentions && cache.mentions.last_id && !filters.since_id) {
					newFilters = {since_id: cache.mentions.last_id, since_id_entity: cache.mentions.last_id_entity};
					filters = ( filters? _.extend(newFilters, _.omit(filters, 'mentions')) : newFilters );
				}
				else if (!isMentions && cache.posts.last_id && !filters.since_id) {
					newFilters = {since_id: cache.posts.last_id, since_id_entity: cache.posts.last_id_entity};
					filters = ( filters? _.extend(newFilters, filters) : newFilters );
				}

				var xhr = new XHR({
					onload: function(resp){
						var postsLen = resp.json.length;
						Ti.API.debug('Received '+ postsLen +' posts.');
						if (callback && callback.onload) callback.onload({posts: resp.json});
						else {
							var eventName = isMentions? 'api:mentions_GET:success' : 'api:posts_GET:success';
							Ti.App.fireEvent(eventName, {posts: resp.json});
						}
					},
					onerror: function(resp) {
						if (callback && callback.onerror) callback.onerror(resp);
						else {
							var eventName = isMentions? 'api:mentions_GET:error' : 'api:posts_GET:error';
							Ti.App.fireEvent(eventName, resp);
						}
					},
					noJson: false
				});

				this.posts_GET({
					xhr: xhr,
					filters: filters,
					url: url,
					auth: true
				});
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	entity_posts_GET: function(filters, callback) {
		Ti.API.debug('Starting GET entity/posts');
		checkNetwork({
			online: function(){
				var api_posts_GET = this.posts_GET;

				this.discover(filters.entity, {
					onload: function(){
						var xhr = new XHR({
							onload: function(resp){
								if (callback) callback.onload({posts: resp.json});
								else Ti.App.fireEvent('api:entity_posts_GET:success', {posts: resp.json});
							},
							onerror: function(resp) {
								if (callback) callback.onerror(resp);
								else Ti.App.fireEvent('api:entity_posts_GET:error', resp);
							},
							noJson: false
						});


						var i = 0,  // index of server to hit
							apiRootUrls = this.getApiRootUrls(filters.entity),
							url = apiRootUrls[i]+'/posts';

						// delete filters.entity;

						api_posts_GET({
							xhr: xhr,
							url: url,
							filters: _.omit(filters, 'entity')
						});
					},
					onerror: function(resp) {
						if (callback) callback.onerror(resp);
						else Ti.App.fireEvent('api:entity_posts_GET:error', resp);
					}
				});
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	entity_post_GET: function(args) {
		Ti.API.debug('Starting GET entity/post');
		var that = this;
		checkNetwork({
			online: function(){
				Ti.API.debug('entity_post_GET args: '+JSON.stringify(args));
				Ti.API.debug('args: '+JSON.stringify(args));
				var entity = args.entity,
					post_id = args.post_id,
					entity_profile = null,
					callback = args.callback,
					api_posts_GET = this.posts_GET;

				this.discover(entity, {
					onload: function(){
						entity_profile = cache.discover[entity];

						Ti.API.debug('entity_profile: '+JSON.stringify(entity_profile));
						if (entity_profile.core.servers) {
							var post_url = entity_profile.core.servers[0] +'/posts/'+ post_id; // TODO: support more than 1 server

							var xhr = new XHR({
								onload: function(resp){
									if (callback && callback.onload) callback.onload(resp.json);
									else Ti.App.fireEvent('api:entity_post_GET:success', resp.json);
								},
								onerror: function(resp) {
									if (resp.status === 404) {
										var i = 0,  // index of server to hit
											apiRootUrls = that.getApiRootUrls();

										// construct url for private posts
										post_url = apiRootUrls[i]+'/posts/'+encodeURIComponent(entity)+'/'+post_id;

										Ti.API.warn('Trying with this URL: '+post_url);

										var xhr = new XHR({
											onload: function(resp){
												if (callback && callback.onload) callback.onload(resp.json);
												else Ti.App.fireEvent('api:entity_post_GET:success', resp.json);
											},
											onerror: function(resp) {
												if (callback && callback.onerror) callback.onerror(JSON.stringify(resp));
												else Ti.App.fireEvent('api:entity_post_GET:error', resp);
											},
											noJson: false
										});

										api_posts_GET({
											xhr: xhr,
											url: post_url,
											auth: true
										});
									}
									else if (callback && callback.onerror) callback.onerror(JSON.stringify(resp));
									else Ti.App.fireEvent('api:entity_post_GET:error', resp);
								},
								noJson: false
							});

							api_posts_GET({
								xhr: xhr,
								url: post_url,
								auth: args.auth
							});
						}
						else if (callback && callback.onerror) callback.onerror({status: 404}); else Ti.App.fireEvent('api:entity_post_GET:error', {status: 404});
					},
					onerror: function(resp) {
						if (callback && callback.onerror) callback.onerror(resp);
						else Ti.App.fireEvent('api:entity_post_GET:error', resp);
					}
				});
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	deleted_post_GET: function(filters, callback) {
		Ti.API.debug('Grabbing DELETE posts');
		this.stream_posts_GET(
			_.extend({post_types: post_types.Delete}, filters),
			callback);
	},

	posts_POST: function(payload, callback){
		Ti.API.debug('Starting POST /posts');
		checkNetwork({
			online: function(){
				if (!cache.app_auth) {
					Ti.API.warn("App auth keys are not set yet. Will return user's own public posts.");
				}

				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/posts'),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('POST', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				// var setLastKnownPost = this.setLastKnownPost;

				var xhr = new XHR({
					onload: function(resp){
						var last_post = resp.json;
						// setLastKnownPost( last_post );
						if (callback && callback.onload) callback.onload({post: resp.json});
						else Ti.App.fireEvent('api:posts_POST:success', {post: resp.json});
					},
					onerror: function(resp){
						if (callback && callback.onerror) callback.onerror(resp);
						else Ti.App.fireEvent('api:posts_POST:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('POST: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);
				Ti.API.debug('Data: '+JSON.stringify(payload));
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('POST', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send(JSON.stringify(payload));
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	posts_DELETE: function(args, callback){
		Ti.API.debug('Starting DELETE /posts');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/posts/'+args.id),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('DELETE', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				var eventData = args.eventData;
				var xhr = new XHR({
					onload: function(resp){
						if (callback && callback.onload) callback.onload(eventData);
						else Ti.App.fireEvent('api:posts_DELETE', _.extend(eventData, {callback: 'success'}));
					},
					onerror: function(resp){
						if (resp.status === 404) {
							if (callback && callback.onload) callback.onload(eventData);
							else Ti.App.fireEvent('api:posts_DELETE', _.extend(eventData, {callback: 'success'}));
						}
						else {
							if (callback && callback.onerror) callback.onerror(resp);
							else Ti.App.fireEvent('api:posts_DELETE', _.extend(resp, {callback: 'error'}));
						}
					},
					noJson: false
				});

				Ti.API.debug('DELETE: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);

				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('DELETE', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	posts_COUNT: function(filters, callback){
		Ti.API.debug('Starting GET /posts/count');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls((filters && filters.entity)? filters.entity : null),
					url = new URI(apiRootUrls[i]+'/posts'),
					authHeader,
					checkingMe = (filters && filters.entity && filters.entity !== GLOBAL.entity) ? false: true;

				var posttypes = api.activePostTypes();
				filters = _.extend({
					post_types: posttypes.join(',')
				}, filters || {});

				_.each(_.pairs(_.omit(filters, 'entity')), function(pair){
					Ti.API.debug(pair[0] +' > '+ encodeURIComponent(pair[1]));
					url.addQueryParam(pair[0], encodeURIComponent(pair[1]));
				});

				// delete filters.entity;
				// for (var filter in filters) {
				// 	if (filters.hasOwnProperty(filter)) {
				// 		Ti.API.debug(filter +' > '+ encodeURIComponent(filters[filter]));
				// 		url.addQueryParam(filter, encodeURIComponent(filters[filter]));
				// 	}
				// }

				if (checkingMe) {
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('HEAD', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');
				}

				var xhr = new XHR({
					onload: function(resp){
						var _count = xhr.getResponseHeader( 'Count' );
						Ti.App.Properties.setString('posts_count', _count);
						if (callback && callback.onload) callback.onload( _count );
						else Ti.App.fireEvent('api:posts_COUNT:success', {count: _count});
					},
					onerror:  function(resp){
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:posts_COUNT:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('HEAD: '+url.toString());
				if (checkingMe) Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('HEAD', url.toString(), true);
				if (checkingMe) xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();

				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	posts_MINE: function(filters, callback) {
		Ti.API.debug('Starting GET /posts (mine)');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = apiRootUrls[i]+'/posts';

				var xhr = new XHR({
					onload: function(resp){
						var postsLen = resp.json.length;
						Ti.API.debug('Received '+ postsLen +' posts.');
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:posts_MINE:success', {posts: resp.json});
					},
					onerror: function(resp) {
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:posts_MINE:error', resp);
					},
					noJson: false
				});

				this.posts_GET({
					xhr: xhr,
					filters: _.extend({
						entity: GLOBAL.entity,
						limit: 10
					}, filters),
					url: url,
					auth: true
				});
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	posts_OLD: function(filters, callback) {
		Ti.API.debug('Starting GET /posts (old)');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = apiRootUrls[i]+'/posts';

				var xhr = new XHR({
					onload: function(resp){
						var postsLen = resp.json.length;
						Ti.API.debug('Received '+ postsLen +' posts.');
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:posts_OLD:success', {posts: resp.json});
					},
					onerror: function(resp) {
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:posts_OLD:error', resp);
					},
					noJson: false
				});

				this.posts_GET({
					xhr: xhr,
					filters: _.extend({
						limit: 10
					}, filters),
					url: url,
					auth: true
				});
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	mentions_GET: function(filters, callback){
		Ti.API.debug('Starting GET /mentions');
		var that = this;
		checkNetwork({
			online: function(){
				entity = (filters && filters.entity) ? filters.entity : GLOBAL.entity;
				that.discover(entity, {
					onload: function(discoverData){
						Ti.API.debug('cache.discover: '+JSON.stringify(discoverData));
						var mentionsFilters = {
							mentioned_entity: discoverData.core.entity,
							mentions: true,
							eventName: 'mentions_GET'
						};
						if (filters) _.extend(mentionsFilters, filters);
						that.stream_posts_GET(mentionsFilters, callback);
					}
				});
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	mentions_OLD: function(filters, callback){
		Ti.API.debug('Starting GET /mentions (old)');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = apiRootUrls[i]+'/posts';

				var xhr = new XHR({
					onload: function(resp){
						var postsLen = resp.json.length;
						Ti.API.debug('Received '+ postsLen +' mentions.');
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:mentions_OLD:success', {posts: resp.json});
					},
					onerror: function(resp) {
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:mentions_OLD:error', resp);
					},
					noJson: false
				});

				this.posts_GET({
					xhr: xhr,
					filters: _.extend({
						limit: 10,
						mentioned_entity: GLOBAL.entity
					}, filters),
					url: url,
					auth: true
				});
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followings_GET: function(callback, entity, filters){
		Ti.API.debug('Starting GET /followings');
		var that = this;
		checkNetwork({
			online: function(){
				entity = entity || GLOBAL.entity;
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(entity),
					url = new URI(apiRootUrls[i]+'/followings'),
					authHeader = null,
					isMe = entity === GLOBAL.entity;

				if (filters) {
					for (var filter in filters) {
						if (filters.hasOwnProperty(filter)) {
							Ti.API.debug(filter +' > '+ encodeURIComponent(filters[filter]));
							url.addQueryParam(filter, encodeURIComponent(filters[filter]));
						}
					}
				}
				if (entity === GLOBAL.entity) {
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('GET', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');
				}

				var xhr = new XHR({
					onload: function(resp){
						// cache the profiles
						for (var i=resp.json.length, f; i--;) {
							f = resp.json[i];
							if (f.entity === entity) continue;

							Ti.API.debug('Saving profile dicovery -> '+JSON.stringify(f));
							if (f.profile) that.saveProfile(f.entity, {basic: f.profile[uri.basic], core: f.profile[uri.core]}, true);

							// check if this bond is mine and its cached
							if (isMe && cache.following.indexOf(f.entity) === -1) {
								cache.following.push(f.entity);
								cache.following_ext[f.entity] = f;
							}
						}
						Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
						Ti.App.Properties.setList('bonds:following', cache.following);
						Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext));

						// fire success
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:followings_GET:success', {followings: resp.json});
					},
					onerror:  function(resp){
						if (callback && callback.onerror) callback.onerror();
						else Ti.App.fireEvent('api:followings_GET:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('GET: '+url.toString());
				if (authHeader) Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('GET', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				if (authHeader) xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();

				if (callback && callback.onid) callback.onid(xhr.conId);
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followings_GET_ENTITY: function(entity, callback) {
		Ti.API.debug('Starting GET /followings/:entity');
		var that = this;
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/followings/'+encodeURIComponent(entity)),
					authHeader = null;

				var xhr = new XHR({
					onload: function(resp){
						// cache the profiles
						if (resp.json.profile) {
							that.saveProfile(resp.json.entity, {basic: resp.json.profile[uri.basic], core: resp.json.profile[uri.core]});
						}

						// check if this bond is mine and its cached
						if (cache.following.indexOf(resp.json.entity) === -1) {
							cache.following.push(resp.json.entity);
							Ti.App.Properties.setList('bonds:following', cache.following);
						}
						cache.following_ext[resp.json.entity] = resp.json;
						Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext));

						// fire success
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:followings_GET_ENTITY:success', {followings: resp.json});
					},
					onerror:  function(resp){
						if (callback && callback.onerror) callback.onerror();
						else Ti.App.fireEvent('api:followings_GET_ENTITY:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('GET: '+url.toString());
				if (authHeader) Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('GET', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				if (authHeader) xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();
				
				if (callback && callback.onid) callback.onid(xhr.conId);
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followings_POST: function(payload, callback) {
		Ti.API.debug('Starting POST /followings');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/followings'),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('POST', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				var xhr = new XHR({
					onload: function(resp){
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:followings_POST:success', resp.json);
					},
					onerror: function(resp){
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:followings_POST:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('POST: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);
				Ti.API.debug('Data: '+JSON.stringify(payload));
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('POST', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send(JSON.stringify(payload));
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followings_DELETE: function(payload, callback) {
		Ti.API.debug('Starting DELETE /followings');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/followings/'+payload.id),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('DELETE', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				var xhr = new XHR({
					onload: function(resp){
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:followings_DELETE:success', resp.json);
					},
					onerror: function(resp){
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:followings_DELETE:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('DELETE: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('DELETE', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followings_COUNT: function(entity, callback){
		Ti.API.debug('Starting HEAD /followings');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(entity || null),
					url = new URI(apiRootUrls[i]+'/followings');

				var xhr = new XHR({
					onload: function(resp){
						var _count = xhr.getResponseHeader( 'Count' );
						Ti.App.Properties.setString('followings_count', _count);
						if (callback && callback.onload) callback.onload( _count );
						else Ti.App.fireEvent('api:followings_COUNT:success', {count: _count});
					},
					onerror:  function(resp){
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:followings_COUNT:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('HEAD: '+url.toString());
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('HEAD', url.toString(), true);
				xhr.send();
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followers_GET: function(callback, entity, filters){
		Ti.API.debug('Starting GET /followers');
		var that = this;
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(entity || null),
					url = new URI(apiRootUrls[i]+'/followers'),
					authHeader;

				if (filters) {
					for (var filter in filters) {
						if (filters.hasOwnProperty(filter)) {
							Ti.API.debug(filter +' > '+ encodeURIComponent(filters[filter]));
							url.addQueryParam(filter, encodeURIComponent(filters[filter]));
						}
					}
				}

				if (entity === GLOBAL.entity) {
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('GET', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');
				}

				var xhr = new XHR({
					onload: function(resp){
						// cache the profiles
						for (var i=resp.json.length; i--;) {
							var f = resp.json[i];
							if (f.profile) that.saveProfile(f.entity, {basic: f.profile[uri.basic], core: f.profile[uri.core]}, true);
						}
						Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));

						// cache it
						if (entity === GLOBAL.entity) {
							cache.followers = resp.json;
							Ti.App.Properties.setString('bonds:followers', JSON.stringify(resp.json));
						}

						// fire success
						if (callback && callback.onload) callback.onload( resp.json );
						else Ti.App.fireEvent('api:followers_GET:success', {followers: resp.json});
					},
					onerror: function(resp){
						if (callback && callback.onerror) callback.onerror( resp.json );
						else Ti.App.fireEvent('api:followers_GET:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('GET: '+url.toString());
				if (authHeader) Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('GET', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				if (authHeader) xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();

				if (callback && callback.onid) callback.onid(xhr.conId);
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	followers_COUNT: function(entity, callback){
		Ti.API.debug('Starting HEAD /followers');
		checkNetwork({
			online: function(){
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(entity || null),
					url = new URI(apiRootUrls[i]+'/followers');

				var xhr = new XHR({
					onload: function(resp){
						var _count = xhr.getResponseHeader( 'Count' );
						Ti.App.Properties.setString('followers_count', _count);
						if (callback && callback.onload) callback.onload( _count );
						else Ti.App.fireEvent('api:followers_COUNT:success', {count: _count});
					},
					onerror:  function(resp){
						if (callback && callback.onerror) callback.onerror( resp );
						else Ti.App.fireEvent('api:followers_COUNT:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('HEAD: '+url.toString());
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('HEAD', url.toString(), true);
				xhr.send();
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	apps_GET: function(args) {
		Ti.API.debug('Starting GET /apps');
		var that = this;
		checkNetwork({
			online: function(){
				if (!cache.app_auth) {
					Ti.API.warn('cache.app_auth is no set. Stopping apps_GET api call.');
					return;
				}
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/apps/'+cache.app_auth.id),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('GET', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				var callback = (args && args.callback)? args.callback : null;

				var xhr = new XHR({
					onload: function(resp){
						if (resp.status === 200) {
							if (!resp.json.authorizations.length) {
								if (callback) callback('unauthorized');
								else Ti.App.fireEvent('api:apps_GET:unauthorized');
							}
							else {
								try {
									var utils = require('lib/utils'),
										thisAppAuth = cache.getAppAuth,
										serverAppAuth = resp.json.authorizations[0];

									Ti.API.debug('Matching authorization info:');
									Ti.API.debug(JSON.stringify(thisAppAuth));
									Ti.API.debug(JSON.stringify(serverAppAuth));

									if (api.matchAppInfo(resp.json) && _.isEqual(thisAppAuth.scope.split(','), serverAppAuth.scopes) &&
										_.isEqual(thisAppAuth.tent_post_types.split(','), serverAppAuth.post_types) &&
										_.isEqual(thisAppAuth.tent_profile_info_types.split(','), serverAppAuth.profile_info_types) &&
										thisAppAuth.tent_notification_url === serverAppAuth.notification_url) 
									{
									// if (api.matchAppInfo(resp.json) && _.isEqual(thisAppAuth.scope.split(','), serverAppAuth.scopes) &&
									// 	thisAppAuth.tent_notification_url === serverAppAuth.notification_url) 
									// {
										if (callback) callback('success');
										else Ti.App.fireEvent('api:apps_GET:success', resp.json);
									}
									else {
										if (callback) callback('reauth');
										else Ti.App.fireEvent('api:apps_GET:reauth', resp.json);
									}
								} catch (err) { Ti.API.error(err); }
							}
						}
						else if (resp.status === 403 || resp.status === 401) {
							Ti.App.fireEvent('api:apps_GET:unauthorized');
							if (callback) callback('unauthorized');
						}
						else {
							Ti.App.fireEvent('api:apps_GET:error', resp);
							if (callback) callback('error');
						}
					},
					onerror: function(resp){
						if (resp.status === 403 || resp.status === 401) {
							Ti.App.fireEvent('api:apps_GET:unauthorized');
							if (callback) callback('unauthorized');
						}
						else {
							Ti.App.fireEvent('api:apps_GET:error', resp);
							if (callback) callback('error');
						}
					},
					noJson: false
				});
				
				Ti.API.debug('GET: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);
				
				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('GET', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();
				
				return xhr.conId;
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},

	apps_PUT: function(payload, callback) {
		Ti.API.debug('Starting PUT /apps');
		checkNetwork({
			online: function(){
				if (!cache.app_auth) {
					Ti.API.warn('cache.app_auth is no set. Stopping apps_PUT api call.');
					return;
				}
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/apps/'+cache.app_auth.id),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('PUT', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				var xhr = new XHR({
					onload: function(resp){
						Ti.App.fireEvent('api:apps_PUT:success', resp);
						if (callback) callback.onload(resp.json);
					},
					onerror: function(resp){
						Ti.App.fireEvent('api:apps_PUT:error', resp);
						if (callback) callback.onerror(resp);
					},
					noJson: false
				});
				
				Ti.API.debug('PUT: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);
				Ti.API.debug('Data: '+JSON.stringify(payload));

				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('PUT', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send(JSON.stringify(payload));
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	},
	apps_DELETE: function(callback) {
		Ti.API.debug('Starting DELETE /apps');
		checkNetwork({
			online: function(){
				if (!cache.app_auth) {
					Ti.API.warn('cache.app_auth is no set. Stopping apps_DELETE api call.');
					return;
				}
				var i = 0,  // index of server to hit
					apiRootUrls = this.getApiRootUrls(),
					url = new URI(apiRootUrls[i]+'/apps/'+cache.app_auth.id),
					authHeader = (cache.app_auth? macauth.getAuthorizationHeader('DELETE', url.toString(), url.host(), (url.port() || (url.protocol() === 'https'? 443:80)), {
						id: cache.app_auth.mac_key_id,
						key: cache.app_auth.mac_key,
						algorithm: cache.app_auth.mac_algorithm,
						issued: (new Date()).getTime()
					}) : '');

				var xhr = new XHR({
					onload: function(resp){
						cache.app_auth = null;
						Ti.App.Properties.removeProperty('app_auth');
						if (callback) callback.onload(resp.json);
						else Ti.App.fireEvent('api:apps_DELETE:success', resp);
					},
					onerror: function(resp){
						if (callback) callback.onerror(resp);
						else Ti.App.fireEvent('api:apps_DELETE:error', resp);
					},
					noJson: false
				});
				
				Ti.API.debug('DELETE: '+url.toString());
				Ti.API.debug('Authorization: '+authHeader);

				// Clears any cookies stored for the host
				xhr.clearCookies(apiRootUrls[i]);
				xhr.open('DELETE', url.toString(), true);
				xhr.setRequestHeader('Content-Type', tentMimeType);
				xhr.setRequestHeader('Accept', tentMimeType);
				xhr.setRequestHeader('Authorization', authHeader);
				xhr.send();
			},
			offline: function(){ alert('Looks like you are offline.'); }
		});
	}
};



// function initOAuthCallback(e){ api.oauthApp(); }
// Ti.App.addEventListener('api:oauth:init', initOAuthCallback);

// function initGrabTokenCallback(e){ api.grabToken(e.secret_code); }
// Ti.App.addEventListener('api:grabToken:init', initGrabTokenCallback);

module.exports = api;