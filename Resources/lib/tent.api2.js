var API, URI, XHR, appName, app_notification_post_types, app_required_scopes, app_supported_post_types, cache, cacheLib, cachePropName, cancelXHR, checkNetwork, content_types, defaultCache, hawk, post_types, saveCache, tempCache, tentMimeType, timeout, uri, utils, _,
  __hasProp = {}.hasOwnProperty,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

_ = require('lib/vendor/underscore-min');

URI = require('lib/vendor/jsuri.min');

cacheLib = require('lib/cache');

hawk = require('lib/vendor/hawk/hawk');

utils = require('lib/utils');

appName = GLOBAL.appName;

timeout = 15;

tentMimeType = 'application/vnd.tent.post.v0+json; charset=utf-8';

uri = {
  redirect: 'trion://tentauth',
  tent_notification_url: 'http://apn.tentrapp.com',
  app_url: "https://itunes.apple.com/us/app/trion/id589295052",
  url_template: /\{(.+?)\}/g
};

content_types = {
  Post: "application/vnd.tent.post.v0+json",
  Mentions: "application/vnd.tent.post-mentions.v0+json",
  Children: "application/vnd.tent.post-children.v0+json",
  Feed: "application/vnd.tent.posts-feed.v0+json",
  Versions: "application/vnd.tent.post-versions.v0+json",
  All: "*/*"
};

post_types = {
  "App": "https://tent.io/types/app/v0",
  "AppAuthorization": "https://tent.io/types/app-auth/v0",
  "Credentials": "https://tent.io/types/credentials/v0",
  "Status": "https://tent.io/types/status/v0",
  "Essay": "https://tent.io/types/essay/v0",
  "Photo": "https://tent.io/types/photo/v0",
  "Album": "https://tent.io/types/album/v0",
  "Repost": "https://tent.io/types/repost/v0",
  "Profile": "https://tent.io/types/profile/v0",
  "Delete": "https://tent.io/types/delete/v0",
  "Follower": "https://tent.io/types/follower/v0",
  "Following": "https://tent.io/types/following/v0",
  "Group": "https://tent.io/types/group/v0",
  "Favorite": "https://tent.io/types/favorite/v0",
  "Tag": "https://tent.io/types/tag/v0",
  "Cursor": "https://tent.io/types/cursor/v0",
  "Relationship": "https://tent.io/types/relationship/v0",
  "Subscription": "https://tent.io/types/subscription/v0",
  "DeliveryFailure": "https://tent.io/types/delivery-failure/v0"
};

post_types.postTypeProperties = {};

post_types.postTypeProperties[post_types.Status] = {
  textLimit: 256
};

app_supported_post_types = {
  "read": [post_types.App, post_types.App, post_types.Follower],
  "write": [post_types.Profile, post_types.Status, post_types.Repost, post_types.Photo, post_types.Following, post_types.Delete, post_types.Group]
};

app_notification_post_types = [post_types.Status, post_types.Repost, post_types.Photo, post_types.Follower];

app_required_scopes = ["permissions"];

cachePropName = 'apiCache';

defaultCache = {
  discover: {},
  avatars: {},
  uris: [],
  registerApp: null,
  appRegistered: false,
  oauth: false,
  access_token: null,
  token_type: "https://tent.io/oauth/hawk-token",
  credentials: {},
  posts: {
    last_id: null,
    last_time: null,
    last_id_entity: null
  },
  mentions: {
    last_id: null,
    last_time: null,
    last_id_entity: null
  }
};

cache = _.defaults(cacheLib.loadCache(cachePropName), defaultCache);

tempCache = {
  conQ: {
    id: 0,
    q: {}
  }
};

saveCache = function() {
  return cacheLib.saveCache(cachePropName, cache);
};

checkNetwork = function(callback) {
  Ti.API.debug("Checking if network is available ...");
  if (Ti.Network.online) {
    Ti.API.debug("... device is online");
    return callback.online.apply(callback["this"]);
  } else {
    Ti.API.warn("... device is offline");
    if (callback.offline) {
      return callback.offline.apply(callback["this"]);
    }
  }
};

XHR = function(method, url, args) {
  var header, myId, value, xhr, _ref;
  tempCache.conQ.id += 1;
  myId = tempCache.conQ.id;
  xhr = Ti.Network.createHTTPClient({
    conId: myId,
    timeout: timeout * 1000,
    tlsVersion: Ti.Network.TLS_VERSION_1_2,
    withCredentials: true,
    onload: function(e) {
      var responseParsed;
      Ti.API.log("xhr::" + this.conId, this.statusText);
      if (tempCache.conQ.q[this.conId]) {
        tempCache.conQ.q[this.conId] = null;
        delete tempCache.conQ.q[this.conId];
        Ti.API.log("xhr::" + this.conId, "HTTP Server-Authorization: " + (this.getResponseHeader('Server-Authorization')));
        Ti.API.log("xhr::" + this.conId, "Received text: " + this.responseText);
        responseParsed = args.noJson ? '' : JSON.parse(this.responseText || '{}');
        if (args.onload) {
          args.onload.call(args["this"] || this, {
            json: responseParsed,
            raw: this.responseText,
            data: this.responseData,
            status: this.status
          });
        }
      }
      return args.onload = null;
    },
    onerror: function(e) {
      Ti.API.log("xhr::" + this.conId, this.statusText);
      Ti.API.error("" + e.status + " " + this.responseText);
      if (tempCache.conQ.q[this.conId]) {
        tempCache.conQ.q[this.conId] = null;
        delete tempCache.conQ.q[this.conId];
        if (args.onerror) {
          args.onerror.call(args["this"] || this, {
            status: this.status,
            error: this.responseText
          });
        }
      }
      return args.onerror = null;
    },
    onreadystatechange: function() {
      return Ti.API.log("xhr::" + this.conId, "Ready state: " + ['UNSENT', 'OPENED', 'HEADERS_RECEIVED', 'LOADING', 'DONE'][this.readyState]);
    }
  });
  xhr.open(method, url, args.async || true);
  if (args.headers) {
    _ref = args.headers;
    for (header in _ref) {
      if (!__hasProp.call(_ref, header)) continue;
      value = _ref[header];
      xhr.setRequestHeader(header, value);
    }
  }
  tempCache.conQ.q[myId] = xhr;
  Ti.API.log("xhr::" + xhr.conId, "" + method + ": " + url);
  Ti.API.log("xhr::" + xhr.conId, "HEADER: " + (JSON.stringify(args.headers)));
  return tempCache.conQ.q[myId];
};

cancelXHR = function(id) {
  var _i, _id, _len, _results;
  Ti.API.log('api', "cancelXHR <- " + (JSON.stringify(id)));
  if (id) {
    if (typeof id === 'number' && tempCache.conQ.q[id]) {
      Ti.API.warn('Terminating XHR with ID: ' + id);
      tempCache.conQ.q[id].abort();
      return delete tempCache.conQ.q[id];
    } else {
      _results = [];
      for (_i = 0, _len = id.length; _i < _len; _i++) {
        _id = id[_i];
        if (tempCache.conQ.q[_id]) {
          Ti.API.warn('Terminating XHR with IDs: ' + _id);
          tempCache.conQ.q[_id].abort();
          _results.push(delete tempCache.conQ.q[_id]);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    }
  }
};

API = (function() {
  function API() {
    this.followings_GET = __bind(this.followings_GET, this);
    this.followings_COUNT = __bind(this.followings_COUNT, this);
    this.followers_GET = __bind(this.followers_GET, this);
    this.followers_COUNT = __bind(this.followers_COUNT, this);
    this.deleted_post_GET = __bind(this.deleted_post_GET, this);
    this.mentions_GET = __bind(this.mentions_GET, this);
    this.stream_posts_GET = __bind(this.stream_posts_GET, this);
    this.posts_GET = __bind(this.posts_GET, this);
    this.posts_COUNT = __bind(this.posts_COUNT, this);
    this.apps_GET = __bind(this.apps_GET, this);
    this.grabToken = __bind(this.grabToken, this);
    this.oauthApp = __bind(this.oauthApp, this);
    this.registerApp = __bind(this.registerApp, this);
    this.discover = __bind(this.discover, this);
    this.doHeaderDiscovery = __bind(this.doHeaderDiscovery, this);
    this.generateAuthHeader = __bind(this.generateAuthHeader, this);
  }

  API.prototype.XHR = XHR;

  API.prototype.cancelXHR = cancelXHR;

  API.prototype.checkNetwork = checkNetwork;

  API.prototype.execIfOnline = function(execThis) {
    return checkNetwork({
      online: execThis,
      offline: function() {
        return alert('Looks like you are offline.');
      }
    });
  };

  API.prototype.tempCache = function() {
    return tempCache;
  };

  API.prototype.getCache = function(req) {
    return cache[req];
  };

  API.prototype.clearAuthCache = function() {
    cache.registerApp = null;
    cache.credentials = {};
    return cache.secret_code = null;
  };

  API.prototype.isAuthenticated = function() {
    if (cache.registerApp) {
      if (cache.access_token && cache.appRegistered && cache.oauth && cache.registerApp.id) {
        return true;
      } else if (cache.access_token && cache.registerApp.mac_key_id && cache.registerApp.mac_key && cache.registerApp.id) {
        cache.appRegistered = true;
        cache.oauth = true;
        saveCache();
        return true;
      }
    }
    return false;
  };

  API.prototype.getHawkCredentials = function() {
    var ret;
    ret = {
      id: cache.credentials.id,
      key: cache.credentials.content.hawk_key,
      algorithm: cache.credentials.content.hawk_algorithm
    };
    return ret;
  };

  API.prototype.generateAuthHeader = function(method, url, contentType, payload) {
    if (contentType == null) {
      contentType = null;
    }
    if (payload == null) {
      payload = null;
    }
    return hawk.client.header(url, method, {
      credentials: this.getHawkCredentials(),
      contentType: contentType,
      payload: payload,
      app: cache.registerApp.id
    }).field;
  };

  API.prototype.postType = function(type) {
    return post_types[type];
  };

  API.prototype.activePostTypes = function() {
    return [post_types.Status, post_types.Repost, post_types.Photo];
  };

  API.prototype.getAppInfo = function() {
    return {
      type: post_types.App + "#",
      content: {
        name: appName,
        description: "Lets you shoot amazing posts from your iOS device to the Tent community.",
        url: uri.app_url,
        types: app_supported_post_types,
        redirect_uri: uri.redirect,
        notification_url: uri.tent_notification_url,
        notification_types: app_notification_post_types,
        scopes: app_required_scopes
      },
      permissions: {
        "public": false
      }
    };
  };

  API.prototype.getApiRootUrls = function(entity) {
    var ret;
    entity = entity || GLOBAL.entity;
    ret = cache.discover[entity] && cache.discover[entity].servers ? cache.discover[entity].servers[0].urls : {};
    return ret;
  };

  API.prototype.clearDiscoveryCache = function() {
    var me;
    Ti.API.log('api', 'Clearing discovery cache.');
    me = cache.discover[GLOBAL.entity];
    cache.discover = {};
    cache.discover[GLOBAL.entity] = me;
    saveCache();
    Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
    return Ti.API.debug("cache.discover: " + (JSON.stringify(cache.discover)));
  };

  API.prototype.doHeaderDiscovery = function(defined_uri, headerLink, callback) {
    var match, re_match, url, xhr;
    Ti.API.log('api', "doHeaderDiscovery <- " + defined_uri + ", " + headerLink);
    re_match = /<([^>]*)>\; rel\=\"https\:\/\/tent.io\/rels\/meta-post\"/ig;
    while ((match = re_match.exec(headerLink))) {
      if (match[1].match(/(https?:\/\/[^>]*)/i)) {
        cache.uris[defined_uri].push(match[1]);
      } else {
        cache.uris[defined_uri].push(defined_uri + match[1]);
      }
    }
    saveCache();
    url = cache.uris[defined_uri][0];
    xhr = new XHR('GET', url, {
      onload: function(resp) {
        var attachments, discoveredData;
        Ti.API.debug('Discovery finish successfuly. Saving info.');
        discoveredData = resp.json.post.content;
        attachments = resp.json.post.attachments;
        cache.discover[discoveredData.entity] = discoveredData;
        if (attachments && attachments.length) {
          cache.avatars[discoveredData.entity] = _.findWhere(attachments, {
            category: 'avatar'
          });
        }
        saveCache();
        Ti.API.debug('firing callback / event');
        if (callback) {
          return callback.onload.apply(callback.thiz || this, callback.data ? [discoveredData, callback.data] : [discoveredData]);
        } else {
          return Ti.App.fireEvent('api:discover:success', discoveredData);
        }
      },
      onerror: function(resp) {
        if (callback) {
          return callback.onerror.apply(callback.thiz || this, callback.data ? [resp, callback.data] : [resp]);
        } else {
          return Ti.App.fireEvent('api:discover:failed');
        }
      },
      headers: {
        'Accept': tentMimeType
      }
    });
    xhr.send();
  };

  API.prototype.discover = function(defined_uri, callback, noCache) {
    var xhr;
    Ti.API.log('api', "discover <- " + defined_uri);
    if (!defined_uri) {
      return;
    }
    defined_uri = defined_uri.replace(/\/$/g, "");
    Ti.API.debug('Starting discovery on URI: ' + defined_uri);
    if (cache.discover[defined_uri] && !noCache) {
      Ti.API.debug('Discovery data found in cache. Firing success.');
      if (callback) {
        return callback.onload.apply(callback.thiz || this, callback.data ? [cache.discover[defined_uri], callback.data] : [cache.discover[defined_uri]]);
      } else {
        return Ti.App.fireEvent('api:discover:success', cache.discover[defined_uri]);
      }
    } else {
      cache.uris[defined_uri] = [];
      xhr = new XHR("HEAD", defined_uri, {
        onload: (function(_this) {
          return function(resp) {
            var headerLink;
            headerLink = xhr.getResponseHeader('Link');
            Ti.API.debug('HEADER Link: ' + headerLink);
            return _this.doHeaderDiscovery(defined_uri, headerLink, callback);
          };
        })(this),
        onerror: (function(_this) {
          return function(resp) {
            var headerLink;
            headerLink = xhr.getResponseHeader('Link');
            Ti.API.debug('HEADER Link: ' + headerLink);
            if (headerLink) {
              return _this.doHeaderDiscovery(defined_uri, headerLink, callback);
            } else if (callback) {
              return callback.onerror.apply(callback.thiz || _this, callback.data ? [resp, callback.data] : [resp]);
            } else {
              return Ti.App.fireEvent('api:discover:failed');
            }
          };
        })(this),
        noJson: true
      });
      Ti.API.debug('HEAD: ' + defined_uri);
      return xhr.send();
    }
  };

  API.prototype.registerApp = function(forceRegister) {
    Ti.API.debug('Starting app registration proccess.');
    return checkNetwork({
      "this": this,
      online: (function(_this) {
        return function() {
          var getAppCredentials, regFunc;
          Ti.API.debug(JSON.stringify(cache));
          if (cache.access_token === null || forceRegister) {
            Ti.API.debug('No access token found.');
            getAppCredentials = function(url) {
              var xhr;
              xhr = new XHR('GET', url, {
                onload: function(resp) {
                  cache.credentials = resp.json.post;
                  saveCache();
                  return _this.oauthApp.call(_this, cache);
                },
                headers: {
                  'Accept': tentMimeType
                }
              });
              xhr.clearCookies(url);
              return xhr.send();
            };
            regFunc = function() {
              var httpData, url, xhr;
              Ti.API.info('Registering app to tent service...');
              url = _this.getApiRootUrls().new_post;
              httpData = JSON.stringify(_this.getAppInfo());
              xhr = new XHR('POST', url, {
                onload: function(resp) {
                  Ti.API.debug('Data recieved from app register: ' + resp.raw);
                  cache.registerApp_headerLink = xhr.getResponseHeader('Link').match(/<([^>]*)>\; rel\=\"https?\:\/\/[\S\/]*"/i)[1];
                  cache.registerApp = resp.json.post;
                  cache.appRegistered = true;
                  return getAppCredentials(cache.registerApp_headerLink);
                },
                noJson: false,
                headers: {
                  'Content-Type': "" + tentMimeType + "; type=\"" + post_types.App + "#\"",
                  'Accept': tentMimeType
                }
              });
              Ti.API.debug('POST: ' + url);
              Ti.API.debug('Data: ' + httpData);
              xhr.clearCookies(url);
              return xhr.send(httpData);
            };
            if (cache.registerApp === null || forceRegister) {
              return regFunc();
            } else {
              Ti.API.debug('cache.registerApp: ' + JSON.stringify(cache.registerApp));
              Ti.API.info('App should be already registered with the tent service. Checking...');
              return _this.apps_GET({
                callback: function(result) {
                  switch (result) {
                    case "success":
                      Ti.API.debug('Server accepts this app.');
                      Ti.API.debug('OAuth proccess starting.');
                      return _this.oauthApp();
                    case "unauthorized":
                      Ti.API.warn('Server did not accept this app. Start registration.');
                      cache.access_token = null;
                      saveCache();
                      return regFunc();
                    default:
                      Ti.API.debug('Server error. Failing.');
                      return Ti.App.fireEvent('api:registerApp:error');
                  }
                }
              });
            }
          } else {
            return Ti.API.debug('Access token found. Skipping app auth and registration.');
          }
        };
      })(this),
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this)
    });
  };

  API.prototype.oauthApp = function(cacheData) {
    var _cache;
    this.cacheData = cacheData || cache;
    _cache = cacheData || cache;
    return checkNetwork({
      "this": this,
      online: (function(_this) {
        return function() {
          var url;
          if (_cache.registerApp === null) {
            return _this.registerApp();
          } else if (_cache.secret_code && cache.credentials.content) {
            Ti.API.info('Secret code found in cache. Skipping OAuth.');
            return _this.grabToken(_cache.secret_code);
          } else {
            Ti.API.info('OAuth app to tent service...');
            tempCache.oauthState = hawk.utils.nonce(32);
            url = "" + (_this.getApiRootUrls().oauth_auth) + "?client_id=" + cache.registerApp.id + "&state=" + tempCache.oauthState;
            Ti.API.info("Opening browser with url: " + url);
            return Ti.Platform.openURL(url);
          }
        };
      })(this),
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this)
    });
  };

  API.prototype.grabToken = function(secret_code) {
    var authHeader, contentType, httpData, httpMethod, url, xhr;
    Ti.API.debug('Exchanging secret code for permanet token.');
    cache.access_token = null;
    url = this.getApiRootUrls().oauth_token;
    httpMethod = 'POST';
    contentType = 'application/json';
    httpData = {
      code: secret_code,
      token_type: cache.token_type
    };
    authHeader = this.generateAuthHeader(httpMethod, url, contentType, httpData);
    xhr = new XHR(httpMethod, url, {
      onload: (function(_this) {
        return function(resp) {
          if (!resp.json) {
            return Ti.API.debug('Exchanging secrets failed.');
          } else {
            Ti.API.debug('Updating app auth keys.');
            cache.access_token = resp.json.access_token;
            cache.credentials.id = resp.json.access_token;
            cache.credentials.content.hawk_key = resp.json.hawk_key;
            saveCache();
            Ti.API.debug('We have the OAuth key now: ' + cache.credentials.id);
            return Ti.App.fireEvent('api:grabToken:success');
          }
        };
      })(this),
      onerror: (function(_this) {
        return function(resp) {
          Ti.API.debug('Exchanging secrets failed.');
          if (resp.status === 404) {
            _this.cacheData.secret_code = null;
            return _this.oauthApp(_this.cacheData);
          } else {
            return Ti.App.fireEvent('api:grabToken:failed');
          }
        };
      })(this),
      noJson: false,
      headers: {
        'Content-Type': contentType,
        'Accept': contentType,
        'Authorization': authHeader
      }
    });
    Ti.API.debug("" + httpMethod + ": " + url);
    Ti.API.debug('Authorization: ' + authHeader);
    Ti.API.debug('Data: ' + JSON.stringify(httpData));
    xhr.clearCookies(url);
    return xhr.send(JSON.stringify(httpData));
  };

  API.prototype.apps_GET = function(args) {
    Ti.API.debug('Starting GET /apps');
    return checkNetwork({
      "this": this,
      online: (function(_this) {
        return function() {
          var authHeader, callback, contentType, httpMethod, url, xhr;
          if (!cache.registerApp) {
            Ti.API.warn('cache.registerApp is no set. Stopping apps_GET api call.');
            return null;
          }
          url = "" + (_this.getApiRootUrls().posts_feed) + "?types=" + (encodeURIComponent(post_types.App)) + "&limit=1";
          httpMethod = 'GET';
          contentType = "" + content_types.Feed;
          authHeader = _this.generateAuthHeader(httpMethod, url);
          callback = args && args.callback ? args.callback : null;
          xhr = new XHR(httpMethod, url, {
            onload: function(resp) {
              var appIsOK, auth, err, localAppAuth, scope, serverAppAuth, _i, _j, _k, _l, _len, _len1, _len2, _len3, _ref, _ref1, _ref2, _ref3, _ref4;
              if (resp.status === 200) {
                if (!resp.json.posts.length) {
                  if (callback(callback('unauthorized'))) {

                  } else {
                    return Ti.App.fireEvent('api:apps_GET:unauthorized');
                  }
                } else {
                  try {
                    serverAppAuth = resp.json.posts[0].content;
                    localAppAuth = _this.getAppInfo().content;
                    appIsOK = true;
                    _ref = localAppAuth.types.read;
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                      auth = _ref[_i];
                      if (__indexOf.call(serverAppAuth.types.read, auth) < 0) {
                        appIsOK = false;
                      }
                    }
                    _ref1 = localAppAuth.types.write;
                    for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                      auth = _ref1[_j];
                      if (__indexOf.call(serverAppAuth.types.write, auth) < 0) {
                        appIsOK = false;
                      }
                    }
                    _ref2 = localAppAuth.scopes;
                    for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
                      scope = _ref2[_k];
                      if (__indexOf.call(serverAppAuth.scopes, scope) < 0) {
                        appIsOK = false;
                      }
                    }
                    _ref3 = localAppAuth.notification_types;
                    for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
                      scope = _ref3[_l];
                      if (__indexOf.call(serverAppAuth.notification_types, scope) < 0) {
                        appIsOK = false;
                      }
                    }
                    if (localAppAuth.name !== serverAppAuth.name) {
                      appIsOK = false;
                    }
                    if (localAppAuth.description !== serverAppAuth.description) {
                      appIsOK = false;
                    }
                    if (localAppAuth.url !== serverAppAuth.url) {
                      appIsOK = false;
                    }
                    if (localAppAuth.redirect_uri !== serverAppAuth.redirect_uri) {
                      appIsOK = false;
                    }
                    if (localAppAuth.notification_url !== serverAppAuth.notification_url) {
                      appIsOK = false;
                    }
                    Ti.API.info('Matching app authorization info...');
                    if (appIsOK) {
                      Ti.API.info("Authorization seems to be ok. Calling :success: ...");
                      if (callback) {
                        Ti.API.debug("Executing success callback ...");
                        return callback('success');
                      } else {
                        Ti.API.debug("Firing success event ...");
                        return Ti.App.fireEvent('api:apps_GET:success', resp.json);
                      }
                    } else {
                      Ti.API.info("Authorization is not ok. Calling :reauth: ...");
                      if (callback) {
                        return callback('reauth');
                      } else {
                        return Ti.App.fireEvent('api:apps_GET:reauth', resp.json);
                      }
                    }
                  } catch (_error) {
                    err = _error;
                    return Ti.API.error(err);
                  }
                }
              } else if ((_ref4 = resp.status) === 403 || _ref4 === 401) {
                Ti.App.fireEvent('api:apps_GET:unauthorized');
                if (callback) {
                  return callback('unauthorized');
                }
              } else {
                Ti.App.fireEvent('api:apps_GET:error', resp);
                if (callback) {
                  return callback('error');
                }
              }
            },
            onerror: function(resp) {
              var _ref;
              if ((_ref = resp.status) === 403 || _ref === 401) {
                Ti.App.fireEvent('api:apps_GET:unauthorized');
                if (callback) {
                  return callback('unauthorized');
                }
              } else {
                Ti.App.fireEvent('api:apps_GET:error', resp);
                if (callback) {
                  return callback('error');
                }
              }
            },
            noJson: false,
            headers: {
              'Accept': contentType,
              'Authorization': authHeader
            }
          });
          Ti.API.debug("" + httpMethod + ": " + url);
          Ti.API.debug('Accept: ' + contentType);
          Ti.API.debug('Authorization: ' + authHeader);
          xhr.clearCookies(url);
          xhr.send();
          return xhr.conId;
        };
      })(this),
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this)
    });
  };

  API.prototype.posts_COUNT = function(filters, callback) {
    Ti.API.log('api', 'posts_COUNT <- ' + JSON.stringify(filters));
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          Ti.API.log('api', 'posts_COUNT -- Looks like you are offline');
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var authHeader, checkingMe, filter, httpMethod, url, xhr, xhrArgs, _ref;
          url = new URI(_this.getApiRootUrls(filters && filters.entity ? filters.entity : null).posts_feed);
          httpMethod = 'HEAD';
          checkingMe = filters && filters.entity && filters.entity !== GLOBAL.entity ? false : true;
          filters = _.defaults(filters || {}, {
            types: _this.activePostTypes().join(',')
          });
          Ti.API.log('api', 'posts_GET -- Encoding filters into the url.');
          _ref = _.omit(filters, 'entity');
          for (filter in _ref) {
            if (!__hasProp.call(_ref, filter)) continue;
            url.addQueryParam(filter, encodeURIComponent(filters[filter]));
          }
          url = url.toString();
          xhrArgs = {
            "this": _this,
            onload: function(resp) {
              var _count;
              _count = xhr.getResponseHeader('Count');
              Ti.App.Properties.setString('posts_count', _count);
              if (callback && callback.onload) {
                return callback.onload(_count);
              } else {
                return Ti.App.fireEvent('api:posts_COUNT:success', {
                  count: _count
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:posts_COUNT:error', resp);
              }
            },
            headers: {
              'Accept': "" + content_types.Feed
            }
          };
          if (checkingMe) {
            authHeader = _this.generateAuthHeader(httpMethod, url);
            xhrArgs.Authorization = authHeader;
          }
          xhr = new XHR(httpMethod, url, xhrArgs);
          Ti.API.debug("" + httpMethod + ": " + url);
          if (filters) {
            Ti.API.debug('Filters: ' + JSON.stringify(filters));
          }
          if (authHeader) {
            Ti.API.debug('Authorization: ' + authHeader);
          }
          xhr.clearCookies(url);
          xhr.send();
          return xhr.conId;
        };
      })(this)
    });
  };

  API.prototype.posts_GET = function(args) {
    Ti.API.log('api', 'posts_GET <- ' + JSON.stringify(args));
    return checkNetwork({
      "this": this,
      online: (function(_this) {
        return function() {
          var authHeader, filter, filters, httpMethod, url, xhr, xhrArgs;
          url = new URI(_this.getApiRootUrls().posts_feed);
          httpMethod = 'GET';
          filters = _.defaults(args.filters || {}, {
            limit: 10,
            types: _this.activePostTypes().join(',')
          });
          Ti.API.log('api', 'posts_GET -- Encoding filters into the url.');
          for (filter in filters) {
            if (!__hasProp.call(filters, filter)) continue;
            url.addQueryParam(filter, encodeURIComponent(filters[filter]));
          }
          url = url.toString();
          xhrArgs = {
            "this": args["this"],
            onload: args.onload,
            onerror: args.onerror,
            headers: {
              'Accept': "" + content_types.Feed
            }
          };
          if (args.auth) {
            authHeader = _this.generateAuthHeader(httpMethod, url);
            xhrArgs.headers.Authorization = authHeader;
          }
          xhr = new XHR(httpMethod, url, xhrArgs);
          Ti.API.debug("" + httpMethod + ": " + url);
          if (filters) {
            Ti.API.debug('Filters: ' + JSON.stringify(filters));
          }
          if (args.auth) {
            Ti.API.debug('Authorization: ' + authHeader);
          }
          xhr.clearCookies(url);
          xhr.send();
          return xhr.conId;
        };
      })(this),
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this)
    });
  };

  API.prototype.posts_OLD = function(filters, callback) {
    Ti.API.log('api', 'posts_OLD <- ' + JSON.stringify(filters));
    return checkNetwork({
      online: (function(_this) {
        return function() {
          return _this.stream_posts_GET(filters, callback);
        };
      })(this),
      offline: function() {
        return alert('Looks like you are offline.');
      }
    });
  };

  API.prototype.posts_POST = function(payload, callback) {
    Ti.API.log('api', 'posts_POST <- ' + JSON.stringify(payload));
    return checkNetwork({
      online: (function(_this) {
        return function() {
          var attachments, blob, boundary, content, contentType, file, full_content, httpData, httpMethod, i, url, xhr, _i, _len;
          payload.type += "#";
          if (payload.isReply) {
            payload.type += "reply";
          }
          if (payload.isReply) {
            payload = _.omit(payload, 'isReply');
          }
          if (!payload.permissions.entities.length) {
            payload.permissions = _.omit(payload.permissions, 'entities');
          }
          if (!payload.mentions.length) {
            payload = _.omit(payload, 'mentions');
          }
          url = _this.getApiRootUrls().new_post;
          httpMethod = 'POST';
          if (payload.attachments) {
            Ti.API.log('api', 'posts_POST -- Payload has attachments. Creating multipart head...');
            attachments = payload.attachments;
            payload = _.omit(payload, 'attachments');
            httpData = JSON.stringify(payload);
            boundary = "-----------------------" + (utils.randomString(15));
            contentType = "multipart/form-data; boundary=\"" + boundary + "\"";
            content = '--' + boundary + '\r\n';
            content += 'Content-Disposition: form-data; name="post"; filename="post.json"\r\n';
            content += "Content-Type: " + content_types.Post + "; type=\"" + payload.type + "\"\r\n";
            content += "Content-Length: " + httpData.length + "\r\n";
            content += '\r\n';
            content += "" + httpData + "\r\n";
            full_content = Ti.createBuffer({
              value: content
            });
            for (i = _i = 0, _len = attachments.length; _i < _len; i = ++_i) {
              blob = attachments[i];
              file = Titanium.Filesystem.createTempFile();
              file.write(blob);
              content = '--' + boundary + '\r\n';
              content += "Content-Disposition: form-data; name=\"photo[" + i + "]\"; filename=\"" + file.name + "\"\r\n";
              content += "Content-Type: " + blob.mimeType + "\r\n";
              content += "Content-Length: " + file.size + "\r\n";
              content += "\r\n";
              full_content.append(Ti.createBuffer({
                value: content
              }));
              full_content.append(utils.toBuffer(blob));
              full_content.append(Ti.createBuffer({
                value: "\r\n"
              }));
            }
            content = '--' + boundary + '--\r\n';
            full_content.append(Ti.createBuffer({
              value: content
            }));
            content = full_content.toBlob();
          } else {
            content = JSON.stringify(payload);
            contentType = "" + content_types.Post + "; type=\"" + payload.type + "\"";
          }
          xhr = new XHR(httpMethod, url, {
            onload: function(resp) {
              var last_post;
              last_post = resp.json.post;
              if (callback && callback.onload) {
                return callback.onload({
                  post: last_post
                });
              } else {
                return Ti.App.fireEvent('api:posts_POST:success', {
                  post: last_post
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:posts_POST:error', resp);
              }
            },
            noJson: false,
            headers: {
              'Content-Type': contentType,
              'Authorization': _this.generateAuthHeader(httpMethod, url, contentType)
            }
          });
          Ti.API.debug('Data: ' + content);
          xhr.clearCookies(url);
          return xhr.send(content);
        };
      })(this),
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this)
    });
  };

  API.prototype.posts_DELETE = function(args, callback) {
    Ti.API.log('api', "posts_DELETE <- " + (JSON.stringify(args)));
    return checkNetwork({
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var contentType, eventData, httpMethod, url, xhr;
          url = _.template(_this.getApiRootUrls().post, {
            entity: encodeURIComponent(GLOBAL.entity),
            post: args.id
          }, {
            interpolate: uri.url_template
          });
          httpMethod = 'DELETE';
          contentType = "" + content_types.Post;
          eventData = args.eventData;
          xhr = new XHR(httpMethod, url, {
            onload: function(resp) {
              if (callback && callback.onload) {
                return callback.onload(eventData);
              } else {
                return Ti.App.fireEvent('api:posts_DELETE', _.extend(eventData, {
                  callback: 'success'
                }));
              }
            },
            onerror: function(resp) {
              if (resp.status === 404) {
                if (callback && callback.onload) {
                  return callback.onload(eventData);
                } else {
                  return Ti.App.fireEvent('api:posts_DELETE', _.extend(eventData, {
                    callback: 'success'
                  }));
                }
              } else {
                if (callback && callback.onerror) {
                  return callback.onerror(resp);
                } else {
                  return Ti.App.fireEvent('api:posts_DELETE', _.extend(resp, {
                    callback: 'error'
                  }));
                }
              }
            },
            noJson: false,
            headers: {
              'Content-Type': contentType,
              'Authorization': _this.generateAuthHeader(httpMethod, url, contentType)
            }
          });
          xhr.clearCookies(url);
          return xhr.send();
        };
      })(this)
    });
  };

  API.prototype.stream_posts_GET = function(filters, callback) {
    Ti.API.log('api', "stream_posts_GET <- " + (JSON.stringify(filters)));
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var apiRootUrls, isMentions, newFilters;
          apiRootUrls = _this.getApiRootUrls();
          isMentions = filters && filters.mentions;
          if (apiRootUrls === {}) {
            Ti.API.warn("stream_posts_GET -- Entity root URLs are not set. Stopping API call.");
            Ti.App.fireEvent('api:posts_GET:success', {
              posts: []
            });
            return;
          }
          if (isMentions && cache.mentions.last_id && !filters.since_id) {
            newFilters = {
              since_id: cache.mentions.last_id,
              since_id_entity: cache.mentions.last_id_entity
            };
            filters = filters ? _.extend(newFilters, _.omit(filters, 'mentions')) : newFilters;
          } else if (!isMentions && cache.posts.last_id && !filters.since_id) {
            newFilters = {
              since_id: cache.posts.last_id,
              since_id_entity: cache.posts.last_id_entity
            };
            filters = filters ? _.extend(newFilters, filters) : newFilters;
          }
          return _this.posts_GET({
            "this": _this,
            onload: function(resp) {
              var eventName, postsLen;
              postsLen = resp.json.posts.length;
              Ti.API.log('api', "stream_posts_GET -- Received " + postsLen + " posts.");
              if (callback && callback.onload) {
                return callback.onload({
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              } else {
                eventName = isMentions ? 'api:mentions_GET:success' : 'api:posts_GET:success';
                return Ti.App.fireEvent(eventName, {
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              }
            },
            onerror: function(resp) {
              var eventName;
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                eventName = isMentions ? 'api:mentions_GET:error' : 'api:posts_GET:error';
                return Ti.App.fireEvent(eventName, resp);
              }
            },
            filters: filters,
            auth: true
          });
        };
      })(this)
    });
  };

  API.prototype.mentions_GET = function(filters, callback) {
    Ti.API.log('api', "mentions_GET <- " + (JSON.stringify(filters)));
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var entity;
          entity = filters && filters.entity ? filters.entity : GLOBAL.entity;
          return _this.discover(entity, {
            onload: function(discoverData) {
              var mentionsFilters;
              Ti.API.debug('cache.discover: ' + JSON.stringify(discoverData));
              mentionsFilters = {
                mentions: entity,
                eventName: 'mentions_GET'
              };
              if (filters) {
                _.extend(mentionsFilters, filters);
              }
              return _this.stream_posts_GET(mentionsFilters, callback);
            }
          });
        };
      })(this)
    });
  };

  API.prototype.deleted_post_GET = function(filters, callback) {
    Ti.API.log('api', "deleted_post_GET <- " + (JSON.stringify(filters)));
    return checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          return _this.stream_posts_GET(_.extend({
            types: post_types.Delete
          }, filters), callback);
        };
      })(this)
    });
  };

  API.prototype.entity_posts_GET = function(filters, callback) {
    Ti.API.log('api', "entity_posts_GET <- " + (JSON.stringify(filters)));
    checkNetwork({
      offline: (function(_this) {
        return function() {
          Ti.API.log('api', 'entity_posts_GET -- Looks like you are offline');
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var api_posts_GET;
          api_posts_GET = _this.posts_GET;
          return _this.discover(filters.entities, {
            onload: function() {
              return _this.posts_GET({
                "this": _this,
                onload: function(resp) {
                  if (callback && callback.onload) {
                    return callback.onload({
                      posts: resp.json.posts,
                      pages: resp.json.pages
                    });
                  } else {
                    return Ti.App.fireEvent('api:entity_posts_GET:success', {
                      posts: resp.json
                    });
                  }
                },
                onerror: function(resp) {
                  if (callback && callback.onerror) {
                    return callback.onerror(resp);
                  } else {
                    return Ti.App.fireEvent('api:entity_posts_GET:error', resp);
                  }
                },
                filters: filters,
                auth: true
              });
            },
            onerror: function(resp) {
              if (callback) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:entity_posts_GET:error', resp);
              }
            }
          });
        };
      })(this)
    });
  };

  API.prototype.entity_post_GET = function(args) {
    Ti.API.log('api', "entity_post_GET <- " + (JSON.stringify(args)));
    return checkNetwork({
      offline: (function(_this) {
        return function() {
          Ti.API.log('api', 'entity_post_GET -- Looks like you are offline');
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var callback, entity, entity_profile, post_id;
          entity = args.entity;
          post_id = args.post_id;
          entity_profile = null;
          callback = args.callback;
          return _this.discover(entity, {
            onload: function() {
              var authHeader, httpMethod, url, xhr, xhrArgs;
              entity_profile = cache.discover[entity];
              Ti.API.debug("entity_profile: " + (JSON.stringify(entity_profile)));
              url = _.template(_this.getApiRootUrls().post, {
                entity: encodeURIComponent(entity),
                post: post_id
              }, {
                interpolate: uri.url_template
              });
              httpMethod = 'GET';
              authHeader = _this.generateAuthHeader(httpMethod, url);
              xhrArgs = {
                "this": _this,
                onload: function(resp) {
                  if (callback && callback.onload) {
                    return callback.onload(resp.json.post);
                  } else {
                    return Ti.App.fireEvent('api:entity_post_GET:success', resp.json.post);
                  }
                },
                onerror: function(resp) {
                  if (callback && callback.onerror) {
                    return callback.onerror({
                      status: 404
                    });
                  } else {
                    return Ti.App.fireEvent('api:entity_post_GET:error', {
                      status: 404
                    });
                  }
                },
                headers: {
                  Accept: content_types.Post,
                  Authorization: authHeader
                }
              };
              xhr = new XHR(httpMethod, url, xhrArgs);
              xhr.clearCookies(url);
              return xhr.send();
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:entity_post_GET:error', resp);
              }
            }
          });
        };
      })(this)
    });
  };

  API.prototype.followers_COUNT = function(entity, callback) {
    Ti.API.log('api', 'followers_COUNT <- ' + entity);
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          Ti.API.log('api', 'followers_COUNT -- Looks like you are offline');
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var httpMethod, url, xhr;
          url = _this.getApiRootUrls(entity || null).posts_feed + ("?types=" + (encodeURIComponent(post_types.Follower)));
          httpMethod = 'HEAD';
          xhr = new XHR(httpMethod, url, {
            onload: function(resp) {
              var _count;
              _count = xhr.getResponseHeader('Count');
              if (entity === GLOBAL.entity) {
                Ti.App.Properties.setString('followers_count', _count);
              }
              if (callback && callback.onload) {
                return callback.onload(_count);
              } else {
                return Ti.App.fireEvent('api:followers_COUNT:success', {
                  count: _count
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:followers_COUNT:error', resp);
              }
            },
            headers: {
              'Accept': "" + content_types.Feed
            }
          });
          Ti.API.debug("" + httpMethod + ": " + url);
          xhr.clearCookies(url);
          xhr.send();
          return xhr.conId;
        };
      })(this)
    });
  };

  API.prototype.followers_GET = function(callback, entity, filters) {
    if (entity == null) {
      entity = GLOBAL.entity;
    }
    if (filters == null) {
      filters = null;
    }
    Ti.API.log('api', "followers_GET <- " + entity);
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          return _this.posts_GET({
            "this": _this,
            onload: function(resp) {
              var postsLen;
              postsLen = resp.json.posts.length;
              Ti.API.log('api', "followers_GET -- Received " + postsLen + " posts.");
              if (callback && callback.onload) {
                callback.onload({
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              } else {
                Ti.App.fireEvent('api:followers_GET:success', {
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:followers_GET:error', resp);
              }
            },
            filters: _.extend({
              types: post_types.Follower,
              entities: entity
            }, filters),
            auth: entity === GLOBAL.entity
          });
        };
      })(this)
    });
  };

  API.prototype.followings_COUNT = function(entity, callback) {
    Ti.API.log('api', 'followings_COUNT <- ' + entity);
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          Ti.API.log('api', 'followings_COUNT -- Looks like you are offline');
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          var httpMethod, url, xhr;
          url = _this.getApiRootUrls(entity || null).posts_feed + ("?types=" + (encodeURIComponent(post_types.Following)));
          httpMethod = 'HEAD';
          xhr = new XHR(httpMethod, url, {
            onload: function(resp) {
              var _count;
              _count = xhr.getResponseHeader('Count');
              if (entity === GLOBAL.entity) {
                Ti.App.Properties.setString('followings_count', _count);
              }
              if (callback && callback.onload) {
                return callback.onload(_count);
              } else {
                return Ti.App.fireEvent('api:followings_COUNT:success', {
                  count: _count
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:followings_COUNT:error', resp);
              }
            },
            headers: {
              'Accept': "" + content_types.Feed
            }
          });
          Ti.API.debug("" + httpMethod + ": " + url);
          xhr.clearCookies(url);
          xhr.send();
          return xhr.conId;
        };
      })(this)
    });
  };

  API.prototype.followings_GET = function(callback, entity, filters) {
    if (entity == null) {
      entity = GLOBAL.entity;
    }
    if (filters == null) {
      filters = null;
    }
    Ti.API.log('api', "followings_GET <- " + entity);
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          return _this.posts_GET({
            "this": _this,
            onload: function(resp) {
              var postsLen;
              postsLen = resp.json.posts.length;
              Ti.API.log('api', "followings_GET -- Received " + postsLen + " posts.");
              if (callback && callback.onload) {
                callback.onload({
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              } else {
                Ti.App.fireEvent('api:followings_GET:success', {
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:followings_GET:error', resp);
              }
            },
            filters: _.extend({
              types: post_types.Following,
              entities: entity
            }, filters),
            auth: entity === GLOBAL.entity
          });
        };
      })(this)
    });
  };

  API.prototype.getFollowing = function(uri) {
    Ti.API.debug('Getting following from cache: ' + uri);
    if (cache.following.indexOf(uri) > -1 && cache.following_ext.hasOwnProperty(uri)) {
      return cache.following_ext[uri];
    } else if (!cache.following_ext.hasOwnProperty(uri)) {
      Ti.API.warn('No cache data for "' + uri + '" in following_ext.');
    }
    return Ti.API.debug(JSON.stringify(cache.following_ext));
  };

  API.prototype.addFollowing = function(followData) {
    Ti.API.debug('Adding following to cache: ' + followData.entity);
    if (cache.following.indexOf(followData.entity) > -1) {
      return;
    }
    cache.following.push(followData.entity);
    Ti.App.Properties.setList('bonds:following', cache.following);
    cache.following_ext[followData.entity] = followData;
    return Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext));
  };

  API.prototype.removeFollowing = function(uri) {
    var index;
    Ti.API.debug('Removing following from cache: ' + uri);
    index = cache.following.indexOf(uri);
    if (index > -1) {
      cache.following.splice(index, 1);
      Ti.App.Properties.setList('bonds:following', cache.following);
      delete cache.following_ext[uri];
      return Ti.App.Properties.setString('bonds:following_ext', JSON.stringify(cache.following_ext));
    }
  };

  API.prototype.isFollowing = function(uri, checkOnline, callback) {
    var ret;
    Ti.API.log('api', "isFollowing <- " + uri);
    if (!checkOnline) {
      ret = cache.following.indexOf(uri) > -1;
      Ti.API.debug('Following entity ' + uri(+' > ' + ret));
      return ret;
    } else {
      return checkNetwork({
        online: (function(_this) {
          return function() {
            return _this.posts_GET({
              "this": _this,
              onload: function(resp) {
                var isFollowing, postsLen;
                postsLen = resp.json.posts.length;
                isFollowing = postsLen > 0;
                Ti.API.log('api', "isFollowing -- Received " + postsLen + " posts.");
                if (callback && callback.onload) {
                  return callback.onload(isFollowing);
                } else {
                  return Ti.App.fireEvent('api:isFollowing:success', isFollowing);
                }
              },
              onerror: function(resp) {
                if (callback && callback.onerror) {
                  return callback.onerror(resp);
                } else {
                  return Ti.App.fireEvent('api:isFollowing:error', resp);
                }
              },
              filters: {
                types: encodeURIComponent(post_types.Following),
                entity: encodeURIComponent(uri)
              },
              auth: true
            });
          };
        })(this),
        offline: function() {
          alert('Looks like you are offline.');
          if (callback && callback.offline) {
            return callback.offline();
          }
        }
      });
    }
  };

  API.prototype.isFollowed = function(uri) {
    var allFollowers, f, res, _i, _len;
    res = false;
    allFollowers = this.getCache('followers');
    for (_i = 0, _len = allFollowers.length; _i < _len; _i++) {
      f = allFollowers[_i];
      if (uri === f.entity) {
        res = true;
        break;
      }
    }
    return res;
  };

  API.prototype.favorites_GET = function(entity, filters, callback) {
    Ti.API.log('api', "favorites_GET <- " + (JSON.stringify(filters)));
    checkNetwork({
      "this": this,
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this),
      online: (function(_this) {
        return function() {
          return _this.posts_GET({
            "this": _this,
            onload: function(resp) {
              var postsLen;
              postsLen = resp.json.posts.length;
              Ti.API.log('api', "favorites_GET -- Received " + postsLen + " posts.");
              if (callback && callback.onload) {
                callback.onload({
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              } else {
                Ti.App.fireEvent('api:favorites_GET:success', {
                  posts: resp.json.posts,
                  pages: resp.json.pages
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:favorites_GET:error', resp);
              }
            },
            filters: _.extend({
              types: post_types.Favorite,
              entities: entity
            }, filters),
            auth: entity === GLOBAL.entity
          });
        };
      })(this)
    });
  };

  API.prototype.favorites_POST = function(args, callback) {
    Ti.API.log('api', 'favorites_POST <- ' + JSON.stringify(args));
    checkNetwork({
      online: (function(_this) {
        return function() {
          var contentType, httpData, httpMethod, payload, postType, ref, url, xhr;
          ref = {
            post: args.post.id
          };
          if (!args.post.permissions) {
            ref.type = args.post.type;
          }
          postType = post_types.Favorite + "#";
          url = _this.getApiRootUrls().new_post;
          httpMethod = 'POST';
          contentType = "" + content_types.Post + "; type=\"" + postType + "\"";
          payload = {
            refs: [ref]
          };
          if (args.note) {
            payload.note = args.note;
          }
          httpData = JSON.stringify(payload);
          xhr = new XHR(httpMethod, url, {
            onload: function(resp) {
              if (callback && callback.onload) {
                return callback.onload({
                  post: last_post
                });
              } else {
                return Ti.App.fireEvent('api:favorites_POST:success', {
                  post: last_post
                });
              }
            },
            onerror: function(resp) {
              if (callback && callback.onerror) {
                return callback.onerror(resp);
              } else {
                return Ti.App.fireEvent('api:favorites_POST:error', resp);
              }
            },
            noJson: false,
            headers: {
              'Content-Type': contentType,
              'Authorization': _this.generateAuthHeader(httpMethod, url, contentType, payload)
            }
          });
          Ti.API.debug('Data: ' + httpData);
          xhr.clearCookies(url);
          return xhr.send(httpData);
        };
      })(this),
      offline: (function(_this) {
        return function() {
          return alert('Looks like you are offline.');
        };
      })(this)
    });
  };

  API.prototype.isFavorite = function(postId) {
    return this.favorites_GET(GLOBAL.entity);
  };

  API.prototype.hasProfile = function(entity_uri) {
    return cache.discover.hasOwnProperty(entity_uri);
  };

  API.prototype.getMyProfile = function() {
    return this.getProfile(GLOBAL.entity);
  };

  API.prototype.saveMyProfile = function(profileData) {
    if (this.hasProfile(GLOBAL.entity)) {
      cache.discover[GLOBAL.entity] = profileData;
      Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
    }
  };

  API.prototype.getProfile = function(entity_uri) {
    return cache.discover[entity_uri] || null;
  };

  API.prototype.saveProfile = function(entity_uri, profileData, dontStore) {
    Ti.API.log('api', "saveProfile() <- " + entity_uri + ", " + (JSON.stringify(profileData)));
    if (_.isEmpty(profileData)) {
      Ti.API.log('api', 'saveProfile() -- Empty profile. Skipping.');
      return;
    }
    cache.discover[entity_uri] = profileData;
    if (!dontStore) {
      Ti.App.Properties.setString('discover', JSON.stringify(cache.discover));
    }
  };

  API.prototype.profile_PUT = function(data, callback) {
    Ti.API.log('api', "profile_PUT <- " + (JSON.stringify(data)));
    return Ti.API.log('api', "isFollowing -- NOT IMPLEMENTET YET");
  };

  API.prototype.attachment_GET = function(args, callback) {
    var httpMethod, url, xhr;
    Ti.API.log('api', "attachment_GET <- " + (JSON.stringify(args)));
    url = _.template(this.getApiRootUrls().attachment, {
      entity: encodeURIComponent(args.entity),
      digest: args.attachments[0].digest
    }, {
      interpolate: uri.url_template
    });
    httpMethod = 'GET';
    xhr = new XHR(httpMethod, url, {
      onload: function(resp) {
        if (callback && callback.onload) {
          return callback.onload(resp);
        } else {
          return Ti.App.fireEvent('api:attachment_GET:success', resp);
        }
      },
      onerror: function(resp) {
        if (callback && callback.onerror) {
          return callback.onerror(resp);
        } else {
          return Ti.App.fireEvent('api:attachment_GET:error', resp);
        }
      },
      noJson: true,
      headers: {
        'Authorization': this.generateAuthHeader(httpMethod, url)
      }
    });
  };

  API.prototype.attachment_URL = function(args) {
    var bewitOpts, digest, url;
    Ti.API.log('api', "attachment_URL <- " + (JSON.stringify(args)));
    digest = args.attachment ? args.attachment.digest : args.attachments[0].digest;
    url = _.template(this.getApiRootUrls().attachment, {
      entity: encodeURIComponent(args.entity),
      digest: digest
    }, {
      interpolate: uri.url_template
    });
    bewitOpts = {
      credentials: this.getHawkCredentials(),
      ttlSec: 120
    };
    url += "?bewit=" + (hawk.client.getBewit(url, bewitOpts));
    Ti.API.log('api', "attachment_URL -> " + url);
    return url;
  };

  return API;

})();

module.exports = new API();
