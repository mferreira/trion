var APP_ID = 'fx6U2zg10dAdtZYet93vvEGfqXA4q8S4kdPRk0Sx',
	REST_KEY = 'EOHCPuhNr7p5cIpSl3Ll61kRVFNrSazzSIYymJWz',
	parseInstallation = Ti.App.Properties.getString(GLOBAL.entity+'_installation', null);


GLOBAL.apn_channel = GLOBAL.entity ? GLOBAL.entity.replace('://','_').replace(/\./g,'_0_').replace(/\:/g,'_00_') : null;

exports.registerForPushNotifications = function(){
	Ti.API.info("Push notification enabled: "+Titanium.Network.remoteNotificationsEnabled);
	
	// don't register for APN twice
	if (Ti.Network.remoteNotificationsEnabled) return;

	Ti.API.info('Registering app for push notifications.');

	Ti.Network.registerForPushNotifications({
		types:[
			Ti.Network.NOTIFICATION_TYPE_BADGE,
			Ti.Network.NOTIFICATION_TYPE_ALERT,
			Ti.Network.NOTIFICATION_TYPE_SOUND
		],
		success: function(e){
			Ti.API.info('successfully registered for apple device token with '+e.deviceToken);
			exports.uploadParseInstallation(e);
		},
		error: errorCallback,
		callback: messageCallback
	});

	// error callBack
	function errorCallback(e) {
		Ti.API.warn("Error during Push Notifications registration: " + JSON.stringify(e));
	}

	// message callBack
	function messageCallback(e) {
		// alert('APN: '+JSON.stringify(e));
		var post = e.data.post;
		if (post && post.entity && post.id) {
			try {
			var api = require('lib/tent.api');
			api.entity_post_GET({
				entity: post.entity,
				post_id: post.id,
				callback: {
					onload: function(postData){
						var openPostModal = function(data){
							var PostDetails = require('views/postDetails');
							var postDetails = new PostDetails({rowData: {postData: data}});
							postDetails.setRightNavButton(ui.button({
								properties: {title: L('close', 'Close')},
								events: [['click', function(e){ postDetails.close(); }]]
							}));
							postDetails.open({modal: true});
						};

						if (postData.type === api.postType('Repost')) {
							Ti.API.debug('APN seems to habe pushed a Repost. Grabbing the original now.');
							postData.original_post = {};
							api.entity_post_GET({
								entity: postData.content.entity,
								post_id: postData.content.id,
								callback: {
									onload: function(post){
										Ti.API.debug('Entity post seems to exist. Opening modal view.');
										postData.original_post.post_content = post;
										postData.original_post.profile = api.getCache('discover')[postData.content.entity];
										postData.content.text = post.content.text;
										openPostModal(postData);
									},
									onerror: function(resp){
										if (resp.status === 404) {
											Ti.API.warn('Entity post does not exist anymore (404). Opening modal view.');
										}
										openPostModal(postData);
									}
								}
							});
						}
						else {
							openPostModal(postData);
						}

					},
					onerror: function(e){ alert(e); }
				}
			});
			} catch (err) {alert(err); }
		}
		else alert(e);
	}
};

exports.unregisterForPushNotifications = function(){
	Ti.Network.unregisterForPushNotifications();
};

exports.uploadParseInstallation = function(e, callback){
	Ti.API.log('parse', 'Starting uploadParseInstallation...');
	var request = Titanium.Network.createHTTPClient({
		onload: function(e) {
			if (request.status != 200 && request.status != 201) {
				request.onerror(e);
				return;
			}
			var installationData = JSON.parse(this.responseText);
			parseInstallation = installationData.objectId;
			Ti.App.Properties.setString(GLOBAL.entity+'_installation', parseInstallation);

			Ti.API.info("Parse installation success.");
			if (callback && callback.onload) callback.onload(installationData);
			// alert(this.responseText);
		},
		onerror: function(e) {
			Ti.API.error("Parse installation failed. Error: " + this.responseText);
			if (callback && callback.onerror) callback.onerror("Parse installation failed. Error: " + this.responseText);
			// alert(this.responseText);
		},
		timeout: 15000
	});

	var params = {
		'deviceType': 'ios',
		'deviceToken': e ? e.deviceToken : Ti.Network.remoteDeviceUUID,
		'channels': ['', GLOBAL.apn_channel]
	};

	// Register device token with Parse
	request.open('POST', 'https://api.parse.com/1/installations', true);
	request.setRequestHeader('X-Parse-Application-Id', APP_ID);
	request.setRequestHeader('X-Parse-REST-API-Key', REST_KEY);
	request.setRequestHeader('Content-Type', 'application/json');
	request.send(JSON.stringify(params));
};

exports.deleteParseInstallation = function(){
	var request = Titanium.Network.createHTTPClient({
		onload: function(e) {
			if (request.status != 200 && request.status != 201) {
				request.onerror(e);
				return;
			}
			parseInstallation = null;
			Ti.App.Properties.setString(GLOBAL.entity+'_installation', parseInstallation);
			Ti.API.error("Parse installation deleted.");
		},
		onerror: function(e) {
			Ti.API.error("Installation delete with Parse failed. Error: " + e.error);
		}
	});

	// Register device token with Parse
	request.open('DELETE', 'https://api.parse.com/1/installations/'+parseInstallation, true);
	request.setRequestHeader('X-Parse-Application-Id', APP_ID);
	request.setRequestHeader('X-Parse-REST-API-Key', REST_KEY);
	request.setRequestHeader('Content-Type', 'application/json');
	request.send();
};

exports.checkInstallation = function(callback){
	Ti.API.log('parse', 'Starting checkInstallation...');
	if (!Ti.Network.remoteDeviceUUID) {
		if (callback && callback.onerror) callback.onerror(this.responseText);
		return;
	}

	var request = Titanium.Network.createHTTPClient({
		onload: function(e) {
			if (request.status != 200 && request.status != 201) {
				request.onerror(e);
				return;
			}
			var installationData = JSON.parse(this.responseText);
			parseInstallation = installationData.objectId;
			Ti.App.Properties.setString(GLOBAL.entity+'_installation', parseInstallation);

			Ti.API.error("Installation check complete.");
			if (callback && callback.onload) callback.onload(installationData);
		},
		onerror: function(e) {
			Ti.API.error("Installation check failed. Error: " + this.responseText);
			if (callback && callback.onerror) callback.onerror(this.responseText);
		}
	});

	// Register device token with Parse
	request.open('GET', 'https://api.parse.com/1/installations/'+parseInstallation, true);
	request.setRequestHeader('X-Parse-Application-Id', APP_ID);
	request.setRequestHeader('X-Parse-REST-API-Key', REST_KEY);
	request.setRequestHeader('Content-Type', 'application/json');
	request.send();
};

exports.subscribeChannel = function(channelName, callback){
	Ti.API.log('parse', 'Starting subscribeChannel...');

	if (!parseInstallation) {
		Ti.API.info('Unable to subscribe to APN channel - No Parse installation');
		return; // nothing to do if we dont have this
	}

	var request = Titanium.Network.createHTTPClient({
		onload: function(e) {
			if (request.status != 200 && request.status != 201) {
				request.onerror(e);
				return;
			}
			Ti.API.error("Channel subscription with Parse complete.");
			if (callback && callback.onload) callback.onload();
		},
		onerror: function(e) {
			Ti.API.error("Channel subscription with Parse failed. Error: " + e.error);
			if (callback && callback.onerror) callback.onerror(e.error);
		}
	});

	var params = {
		'deviceType': 'ios',
		'deviceToken': Ti.Network.remoteDeviceUUID,
		'channels': ['', channelName]
	};

	// Register device token with Parse
	request.open('PUT', 'https://api.parse.com/1/installations/'+parseInstallation, true);
	request.setRequestHeader('X-Parse-Application-Id', APP_ID);
	request.setRequestHeader('X-Parse-REST-API-Key', REST_KEY);
	request.setRequestHeader('Content-Type', 'application/json');
	request.send(JSON.stringify(params));
};