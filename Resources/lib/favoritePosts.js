var cache;

cache = {
  favorites_id: Ti.App.Properties.hasProperty('favorite_posts_id') ? JSON.parse(Ti.App.Properties.getString('favorite_posts_id')) : [],
  favorites: Ti.App.Properties.hasProperty('favorite_posts') ? JSON.parse(Ti.App.Properties.getString('favorite_posts')) : []
};

exports.isFavorite = function(id) {
  return cache.favorites_id.indexOf(id) > -1;
};

exports.addFavorite = function(postData) {
  if (!exports.isFavorite(postData.id)) {
    cache.favorites.push(postData);
    cache.favorites_id.push(postData.id);
    Ti.App.Properties.setString('favorite_posts', JSON.stringify(cache.favorites));
    return Ti.App.Properties.setString('favorite_posts_id', JSON.stringify(cache.favorites_id));
  }
};

exports.removeFavorite = function(id) {
  var fav_index;
  if (exports.isFavorite(id)) {
    fav_index = cache.favorites_id.indexOf(id);
    cache.favorites.splice(fav_index, 1);
    cache.favorites_id.splice(fav_index, 1);
    Ti.App.Properties.setString('favorite_posts', JSON.stringify(cache.favorites));
    return Ti.App.Properties.setString('favorite_posts_id', JSON.stringify(cache.favorites_id));
  }
};

exports.getFavorites = function() {
  return cache.favorites;
};
