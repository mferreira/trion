var GLOBAL, delay;

Ti.API.info("Starting app on " + Ti.Platform.model + " ...");

require('ti.newrelic').start("AA9d9d7fc562863a8b01db43125a379fb338e81506");

GLOBAL = require('globals');

delay = function(ms, func) {
  return setTimeout(func, ms);
};

require('main');
