var	testflight = require('lib/testflight'),
	GLOBAL = require('globals'),
	api = require('lib/tent.api'),
	apn = require('lib/apn'),
	authChecker;

// start APN
apn.registerForPushNotifications();

Ti.App.Properties.setBool("isResumed",false);
Ti.App.Properties.setBool("isPaused",false);


// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');

GLOBAL.appArguments = Ti.App.getArguments();

Ti.API.info('App name is: '+GLOBAL.appName);
Ti.API.info('App arguments: '+JSON.stringify(GLOBAL.appArguments));
Ti.API.info('App launchURL: '+GLOBAL.appArguments.url);

function openAuthScreen(){
	var auth = require('views/authorize');
	auth.open();
}

function openMainScreen(){
	try {
	api.checkNetwork({
		online: function(){
			var updateFollows = function(callback) {
				if (api.isAuthenticated() && Ti.Network.online) {
					Ti.API.info('Updating followings...');
					api.followings_GET({
						onload: function(){
							Ti.API.info('Updating followers...');
							api.followers_GET({
								onload: function() { callback(); },
								onerror: function(){
									Ti.API.warn('Searching followings - Server having problems');
									callback();
								}
							});
						},
						onerror: function(){
							Ti.API.warn('Searching followers - Server having problems');
							callback();
						}
					});
				}
			};
			var openMain = function(){
				updateFollows(function(){
					var main = require('views/main');
					main.addTabGroupEvents();
					var mainWin = main.getView();
					mainWin.open();
				});
			};
			api.discover(GLOBAL.entity, {
				onload: openMain,
				onerror: openMain
			}, true);
		},
		offline: function(){
			var main = require('views/main');
			main.addTabGroupEvents();
			var mainWin = main.getView();
			mainWin.open();
		}
	});
	}
	catch(er) {Ti.API.error(er);}
}

function checkAppAuth(callback){
	api.apps_GET({
		callback: function(result){
			if (result === 'success') { callback.ok(); }
			else if (result === 'reauth') {
				Ti.API.warn('Server accepted this app, but a new auth is needed to change some information.');
				GLOBAL.reAuthApp = true;
				callback.nok();
				alert('Our apologies, but due to some internal changes, you need to re-authorize this app.');
			}
			else if (result === 'error') {
				Ti.API.warn('Server responded with an error. Rechecking app authorization...');
				api.apps_GET({
					callback: function(result){
						if (result === 'success') { callback.ok(); }
						else if (result === 'error') {
							Ti.API.warn('Server responded with an error. Continuing to cached data.');
							callback.ok();
						}
						else {
							Ti.API.warn('Server did not accept this app. Opening auth screen.');
							callback.nok();
						}
					}
				});
			}
			else {
				Ti.API.warn('Server did not accept this app. Opening auth screen.');
				callback.nok();
			}
		}
	});
}

function stopAuthChecker(){ clearInterval(authChecker); }

if (GLOBAL.app_firstRun || !api.isAuthenticated()) {
	Ti.API.info('App is running for the first time or not authenticated.');
	Ti.App.Properties.setBool('app_firstRun', false);
	openAuthScreen();
}
else {
	api.checkNetwork({
		online: function(){
			checkAppAuth({
				ok: function() { openMainScreen(); },
				nok: function() { openAuthScreen(); }
			});
		},
		offline: function(){
			openMainScreen();
			setTimeout(function(){ alert('Looks like you are offline.'); }, 1000);
			// check app auth when device comes back online
			authChecker = setInterval(function(){
				api.checkNetwork({
					online: function(){
						stopAuthChecker();
						checkAppAuth({
							ok: function(){},
							nok: function() {
								var auth = require('views/authorize');
								auth.open({transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
							}
						});
					}
				});
			}, 2000);
		}
	});
}

Ti.App.addEventListener('resumed', function(){

	if(Ti.App.Properties.getBool("isPaused",false)) {
		Ti.App.Properties.setBool("isResumed",true);
	} else {
		Ti.App.Properties.setBool("isResumed",false);
	}

	Ti.API.info('App resuming with arguments: '+JSON.stringify(Ti.App.getArguments()));
	if (Ti.App.getArguments().url) {
		var URI = require('lib/vendor/jsuri.min'),
			NewPost = require('views/newPost2');

		var uri =  new URI(Ti.App.getArguments().url);
		if (uri.protocol() === 'tentstatus') {
			(new NewPost({
				prefill: {content: {text: decodeURIComponent(uri.host()) }}
			})).open({modal: true});
		}
		else if (uri.host() === 'tentauth') { // authenticating the app with the service
			if (uri.query().getParamValue('error')) {
				Ti.UI.createAlertDialog({
					message: 'Error: '+uri.query().getParamValue('error')+'\n'+
						'Description: '+uri.query().getParamValue('error_description'),
					title: 'Auth Failed'
				}).show();
				Ti.App.fireEvent('api:grabToken:failed');
			}
			else if (uri.query().getParamValue('state') !== api.cacheData.getAppAuth.state) {
				Ti.API.error("Auth 'state' does not match internal value: "+api.cacheData.getAppAuth.state);
				Ti.App.fireEvent('api:grabToken:failed');
			}
			else {
				api.cacheData.secret_code = uri.query().getParamValue('code');
				Ti.API.debug('Grabbed secret code: '+ api.cacheData.secret_code);
				Ti.App.Properties.getBool('oauth', true); api.cacheData.oauth = true;
				// exchange the code for the access token
				api.grabToken.call(api, api.cacheData.secret_code);
			}
		}
	}
});

Ti.App.addEventListener('pause', function(){
	Ti.API.info('Pause is fired now');
	//Ti.App.Properties.setString("isResumed",0);
	Ti.App.Properties.setBool("isPaused",true);
});
