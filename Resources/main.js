var api, checkAppAuth, openAuthScreen, openMainScreen, stopAuthChecker, testflight;

testflight = require('lib/testflight');

api = require('lib/tent.api2');

Ti.App.Properties.setBool("isResumed", false);

Ti.App.Properties.setBool("isPaused", false);

Titanium.UI.setBackgroundColor('#000');

GLOBAL.appArguments = Ti.App.getArguments();

Ti.API.info('App name is: ' + GLOBAL.appName);

Ti.API.info('App arguments: ' + JSON.stringify(GLOBAL.appArguments));

Ti.API.info('App launchURL: ' + GLOBAL.appArguments.url);

openAuthScreen = function() {
  var auth;
  auth = require('views/authorize');
  return auth.open();
};

openMainScreen = function() {
  var apn, er;
  Ti.API.debug("Opening main screen ...");
  if (Ti.Platform.model !== "Simulator") {
    apn = require('lib/apn');
    apn.registerForPushNotifications();
  }
  try {
    return api.checkNetwork({
      "this": this,
      online: function() {
        var openMain, updateFollows;
        updateFollows = function(callback) {
          if (api.isAuthenticated() && Ti.Network.online) {
            Ti.API.info('Updating followings...');
            return api.followings_GET({
              onload: function() {
                Ti.API.info('Updating followers...');
                return api.followers_GET({
                  onload: function() {
                    return callback();
                  },
                  onerror: function() {
                    Ti.API.warn('Searching followings - Server having problems');
                    return callback();
                  }
                });
              },
              onerror: function() {
                Ti.API.warn('Searching followers - Server having problems');
                return callback();
              }
            });
          }
        };
        openMain = function() {
          var main, mainWin;
          main = require('views/main');
          main.addTabGroupEvents();
          mainWin = main.getView();
          return mainWin.open();
        };
        return api.discover(GLOBAL.entity, {
          onload: openMain,
          onerror: openMain
        }, true);
      },
      offline: function() {
        var main, mainWin;
        main = require('views/main');
        main.addTabGroupEvents();
        mainWin = main.getView();
        return mainWin.open();
      }
    });
  } catch (_error) {
    er = _error;
    return Ti.API.error(er);
  }
};

checkAppAuth = function(callback) {
  return api.apps_GET({
    callback: function(result) {
      if (result === 'success') {
        return callback.ok();
      } else if (result === 'reauth') {
        Ti.API.warn('Server accepted this app, but a new auth is needed to change some information.');
        GLOBAL.reAuthApp = true;
        return callback.nok();
      } else if (result === 'error') {
        Ti.API.warn('Server responded with an error. Rechecking app authorization...');
        return api.apps_GET({
          callback: function(result) {
            if (result === 'success') {
              return callback.ok();
            } else if (result === 'error') {
              Ti.API.warn('Server responded with an error. Continuing to cached data.');
              return callback.ok();
            } else {
              Ti.API.warn('Server did not accept this app. Opening auth screen.');
              return callback.nok();
            }
          }
        });
      } else {
        Ti.API.warn('Server did not accept this app. Opening auth screen.');
        return callback.nok();
      }
    }
  });
};

stopAuthChecker = function() {
  return clearInterval(authChecker);
};

if (GLOBAL.app_firstRun || !api.isAuthenticated()) {
  Ti.API.info('App is running for the first time or not authenticated.');
  Ti.App.Properties.setBool('app_firstRun', false);
  openAuthScreen();
} else {
  api.checkNetwork({
    online: function() {
      return checkAppAuth({
        ok: function() {
          return openMainScreen();
        },
        nok: function() {
          return openAuthScreen();
        }
      });
    },
    offline: function() {
      var authChecker;
      openMainScreen();
      delay(1000, function() {
        return alert('Looks like you are offline.');
      });
      return authChecker = setInterval(function() {
        return api.checkNetwork({
          online: function() {
            stopAuthChecker();
            return checkAppAuth({
              ok: function() {},
              nok: function() {
                var auth;
                auth = require('views/authorize');
                return auth.open({
                  transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
                });
              }
            });
          }
        }, 2000);
      });
    }
  });
}

Ti.App.addEventListener('resumed', function() {
  var NewPost, URI, uri;
  if (Ti.App.Properties.getBool("isPaused", false)) {
    Ti.App.Properties.setBool("isResumed", true);
  } else {
    Ti.App.Properties.setBool("isResumed", false);
  }
  if (GLOBAL.proccessingData) {
    GLOBAL.proccessingData = false;
  }
  Ti.API.info("App resuming with arguments: " + (JSON.stringify(Ti.App.getArguments())));
  if (Ti.App.getArguments().url) {
    URI = require('lib/vendor/jsuri.min');
    NewPost = require('views/newPost2');
    uri = new URI(Ti.App.getArguments().url);
    if (uri.protocol() === 'tentstatus') {
      return (new NewPost({
        prefill: {
          content: {
            text: decodeURIComponent(uri.host())
          }
        }
      })).open({
        modal: true
      });
    } else if (uri.host() === 'tentauth') {
      if (uri.query().getParamValue('error')) {
        Ti.UI.createAlertDialog({
          message: uri.query().split('&').join('\n'),
          title: 'Auth Failed'
        }).show();
        return Ti.App.fireEvent('api:grabToken:failed');
      } else if (uri.query().getParamValue('state') === api.tempCache.oauthState) {
        Ti.API.error("Auth 'state' does not match internal value: " + api.tempCache.oauthState);
        return Ti.App.fireEvent('api:grabToken:failed');
      } else {
        api.cacheData.secret_code = uri.query().getParamValue('code');
        Ti.API.debug("Grabbed secret code: " + api.cacheData.secret_code);
        Ti.App.Properties.getBool('oauth', true);
        api.cacheData.oauth = true;
        return api.grabToken.call(api, api.cacheData.secret_code);
      }
    }
  }
});

Ti.App.addEventListener('pause', function() {
  Ti.API.info('Pause is fired now');
  return Ti.App.Properties.setBool("isPaused", true);
});
