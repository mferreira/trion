var globals, settings;

settings = (Ti.App.Properties.hasProperty("settings") ? JSON.parse(Ti.App.Properties.getString("settings")) : {});

globals = {
  app_firstRun: Ti.App.Properties.getBool("app_firstRun", true),
  entity: Ti.App.Properties.getString("entity", null),
  appConfig: {
    bgCheck: {
      posts_timeout: (settings && settings.updateFreq ? settings.updateFreq.minutes_stream : 1.7) * 60000,
      mentions_timeout: (settings && settings.updateFreq ? settings.updateFreq.minutes_mentions : 1.2) * 60000
    }
  },
  newPostData: null,
  dontProccessNewPosts: false,
  appName: ("" + Ti.App.name + " on ") + (function() {
    var osname;
    if (Ti.Platform.model === "Simulator") {
      return "Simulator";
    }
    osname = Ti.Platform.osname;
    if (osname.charAt(0) !== 'i') {
      return osname.charAt(0).toUpperCase() + osname.slice(1);
    } else {
      return osname.charAt(0) + osname.charAt(1).toUpperCase() + osname.slice(2);
    }
  })(),
  isTablet: Ti.Platform.osname === 'ipad' || (Ti.Platform.displayCaps.platformWidth > 899 || Ti.Platform.displayCaps.platformHeight > 899),
  keyboardHeight: function() {
    if (this.isTablet) {
      if (Ti.Platform.displayCaps.platformWidth === 1024) {
        return 352;
      } else {
        return 264;
      }
    } else {
      if (Ti.Platform.displayCaps.platformWidth === 320) {
        return 216;
      } else {
        return 162;
      }
    }
  },
  timestampCache: [],
  cur_tab: null,
  appArguments: null
};

module.exports = globals;
