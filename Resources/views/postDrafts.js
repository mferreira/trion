var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api'),
	NewPost = require('views/newPost2'),
	drafts = require('lib/drafts'),
	_f = require('views/func_repo');


function createView(conf) {

	var
	section = null,

	self = ui.win({
		config: 'windowProperties',
		properties: {
			title:'Drafts',
			rightNavButton: ui.button({
				config: 'newPostButton',
				events: [ ['click', function(e){
					e.source.enabled = false;
					(new NewPost(null, null, {button: e.source})).open({modal: true});
				}] ]
			}),
			leftNavButton: (function(){
				if (conf.hasBackButton) {
					return ui.button({
						config: 'backButton',
						properties: {backgroundImage: 'images/back_profile.png', width: 64},
						events: [['click', function(e){ ignore_clicks = true; GLOBAL.tabGroup.activeTab.close(self, {animated: true}); ignore_clicks = false; }]]
					});
				}
			})(),
			navBarHidden: false
		},
		events: [
			['open', function(e){
				Ti.API.debug('postDrafts fired event: OPEN');
			}],
			['focus', function(e){
				var allDrafts = drafts.all();
				section = Ti.UI.createTableViewSection();

				if (!allDrafts.length) {
					section.add(ui.row({
						config: 'rowWithoutChild_full',
						properties: {
							height: 40
						},
						content: [
							ui.view({
								properties: {touchEnabled: false, top: 12, bottom: 12, left: 14, right: 14},
								content: [
									ui.label({touchEnabled: false, text: 'No drafts', width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
										color: '#6B6A67', font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'} })
								]
							})
						]
					}));
				}
				else {
					for (var i=0, j=allDrafts.length; i<j; i++) {
						var draft = allDrafts[i];
						section.add(ui.row({
							config: (j === 1) ? 'rowWithoutChild_full' : (!i) ? 'rowWithoutChild_top' : (i===j-1) ? 'rowWithoutChild_bottom' : 'rowWithoutChild',
							properties: {
								draft: draft
							},
							content: [
								ui.view({
									properties: {touchEnabled: false, top: 12, bottom: 12, left: 14, right: 14},
									content: [
										ui.label({touchEnabled: false, text: draft.content.text, width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#2F3238', font: { fontSize: 13 }})
									]
								})
							]
						}));
					}
				}

				tableView.setData([section]);
			}]
		]
	}),

	tableView = ui.table({
		config: 'tableView',
		properties: {minRowHeight: 75},
		events: [
			['click', function(e){
				var index = e.index;
				if (index === null) return;
				Ti.API.debug(JSON.stringify(e));
				if (e.rowData.draft)
					(new NewPost({
						prefill: e.rowData.draft,
						callbackArgs: index
					}, function(i){
						tableView.deleteRow(i);
						drafts.remove(i);
					})).open({modal: true});
			}],
			['longpress', function(e){
				if (!drafts.all().length) return; // dont act if we have no drafts
				var index = e.index;
				ui.alert({
					properties: {
						cancel: 1,
						buttonNames: [L('yes', 'Yes'), L('no', 'No')],
						message: L('delete_the_draft','Delete the draft')+'?',
						title: L('confirm','Confirm')
					},
					events: [
						['click', function(e){
							if (e.index != e.cancel) {
								tableView.deleteRow(index);
								drafts.remove(index);
								if (section.rowCount === 0) {
									tableView.appendRow(ui.row({
										config: 'rowWithoutChild_full',
										properties: {
											height: 40
										},
										content: [
											ui.view({
												properties: {touchEnabled: false, top: 12, bottom: 12, left: 14, right: 14},
												content: [
													ui.label({touchEnabled: false, text: 'No drafts', width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#2F3238', font: { fontSize: 13 }})
												]
											})
										]
									}));
								}
								else if (section.rowCount === 1) section.rows[0].applyProperties({ backgroundImage: 'images/row_bg_full.png'});
								else if (index === 0) section.rows[0].applyProperties({ backgroundImage: 'images/row_bg_top.png'});
								else if (index > section.rowCount) section.rows[section.rowCount-1].applyProperties({ backgroundImage: 'images/row_bg_bottom.png'});
							}
						}]
					]
				}).show();
			}]
		]

	});

	self.add(tableView);

	return self;
}

module.exports = createView;
