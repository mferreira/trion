var GLOBAL, Map, RE_mention, api, createRowDetailsFromPost, createRowFromPost, createWindowTitle, getInlineLinks, getInlineMentions, moment, overrideFocusTab, post2HTML, ui, uri, utils, _;

ui = require('lib/ui_sugar');

GLOBAL = require('globals');

api = require('lib/tent.api2');

uri = require('lib/vendor/jsuri.min');

moment = require('lib/vendor/moment.min');

_ = require('lib/vendor/underscore-min');

utils = require('lib/utils');

Map = require('ti.map');

RE_mention = /\^\[([^\[\]]+)\]\(\d+\)/gi;

createRowFromPost = function(post, viewProperties) {
  var className, entityAvatar, entityName, entityProfile, fillEntityInfo, hasDetails, hasLocation, imageURL, isBottom, isPhotoPost, isRepost, mention, postContent, postContentText, postContentView, postEntity, postImage, postInfoView, postTimestampLabel, postType, reply, replyEntityProfile, replyedLabel, repostEntityProfile, repostedLabel, row, rowConfig, rowContent, rowProperties, wrapView, _i, _len, _ref;
  Ti.API.debug("Creating row post for: " + post.type);
  fillEntityInfo = function(profile) {
    Ti.API.debug('post.entity: ', post.entity);
    Ti.API.debug('profile: ', JSON.stringify(profile));
    if (profile.profile.name.length) {
      entityName.setText(profile.profile.name);
    }
    if (api.getCache('avatars')[post.entity]) {
      return entityAvatar.setImage(api.attachment_URL({
        entity: postEntity,
        attachment: api.getCache('avatars')[post.entity]
      }));
    }
  };
  postType = post.type.split('#')[0];
  isBottom = viewProperties && (viewProperties.backgroundImage === 'images/row_bg_bottom.png' || viewProperties.backgroundImage === 'images/row_bg_full.png');
  isRepost = postType === api.postType('Repost');
  hasLocation = !!post.content.location;
  postEntity = isRepost ? post.original_post.profile.entity : post.entity;
  entityProfile = api.getProfile(post.entity);
  repostEntityProfile = null;
  wrapView = ui.view({
    properties: {
      touchEnabled: true,
      top: 11,
      bottom: 12,
      left: 71,
      width: GLOBAL.isTablet ? 510 : 210
    }
  });
  entityName = ui.label({
    touchEnabled: false,
    text: (new uri(postEntity)).host(),
    color: '#2E3238',
    top: 0,
    left: 0,
    width: 225,
    height: 13,
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    font: {
      fontSize: 13,
      fontWeight: 'bold'
    }
  });
  entityAvatar = ui.image({
    properties: {
      bubbleParent: false,
      defaultImage: 'images/avatar.png',
      image: 'images/avatar.png',
      width: 46,
      height: 46,
      top: 13,
      left: 14,
      borderRadius: 3
    },
    events: [
      [
        'click', function(e) {
          var Profile, profileView;
          if (postEntity === GLOBAL.entity) {
            return GLOBAL.tabGroup.setActiveTab(2);
          } else {
            Profile = require('views/profile');
            profileView = new Profile(postEntity, {
              backButton: 'back'
            });
            if (!profileView) {
              return alert('Error discovering this entity.');
            } else {
              return GLOBAL.tabGroup.activeTab.open(profileView);
            }
          }
        }
      ]
    ]
  });
  postTimestampLabel = ui.label({
    touchEnabled: false,
    text: moment(post.published_at).fromNow(),
    top: 0,
    right: 0,
    width: 90,
    height: 10,
    textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
    color: '#909AA2',
    font: {
      fontSize: 10
    }
  });
  GLOBAL.timestampCache.push([postTimestampLabel, post.published_at]);
  postInfoView = ui.view({
    properties: {
      touchEnabled: false,
      bottom: isBottom ? 4 : 0,
      height: 28,
      left: 2,
      right: 2,
      layout: 'horizontal',
      backgroundImage: isBottom ? 'images/postinfo_bottom_bg.png' : 'images/postinfo_bg.png'
    }
  });
  rowConfig = 'genericPostRow';
  className = '';
  hasDetails = true;
  if (hasLocation) {
    postInfoView.add(ui.image({
      properties: {
        widht: 8,
        height: 8,
        top: 11,
        left: postInfoView.children.length ? 3 : 69,
        image: 'images/postinfo_location.png',
        touchEnabled: false
      }
    }));
  }
  switch (postType) {
    case api.postType('Status'):
      className = 'status';
      postContent = post.content.text.replace(RE_mention, "$1");
      break;
    case api.postType('Repost'):
      className = 'repost';
      postContent = post.content.text.replace(RE_mention, "$1");
      repostEntityProfile = post.original_post.profile;
      postInfoView.add(ui.image({
        properties: {
          widht: 9,
          height: 8,
          top: 12,
          left: postInfoView.children.length ? 3 : 69,
          image: 'images/repost_icon.png',
          touchEnabled: false
        }
      }));
      repostedLabel = ui.label({
        touchEnabled: false,
        text: "Repost by " + (entityProfile ? entityProfile.profile.name : (new uri(post.entity)).host()),
        left: 5,
        height: 13,
        top: 9,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: '#5E6165',
        font: {
          fontSize: 11
        },
        shadowColor: '#fff',
        shadowOffset: {
          x: 0,
          y: 1
        }
      });
      if (!entityProfile) {
        api.discover(post.entity, {
          onload: function(profileData) {
            return repostedLabel.text = "Repost by " + profileData.profile.name;
          }
        });
      }
      postInfoView.add(repostedLabel);
      break;
    case api.postType('Follower'):
      className = 'follower';
      postContent = 'You have a new follower';
      hasDetails = false;
      break;
    case api.postType('Photo'):
      className = 'photo';
      postContent = post.content.caption.replace(RE_mention, "$1");
      Ti.API.debug("postContent is: " + postContent);
      imageURL = api.attachment_URL({
        entity: isRepost ? post.original_post.profile.entity : post.entity,
        attachments: isRepost ? post.original_post.attachments : post.attachments
      });
      postImage = Ti.UI.createImageView({
        image: utils.cropImageURL(imageURL, 210, 110),
        width: 210,
        height: 110,
        top: 5,
        left: 0,
        right: 0
      });
      isPhotoPost = true;
  }
  if (repostEntityProfile) {
    fillEntityInfo(repostEntityProfile);
  } else if (entityProfile) {
    fillEntityInfo(entityProfile);
  } else {
    api.discover(post.entity, {
      onload: function(profileData) {
        return fillEntityInfo(profileData);
      },
      onerror: function(resp) {
        Ti.API.warn('Failed entity profile discovery. Retrying...');
        return api.discover(post.entity, {
          onload: function(profileData) {
            return fillEntityInfo(profileData);
          },
          onerror: function(resp) {
            return Ti.API.warn('Failed entity profile discovery. Giving up.');
          }
        });
      }
    });
  }
  if (viewProperties && viewProperties.autoLink) {
    postContentText = ui.textArea({
      properties: {
        value: postContent,
        color: '#2F3238',
        font: {
          fontFamily: 'Helvetica',
          fontSize: 13
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        autoLink: Ti.UI.AUTOLINK_ALL,
        editable: false,
        scrollable: false,
        backgroundColor: 'white',
        bubbleParent: true,
        left: -8,
        right: -9,
        height: Ti.UI.SIZE
      }
    });
    if (isPhotoPost) {
      postContentText.applyProperties({
        top: 0
      });
    } else {
      postContentText.applyProperties({
        top: 7,
        bottom: -1
      });
    }
  } else {
    postContentText = ui.label({
      touchEnabled: false,
      text: postContent,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      color: '#2F3238',
      font: {
        fontSize: 13
      },
      left: 0,
      right: 0,
      height: Ti.UI.SIZE
    });
    if (isPhotoPost) {
      postContentText.applyProperties({
        top: 0
      });
    } else {
      postContentText.applyProperties({
        top: 16,
        bottom: 0
      });
    }
  }
  if (isPhotoPost) {
    postContentText = ui.view({
      properties: {
        layout: 'vertical',
        top: 16,
        bottom: 0,
        left: 0,
        right: 0,
        height: Ti.UI.SIZE
      },
      content: [postContentText, postImage]
    });
  }
  postContentView = ui.view({
    properties: {
      touchEnabled: true,
      top: 0,
      left: 0,
      right: 0
    },
    content: [postContentText, entityName, postTimestampLabel]
  });
  wrapView.add(postContentView);
  if (post.permissions && !post.permissions['public']) {
    className += 'Private';
    postInfoView.add(ui.label({
      touchEnabled: false,
      text: '$',
      height: 11,
      top: 10,
      left: postInfoView.children.length ? 3 : 69,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      color: '#5E6165',
      font: {
        fontSize: 10,
        fontFamily: 'Tentr-symbols'
      }
    }));
    postInfoView.add(ui.label({
      touchEnabled: false,
      text: 'Private',
      left: 2,
      height: Ti.UI.SIZE,
      top: 9,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      color: '#5E6165',
      font: {
        fontSize: 11
      },
      shadowColor: '#fff',
      shadowOffset: {
        x: 0,
        y: 1
      }
    }));
  }
  reply = null;
  if (postType !== api.postType('Repost') && post.mentions) {
    _ref = post.mentions;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      mention = _ref[_i];
      if (mention.post) {
        className += 'Reply';
        reply = mention;
        replyEntityProfile = api.getProfile(reply.entity);
        postInfoView.add(ui.image({
          properties: {
            widht: 9,
            height: 8,
            top: 11,
            left: postInfoView.children.length ? 3 : 69,
            image: 'images/reply_icon.png',
            touchEnabled: false
          }
        }));
        replyedLabel = ui.label({
          touchEnabled: false,
          text: "Reply to " + (replyEntityProfile ? replyEntityProfile.profile.name : (new uri(reply.entity)).host()),
          left: 5,
          height: 13,
          top: 9,
          textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
          color: '#5E6165',
          font: {
            fontSize: 11
          },
          shadowColor: '#fff',
          shadowOffset: {
            x: 0,
            y: 1
          }
        });
        if (!entityProfile) {
          api.discover(reply.entity, {
            onload: function(profileData) {
              return replyedLabel.text = "Repost by " + profileData.profile.name;
            }
          });
        }
        postInfoView.add(replyedLabel);
        break;
      }
    }
  }
  postContentView.height = Ti.UI.SIZE;
  wrapView.height = Ti.UI.SIZE;
  rowProperties = {
    className: className,
    postData: post,
    height: Ti.UI.SIZE,
    hasDetails: hasDetails
  };
  if (viewProperties) {
    _.extend(rowProperties, viewProperties);
  }
  rowContent = [entityAvatar, wrapView];
  if (postInfoView.children.length) {
    postContentView.bottom = 30;
    rowContent.push(postInfoView);
    entityAvatar.bottom = 42;
  } else {
    entityAvatar.bottom = isBottom ? 18 : 13;
  }
  row = ui.row({
    config: rowConfig,
    properties: rowProperties,
    content: rowContent
  });
  return row;
};

createRowDetailsFromPost = function(post, viewProperties) {
  var StyledLabel, className, entityAvatar, entityLocation, entityName, entityProfile, entityURI, fillEntityInfo, hasDetails, hasLocation, imageURL, isBottom, isPhotoPost, isRepost, mapView, postContent, postContentText, postContentView, postEntity, postImage, postInfoView, postInfoView_padding, postTimestampLabel, postType, repostEntityProfile, repostedLabel, row, rowConfig, rowContent, rowProperties, wrapView, _entityURI;
  Ti.API.debug('Creating row from post data: ' + JSON.stringify(post));
  fillEntityInfo = function(profile) {
    Ti.API.debug('fillEntityInfo <- ', JSON.stringify(profile));
    if (profile.profile.name.length) {
      entityName.setText(profile.profile.name);
    }
    if (api.getCache('avatars')[post.entity]) {
      entityAvatar.setImage(api.attachment_URL({
        entity: postEntity,
        attachment: api.getCache('avatars')[post.entity]
      }));
    }
    return entityURI.setText((new uri(profile.entity)).host());
  };
  postType = post.type.split('#')[0];
  isBottom = viewProperties && (viewProperties.backgroundImage === 'images/row_bg_bottom.png' || viewProperties.backgroundImage === 'images/row_bg_full.png');
  isRepost = postType === api.postType('Repost');
  hasLocation = !!post.content.location;
  postEntity = isRepost ? post.original_post.profile.entity : post.entity;
  _entityURI = new uri(postEntity);
  entityProfile = api.getProfile(post.entity);
  repostEntityProfile = null;
  wrapView = ui.view({
    properties: {
      touchEnabled: true,
      top: 11,
      width: GLOBAL.isTablet ? 510 : 210,
      left: 71
    }
  });
  entityName = ui.label({
    touchEnabled: false,
    text: _entityURI.host(),
    color: '#2E3238',
    top: 7,
    left: 0,
    width: 225,
    height: 15,
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    font: {
      fontSize: 15,
      fontWeight: 'bold'
    }
  });
  entityURI = ui.label({
    touchEnabled: false,
    text: '^' + _entityURI.host(),
    color: '#97989b',
    top: 29,
    left: 0,
    width: 225,
    height: 13,
    textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
    font: {
      fontSize: 13
    }
  });
  entityAvatar = ui.image({
    properties: {
      bubbleParent: false,
      width: 46,
      height: 46,
      top: 13,
      left: 14,
      borderRadius: 3,
      defaultImage: 'images/avatar.png',
      image: 'images/avatar.png'
    },
    events: [
      [
        'click', function(e) {
          var Profile;
          if (postEntity === GLOBAL.entity) {
            return GLOBAL.tabGroup.setActiveTab(2);
          } else {
            Profile = require('views/profile');
            return GLOBAL.tabGroup.activeTab.open(new Profile(postEntity, {
              backButton: 'back'
            }));
          }
        }
      ]
    ]
  });
  postTimestampLabel = ui.label({
    touchEnabled: false,
    text: moment(post.published_at).fromNow(),
    top: 8,
    right: 0,
    width: 90,
    height: 10,
    textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
    color: '#909AA2',
    font: {
      fontSize: 10
    }
  });
  GLOBAL.timestampCache.push([postTimestampLabel, post.published_at]);
  postContent = post.content.text;
  postInfoView = ui.view({
    properties: {
      touchEnabled: false,
      bottom: isBottom ? 4 : 0,
      height: 28,
      left: 2,
      right: 2,
      layout: 'horizontal',
      backgroundImage: isBottom ? 'images/postinfo_bottom_bg.png' : 'images/postinfo_bg.png'
    }
  });
  rowConfig = 'genericPostRow';
  className = '';
  hasDetails = true;
  switch (postType) {
    case api.postType('Status'):
      className = 'status';
      postContent = post.content.text.replace(RE_mention, "$1");
      break;
    case api.postType('Repost'):
      className = 'repost';
      postContent = post.content.text.replace(RE_mention, "$1");
      repostEntityProfile = post.original_post.profile;
      postInfoView.add(ui.image({
        properties: {
          widht: 9,
          height: 8,
          top: 12,
          left: postInfoView.children.length ? 3 : 69,
          image: 'images/repost_icon.png',
          touchEnabled: false
        }
      }));
      repostedLabel = ui.label({
        touchEnabled: false,
        text: "Repost by " + (entityProfile ? entityProfile.profile.name : (new uri(post.entity)).host()),
        left: 5,
        height: 13,
        top: 9,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: '#5E6165',
        font: {
          fontSize: 11
        },
        shadowColor: '#fff',
        shadowOffset: {
          x: 0,
          y: 1
        }
      });
      if (!entityProfile) {
        api.discover(post.entity({
          onload: function(profileData) {
            return repostedLabel.text = "Repost by " + profileData.profile.name;
          }
        }));
      }
      postInfoView.add(repostedLabel);
      break;
    case api.postType('Follower'):
      className = 'follower';
      postContent = 'You have a new follower';
      hasDetails = false;
      break;
    case api.postType('Photo'):
      className = 'photo';
      postContent = post.content.caption.replace(RE_mention, "$1");
      imageURL = api.attachment_URL({
        entity: isRepost ? post.original_post.profile.entity : post.entity,
        attachments: isRepost ? post.original_post.attachments : post.attachments
      });
      postImage = Ti.UI.createImageView({
        image: utils.cropImageURL(imageURL, 268),
        height: Ti.UI.SIZE,
        top: 5,
        left: 0,
        right: 0
      });
      isPhotoPost = true;
  }
  if (repostEntityProfile) {
    fillEntityInfo(repostEntityProfile);
  } else if (entityProfile) {
    fillEntityInfo(entityProfile);
  } else {
    api.discover(post.entity, {
      onload: function(profileData) {
        return fillEntityInfo(profileData);
      },
      onerror: function(resp) {
        Ti.API.warn('Failed entity profile discovery. Retrying...');
        return api.discover(post.entity, {
          onload: function(profileData) {
            return fillEntityInfo(profileData);
          },
          onerror: function(resp) {
            return Ti.API.warn('Failed entity profile discovery. Giving up.');
          }
        });
      }
    });
  }
  if (viewProperties && viewProperties.autoLink) {
    StyledLabel = require('ti.styledlabel');
    postContentText = StyledLabel.createLabel(_.extend({
      html: post2HTML(post),
      height: Ti.UI.SIZE
    }));
    if (isPhotoPost) {
      postContentText.applyProperties({
        top: 0,
        left: 0,
        right: 0
      });
    } else {
      postContentText.applyProperties({
        top: 68,
        bottom: 13,
        left: 14,
        right: 14
      });
    }
    postContentText.addEventListener('click', function(e) {
      var entity;
      if (e.url) {
        entity = e.url.replace(/\/+$/, '');
        if (entity === GLOBAL.entity) {
          return GLOBAL.tabGroup.setActiveTab(2);
        } else {
          return api.discover(entity, {
            thiz: this,
            onload: function(data) {
              var Profile;
              Profile = require('views/profile');
              return GLOBAL.tabGroup.activeTab.open(new Profile(entity, {
                backButton: 'back'
              }));
            },
            onerror: function(e) {
              return alert('Error retrieving profile data.');
            }
          });
        }
      }
    });
  } else {
    postContentText = ui.label({
      properties: _.extend({
        touchEnabled: false,
        text: postContent,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: '#2F3238',
        font: {
          fontSize: 15
        },
        height: Ti.UI.SIZE
      })
    });
    if (isPhotoPost) {
      postContentText.applyProperties({
        top: 0,
        left: 0,
        right: 0
      });
    } else {
      postContentText.applyProperties({
        top: 68,
        bottom: 13,
        left: 14,
        right: 14
      });
    }
  }
  if (isPhotoPost) {
    postContentText = ui.view({
      properties: {
        layout: 'vertical',
        top: 68,
        bottom: 13,
        left: 14,
        right: 14,
        height: Ti.UI.SIZE
      },
      content: [postContentText, postImage]
    });
  }
  postContentView = ui.view({
    properties: {
      touchEnabled: true,
      top: 0,
      left: 0,
      right: 0
    },
    content: [entityName, entityURI, postTimestampLabel]
  });
  wrapView.add(postContentView);
  if (post.permissions && !post.permissions['public']) {
    className += 'Private';
    postInfoView.add(ui.label({
      touchEnabled: false,
      text: '$',
      left: postInfoView.children.length ? 3 : 69,
      height: 11,
      top: 10,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      color: '#5E6165',
      font: {
        fontSize: 10,
        fontFamily: 'Tentr-symbols'
      }
    }));
    postInfoView.add(ui.label({
      touchEnabled: false,
      text: 'Private',
      left: 2,
      height: Ti.UI.SIZE,
      top: 9,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      color: '#5E6165',
      font: {
        fontSize: 11
      },
      shadowColor: '#fff',
      shadowOffset: {
        x: 0,
        y: 1
      }
    }));
  }
  rowContent = [entityAvatar, wrapView, postContentText];
  if (postInfoView.children.length) {
    postContentText.applyProperties({
      bottom: viewProperties && viewProperties.autoLink ? 29 : 30,
      height: Ti.UI.SIZE
    });
    rowContent.push(postInfoView);
    entityAvatar.bottom = 42;
  } else {
    entityAvatar.bottom = isBottom ? 18 : 13;
  }
  if (hasLocation) {
    entityLocation = Map.createAnnotation({
      latitude: post.content.location.latitude,
      longitude: post.content.location.longitude,
      title: _entityURI.host(),
      pincolor: Map.ANNOTATION_RED,
      animate: true,
      myid: 1
    });
    Ti.API.debug("postInfoView.children.length -> " + postInfoView.children.length);
    postInfoView_padding = postInfoView.children.length ? 13 : 0;
    mapView = ui.view({
      properties: {
        left: 14,
        right: 14,
        height: 60,
        bottom: (postContent && viewProperties && viewProperties.autoLink ? 15 : 16) + postInfoView_padding,
        location: {
          latitude: post.content.location.latitude,
          longitude: post.content.location.longitude,
          title: _entityURI.host()
        }
      },
      content: [
        Map.createView({
          left: 0,
          right: 0,
          height: 60,
          touchEnabled: false,
          mapType: Map.STANDARD_TYPE,
          region: {
            latitude: post.content.location.latitude,
            longitude: post.content.location.longitude,
            latitudeDelta: 1,
            longitudeDelta: 1
          },
          animate: true,
          regionFit: true,
          userLocation: false,
          annotations: [entityLocation]
        })
      ],
      events: [
        [
          'click', function(e) {
            var location, mapViewWin;
            location = e.source.location;
            Ti.API.debug("Location: " + (JSON.stringify(location)));
            entityLocation = Map.createAnnotation({
              latitude: location.latitude,
              longitude: location.longitude,
              title: location.title,
              pincolor: Map.ANNOTATION_RED,
              animate: true,
              myid: 1
            });
            mapViewWin = ui.win({
              config: 'windowProperties',
              properties: {
                leftNavButton: ui.button({
                  config: 'backButton',
                  events: [
                    [
                      'click', function(e) {
                        return GLOBAL.tabGroup.activeTab.close(mapViewWin, {
                          animated: true
                        });
                      }
                    ]
                  ]
                })
              },
              content: [
                Map.createView({
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  mapType: Map.STANDARD_TYPE,
                  region: {
                    latitude: location.latitude,
                    longitude: location.longitude,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01
                  },
                  animate: true,
                  regionFit: true,
                  userLocation: false,
                  annotations: [entityLocation]
                })
              ]
            });
            return GLOBAL.tabGroup.activeTab.open(mapViewWin);
          }
        ]
      ]
    });
    rowContent.push(mapView);
    postContentText.bottom = postContentText.bottom + 68;
  }
  postContentView.height = Ti.UI.SIZE;
  wrapView.height = Ti.UI.SIZE;
  rowProperties = {
    className: className,
    postData: post,
    height: Ti.UI.SIZE,
    hasDetails: hasDetails
  };
  if (viewProperties) {
    _.extend(rowProperties, viewProperties);
  }
  row = ui.row({
    config: rowConfig,
    properties: rowProperties,
    content: rowContent
  });
  return row;
};

getInlineMentions = function(postText) {
  var match, mentions, re;
  re = /\^\[(.+)\]\((\d+)\)/ig;
  mentions = [];
  while (match = re.exec(postText)) {
    mentions.push({
      url: match[1],
      index: match[2]
    });
  }
  return mentions;
};

getInlineLinks = function(postText) {
  var match, re;
  re = /\[(.+)\]\((\d+)\)/ig;
  return [
    (function() {
      var _results;
      _results = [];
      while (match = re.exec(postText)) {
        _results.push({
          url: match[1],
          index: match[2]
        });
      }
      return _results;
    })()
  ];
};

post2HTML = function(postData) {
  var postMentions, postText, regex2anchor;
  postText = postData.content.text || postData.content.caption;
  postMentions = postData.mentions;
  regex2anchor = function(foo, p1, p2) {
    return "<a href='" + postMentions[parseInt(p2, 10)].entity + "'>" + p1 + "</a>";
  };
  return postText.replace(/\^\[([^\[\]]+)\]\((\d+)\)/ig, regex2anchor);
};

createWindowTitle = function(title) {
  return Ti.UI.createLabel({
    color: '#2e89c4',
    width: 210,
    height: 18,
    top: 10,
    text: title,
    textAlign: 'center',
    font: {
      fontFamily: 'HelveticaNeue-Medium',
      fontSize: 18
    },
    shadowColor: '#fff',
    shadowOffset: {
      x: 0,
      y: 1
    }
  });
};

exports.overrideTabs = function(tabGroup, backgroundOptions, selectedOptions, deselectedOptions) {
  var background, buttonWrap, i, increment, tab, _i, _len, _ref;
  backgroundOptions = _.extend({
    bottom: 0,
    height: 49,
    width: Ti.UI.FILL
  }, backgroundOptions);
  selectedOptions = _.extend({
    bottom: 0,
    height: 49
  }, selectedOptions);
  background = Ti.UI.createView(backgroundOptions);
  background.touchEnabled = false;
  increment = GLOBAL.isTablet ? 76 : 100 / tabGroup.tabs.length;
  selectedOptions.width = GLOBAL.isTablet ? increment : increment + '%';
  buttonWrap = Ti.UI.createView({
    left: (Ti.Platform.displayCaps.platformWidth - 296) / 2
  });
  background.add(buttonWrap);
  _ref = tabGroup.tabs;
  for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
    tab = _ref[i];
    selectedOptions.left = GLOBAL.isTablet ? (increment * i) + (34 * i) : increment * i + '%';
    selectedOptions.title = tab.title;
    if (tab.backgroundImage) {
      selectedOptions.image = tab.backgroundImage;
    }
    tab.ct = Ti.UI.createImageView(selectedOptions);
    if (GLOBAL.isTablet) {
      buttonWrap.add(tab.ct);
    } else {
      background.add(tab.ct);
    }
  }
  tabGroup.selectTab = function(tab) {
    tab.ct.image = tab.selectedBackgroundImage;
  };
  tabGroup.unselectTab = function(tab) {
    tab.ct.image = tab.backgroundImage;
  };
  tabGroup.hasNotification = function(tab) {
    tab.ct.image = tab.backgroundImage2;
  };
  if (tabGroup.overrideTabs) {
    tabGroup.remove(tabGroup.overrideTabs);
  } else {
    tabGroup.addEventListener('focus', overrideFocusTab);
  }
  tabGroup.add(background);
  tabGroup.overrideTabs = background;
  Titanium.Gesture.addEventListener('orientationchange', function() {
    buttonWrap.left = (Ti.Platform.displayCaps.platformWidth - 296) / 2;
  });
  return tabGroup;
};

overrideFocusTab = function(e) {
  Ti.API.debug('tab focus - overrideFocusTab called');
  if (e.previousIndex >= 0) {
    e.source.unselectTab(e.source.tabs[e.previousIndex]);
  }
  return e.source.selectTab(e.tab);
};

exports.createAvatar = function(image, properties, borderWidth) {
  return ui.view({
    properties: _.extend({
      backgroundImage: 'images/avatar_bg.png',
      width: 46,
      height: 46
    }, properties),
    content: [
      ui.image({
        properties: {
          image: image,
          borderRadius: parseInt((borderWidth ? borderWidth / 2 : 3), 10),
          top: parseInt((borderWidth ? borderWidth / 2 : 4), 10),
          width: properties.width - (borderWidth || 9),
          height: properties.height - (borderWidth || 9)
        }
      })
    ]
  });
};

exports.createTableSection = function(properties, data) {
  var config, cur_data, data_length, i, section, _data, _i, _len;
  section = Ti.UI.createTableViewSection(properties);
  data_length = data.length;
  if (data_length === 1) {
    _data = data[0];
    section.add(ui.row({
      config: _data.config + '_full',
      properties: _data.properties,
      content: [
        ui.view({
          config: 'rowContainerView',
          properties: {
            height: 40,
            top: 1
          },
          content: _data.content
        })
      ],
      events: _data.events
    }));
  } else {
    for (i = _i = 0, _len = data.length; _i < _len; i = ++_i) {
      cur_data = data[i];
      if (i === 0) {
        config = cur_data.config + '_top';
        properties = {
          height: 40,
          bottom: 0
        };
      } else if (i === data_length - 1) {
        config = cur_data.config + '_bottom';
        properties = {
          height: 40,
          top: 0
        };
      } else {
        config = cur_data.config;
        properties = {
          height: 40,
          top: 0
        };
      }
      section.add(ui.row({
        config: config,
        properties: cur_data.properties,
        content: [
          ui.view({
            config: 'rowContainerView',
            properties: properties,
            content: cur_data.content
          })
        ],
        events: cur_data.events
      }));
    }
  }
  return section;
};

exports.createPostQuickMenu = function(eventData) {
  var NewPost, cancelButton, closeMenu, favoritePosts, isFavorite, isRepost, menuWrap, myEntity, myPost, postData, postType, replyAll, rowIndex, slide_menu_in, slide_menu_out, wrapperWin;
  NewPost = require('views/newPost2');
  favoritePosts = require('lib/favoritePosts');
  rowIndex = eventData.index;
  postData = eventData.rowData.postData;
  postType = postData.type.split('#')[0];
  myEntity = GLOBAL.entity;
  replyAll = false;
  myPost = false;
  isRepost = postType === api.postType('Repost');
  isFavorite = favoritePosts.isFavorite(postData.id);
  if (postData.mentions && postData.mentions.length < 2) {
    if (postData.mentions.length && postData.mentions[0].entity !== myEntity) {
      replyAll = true;
    }
  } else {
    replyAll = true;
  }
  if (postData.entity === myEntity) {
    myPost = true;
  }
  wrapperWin = ui.win({
    properties: {
      backgroundColor: 'rgba(0,0,0, 0.3)'
    },
    events: [
      [
        'click', function(e) {
          return closeMenu();
        }
      ], [
        'open', function(e) {
          return menuWrap.animate(slide_menu_in);
        }
      ]
    ]
  });
  menuWrap = ui.view({
    properties: {
      backgroundImage: GLOBAL.isTablet ? 'images/postMenu_bg_ipad.png' : 'images/postMenu_bg.png',
      backgroundLeftCap: GLOBAL.isTablet ? 6 : 0,
      backgroundTopCap: GLOBAL.isTablet ? 6 : 0,
      backgroundRepeat: GLOBAL.isTablet ? false : true,
      height: GLOBAL.isTablet ? 246 : 240,
      bottom: GLOBAL.isTablet ? -246 : -240,
      width: GLOBAL.isTablet ? 320 : Ti.UI.FILL
    },
    content: [
      ui.view({
        properties: {
          height: 234,
          bottom: GLOBAL.isTablet ? 6 : 0,
          bubbleParent: false
        },
        content: [
          Ti.UI.createView({
            backgroundImage: 'images/postMenu_div.png',
            height: 150,
            width: 150,
            top: 46
          }), ui.button({
            properties: {
              top: 23,
              left: 46,
              backgroundImage: 'images/postMenu_reply.png',
              width: 90,
              height: 70
            },
            events: [
              [
                'click', function(e) {
                  (new NewPost({
                    originalPost: postData,
                    reply: true
                  })).open({
                    modal: true
                  });
                  return closeMenu();
                }
              ]
            ]
          }), ui.button({
            properties: {
              enabled: replyAll,
              top: 23,
              right: 46,
              backgroundImage: 'images/postMenu_replyall.png',
              width: 90,
              height: 70
            },
            events: [
              [
                'click', function(e) {
                  (new NewPost({
                    originalPost: postData,
                    reply: true,
                    replyAll: true
                  })).open({
                    modal: true
                  });
                  return closeMenu();
                }
              ]
            ]
          }), (function() {
            if (myPost) {
              return ui.button({
                properties: {
                  bottom: 23,
                  left: 46,
                  backgroundImage: 'images/postMenu_delete.png',
                  width: 90,
                  height: 70
                },
                events: [
                  [
                    'click', function(e) {
                      ui.alert({
                        properties: {
                          cancel: 1,
                          buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                          message: L('delete_the_post', 'Delete the post') + '?',
                          title: L('confirm', 'Confirm')
                        },
                        events: [
                          [
                            'click', function(e) {
                              if (e.index !== e.cancel) {
                                ui.showIndicator();
                                return api.posts_DELETE({
                                  id: postData.id,
                                  eventData: {
                                    rowIndex: rowIndex
                                  }
                                });
                              }
                            }
                          ]
                        ]
                      }).show();
                      return closeMenu();
                    }
                  ]
                ]
              });
            } else {
              return ui.button({
                properties: {
                  bottom: 23,
                  left: 46,
                  backgroundImage: 'images/postMenu_repost.png',
                  width: 90,
                  height: 70
                },
                events: [
                  [
                    'click', function(e) {
                      ui.alert({
                        properties: {
                          cancel: 1,
                          buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                          message: L('repost', 'Repost') + ' ' + (new uri(isRepost ? postData.original_post.post_content.entity : postData.entity)).host()(+'?'),
                          title: L('confirm', 'Confirm')
                        },
                        events: [
                          [
                            'click', function(e) {
                              if (e.index !== e.cancel) {
                                ui.showIndicator();
                                return api.posts_POST({
                                  "permissions": {
                                    "public": true
                                  },
                                  "type": api.postType('Repost'),
                                  "content": {
                                    "entity": isRepost ? postData.original_post.post_content.entity : postData.entity,
                                    "id": isRepost ? postData.original_post.post_content.id : postData.id
                                  },
                                  "mentions": [
                                    {
                                      "entity": isRepost ? postData.original_post.post_content.entity : postData.entity,
                                      "post": isRepost ? postData.original_post.post_content.id : postData.id
                                    }
                                  ]
                                });
                              }
                            }
                          ]
                        ]
                      }).show();
                      return closeMenu();
                    }
                  ]
                ]
              });
            }
          })(), ui.button({
            properties: {
              backgroundImage: isFavorite ? 'images/postMenu_unfav.png' : 'images/postMenu_fav.png',
              bottom: 23,
              right: 46,
              width: 90,
              height: 70
            },
            events: [
              [
                'click', function(e) {
                  if (isFavorite) {
                    favoritePosts.removeFavorite(postData.id);
                    e.source.backgroundImage = 'images/postMenu_unfav.png';
                  } else {
                    favoritePosts.addFavorite(postData);
                    e.source.backgroundImage = 'images/postMenu_fav.png';
                  }
                  return closeMenu();
                }
              ]
            ]
          })
        ]
      })
    ]
  });
  slide_menu_in = Ti.UI.createAnimation({
    bottom: GLOBAL.isTablet ? (Ti.Platform.displayCaps.platformHeught / 2) - 123 : 0,
    duration: 200
  });
  slide_menu_out = Ti.UI.createAnimation({
    bottom: -246,
    duration: 200
  });
  cancelButton = ui.button({
    properties: {
      title: 'Cancel',
      width: 280,
      height: 44,
      bottom: 20
    },
    events: [
      [
        'click', function(e) {
          setTimeout(function() {
            return wrapperWin.close();
          }, 205);
          return menuWrap.animate(slide_menu_out);
        }
      ]
    ]
  });
  wrapperWin.add(menuWrap);
  closeMenu = function() {
    setTimeout(function() {
      return wrapperWin.close();
    }, 205);
    return menuWrap.animate(slide_menu_out);
  };
  return wrapperWin;
};

exports.cropImage = function(original, w, h) {
  var baseImage, cropView;
  baseImage = Ti.UI.createImageView({
    image: original,
    width: w
  });
  cropView = Ti.UI.createView({
    width: w,
    height: h
  });
  cropView.add(baseImage);
  baseImage.left = -(baseImage.size.width / 2 - cropView.width / 2);
  baseImage.top = -(baseImage.size.height / 2 - cropView.height / 2);
  return Ti.UI.createImageView({
    image: cropView.toImage(),
    width: w,
    height: h
  });
};

exports.createRowFromPost = createRowFromPost;

exports.createRowDetailsFromPost = createRowDetailsFromPost;

exports.createWindowTitle = createWindowTitle;
