var apn, ui;

apn = require('lib/apn');

ui = require('lib/ui_sugar');

exports.createView = function() {
  var channelLabel, checkInstallation, self;
  self = ui.win({
    properties: {
      backgroundColor: '#fff',
      layout: 'vertical'
    },
    content: [
      ui.label({
        text: 'remoteNotificationsEnabled: ' + Ti.Network.remoteNotificationsEnabled,
        top: 0,
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE
      }), ui.label({
        text: 'remoteDeviceUUID: ' + Ti.Network.remoteDeviceUUID,
        top: 10,
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE
      })
    ]
  });
  channelLabel = ui.label({
    text: 'My channel: ' + GLOBAL.apn_channel,
    top: 15,
    width: Ti.UI.SIZE,
    height: Ti.UI.SIZE
  });
  self.add(channelLabel);
  checkInstallation = ui.label({
    text: 'checkInstallation: ...',
    top: 10,
    width: Ti.UI.SIZE,
    height: Ti.UI.SIZE
  });
  self.add(checkInstallation);
  apn.checkInstallation({
    onload: function(installationData) {
      var reregisterapn;
      alert(installationData);
      if (installationData.channels.indexOf(GLOBAL.apn_channel) > -1) {
        return checkInstallation.text = 'checkInstallation: OK';
      } else {
        checkInstallation.text = 'checkInstallation: NO CHANNEL';
        reregisterapn = ui.button({
          properties: {
            title: 'Subscribe APN channel',
            top: 15,
            width: Ti.UI.SIZE,
            height: Ti.UI.SIZE
          },
          events: [
            [
              'click', function() {
                return ui.alert({
                  properties: {
                    cancel: 1,
                    buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                    title: 'Are you sure?'
                  },
                  events: [
                    [
                      'click', function(e) {
                        var err;
                        if (e.index !== e.cancel) {
                          try {
                            return apn.subscribeChannel(GLOBAL.apn_channel);
                          } catch (_error) {
                            err = _error;
                            return alert(err);
                          }
                        }
                      }
                    ]
                  ]
                }).show();
              }
            ]
          ]
        });
        return self.add(reregisterapn);
      }
    },
    onerror: function(e) {
      var reregisterapn;
      checkInstallation.text = 'checkInstallation: ERROR';
      alert(e);
      reregisterapn = ui.button({
        properties: {
          title: 'Re-register for APN',
          top: 15,
          width: Ti.UI.SIZE,
          height: Ti.UI.SIZE
        },
        events: [
          [
            'click', function(e) {
              return ui.alert({
                properties: {
                  cancel: 1,
                  buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                  title: 'Are you sure?'
                },
                events: [
                  [
                    'click', function(e) {
                      var err;
                      if (e.index !== e.cancel) {
                        try {
                          apn.unregisterForPushNotifications();
                          return apn.registerForPushNotifications();
                        } catch (_error) {
                          err = _error;
                          return alert(err);
                        }
                      }
                    }
                  ]
                ]
              }).show();
            }
          ]
        ]
      });
      return self.add(reregisterapn);
    }
  });
  return self;
};
