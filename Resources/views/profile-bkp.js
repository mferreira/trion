var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api2'),
	URI = require('lib/vendor/jsuri.min'),
	_f = require('views/func_repo'),
	NewPost = require('views/newPost2'),
	ProfileEdit = require('views/profile_edit'),
	ProfileDetails = require('views/profile_details');


function createView (profile, viewOptions) {
	Ti.API.debug('Creating a profile view for: '+JSON.stringify(profile));

	try {

	if (typeof profile === 'string') profile = api.getCache('discover')[profile];

	// for some error we might not have the profile
	if (!profile) return;

	var isMe = (profile.entity === GLOBAL.entity),
		canViewFollowing = false,
		canViewFollowers = false,
		hasProfile = !!profile.basic,
		openCons = [];

	function updateCounters() {
		if (Ti.Network.online && !GLOBAL.loggedOut && ((new Date()).getTime() - (lastCountUpdate || 0)) > 10000) { // give 10sec between count updates
			var postCountfilters = null;
			if (profile.entity !== GLOBAL.entity) postCountfilters = {entity: profile.entity};

			// setTimeout(function(){
				openCons.push( api.posts_COUNT(postCountfilters, {
					onload: function(count){
						if (isMe) Ti.App.Properties.setInt(GLOBAL.entity+'profile_postCounter', count);
						postCountLabel.text = count;
					},
					onerror: function(e){
						Ti.API.error('posts_COUNT - '+e.status+' - '+e.error);
					}
				}) );
				lastCountUpdate = (new Date()).getTime();
				openCons.push( api.followers_COUNT(profile.entity, {
					onload: function(count){
						if (isMe) Ti.App.Properties.setInt(GLOBAL.entity+'profile_followersCounter', count);
						followersCountLabel.text = count;
						canViewFollowers = true;
					},
					onerror: function(e){
						Ti.API.error('followers_COUNT - '+e.status+' - '+e.error);
					}
				}) );
				openCons.push( api.followings_COUNT(profile.entity, {
					onload: function(count){
						if (isMe) Ti.App.Properties.setInt(GLOBAL.entity+'profile_followingCounter', count);
						followingCountLabel.text = count;
						canViewFollowing = true;
					},
					onerror: function(e){
						Ti.API.error('followings_COUNT - '+e.status+' - '+e.error);
					}
				}) );
			// }, 1000);
		}
		canViewFollowing = true;
		canViewFollowers = true;
	}

	var postCounter = isMe ? Ti.App.Properties.getInt(GLOBAL.entity+'profile_postCounter', '-') : '-',
		followingCounter = isMe ? Ti.App.Properties.getInt(GLOBAL.entity+'profile_followingCounter', '-') : '-',
		followersCounter = isMe ? Ti.App.Properties.getInt(GLOBAL.entity+'profile_followersCounter', '-') : '-';

	var ignore_clicks = false, lastCountUpdate, myProfileData, updateCountersTimer,

	self = ui.win({
		config: 'windowProperties',
		properties: {
			title: 'Profile',
			rightNavButton: ui.button({
				config: 'newPostButton',
				events: [
					['click', function(e){
						e.source.enabled = false;
						(new NewPost(null, null, {button: e.source})).open({modal: true});
					}]
				]
			}),
			leftNavButton: (function(){
				if (viewOptions && viewOptions.backButton) {
					return ui.button({
						config: 'backButton',
						events: [['click', function(e){ GLOBAL.tabGroup.activeTab.close(self, {animated: true}); }]]
					});
				}
			})(),
			layout: 'vertical'
		},
		events: [
			['open', function(e){
				updateCounters();
				if (isMe) updateCountersTimer = setInterval(updateCounters, 300000); // update every 5 minutes
			}],
			['close', function(e){
				// cancel all open connections
				api.cancelXHR(openCons);
				openCons.length = 0;
				self.remove( profileHeader );
				self.remove( tableView );
				clearInterval(updateCountersTimer);
				profileHeader = tableView = tableData = updateCountersTimer = null;
			}],
			['focus', function(e){ tableView.scrollsToTop = true; }],
			['blur', function(e){ tableView.scrollsToTop = false; }]
		]
	});

	if (viewOptions && viewOptions.backButton) {
		self.leftNavButton = ui.button({
			config: 'backButton',
			events: [['click', function(e){ GLOBAL.tabGroup.activeTab.close(self, {animated: true}); }]]
		});
	}

	var profileHeader = ui.view({
		properties: {
			width: Ti.UI.FILL, height: 67,
			backgroundImage: 'images/profile_top_bg.png'
		},
		content: [
			// profile avatar
			(function(){
				var avatar = _f.createAvatar( profile.basic ? profile.basic.avatar_url : 'images/avatar.png', {
					width: 48, height: 48, left: 10
				});
				avatar.addEventListener('click', function(e){
					ignore_clicks = true;
					var profileDetailsView = new ProfileDetails( api.getProfile(profile.entity), {backButton: 'Back'});
					GLOBAL.tabGroup.activeTab.open(profileDetailsView.getView());
					ignore_clicks = false;
				});
				return avatar;
			})(),
			// profile real name
			ui.label({
				config: 'defaultLabel',
				properties: { left: 66, top: 14, text: hasProfile ? profile.basic.name : 'No Profile', font: { fontSize: 16, fontWeight:'bold' }, color: hasProfile ? '#fff' : '#c4c4c4', shadowColor: '#000', shadowOffset: {x:0,y:1} }
			}),
			// profile entity name
			ui.label({
				config: 'defaultLabel',
				properties: { left: 66, top: 34, text: (new URI(profile.entity)).host(), font: { fontSize: 14}, color: '#d4d5d7', shadowColor: '#000', shadowOffset: {x:0,y:1} }
			})
		]
	});
	self.add( profileHeader );

	var tableView = ui.table({ config: 'tableView' }),
	tableData = [];

	// if we're looking at our own profile
	if (isMe) {

		// config button
		profileHeader.add(ui.button({
			properties: { right: 2, backgroundImage: 'images/cog.png', width: 44, height: 44, style:Ti.UI.iPhone.SystemButtonStyle.PLAIN },
			events: [['click', function(e){
				var settings = require('views/settings');
				GLOBAL.tabGroup.activeTab.open( settings( {backButton: self.title} ));
			}]]
		}));

		tableData = [
			_f.createTableSection({
				headerView: Ti.UI.createView({height:7}),
				footerView: Ti.UI.createView({height:5})
			}, [
				{
					config: 'rowWithChild',
					content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Edit Profile'} }), Ti.UI.createImageView({image: 'images/arrow_child.png', right: 0, width: 11, height: 11}) ],
					events: [['click', function(e){
						if (!Ti.Network.online) alert('Looks like you are offline.');
						else if (!ignore_clicks) {
							ignore_clicks = true;
							var profileEditView = new ProfileEdit( api.getProfile(GLOBAL.entity), {backButton: 'Back'});
							GLOBAL.tabGroup.activeTab.open(profileEditView.getView());
							ignore_clicks = false;
						}
					}]]
				}
			]),

			_f.createTableSection({
				headerView: Ti.UI.createView({height:5}),
				footerView: Ti.UI.createView({height:5})
			}, [
				{
					config: 'rowWithChild',
					content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Favorites'} }), Ti.UI.createImageView({image: 'images/arrow_child.png', right: 0, width: 11, height: 11}) ],
					events: [['click', function(e){
						if (!Ti.Network.online) alert('Looks like you are offline.');
						else if (!ignore_clicks) {
							ignore_clicks = true;
							var postFavorites = require('views/postFavorites');
							GLOBAL.tabGroup.activeTab.open(postFavorites({hasBackButton: true}));
							ignore_clicks = false;
						}
					}]]
				},
				{
					config: 'rowWithChild',
					content:  [ ui.label({config: 'defaultRowLabel', properties: {text: 'Drafts'} }), Ti.UI.createImageView({image: 'images/arrow_child.png', right: 0, width: 11, height: 11}) ],
					events: [['click', function(e){
						if (!Ti.Network.online) alert('Looks like you are offline.');
						else if (!ignore_clicks) {
							ignore_clicks = true;
							var postDrafts = require('views/postDrafts');
							GLOBAL.tabGroup.activeTab.open(postDrafts({hasBackButton: true}));
							ignore_clicks = false;
						}
					}]]
				}
			])

		];
	}
	else {
		var addFollowButton = function(isFollowing) {
			// follow button
			var follow_button = ui.button({
				config: isFollowing ? 'followingButton_profile' : 'followButton_profile',
				properties: {
					right: 15,
					isFollowing: isFollowing,
					actInd: Ti.UI.createActivityIndicator({
						style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						right: 15
					})},
				events: [['click', function(e){
					if (!Ti.Network.online) alert('Looks like you are offline.');
					else {
						var source = e.source;
						source.hide();
						source.actInd.show();
						if (!source.isFollowing) {
							openCons.push( api.followings_POST({
								entity: profile.entity
							}, {
								onload: function(newFollow){
									api.addFollowing(newFollow);
									source.applyProperties(ui.defaultConfig['followingButton_profile']);
									source.actInd.hide();
									source.show();
								},
								onerror: function(e){
									if (e.status === 404){
										api.removeFollowing(profile.entity);
										source.applyProperties(ui.defaultConfig['followButton_profile']);
										source.actInd.hide();
										source.show();
									}
									else if (e.status === 409) { // already following error
										api.followings_GET_ENTITY(profile.entity, {
											onload: function(followData) {
												api.addFollowing(followData);
												source.applyProperties(ui.defaultConfig['followingButton_profile']);
												source.actInd.hide();
												source.show();
											},
											onerror: function(e) {
												source.applyProperties(ui.defaultConfig['followingButton_profile']);
												source.actInd.hide();
												source.show();
											}
										});
									}
									else {
										source.actInd.hide();
										source.show();
									}
								}
							}) );
						}
						else {
							openCons.push( api.followings_DELETE({
								id: api.getFollowing(profile.entity).id
							}, {
								onload: function(e){
									api.removeFollowing(profile.entity);
									source.applyProperties(ui.defaultConfig['followButton_profile']);
									source.actInd.hide();
									source.show();
								},
								onerror: function(e){
									if (e.status === 404) {
										api.followings_GET_ENTITY(profile.entity, {
											onload: function(followData) {
												api.followings_DELETE({
													id: followData.id
												}, {
													onload: function(e){
														source.applyProperties(ui.defaultConfig['followButton_profile']);
														source.actInd.hide();
														source.show();
													},
													onerror: function(e){
														source.actInd.hide();
														source.show();
													}
												});
											}
										});
									}
									else {
										source.actInd.hide();
										source.show();
									}
								}
							}) );
						}

					}
				}]]
			});
			profileHeader.add(follow_button);
			profileHeader.add(follow_button.actInd);
		};

		api.isFollowing(profile.entity, Ti.Network.online, {
			onload: function(isFollowing) {
				addFollowButton(isFollowing);
			}
		});

		tableData = [
			_f.createTableSection({
				headerView: Ti.UI.createView({height:7}),
				footerView: Ti.UI.createView({height:5})
			}, [
				{
					config: 'rowWithoutChild',
					content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'New Message'} }) ],
					events: [['click', function(e){
						if (!Ti.Network.online) alert('Looks like you are offline.');
						else if (!ignore_clicks) {
							ignore_clicks = true;
							var newPost = new NewPost({pmSwitch:true, prefill: {content: {text: '^'+profile.entity+' '}, mentions: [profile.entity]} });
							newPost.open({modal: true});
							ignore_clicks = false;
						}
					}]]
				}
			])
		];
	}

	var postCountLabel = ui.label({text: postCounter, textAlign: 'right', color: '#8f9194', right: 0, width: 200, height: Ti.UI.SIZE,
		font: {fontSize:14,fontFamily:'Helvetica Neue'}}),
	followingCountLabel = ui.label({text: followingCounter, textAlign: 'right', color: '#8f9194', right: 0, width: 200, height: Ti.UI.SIZE,
		font: {fontSize:14,fontFamily:'Helvetica Neue'}}),
	followersCountLabel = ui.label({text: followersCounter, textAlign: 'right', color: '#8f9194', right: 0, width: 200, height: Ti.UI.SIZE,
		font: {fontSize:14,fontFamily:'Helvetica Neue'}});

	tableData.push(
		_f.createTableSection({
			headerView: Ti.UI.createView({height:5}),
			footerView: Ti.UI.createView({height:5})
		}, [
			{
				config: 'rowWith2labels',
				content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'All Posts'} }), postCountLabel ],
				events: [['click', function(e){
					if (!Ti.Network.online) alert('Looks like you are offline.');
					else if (!ignore_clicks) {
						ignore_clicks = true;
						var Posts = require('views/userPosts');
						GLOBAL.tabGroup.activeTab.open(new Posts({
							title: (new URI(profile.entity)).host(),
							entity: profile.entity
						}));
						ignore_clicks = false;
					}
				}]]
			},
			{
				config: 'rowWith2labels',
				content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Following'} }), followingCountLabel ],
				events: [['click', function(e){
					if (!Ti.Network.online) alert('Looks like you are offline.');
					else if (!ignore_clicks && (canViewFollowing || followingCounter)) {
						ignore_clicks = true;
						var Bonds = require('views/bonds');
						GLOBAL.tabGroup.activeTab.open(new Bonds('following', profile.entity, { backButton: self.title, maxBonds: followingCountLabel.text }));
						ignore_clicks = false;
					}
				}]]
			},
			{
				config: 'rowWith2labels',
				content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Followers'} }), followersCountLabel ],
				events: [['click', function(e){
					if (!Ti.Network.online) alert('Looks like you are offline.');
					else if (!ignore_clicks && (canViewFollowers || followersCounter)) {
						ignore_clicks = true;
						var Bonds = require('views/bonds');
						GLOBAL.tabGroup.activeTab.open(new Bonds('followers', profile.entity, { backButton: self.title, maxBonds: followersCountLabel.text }));
						ignore_clicks = false;
					}
				}]]
			}
		])
	);

	tableView.setData( tableData );
	self.add(tableView);

	return self;

	} catch (er) {Ti.API.error(er);}
}

module.exports = createView;
