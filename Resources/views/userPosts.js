var StreamView, createView, moment;

moment = require('lib/vendor/moment.min');

StreamView = require('views/streamView');

moment.lang('timeline');

createView = function(conf) {
  var self;
  self = new StreamView({
    title: conf.title,
    api_GET: 'entity_posts_GET',
    api_posts_OLD: 'posts_OLD',
    entity: conf.entity,
    noEvents: true,
    noCache: true,
    hasBackButton: true
  });
  return self;
};

module.exports = createView;
