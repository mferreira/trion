var api, createView, drafts, isPortrait, statusPostTextLimit, ui, url_short, _;

ui = require('lib/ui_sugar');

api = require('lib/tent.api2');

drafts = require('lib/drafts');

url_short = require('lib/url_shortener');

_ = require('lib/vendor/underscore-min');

statusPostTextLimit = api.postType('postTypeProperties')[api.postType('Status')].textLimit;

isPortrait = null;

createView = function(newPostData, callback, args) {
  var allBonds, allBonds_split, cancelButton_onclick, charCounter, countPostChars, cur_postData, discoverData, entityLookup, flagLocation, flagPrivate, getTopBarHeight, get_name_or_entity, get_uri_avatar, hidePossibleMentions, isPosting, isReply, isRepost, locationActInd, locationSwitch, locationSwitch_wrap, location_latlon, mentionSelector, mentionSelector_callback, mentionSelector_table, mentionSelector_visible, navLeftButton, navLeftButton2, navRightButton, navRightButton2, newPost_args, newPost_callback, photoArray, photoButton, photoFrame, pmLabel, pmSwitch, pmSwitch_value, postButton_onclick, postInput, postInput_focus, prefill, replyAll, selectedPosttype, self, showPossibleMentions, toolbar, topBar, topBar_buttons, updatePostInput, userButton;
  Ti.API.debug("Creating 'New Post' view with data: " + (JSON.stringify(newPostData)));
  newPost_args = newPostData;
  newPost_callback = callback;
  if (newPostData) {
    cur_postData = newPostData.originalPost;
    prefill = newPostData.prefill;
    isReply = newPostData.reply || false;
    replyAll = newPostData.replyAll || false;
  } else {
    cur_postData = null;
    prefill = null;
    isReply = false;
    replyAll = false;
  }
  pmSwitch_value = newPost_args ? newPost_args.pmSwitch || false : false;
  isPosting = false;
  flagPrivate = false;
  flagLocation = false;
  location_latlon = null;
  postInput_focus = true;
  isRepost = newPostData && newPostData.originalPost ? newPostData.originalPost.type === api.postType('Repost') : false;
  discoverData = api.getCache('discover');
  allBonds = _.keys(discoverData).join(' ');
  allBonds_split = allBonds.split(' ');
  selectedPosttype = api.postType('Status');
  getTopBarHeight = function() {
    return 20 + (isPortrait ? 44 : 32);
  };
  updatePostInput = function(e) {
    var _ref, _ref1;
    if ((_ref = e.orientation) !== Ti.UI.FACE_UP && _ref !== Ti.UI.FACE_DOWN && _ref !== Ti.UI.UNKNOWN) {
      return;
    }
    isPortrait = (_ref1 = e.orientation) === Ti.UI.PORTRAIT || _ref1 === Ti.UI.UPSIDE_PORTRAIT;
    topBar.applyProperties({
      height: getTopBarHeight()
    });
    mentionSelector.applyProperties({
      top: topBar.height + 46
    });
    postInput.applyProperties({
      top: topBar.height
    });
    toolbar.bottom = GLOBAL.keyboardHeight();
    if (isPortrait) {
      if (!GLOBAL.isTablet) {
        Ti.API.debug('Updating navbar buttons.');
        self.setLeftNavButton(navLeftButton);
        self.setRightNavButton(navRightButton);
      }
      if (!mentionSelector_visible) {
        postInput.applyProperties({
          height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 64),
          width: Ti.UI.FILL
        });
      } else {
        postInput.applyProperties({
          height: 45,
          width: Ti.UI.FILL
        });
      }
      mentionSelector.applyProperties({
        width: Ti.UI.FILL,
        top: 46,
        bottom: GLOBAL.keyboardHeight() + 34
      });
    } else {
      if (!GLOBAL.isTablet) {
        Ti.API.debug('Updating navbar buttons.');
        self.setLeftNavButton(navLeftButton2);
        self.setRightNavButton(navRightButton2);
      }
      mentionSelector.applyProperties({
        width: 200,
        top: 0,
        bottom: GLOBAL.keyboardHeight() + 34
      });
      if (!mentionSelector_visible) {
        postInput.applyProperties({
          height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 52),
          width: Ti.UI.FILL
        });
      } else {
        postInput.applyProperties({
          height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 52),
          width: Ti.Platform.displayCaps.platformWidth - mentionSelector.width
        });
      }
    }
    return photoFrame.applyProperties({
      height: GLOBAL.keyboardHeight()
    });
  };
  countPostChars = function(text) {
    var match, re, stripText, urls;
    re = /(?:[^\S]|^)(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*)/ig;
    urls = 0;
    stripText = text;
    while (match = re.exec(text)) {
      stripText = stripText.replace(match[1], '');
      urls += 1;
    }
    return stripText.length + (urls * 18);
  };
  entityLookup = function(searchFor) {
    var match, matches, re;
    Ti.API.debug("entityLookup: '" + searchFor + "'");
    re = new RegExp('(\\S*' + searchFor + '\\S*)', 'ig');
    matches = [];
    while (match = re.exec(allBonds)) {
      matches.push(match[1].toLowerCase());
    }
    return matches;
  };
  get_name_or_entity = function(uri) {
    var d;
    d = discoverData[uri];
    if (d && d.profile && d.profile.name) {
      return d.profile.name;
    }
    return uri;
  };
  get_uri_avatar = function(uri) {
    var d;
    return d = discoverData[uri];
  };
  showPossibleMentions = function(uris, callback) {
    var mentionSelector_callback, mentionSelector_visible;
    Ti.API.debug('showPossibleMentions() called');
    Ti.API.debug("isPortrait -> " + isPortrait);
    mentionSelector_callback = callback;
    mentionSelector_table.data = (function() {
      var ent, rows, uri, _i, _len;
      rows = [];
      for (_i = 0, _len = uris.length; _i < _len; _i++) {
        uri = uris[_i];
        ent = discoverData[uri];
        rows.push(ui.row({
          properties: {
            height: 35
          },
          content: [
            Ti.UI.createImageView({
              width: 30,
              height: 30,
              left: 5,
              borderRadius: 3,
              defaultImage: 'images/avatar.png',
              image: ent.basic && ent.basic.avatar_url ? ent.basic.avatar_url : ''
            }), Ti.UI.createLabel({
              text: ent.basic && ent.basic.name ? ent.basic.name : uri,
              left: 40,
              right: 5,
              color: '#313339',
              font: {
                fontSize: 14,
                fontFamily: 'Helvetica'
              }
            })
          ]
        }));
      }
      return rows;
    })();
    mentionSelector_visible = true;
    userButton.image = 'images/post_user_icon_active.png';
    if (isPortrait) {
      return postInput.applyProperties({
        height: 45
      });
    } else {
      if (mentionSelector.width !== 200) {
        mentionSelector.applyProperties({
          width: 200,
          top: 0,
          bottom: GLOBAL.keyboardHeight() + 34
        });
      }
      return postInput.applyProperties({
        width: Ti.Platform.displayCaps.platformWidth - mentionSelector.width
      });
    }
  };
  hidePossibleMentions = function() {
    var mentionSelector_visible;
    Ti.API.debug('hidePossibleMentions() called');
    if (isPortrait) {
      postInput.applyProperties({
        height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 64)
      });
    } else {
      postInput.applyProperties({
        width: Ti.UI.FILL
      });
    }
    userButton.image = 'images/post_user_icon.png';
    return mentionSelector_visible = false;
  };
  cancelButton_onclick = function(e) {
    if (postInput.value.length) {
      return ui.optionDialog({
        properties: {
          options: [L('dont_save', "Don't Save"), L('save_draft', "Save Draft"), L('cancel', "Cancel")],
          cancel: 2,
          destructive: 0
        },
        events: [
          [
            'click', function(e) {
              if (e.index === e.destructive) {
                return self.close();
              } else if (e.index === 1) {
                drafts.save({
                  type: api.postType('Status'),
                  content: {
                    text: postInput.value
                  }
                });
                return self.close();
              }
            }
          ]
        ]
      }).show();
    } else {
      return self.close();
    }
  };
  postButton_onclick = function(e) {
    var checkAllUrls, cur_url, getMentions, getUrls, output, postIt, shortifyURL, urls, _i, _len, _url, _urls;
    isPosting = true;
    ui.showIndicator();
    getUrls = function(text) {
      var match, re, urls;
      re = /(?:[^\S]|^)(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*)/ig;
      urls = [];
      while (match = re.exec(text)) {
        urls.push(match[1]);
      }
      return urls;
    };
    getMentions = function(text) {
      var match, re, urls;
      re = /(\^(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*))/ig;
      urls = [];
      while (match = re.exec(text)) {
        urls.push(match[2].toLowerCase());
      }
      return urls;
    };
    _urls = getUrls(postInput.value);
    urls = {};
    output = postInput.value;
    cur_url = null;
    for (_i = 0, _len = _urls.length; _i < _len; _i++) {
      _url = _urls[_i];
      urls[_url] = null;
    }
    checkAllUrls = function() {
      var match, rep_regex, rep_url, shortified, url, _j, _len1;
      Ti.API.debug('checkAllUrls...');
      for (_j = 0, _len1 = _urls.length; _j < _len1; _j++) {
        url = _urls[_j];
        shortified = urls[url];
        if (shortified === null) {
          if (url.length > url_short.sites[url_short.choice].chars) {
            return shortifyURL(url);
          } else {
            urls[url] = url;
          }
        } else {
          rep_url = url.replace(/\//g, '\\/').replace(':', '\\:').replace(/\./g, '\\.');
          rep_regex = new RegExp("(?:[^\\S]|^)(" + rep_url + ")", 'gi');
          match = rep_regex.exec(output);
          output = output.replace(match[1], shortified);
        }
      }
      return postIt();
    };
    shortifyURL = function(uri) {
      var xhr;
      Ti.API.debug("shortifying: " + uri);
      cur_url = uri;
      xhr = Ti.Network.createHTTPClient({
        onload: function(e) {
          Ti.API.debug("Shortified url: " + this.responseText);
          urls[cur_url] = this.responseText;
          return checkAllUrls();
        },
        onerror: function(e) {
          Ti.API.error("Something went wrong shortifying url: " + uri);
          return checkAllUrls();
        }
      });
      xhr.open('GET', url_short.sites[url_short.choice].uri + encodeURIComponent(uri), false);
      return xhr.send();
    };
    postIt = function() {
      var cur_entity, input_mentions, m, mention, mentionDone, mentionIndex, mentions_post_entity, mentions_post_id, payload, postItNow, _j, _k, _l, _len1, _len2, _len3, _len4, _m, _ref, _ref1, _ref2;
      Ti.API.debug("post shortified: " + output);
      payload = {
        permissions: {
          "public": !flagPrivate,
          entities: []
        },
        type: photoArray.length ? api.postType('Photo') : api.postType('Status'),
        content: photoArray.length ? {
          caption: output
        } : {
          text: output
        },
        mentions: []
      };
      if (flagLocation && location_latlon) {
        payload.content.location = location_latlon;
      }
      input_mentions = getMentions(output);
      if (input_mentions.length) {
        mentionIndex = 0;
        mentionDone = [];
        for (_j = 0, _len1 = input_mentions.length; _j < _len1; _j++) {
          m = input_mentions[_j];
          if (mentionDone.indexOf(m) >= 0 || (isReply && cur_postData.entity === m)) {
            continue;
          }
          mentionDone.push(m);
          payload.mentions.push({
            entity: m
          });
          output = output.replace('^' + m, '^[' + get_name_or_entity(m) + '](' + mentionIndex + ')');
          mentionIndex += 1;
        }
      }
      if (payload.type === api.postType('Photo')) {
        payload.content.caption = output;
        payload.attachments = photoArray;
      } else if (payload.type === api.postType('Status')) {
        payload.content.text = output;
      }
      if (prefill && prefill.mentions) {
        _ref = prefill.mentions;
        for (_k = 0, _len2 = _ref.length; _k < _len2; _k++) {
          cur_entity = _ref[_k];
          payload.mentions.push({
            entity: cur_entity
          });
        }
      }
      if (cur_postData && isReply) {
        mentions_post_id = isRepost ? cur_postData.original_post.post_content.id : cur_postData.id;
        mentions_post_entity = isRepost ? cur_postData.original_post.post_content.entity : cur_postData.entity;
        payload.mentions.push({
          entity: mentions_post_entity,
          post: mentions_post_id
        });
        if (replyAll) {
          _ref1 = cur_postData.mentions;
          for (_l = 0, _len3 = _ref1.length; _l < _len3; _l++) {
            cur_entity = _ref1[_l];
            if (cur_entity === cur_postData.entity || cur_entity === GLOBAL.entity) {
              continue;
            }
            payload.mentions.push({
              entity: cur_entity
            });
          }
        }
      }
      if (flagPrivate) {
        if (payload.mentions.length) {
          _ref2 = payload.mentions;
          for (_m = 0, _len4 = _ref2.length; _m < _len4; _m++) {
            mention = _ref2[_m];
            payload.permissions.entities.push(mention.entity);
          }
        } else if (cur_postData) {
          payload.permissions.entities.push(cur_postData.entity);
        }
      }
      postItNow = function() {
        return api.posts_POST(payload, {
          onload: function(e) {
            if (newPost_callback) {
              newPost_callback(newPost_args.callbackArgs);
            }
            self.close();
            cur_postData = null;
            isPosting = false;
            ui.hideIndicator();
            return Ti.App.fireEvent('ui:myStream:addPost', {
              post: e.post
            });
          },
          onerror: function(e) {
            isPosting = false;
            ui.hideIndicator();
            return ui.alert({
              properties: {
                cancel: 1,
                buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                message: 'Sorry, your post could not be sent. Would you like to try again?',
                title: 'Error'
              },
              events: [
                [
                  'click', function(e) {
                    if (e.index !== e.cancel) {
                      return postItNow();
                    }
                  }
                ]
              ]
            }).show();
          }
        });
      };
      Ti.API.debug("Posting new post with: " + (JSON.stringify(payload)));
      return postItNow();
    };
    return checkAllUrls();
  };
  self = ui.win({
    config: 'windowProperties',
    properties: {
      title: 'New Post',
      orientationModes: (function() {
        if (Ti.Platform.osname === "iphone") {
          return [Ti.UI.PORTRAIT, Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT];
        } else {
          return [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT, Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT];
        }
      })()
    },
    events: [
      [
        'focus', function(e) {
          var canPost, content, inputCharsLeft, postInput_length, _ref;
          isPortrait = (_ref = e.orientation) === Ti.UI.PORTRAIT || _ref === Ti.UI.UPSIDE_PORTRAIT;
          topBar.applyProperties({
            height: getTopBarHeight()
          });
          postInput.applyProperties({
            top: topBar.height
          });
          Ti.API.debug("e.orientation in [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]: " + isPortrait);
          Ti.API.debug("topBar.height: " + topBar.height);
          Ti.API.debug("postInput.top: " + postInput.top);
          if (pmSwitch_value) {
            flagPrivate = true;
            pmSwitch.image = 'images/post_private_active_icon.png';
          } else if (isReply && cur_postData) {
            flagPrivate = cur_postData.permissions;
            pmSwitch.image = flagPrivate ? 'images/post_private_active_icon.png' : 'images/post_private_icon.png';
          }
          postInput.value = postInput.value ? postInput.value : (function() {
            var cur_entity, mention, postInput_value, _i, _len, _ref1;
            postInput_value = '';
            if (isReply) {
              if (cur_postData.entity !== GLOBAL.entity) {
                postInput_value = "^" + cur_postData.entity + " ";
              }
              if (replyAll) {
                _ref1 = cur_postData.mentions;
                for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
                  mention = _ref1[_i];
                  cur_entity = mention.entity;
                  if ([cur_postData.entity, GLOBAL.entity].indexOf(cur_entity) > -1) {
                    continue;
                  }
                  postInput_value += "^" + cur_entity + " ";
                }
              }
            } else if (prefill) {
              postInput_value = prefill.content.text;
            }
            return postInput_value;
          })();
          postInput_length = postInput.value.length;
          postInput.focus();
          postInput.setSelection(postInput_length, postInput_length);
          content = postInput.value;
          if (photoArray.length) {
            inputCharsLeft = "∞";
            canPost = true;
          } else {
            inputCharsLeft = statusPostTextLimit - countPostChars(content);
            canPost = Ti.Network.online && inputCharsLeft > -1 && inputCharsLeft < statusPostTextLimit;
          }
          charCounter.setText(inputCharsLeft);
          navRightButton.enabled = canPost;
          return navRightButton2.enabled = canPost;
        }
      ], [
        'open', function(e) {
          isPortrait = e.source.orientation === Ti.UI.PORTRAIT;
          toolbar.bottom = GLOBAL.keyboardHeight();
          postInput.height = Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 35 + (isPortrait ? 64 : 52));
          Ti.Gesture.addEventListener('orientationchange', updatePostInput);
          if (args && args.button) {
            return args.button.enabled = true;
          }
        }
      ], [
        'close', function(e) {
          var lastKeyboardHeight;
          Ti.Gesture.removeEventListener('orientationchange', updatePostInput);
          return lastKeyboardHeight = GLOBAL.keyboardHeight();
        }
      ]
    ]
  });
  navLeftButton = ui.button({
    config: 'cancelButton',
    events: [['click', cancelButton_onclick]]
  });
  navRightButton = ui.button({
    config: 'postButton',
    events: [['click', postButton_onclick]]
  });
  navLeftButton2 = ui.button({
    properties: {
      systemButton: Ti.UI.iPhone.SystemButton.STOP,
      tintColor: 'white'
    },
    events: [['click', cancelButton_onclick]]
  });
  navRightButton2 = ui.button({
    config: 'postButton2',
    events: [['click', postButton_onclick]]
  });
  topBar_buttons = ui.toolBar({
    properties: {
      top: 12,
      width: Ti.UI.FILL,
      backgroundColor: "#009fdd",
      barColor: "#009fdd",
      items: [navLeftButton, ui.FLEXSPACE, navRightButton],
      borderBottom: false,
      borderTop: false,
      tintColor: 'white'
    }
  });
  topBar = ui.view({
    properties: {
      zIndex: 2,
      top: 0,
      height: getTopBarHeight(),
      width: Ti.UI.FILL,
      backgroundColor: '#009fdd'
    },
    content: [topBar_buttons]
  });
  pmLabel = Ti.UI.createLabel({
    text: 'Private',
    width: Ti.UI.SIZE,
    height: Ti.UI.SZIE,
    textAlign: 'left',
    left: 0
  });
  pmSwitch = ui.button({
    properties: {
      image: 'images/post_private_icon.png',
      style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
      left: 0,
      width: 40,
      height: 25
    },
    events: [
      [
        'singletap', function(e) {
          flagPrivate = !flagPrivate;
          e.source.image = flagPrivate ? 'images/post_private_active_icon.png' : 'images/post_private_icon.png';
          return Ti.API.debug("Private switch is: " + (flagPrivate ? 'on' : 'off'));
        }
      ]
    ]
  });
  locationSwitch_wrap = Ti.UI.createView({
    left: 0,
    width: 40,
    height: 25
  });
  locationActInd = Ti.UI.createActivityIndicator({
    color: '#31333b',
    style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
    width: 40,
    height: 25
  });
  locationSwitch = ui.button({
    properties: {
      image: 'images/post_loc_icon.png',
      style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
      width: 40,
      height: 25
    },
    events: [
      [
        'singletap', function(e) {
          var geo, grabDeviceLocation;
          flagLocation = !flagLocation;
          Ti.API.debug("Location switch is: " + (flagLocation ? 'on' : 'off'));
          if (flagLocation) {
            geo = require('lib/geo');
            grabDeviceLocation = function() {
              return geo.getCurrentPosition({
                onload: function(latlon) {
                  location_latlon = latlon;
                  locationSwitch.image = 'images/post_loc_icon_active.png';
                  locationActInd.hide();
                  return locationSwitch.show();
                },
                onerror: function(e) {
                  locationSwitch.image = 'images/post_loc_icon.png';
                  locationActInd.hide();
                  locationSwitch.show();
                  return flagLocation = false;
                }
              });
            };
            locationSwitch.hide();
            locationActInd.show();
            if (geo.isAvailable()) {
              if (!geo.started) {
                return geo.start({
                  onload: function() {
                    return grabDeviceLocation();
                  },
                  onerror: function() {
                    locationSwitch.image = 'images/post_loc_icon.png';
                    locationActInd.hide();
                    locationSwitch.show();
                    return flagLocation = false;
                  }
                });
              } else {
                return grabDeviceLocation();
              }
            } else if (geo.isEnabled()) {
              return geo.start({
                onload: function() {
                  return grabDeviceLocation();
                },
                onerror: function() {
                  locationSwitch.image = 'images/post_loc_icon.png';
                  locationActInd.hide();
                  locationSwitch.show();
                  flagLocation = false;
                  return alert('Unable to get your location. Please, check your device privacy settings.');
                }
              });
            } else {
              return geo.start({
                onload: function() {
                  return grabDeviceLocation();
                },
                onerror: function() {
                  locationSwitch.image = 'images/post_loc_icon.png';
                  locationActInd.hide();
                  locationSwitch.show();
                  return flagLocation = false;
                }
              });
            }
          } else {
            location_latlon = null;
            return e.source.image = 'images/post_loc_icon.png';
          }
        }
      ]
    ]
  });
  userButton = ui.button({
    properties: {
      image: 'images/post_user_icon.png',
      style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
      left: 0,
      width: 40,
      height: 25
    },
    events: [
      [
        'singletap', function(e) {
          var postInput_length;
          if (mentionSelector_visible) {
            hidePossibleMentions();
          } else {
            if (allBonds_split.length) {
              postInput_length = postInput.value.length;
              if (postInput_length && postInput.value[postInput_length - 1] !== '\n') {
                postInput.value += ' ';
              }
              postInput.value += '^';
              showPossibleMentions(allBonds_split, function(index) {
                return postInput.value = "" + postInput.value + allBonds_split[index] + " ";
              });
            } else {
              hidePossibleMentions();
            }
          }
          return Ti.API.debug("Location switch is: " + (mentionSelector_visible ? 'on' : 'off'));
        }
      ]
    ]
  });
  photoArray = [];
  photoButton = ui.button({
    properties: {
      image: 'images/post_photos_icon.png',
      style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
      left: 0,
      width: 40,
      height: 25
    },
    events: [
      [
        'click', function(e) {
          var onMediaSuccess;
          if (photoArray.length) {
            if (postInput_focus) {
              postInput.blur();
              return postInput_focus = false;
            } else {
              postInput.focus();
              return postInput_focus = true;
            }
          } else {
            onMediaSuccess = function(media) {
              postInput.focus();
              postInput_focus = true;
              photoButton.image = 'images/post_photos_active_icon.png';
              photoArray.push(media.media);
              photoFrame.applyProperties({
                image: media.media,
                height: GLOBAL.keyboardHeight()
              });
              return charCounter.setText("∞");
            };
            return ui.optionDialog({
              properties: {
                options: [L('from_camera', "From Camera"), L('from_library', "From Library"), L('cancel', "Cancel")],
                cancel: 2
              },
              events: [
                [
                  'click', function(e) {
                    if (e.index === 0) {
                      return Ti.Media.showCamera({
                        mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
                        allowEditing: true,
                        success: onMediaSuccess
                      });
                    } else if (e.index === 1) {
                      return Ti.Media.openPhotoGallery({
                        mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
                        allowEditing: true,
                        success: onMediaSuccess
                      });
                    }
                  }
                ]
              ]
            }).show();
          }
        }
      ]
    ]
  });
  photoFrame = ui.image({
    properties: {
      height: GLOBAL.keyboardHeight(),
      bottom: 0
    },
    events: [
      [
        'click', function(e) {
          return ui.optionDialog({
            properties: {
              options: [L('remove_image', "Remove Image"), L('cancel', "Cancel")],
              destructive: 0,
              cancel: 1
            },
            events: [
              [
                'click', function(e) {
                  if (e.index === e.destructive) {
                    postInput.focus();
                    postInput_focus = true;
                    photoButton.image = 'images/post_photos_icon.png';
                    photoFrame.image = null;
                    photoArray.pop();
                    return charCounter.setText(statusPostTextLimit - countPostChars(postInput.value));
                  }
                }
              ]
            ]
          }).show();
        }
      ]
    ]
  });
  charCounter = Ti.UI.createLabel({
    width: Ti.UI.SIZE,
    height: Ti.UI.SZIE,
    textAlign: 'right',
    right: 20,
    text: photoArray.length ? "∞" : "" + statusPostTextLimit,
    font: {
      fontSize: 13,
      fontWeight: 'bold'
    },
    color: '#31333b'
  });
  toolbar = ui.view({
    properties: {
      zIndex: 2,
      bottom: GLOBAL.keyboardHeight(),
      left: 0,
      right: 0,
      height: 34,
      backgroundImage: 'images/kb_toolbar_bg.png',
      backgroundTopCap: 5
    },
    content: [
      ui.view({
        properties: {
          width: Ti.UI.SZIE,
          height: Ti.UI.SZIE,
          layout: 'horizontal',
          left: 10,
          top: 6
        },
        content: [pmSwitch, locationSwitch_wrap, userButton, photoButton]
      }), charCounter
    ]
  });
  postInput = ui.textArea({
    properties: {
      zIndex: 2,
      left: 0,
      top: topBar.height,
      height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() + 34 + 64),
      width: Ti.UI.FILL,
      keyboardType: Ti.UI.KEYBOARD_ASCII,
      suppressReturn: false,
      autocorrect: true,
      autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES,
      color: '#313339',
      font: {
        fontSize: 14,
        fontFamily: 'Helvetica'
      }
    },
    events: [
      [
        'selected', function(e) {
          var textSelected;
          return textSelected = e.range;
        }
      ], [
        'change', function(e) {
          var canPost, content, contentLength, curChar, inputCharsLeft, mentionStart, previousChar, uris;
          contentLength = e.source.value.length;
          content = e.source.value;
          inputCharsLeft = photoArray.length ? "∞" : statusPostTextLimit - countPostChars(content);
          charCounter.setText(inputCharsLeft);
          if (Ti.Network.online) {
            canPost = photoArray.length ? true : inputCharsLeft > -1 && inputCharsLeft < statusPostTextLimit;
            navRightButton.enabled = canPost;
            navRightButton2.enabled = canPost;
          }
          if (contentLength > 1) {
            curChar = content[contentLength - 1];
            previousChar = content[contentLength - 2];
            if (curChar !== ' ' && curChar !== '\n') {
              if (previousChar === '^') {
                mentionStart = contentLength - 1;
                uris = entityLookup(curChar);
                Ti.API.debug("Possible mentions: " + (JSON.stringify(uris)));
                if (uris.length) {
                  return showPossibleMentions(uris, function(index) {
                    return postInput.value = postInput.value.slice(0, mentionStart) + uris[index];
                  });
                } else {
                  return hidePossibleMentions();
                }
              } else if (mentionStart !== null && contentLength > mentionStart) {
                uris = entityLookup(content.slice(mentionStart, contentLength));
                Ti.API.debug("Possible mentions: " + (JSON.stringify(uris)));
                if (uris.length) {
                  return showPossibleMentions(uris, function(index) {
                    return postInput.value = postInput.value.slice(0, mentionStart) + uris[index];
                  });
                } else {
                  return hidePossibleMentions();
                }
              }
            } else {
              mentionStart = null;
              return hidePossibleMentions();
            }
          } else {
            mentionStart = null;
            return hidePossibleMentions();
          }
        }
      ], [
        'focus', function(e) {
          var mentionSelector_visible;
          userButton.image = 'images/post_user_icon.png';
          return mentionSelector_visible = false;
        }
      ]
    ]
  });
  mentionSelector_visible = false;
  mentionSelector_callback = null;
  mentionSelector_table = ui.table({
    properties: {
      width: Ti.UI.FILL
    },
    events: [
      [
        'click', function(e) {
          if (mentionSelector_callback) {
            mentionSelector_callback(e.index);
          }
          return hidePossibleMentions();
        }
      ]
    ]
  });
  mentionSelector = ui.view({
    properties: {
      zIndex: 1,
      right: 0,
      width: Ti.UI.FILL,
      top: topBar.height + 46,
      bottom: GLOBAL.keyboardHeight() + 34,
      backgroundImage: 'images/kb_toolbar_bg.png',
      backgroundTopCap: 5
    },
    content: [mentionSelector_table]
  });
  locationSwitch_wrap.add(locationActInd);
  locationSwitch_wrap.add(locationSwitch);
  self.add(topBar);
  self.add(mentionSelector);
  self.add(postInput);
  self.add(toolbar);
  self.add(photoFrame);
  return self;
};

module.exports = createView;
