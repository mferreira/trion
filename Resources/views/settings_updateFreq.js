var api, createView, ui, _f;

ui = require('lib/ui_sugar');

api = require('lib/tent.api');

_f = require('views/func_repo');

createView = function(settings, callback) {
  var section, self, settings_frequently, settings_never, settings_rarely, tableView, updateCheckboxes;
  Ti.API.debug('settings.updateFreq: ' + JSON.stringify(settings));
  self = ui.win({
    config: 'windowProperties',
    properties: {
      title: 'Update Posts',
      leftNavButton: ui.button({
        config: 'backButton',
        events: [
          [
            'click', function(e) {
              GLOBAL.tabGroup.activeTab.close(self, {
                animated: true
              });
              if (callback && callback.onclose) {
                return callback.onclose(settings);
              }
            }
          ]
        ]
      })
    }
  });
  tableView = ui.table({
    config: 'tableView'
  });
  settings_frequently = Ti.UI.createImageView({
    image: settings.frequently ? 'images/checkbox_active.png' : 'images/checkbox.png',
    width: 25,
    right: 0
  });
  settings_rarely = Ti.UI.createImageView({
    image: settings.rarely ? 'images/checkbox_active.png' : 'images/checkbox.png',
    width: 25,
    right: 0
  });
  settings_never = Ti.UI.createImageView({
    image: settings.never ? 'images/checkbox_active.png' : 'images/checkbox.png',
    width: 25,
    right: 0
  });
  section = _f.createTableSection({
    headerView: Ti.UI.createView({
      height: 9
    }),
    footerView: Ti.UI.createView({
      height: 5
    })
  }, [
    {
      config: 'rowWith2labels',
      properties: {
        className: 'row_labelandimage'
      },
      content: [
        ui.label({
          config: 'defaultRowLabel',
          properties: {
            text: 'Frequently'
          }
        }), settings_frequently
      ],
      events: [
        [
          'click', function(e) {
            settings.frequently = true;
            settings.rarely = false;
            settings.never = false;
            return updateCheckboxes();
          }
        ]
      ]
    }, {
      config: 'rowWith2labels',
      properties: {
        className: 'row_labelandimage'
      },
      content: [
        ui.label({
          config: 'defaultRowLabel',
          properties: {
            text: 'Rarely'
          }
        }), settings_rarely
      ],
      events: [
        [
          'click', function(e) {
            settings.frequently = false;
            settings.rarely = true;
            settings.never = false;
            return updateCheckboxes();
          }
        ]
      ]
    }, {
      config: 'rowWith2labels',
      properties: {
        className: 'row_labelandimage'
      },
      content: [
        ui.label({
          config: 'defaultRowLabel',
          properties: {
            text: 'Never'
          }
        }), settings_never
      ],
      events: [
        [
          'click', function(e) {
            settings.frequently = false;
            settings.rarely = false;
            settings.never = true;
            return updateCheckboxes();
          }
        ]
      ]
    }
  ]);
  tableView.setData([section]);
  self.add(tableView);
  updateCheckboxes = function() {
    settings_frequently.image = settings.frequently ? 'images/checkbox_active.png' : 'images/checkbox.png';
    settings_rarely.image = settings.rarely ? 'images/checkbox_active.png' : 'images/checkbox.png';
    return settings_never.image = settings.never ? 'images/checkbox_active.png' : 'images/checkbox.png';
  };
  return self;
};

module.exports = createView;
