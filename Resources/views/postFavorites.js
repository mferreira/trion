var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api'),
	NewPost = require('views/newPost2'),
	favoritePosts = require('lib/favoritePosts'),
	_f = require('views/func_repo');


function createView(conf) {

	var tableView_clicking = false,

	self = ui.win({
		config: 'windowProperties',
		properties: {
			title:'Favorites',
			rightNavButton: ui.button({
				config: 'newPostButton',
				events: [ ['click', function(e){
					e.source.enabled = false;
					(new NewPost(null, null, {button: e.source})).open({modal: true});
				}] ]
			}),
			leftNavButton: (function(){
				if (conf.hasBackButton) {
					return ui.button({
						config: 'backButton',
						properties: {backgroundImage: 'images/back_profile.png', width: 64},
						events: [['click', function(e){ ignore_clicks = true; GLOBAL.tabGroup.activeTab.close(self, {animated: true}); ignore_clicks = false; }]]
					});
				}
			})(),
			navBarHidden: false
		},
		events: [
			['open', function(e){
				Ti.API.debug('postFavorites fired event: OPEN');
			}],
			['focus', function(e){
				var section = Ti.UI.createTableViewSection(),
					posts = favoritePosts.getFavorites(),
					posts_length = posts.length;

				if (!posts.length) {
					section.add(ui.row({
						config: 'rowWithoutChild_full',
						properties: {
							height: 40
						},
						content: [
							ui.view({
								properties: {touchEnabled: false, top: 12, bottom: 12, left: 14, right: 14},
								content: [
									ui.label({touchEnabled: false, text: 'No Favorites', width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
										color: '#6B6A67', font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'} })
								]
							})
						]
					}));
				}
				else {
					for (var i=posts_length; i--;) {
						var postData = posts[i];
						var rowBg = (posts_length === 1) ? 'images/row_bg_full.png' : (i===posts_length-1) ? 'images/row_bg_top.png' : (!i) ? 'images/row_bg_bottom.png' : 'images/row_bg_mid.png';
						var row = new _f.createRowFromPost(postData, {backgroundImage: rowBg, backgroundTopCap: 8, backgroundLeftCap: 20});
						section.add(row);
					}
				}
				tableView.setData([section]);
			}]
		]
	}),

	tableView = ui.table({
		config: 'tableView',
		properties: {minRowHeight: 75},
		events: [
			['click', function(e){
				if (!tableView_clicking) {
					tableView_clicking = true;
					// Ti.API.debug('Row post data: '+JSON.stringify(postData));
					var PostDetails = require('views/postDetails');
					GLOBAL.tabGroup.activeTab.open( new PostDetails(e, { backButton: self.title }) );
					setTimeout(function(){ tableView_clicking = false; }, 1000);
				}
			}]
		]

	});

	self.add(tableView);

	return self;
}

module.exports = createView;
