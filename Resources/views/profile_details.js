var NewPost, ProfileEdit, URI, api, flickr_key, ui, _f;

ui = require('lib/ui_sugar');

api = require('lib/tent.api');

URI = require('lib/vendor/jsuri.min');

_f = require('views/func_repo');

NewPost = require('views/newPost2');

flickr_key = "189da0233649d27aab6e562c610ae060";

module.exports = ProfileEdit = (function() {
  function ProfileEdit(profile, viewOptions) {
    var bioEntry, birthdateEntry, flickr_image_url, flickr_query, genderEntry, hasHeaderPhoto, headerBg, headerWrap, loadingImageIndicator, locationEntry, nameEntry, urlEntry, xhr;
    this.profile = profile;
    this.viewOptions = viewOptions;
    if (typeof this.profile === 'string') {
      this.profile = api.getCache('discover')[this.profile];
    }
    this.self = ui.win({
      config: 'windowProperties',
      properties: {
        title: 'Details',
        rightNavButton: ui.button({
          config: 'newPostButton',
          events: [
            [
              'click', function() {
                e.source.enabled = false;
                return (new NewPost(null["null"], {
                  button: e.source
                })).open({
                  modal: true
                });
              }
            ]
          ]
        }),
        leftNavButton: ((function(_this) {
          return function() {
            if (_this.viewOptions && _this.viewOptions.backButton) {
              return ui.button({
                config: 'backButton',
                properties: {
                  backgroundImage: 'images/back_profile.png',
                  width: 64
                },
                events: [
                  [
                    'click', function() {
                      _this.tableView.setData();
                      return GLOBAL.tabGroup.activeTab.close(_this.self, {
                        animated: true
                      });
                    }
                  ]
                ]
              });
            }
          };
        })(this))()
      }
    });
    if (viewOptions && viewOptions.backButton) {
      this.self.leftNavButton = ui.button({
        config: 'backButton',
        events: [
          [
            'click', (function(_this) {
              return function(e) {
                return GLOBAL.tabGroup.activeTab.close(_this.self, {
                  animated: true
                });
              };
            })(this)
          ]
        ]
      });
    }
    hasHeaderPhoto = this.profile.avatar_url;
    this.tableView = ui.table({
      config: 'tableView',
      properties: {
        top: hasHeaderPhoto ? (GLOBAL.isTablet ? 250 : 160) : 0
      }
    });
    this.self.add(this.tableView);
    if (hasHeaderPhoto) {
      Ti.API.debug("Generating profile header avatar");
      headerBg = Ti.UI.createImageView({
        width: Ti.UI.FILL,
        height: GLOBAL.isTablet ? 250 : 160,
        top: 0
      });
      loadingImageIndicator = Ti.UI.createActivityIndicator({
        color: '#2f3238',
        style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE
      });
      headerWrap = ui.view({
        properties: {
          top: 0,
          width: GLOBAL.isTablet ? 600 : Ti.UI.FILL,
          height: GLOBAL.isTablet ? 268 : 165
        },
        content: [headerBg]
      });
      Ti.API.debug("Grabbing image from flickr api");
      flickr_image_url = "";
      flickr_query = "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=" + flickr_key + "&text=" + (encodeURIComponent(this.profile.location || 'deep blue sea')) + "&has_geo=1&content_type=1&safe_search=1&accuracy=" + (encodeURIComponent('~11')) + "&privacy_filter=1&extras=o_dims&per_page=100&page=1&format=json&nojsoncallback=1";
      xhr = new api.XHR({
        onload: function(e) {
          var data, photo, _i, _len, _ref, _results;
          data = e.json;
          if (parseInt(data.photos.total, 10) > 0) {
            Ti.API.debug("Got data from Flickr");
            Ti.API.debug("Selecting photo from data");
            _ref = data.photos.photo;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              photo = _ref[_i];
              if (parseInt(photo.o_width, 10) > parseInt(photo.o_height, 10)) {
                Ti.API.debug(JSON.stringify(photo));
                Ti.API.debug("------------- selected -------------");
                flickr_image_url = "http://farm" + photo.farm + ".staticflickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + ".jpg";
                Ti.API.debug(flickr_image_url);
                headerBg.applyProperties({
                  image: flickr_image_url
                });
                headerWrap.add(Ti.UI.createView({
                  bottom: 0,
                  width: Ti.UI.FILL,
                  height: GLOBAL.isTablet ? 18 : 5,
                  backgroundImage: GLOBAL.isTablet ? 'images/dropshadow_ipad_profile.png' : 'images/dropshadow_vert.png',
                  backgroundRepeat: GLOBAL.isTablet ? false : true
                }));
                loadingImageIndicator.hide();
                break;
              } else {
                _results.push(void 0);
              }
            }
            return _results;
          } else {
            flickr_query = "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=" + flickr_key + "&text=" + (encodeURIComponent('deep blue sea')) + "&has_geo=1&content_type=1&safe_search=1&accuracy=" + (encodeURIComponent('~11')) + "&privacy_filter=1&extras=o_dims&per_page=100&page=1&format=json&nojsoncallback=1";
            xhr = new api.XHR({
              onload: function(e) {
                var _j, _len1, _ref1, _results1;
                data = e.json;
                Ti.API.debug("Got data from Flickr");
                Ti.API.debug("Selecting photo from data");
                _ref1 = data.photos.photo;
                _results1 = [];
                for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                  photo = _ref1[_j];
                  if (parseInt(photo.o_width, 10) > parseInt(photo.o_height, 10)) {
                    Ti.API.debug(JSON.stringify(photo));
                    Ti.API.debug("------------- selected -------------");
                    flickr_image_url = "http://farm" + photo.farm + ".staticflickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + ".jpg";
                    Ti.API.debug(flickr_image_url);
                    headerBg.applyProperties({
                      image: flickr_image_url
                    });
                    headerWrap.add(Ti.UI.createView({
                      bottom: 0,
                      width: Ti.UI.FILL,
                      height: GLOBAL.isTablet ? 18 : 5,
                      backgroundImage: GLOBAL.isTablet ? 'images/dropshadow_ipad_profile.png' : 'images/dropshadow_vert.png',
                      backgroundRepeat: GLOBAL.isTablet ? false : true
                    }));
                    loadingImageIndicator.hide();
                    break;
                  } else {
                    _results1.push(void 0);
                  }
                }
                return _results1;
              }
            });
            xhr.open('GET', flickr_query, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            return xhr.send();
          }
        }
      });
      xhr.open('GET', flickr_query, true);
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send();
      headerWrap.add(loadingImageIndicator);
      headerWrap.add(_f.createAvatar(this.profile.avatar_url, {
        width: 100,
        height: 100,
        left: 10,
        bottom: GLOBAL.isTablet ? 25 : 12
      }, 13));
      this.self.add(headerWrap);
      loadingImageIndicator.show();
    }
    nameEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        editable: false,
        hintText: 'Not set',
        value: this.profile.name
      }
    });
    urlEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        editable: false,
        hintText: 'Not set',
        value: this.profile.website
      }
    });
    birthdateEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        editable: false,
        hintText: 'Not set',
        value: this.profile.birthdate
      }
    });
    genderEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        editable: false,
        hintText: 'Not set',
        value: this.profile.gender
      }
    });
    locationEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        editable: false,
        hintText: 'Not set',
        value: this.profile.location
      }
    });
    bioEntry = ui.textArea({
      properties: {
        top: 4,
        bottom: 4,
        left: 5,
        right: 5,
        hintText: 'No Bio',
        value: this.profile.bio,
        editable: false,
        autoLink: Ti.UI.AUTOLINK_ALL,
        color: '#45474c',
        font: {
          fontSize: 13,
          fontFamily: 'Helvetica'
        }
      }
    });
    this.tableData = [
      _f.createTableSection({
        headerView: Ti.UI.createView({
          height: 7
        }),
        footerView: Ti.UI.createView({
          height: 2
        })
      }, [
        {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Name'
              }
            }), nameEntry
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Gender'
              }
            }), genderEntry
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Birthdate'
              }
            }), birthdateEntry
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'URL'
              }
            }), urlEntry
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Location'
              }
            }), locationEntry
          ]
        }
      ]), ui.section({
        properties: {
          headerView: Ti.UI.createView({
            height: 3
          }),
          footerView: Ti.UI.createView({
            height: 5
          })
        },
        content: [
          ui.row({
            properties: {
              height: 104,
              backgroundImage: 'images/row_bg_full.png',
              backgroundTopCap: 8,
              backgroundLeftCap: 20,
              selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
            },
            content: [bioEntry],
            events: [
              [
                'click', function(e) {
                  return nameEntry.focus();
                }
              ]
            ]
          })
        ]
      })
    ];
    this.tableView.setData(this.tableData);
  }

  ProfileEdit.prototype.getView = function() {
    return this.self;
  };

  return ProfileEdit;

})();
