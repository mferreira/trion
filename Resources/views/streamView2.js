var ProfileEdit, ui;

ui = require('lib/ui_sugar');

module.exports = ProfileEdit = (function() {
  function ProfileEdit(conf) {
    this.conf = conf;
    Ti.API.debug("streamView <- " + (JSON.stringify(this.conf)));
    this.self = ui.win({
      config: 'windowProperties',
      properties: {
        title: this.conf.title || 'Title Here',
        navBarHidden: false,
        rightNavButton: ui.button({
          config: 'newPostButton',
          events: [
            [
              'click', function(e) {
                e.source.enabled = false;
                (new NewPost()).open({
                  modal: true
                });
                return setTimeout((function() {
                  return e.source.enabled = true;
                }), 1000);
              }
            ]
          ]
        })
      }
    });
    if (this.conf.hasBackButton) {
      this.self.leftNavButton = ui.button({
        config: 'backButton',
        events: [
          [
            'click', function(e) {
              return GLOBAL.tabGroup.activeTab.close(this.self, {
                animated: true
              });
            }
          ]
        ]
      });
    }
  }

  ProfileEdit.prototype.showPullView = function(text) {
    if (text) {
      tablePullView.message.text = text;
      tablePullView.hide(tablePullView.loadingIndicator);
      tablePullView.show(tablePullView.message);
    } else {
      tablePullView.show(tablePullView.loadingIndicator);
      tablePullView.hide(tablePullView.message);
    }
    return tablePullView.show();
  };

  ProfileEdit.prototype.getView = function() {
    return this.self;
  };

  return ProfileEdit;

})();
