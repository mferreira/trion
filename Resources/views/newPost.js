var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api2'),
	drafts = require('lib/drafts'),
	url_short = require('lib/url_shortener'),
	_ = require('lib/vendor/underscore-min');

var newPost_args = null,
	newPost_callback = null,
	cur_postData = null,
	prefill = null,
	isReply = false,
	replyAll = false,

	isPortrait = null,
	lastKeyboardHeight = null,
	mentionStart, mentionStop,
	textSelected = null,
	statusPostTextLimit = api.postType('postTypeProperties')[api.postType('Status')].textLimit;

function createView(newPostData, callback, args) {
	Ti.API.debug('Creating "New Post" view with data: '+JSON.stringify(newPostData));

	newPost_args = newPostData;
	newPost_callback = callback;

	cur_postData = newPostData ? newPostData.originalPost : null;
	prefill = newPostData ? newPostData.prefill : null;
	isReply = newPostData ? newPostData.reply || false : false;
	replyAll = newPostData ? newPostData.replyAll || false : false;

	pmSwitch_value = newPost_args ? newPost_args.pmSwitch || false : false;

	var
	isPosting = false,
	flagPrivate = false,
	flagLocation = false,
	location_latlon = null,
	postInput_focus = true,
	isRepost = (newPostData && newPostData.originalPost) ? (newPostData.originalPost.type === api.postType('Repost')) : false;

	discoverData = api.getCache('discover'),
	allBonds = _.keys(discoverData).join(' '),
	allBonds_split = allBonds.split(' '),

	selectedPosttype = api.postType('Status');

	var self = ui.win({
		config: 'windowProperties',
		properties: {
			title: 'New Post',
			orientationModes: (function(){
				if(Ti.Platform.osname === "iphone"){
						return [
							Ti.UI.PORTRAIT,
							Ti.UI.LANDSCAPE_LEFT,
							Ti.UI.LANDSCAPE_RIGHT
						];  
				}else{
						return [
							Ti.UI.PORTRAIT,
							Ti.UI.UPSIDE_PORTRAIT,
							Ti.UI.LANDSCAPE_LEFT,
							Ti.UI.LANDSCAPE_RIGHT
						];  
				}				
			})()
		},
		events: [
			['focus', function(e){
				isPortrait = (e.source.orientation === Ti.UI.PORTRAIT);
				if (!GLOBAL.isTablet) {
					self.setLeftNavButton(isPortrait ? navLeftButton : navLeftButton2);
					self.setRightNavButton(isPortrait ? navRightButton : navRightButton2);
				}

				if (pmSwitch_value) {
					flagPrivate = true;
					pmSwitch.image = 'images/post_private_active_icon.png';
				}
				else if (isReply && cur_postData) {
					flagPrivate = cur_postData.permissions;
					pmSwitch.image = flagPrivate ? 'images/post_private_active_icon.png' : 'images/post_private_icon.png';
				}

				// pmSwitch.value = pmSwitch_value ? pmSwitch_value : (isReply && cur_postData) ? !cur_postData.permissions['public'] : false; // private is off by default
				postInput.value = postInput.value ? postInput.value : (function(){
					var postInput_value = '', cur_entity;
					if (isReply) {
						if (cur_postData.entity !== GLOBAL.entity) postInput_value = '^'+ cur_postData.entity +' ';
						if (replyAll) {
							for (var i=cur_postData.mentions.length; i--;){
								cur_entity = cur_postData.mentions[i].entity;
								if ( [cur_postData.entity, GLOBAL.entity].indexOf(cur_entity) > -1) continue;
								postInput_value += '^'+ cur_entity +' ';
							}
						}
					}
					else if (prefill) {
						postInput_value = prefill.content.text;
					}
					return postInput_value;
				})();
				var postInput_length = postInput.value.length;
				postInput.focus();
				// push cursor position to the end
				postInput.setSelection(postInput_length, postInput_length);

				var content = postInput.value,
					inputCharsLeft = photoArray.length ? "∞" : statusPostTextLimit - countPostChars(content),
					canPost = photoArray.length ? true : (Ti.Network.online && inputCharsLeft > -1 && inputCharsLeft < statusPostTextLimit);

				charCounter.setText( inputCharsLeft );
				navRightButton.enabled = canPost;
				navRightButton2.enabled = canPost;

			}],
			['open', function(e){
				isPortrait = (e.source.orientation === Ti.UI.PORTRAIT);
				toolbar.bottom = GLOBAL.keyboardHeight();
				postInput.height = Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() +35 + (isPortrait ? 64:52));
				Ti.Gesture.addEventListener('orientationchange', updatePostInput);
				if (args && args.button) args.button.enabled = true;
			}],
			['close', function(e){
				Ti.Gesture.removeEventListener('orientationchange', updatePostInput);
				lastKeyboardHeight = GLOBAL.keyboardHeight();
			}]
		]
	}),

	navLeftButton = ui.button({
		config: 'cancelButton',
		events: [
			['click', cancelButton_onclick]
		]
	}),

	navRightButton = ui.button({
		config: 'postButton',
		events: [
			['click', postButton_onclick]
		]
	}),

	navLeftButton2 = ui.button({
		config: 'cancelButton2',
		events: [
			['click', cancelButton_onclick]
		]
	}),

	navRightButton2 = ui.button({
		config: 'postButton2',
		events: [
			['click', postButton_onclick]
		]
	});

	self.setLeftNavButton(navLeftButton);
	self.setRightNavButton(navRightButton);

	var
	pmLabel = Ti.UI.createLabel({
		text: 'Private', width: Ti.UI.SIZE, height: Ti.UI.SZIE, textAlign: 'left', left: 0
	}),

	pmSwitch = ui.button({
		properties: { image: 'images/post_private_icon.png', style: Ti.UI.iPhone.SystemButtonStyle.PLAIN, left: 0, width: 40, height: 25 },
		events: [['singletap', function(e){
			flagPrivate = !flagPrivate;
			e.source.image = flagPrivate ? 'images/post_private_active_icon.png' : 'images/post_private_icon.png';
			Ti.API.debug('Private switch is: '+(flagPrivate ? 'on': 'off'));
		}]]
	}),

	locationSwitch_wrap = Ti.UI.createView({ left: 0, width: 40, height: 25 }),

	locationActInd = Ti.UI.createActivityIndicator({
		color: '#31333b',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
		width: 40, height: 25
	}),

	locationSwitch = ui.button({
		properties: { image: 'images/post_loc_icon.png', style: Ti.UI.iPhone.SystemButtonStyle.PLAIN, width: 40, height: 25 },
		events: [['singletap', function(e){
			flagLocation = !flagLocation;
			// e.source.image = flagLocation ? 'images/kb_toolbar_location_active.png' : 'images/kb_toolbar_location.png';
			Ti.API.debug('Location switch is: '+(flagLocation ? 'on': 'off'));
			if (flagLocation) {
				var geo = require('lib/geo');
				var grabDeviceLocation = function(){
					geo.getCurrentPosition({
						onload: function(latlon){
							location_latlon = latlon;
							locationSwitch.image = 'images/post_loc_icon_active.png';
							locationActInd.hide();
							locationSwitch.show();
						},
						onerror: function(e) {
							locationSwitch.image = 'images/post_loc_icon.png';
							locationActInd.hide();
							locationSwitch.show();
							flagLocation = false;
						}
					});
				};

				locationSwitch.hide();
				locationActInd.show();

				if ( geo.isAvailable() ) {
					if (!geo.started) {
						geo.start({
							onload: function(){
								grabDeviceLocation();
							},
							onerror: function(){
								locationSwitch.image = 'images/post_loc_icon.png';
								locationActInd.hide();
								locationSwitch.show();
								flagLocation = false;
							}
						});
					}
					else grabDeviceLocation();
				}
				else if (geo.isEnabled()) {
						geo.start({
							onload: function(){
								grabDeviceLocation();
							},
							onerror: function(){
								locationSwitch.image = 'images/post_loc_icon.png';
								locationActInd.hide();
								locationSwitch.show();
								flagLocation = false;
								alert('Unable to get your location. Please, check your device privacy settings.');
							}
						});
				}
				else {
					geo.start({
						onload: function(){
							grabDeviceLocation();
						},
						onerror: function(){
							locationSwitch.image = 'images/post_loc_icon.png';
							locationActInd.hide();
							locationSwitch.show();
							flagLocation = false;
						}
					});
				}
			}
			else {
				location_latlon = null;
				e.source.image = 'images/post_loc_icon.png';
			}
		}]]
	}),

	userButton = ui.button({
		properties: { image: 'images/post_user_icon.png', style: Ti.UI.iPhone.SystemButtonStyle.PLAIN, left: 0, width: 40, height: 25 },
		events: [['singletap', function(e){
			if (mentionSelector_visible) {
				hidePossibleMentions();
			}
			else {
				if (allBonds_split.length) {
					var postInput_length = postInput.value.length;

					// only adds mentions to the end
					if (postInput_length && postInput.value[postInput_length-1] !== '\n')
						postInput.value += ' ';
					postInput.value += '^';

					// adds mentions to where cursor is positioned - DOES NOT WORK YET
					// ///////////////////////
					// if (!textSelected || (textSelected.location === postInput_length-1 && !textSelected.length)) {
					// 	if (postInput_length && postInput.value[postInput_length-1] !== '\n')
					// 		postInput.value += ' ';
					// 	postInput.value += '^';
					// }
					// else if (textSelected.location !== postInput_length-1) {
					// 	var newCursorPosition = textSelected.location,
					// 	postInput_value = postInput.value;
					// 	postInput_value = postInput_value.slice(0, textSelected.location) +
					// 		'^' + postInput_value.slice(textSelected.location+textSelected.length);
						
					// 	postInput.value = postInput_value;
					// 	Ti.API.debug('repositioning text cursor to position: '+newCursorPosition);
					// 	setTimeout(function(){ postInput.setSelection(newCursorPosition, newCursorPosition); }, 100);
					// }

					showPossibleMentions(allBonds_split, function(index){
						postInput.value = postInput.value + allBonds_split[index] +' ';
					});
				}
				else {
					hidePossibleMentions();
				}
			}
			Ti.API.debug('Location switch is: '+(mentionSelector_visible ? 'on': 'off'));
		}]]
	}),

	photoArray = [],
	photoButton = ui.button({
		properties: { image: 'images/post_photos_icon.png', style: Ti.UI.iPhone.SystemButtonStyle.PLAIN, left: 0, width: 40, height: 25 },
		events: [['click', function(e){
			if (photoArray.length) {
				if (postInput_focus) {
					postInput.blur();
					postInput_focus = false;
				} else {
					postInput.focus();
					postInput_focus = true;
				}
			}
			else {
				var onMediaSuccess = function (media) {
					postInput.focus();
					postInput_focus = true;
					photoButton.image = 'images/post_photos_active_icon.png';
					photoArray.push(media.media);
					// set out photo frame image
					photoFrame.applyProperties({
						image: media.media,
						height: GLOBAL.keyboardHeight()
					});
					// set char counter value to infinite
					charCounter.setText("∞");
				};
				ui.optionDialog({
					properties: {
						options: [L('from_camera', "From Camera"), L('from_library', "From Library"), L('cancel', "Cancel")],
						cancel: 2
					},
					events: [
						['click', function(e){
							if (e.index === 0) {
								Ti.Media.showCamera({
									mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
									allowEditing: true,
									success: onMediaSuccess
								});
							}
							else if (e.index === 1) {
								Ti.Media.openPhotoGallery({
									mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
									allowEditing: true,
									success: onMediaSuccess
								});
							}
						}]
					]
				}).show();
			}
		}]]
	}),
	photoFrame = ui.image({
		properties: {height: GLOBAL.keyboardHeight(), bottom: 0},
		events: [['click', function(e) {
			ui.optionDialog({
				properties: {
					options: [L('remove_image', "Remove Image"), L('cancel', "Cancel")],
					destructive: 0,
					cancel: 1
				},
				events: [
					['click', function(e) {
						if (e.index === e.destructive) {
							postInput.focus();
							postInput_focus = true;
							photoButton.image = 'images/post_photos_icon.png';
							photoFrame.image = null;
							photoArray.pop();
							// set char counter value to status post remaining chars
							charCounter.setText(statusPostTextLimit - countPostChars(postInput.value));
						}
					}]
				]
			}).show();
		}]]
	}),

	charCounter = Ti.UI.createLabel({ width: Ti.UI.SIZE, height: Ti.UI.SZIE, textAlign: 'right', right: 20,
		text: photoArray.length ? "∞" : (''+statusPostTextLimit), font: {fontSize: 13, fontWeight: 'bold'}, color: '#31333b'
	}),

	toolbar = ui.view({
		properties: {zIndex: 2, bottom: GLOBAL.keyboardHeight(), left: 0, right: 0, height: 34, backgroundImage: 'images/kb_toolbar_bg.png', backgroundTopCap: 5},
		content: [
			ui.view({
				properties: { width: Ti.UI.SZIE, height: Ti.UI.SZIE, layout: 'horizontal', left: 10, top: 6 },
				content: [ pmSwitch, locationSwitch_wrap, userButton, photoButton ]
			}),
			charCounter
		]
	});

	var postInput = ui.textArea({
		properties: {
			zIndex: 2,
			left: 0,
			top: 0, height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() +34 +64),
			width: Ti.UI.FILL,
			keyboardType: Ti.UI.KEYBOARD_ASCII,
			suppressReturn: false,
			autocorrect: true,
			autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES,
			color: '#313339',
			font: {fontSize:14,fontFamily:'Helvetica'}
		},
		events: [
			['selected', function(e){
				textSelected = e.range;
			}],
			['change', function(e){
				var contentLength = e.source.value.length,
					content = e.source.value;

				var inputCharsLeft = photoArray.length ? "∞" : (statusPostTextLimit - countPostChars(content));
				charCounter.setText( inputCharsLeft );
				if (Ti.Network.online) {
					var canPost = photoArray.length ? true : (inputCharsLeft > -1 && inputCharsLeft < statusPostTextLimit);
					navRightButton.enabled = canPost;
					navRightButton2.enabled = canPost;
				}

				// check if user is going to mention someone
				if (contentLength > 1) {
					var curChar = content[contentLength-1],
						previousChar = content[contentLength-2],
						uris;
					if (curChar !== ' ' && curChar !== '\n') {
						if (previousChar === '^') {
							mentionStart = contentLength-1;
							uris = entityLookup(curChar);
							Ti.API.debug('Possible mentions: '+JSON.stringify(uris));
							if (uris.length)
								showPossibleMentions(uris, function(index){
									postInput.value = postInput.value.slice(0, mentionStart) + uris[index];
								});
							else hidePossibleMentions();
						}
						else if (mentionStart !== null && contentLength > mentionStart) {
							uris = entityLookup(content.slice(mentionStart, contentLength));
							Ti.API.debug('Possible mentions: '+JSON.stringify(uris));
							if (uris.length)
								showPossibleMentions(uris, function(index){
									postInput.value = postInput.value.slice(0, mentionStart) + uris[index];
								});
							else hidePossibleMentions();
						}
					}
					else {
						mentionStart = null;
						hidePossibleMentions();
					}
				}
				else {
					mentionStart = null;
					hidePossibleMentions();
				}

			}],
			['focus', function(e){
				userButton.image = 'images/post_user_icon.png';
				mentionSelector_visible = false;
			}]
		]
	});

	// mention selection view
	var mentionSelector_visible = false,
		mentionSelector_callback = null,
		mentionSelector_table = ui.table({
			properties: {width: Ti.UI.FILL},
			events: [['click', function(e){
				if (mentionSelector_callback) mentionSelector_callback(e.index);
				hidePossibleMentions();
			}]]
		});
	var mentionSelector = ui.view({
		properties: {zIndex: 1, right: 0, width: Ti.UI.FILL, top: 46, bottom: GLOBAL.keyboardHeight() + 34, backgroundImage: 'images/kb_toolbar_bg.png', backgroundTopCap: 5},
		content: [mentionSelector_table]
	});

	locationSwitch_wrap.add(locationActInd);
	locationSwitch_wrap.add(locationSwitch);

	self.add(mentionSelector);
	self.add(postInput);
	self.add(toolbar);
	self.add(photoFrame);

	////////////////////////////////////////////
	
	function updatePostInput (e) {
		// we dont care about anything but portrait and landscape
		if ([Ti.UI.FACE_UP,Ti.UI.FACE_DOWN,Ti.UI.UNKNOWN].indexOf(e.orientation) > -1) return;

		isPortrait = (e.orientation === Ti.UI.PORTRAIT || e.orientation === Ti.UI.UPSIDE_PORTRAIT);
		toolbar.bottom = GLOBAL.keyboardHeight();

		// portrait mode
		if (isPortrait) {
			if (!GLOBAL.isTablet) {
				Ti.API.debug('Updating navbar buttons.');
				self.setLeftNavButton(navLeftButton);
				self.setRightNavButton(navRightButton);
			}
			
			if (!mentionSelector_visible) {
				postInput.applyProperties({
					height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() +34 +64),
					width: Ti.UI.FILL
				});
			}
			else {
				postInput.applyProperties({
					height: 45,
					width: Ti.UI.FILL
				});
			}
			mentionSelector.applyProperties({
				width: Ti.UI.FILL,
				top: 46,
				bottom: GLOBAL.keyboardHeight() + 34
			});
		}
		// landscape mode
		else {
			if (!GLOBAL.isTablet) {
				Ti.API.debug('Updating navbar buttons.');
				self.setLeftNavButton(navLeftButton2);
				self.setRightNavButton(navRightButton2);
			}

			mentionSelector.applyProperties({
				width: 200,
				top: 0,
				bottom: GLOBAL.keyboardHeight() + 34
			});
			if (!mentionSelector_visible) {
				postInput.applyProperties({
					height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() +34 +52),
					width: Ti.UI.FILL
				});
			}
			else {
				postInput.applyProperties({
					height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() +34 +52),
					width: Ti.Platform.displayCaps.platformWidth - mentionSelector.width
				});
			}
		}

		photoFrame.applyProperties({
			height: GLOBAL.keyboardHeight()
		});
	}

	function countPostChars (text) {
		var re = /(?:[^\S]|^)(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*)/ig,
			match, urls = 0, stripText = text;
		while (match = re.exec(text)) { stripText = stripText.replace( match[1], '' ); urls += 1; }
		return stripText.length + (urls * 18);
	}

	function entityLookup (searchFor) {
		Ti.API.debug('entityLookup: "'+searchFor+'"');
		var re = new RegExp('(\\S*'+searchFor+'\\S*)', 'ig'), matches = [];
		while (match = re.exec(allBonds)) { matches.push( match[1].toLowerCase() ); }
		return matches;
	}

	function get_name_or_entity (uri) {
		var d = discoverData[uri];
		if (d && d.profile && d.profile.name) return d.profile.name;
		return uri;
	}

	function get_uri_avatar (uri){
		var d = discoverData[uri];
	}

	function showPossibleMentions(uris, callback) {
		Ti.API.debug('showPossibleMentions() called');
		Ti.API.debug('isPortrait -> '+isPortrait);
		mentionSelector_callback = callback;
		mentionSelector_table.data = (function(){
			var uri, ent, rows = [];
			for (var i=0, j=uris.length; i<j; i++){
				uri = uris[i];
				ent = discoverData[uri];
				rows.push(ui.row({
					properties: {height: 35},
					content: [
						Ti.UI.createImageView({width: 30, height: 30, left: 5, borderRadius: 3, defaultImage: 'images/avatar.png', image: (ent.basic && ent.basic.avatar_url) ? ent.basic.avatar_url : ''}),
						Ti.UI.createLabel({ text: (ent.basic && ent.basic.name) ? ent.basic.name : uri, left: 40, right: 5,
							color: '#313339', font: {fontSize:14, fontFamily:'Helvetica'} })
					]
				}));
			}
			return rows;
		})();

		mentionSelector_visible = true;
		userButton.image = 'images/post_user_icon_active.png';
		if (isPortrait) {
			postInput.applyProperties({height: 45});
		}
		else {
			if (mentionSelector.width !== 200) mentionSelector.applyProperties({ width: 200, top: 0, bottom: GLOBAL.keyboardHeight() + 34 });
			postInput.applyProperties({width: Ti.Platform.displayCaps.platformWidth - mentionSelector.width});
		}
		// postInput.blur();
	}

	function hidePossibleMentions(){
		Ti.API.debug('hidePossibleMentions() called');
		if (isPortrait) {
			postInput.applyProperties({height: Ti.Platform.displayCaps.platformHeight - (GLOBAL.keyboardHeight() +34 +64)});
		}
		else {
			postInput.applyProperties({width: Ti.UI.FILL});
		}
		// postInput.focus();
		userButton.image = 'images/post_user_icon.png';
		mentionSelector_visible = false;
	}

	function cancelButton_onclick(e){
		if (postInput.value.length) {
			ui.optionDialog({
				properties: {
					options: [L('dont_save', "Don't Save"), L('save_draft', "Save Draft"), L('cancel', "Cancel")],
					cancel: 2,
					destructive: 0
				},
				events: [
					['click', function(e){
						if (e.index === e.destructive) self.close();
						else if (e.index === 1) {
							// TODO: support more post types
							// save the post to drafts and close
							drafts.save({
								type: api.postType('Status'),
								content: {text: postInput.value}
							});
							self.close();
						}
					}]
				]
			}).show();
		}
		else {
			self.close();
		}
	}

	function postButton_onclick(e){
		isPosting = true;
		ui.showIndicator();

		function getUrls(text) {
			// var exp = /(\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])))/ig;
			var re = /(?:[^\S]|^)(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*)/ig,
				match, urls = [];
			while (match = re.exec(text)) { urls.push( match[1] ); }
			return urls;
		}
		function getMentions(text) {
			var re = /(\^(https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}[\/\S]*))/ig,
				match, urls = [];
			while (match = re.exec(text)) { urls.push( match[2].toLowerCase() ); }
			return urls;
		}

		var _urls = getUrls(postInput.value),
			urls = {},
			output = postInput.value,
			cur_url = null;

		for (var i=_urls.length; i--;){
			urls[_urls[i]] = null;
		}

		function checkAllUrls(){
			Ti.API.debug('checkAllUrls...');
			for (var i=_urls.length; i--;){
				var url = _urls[i],
					shortified = urls[url];
				if (shortified === null) {
					if (url.length > url_short.sites[url_short.choice].chars) return shortifyURL(url);
					else urls[url] = url;
				}
				else {
					// make some dark magic regex stuff - gotta love it
					var rep_url = url.replace(/\//g, '\\/').replace(':', '\\:').replace(/\./g, '\\.'),
						rep_regex = new RegExp("(?:[^\\S]|^)("+rep_url+")", 'gi');

					var match = rep_regex.exec(output);

					output = output.replace(match[1], shortified);
				}
			}
			postIt();
		}
		
		function shortifyURL(uri){
			Ti.API.debug('shortifying: '+uri);
			cur_url = uri;
			var xhr = Ti.Network.createHTTPClient({
				// timeout : timeout *1000,  // in milliseconds
				// function called when the response data is available
				onload : function(e) {
					Ti.API.debug('Shortified url: '+this.responseText);
					urls[cur_url] = this.responseText;
					checkAllUrls();
				},
				// function called when an error occurs, including a timeout
				onerror : function(e) {
					Ti.API.error('Something went wrong shortifying url: '+uri);
					checkAllUrls();
				}
			});
			xhr.open('GET', url_short.sites[url_short.choice].uri + encodeURIComponent(uri), false);
			xhr.send();
		}

		function postIt(){
			Ti.API.debug('post shortified: '+output);

			var i,
			payload = {
				permissions: { "public": !flagPrivate, entities: [] },
				// choose the post type
				type: photoArray.length ? api.postType('Photo') : api.postType('Status'),
				content: photoArray.length ? {caption: output} : {text: output },
				mentions: []
			};

			if (flagLocation && location_latlon) {
				payload.content.location = location_latlon;
			}

			var input_mentions = getMentions(output);
			if (input_mentions.length) {
				var mentionIndex = 0, mentionDone = [];
				for (i=input_mentions.length; i--;){
					var m = input_mentions[i];
					if (mentionDone.indexOf(m) >= 0 || (isReply && cur_postData.entity === m)) continue;

					mentionDone.push(m);
					payload.mentions.push({entity: m});

					// build inline mentions
					output = output.replace( '^'+m, '^['+get_name_or_entity(m)+']('+mentionIndex+')');
					mentionIndex += 1;
				}
			}

			// set post content text based on post type
			if (payload.type === api.postType('Photo')) {
				payload.content.caption = output;
				payload.attachments = photoArray;
			}
			else if (payload.type === api.postType('Status')) payload.content.text = output;

			// TODO: do we really want to do this? The code above already takes care of mentions from the input...
			if (prefill && prefill.mentions) {
				for (i=prefill.mentions; i--;){
					payload.mentions.push({entity: prefill.mentions[i]});
				}
			}

			if (cur_postData) {
				if (isReply) {
					mentions_post_id = isRepost ? cur_postData.original_post.post_content.id : cur_postData.id;
					mentions_post_entity = isRepost ? cur_postData.original_post.post_content.entity : cur_postData.entity;
					payload.mentions.push({ entity: mentions_post_entity, post: mentions_post_id });
					if (replyAll) {
						var cur_entity;
						for (i=cur_postData.mentions; i--;){
							cur_entity = cur_postData.mentions[i].entity;
							// don't mention me or the entity i'm replying to
							if ( [cur_postData.entity, GLOBAL.entity].indexOf(cur_entity) > -1 ) continue;
							payload.mentions.push({entity: cur_entity});
						}
					}
				}
			}

			if (flagPrivate) {
				if (payload.mentions.length) {
					for (i=payload.mentions.length; i--;){
						payload.permissions.entities.push(payload.mentions[i].entity);
					}
				}
				else if (cur_postData) payload.permissions.entities.push(cur_postData.entity);
			}

			function postItNow(){
				api.posts_POST(payload, {
					onload: function(e){
						if (newPost_callback) newPost_callback(newPost_args.callbackArgs);
						self.close();
						cur_postData = null;
						isPosting = false;
						ui.hideIndicator();
						Ti.App.fireEvent('ui:myStream:addPost', {post: e.post});
					},
					onerror: function(e){
						isPosting = false;
						ui.hideIndicator();

						ui.alert({
							properties: {
								cancel: 1,
								buttonNames: [L('yes', 'Yes'), L('no', 'No')],
								message: 'Sorry, your post could not be sent. Would you like to try again?',
								title: 'Error'
							},
							events: [
								['click', function(e){
									if (e.index != e.cancel) postItNow();
								}]
							]
						}).show();
					}
				});
			}

			Ti.API.debug('Posting new post with: '+JSON.stringify(payload));
			postItNow();
		}

		// start the proccess
		checkAllUrls();
	}

	return self;
}

module.exports = createView;