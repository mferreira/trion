var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api'),
	URI = require('lib/vendor/jsuri.min'),
	_f = require('views/func_repo'),
	NewPost = require('views/newPost2');



function createView (profile, viewOptions) {

	if (typeof profile === 'string') profile = api.getCache('discover')[profile];

	function saveProfile(type){
		api.profile_PUT({
			type: type,
			payload: {
				name: nameEntry.value,
				avatar_url: profile.basic.avatar_url,
				birthdate: profile.basic.birthdate,
				location: locationEntry.value,
				gender: profile.basic.gender,
				bio: bioEntry.value,
				website_url: urlEntry.value
			}
		});
	}

	var self = ui.win({
		config: 'windowProperties',
		properties: {
			title: 'Edit Profile',
			rightNavButton: ui.button({
				config: 'newPostButton',
				events: [
					['click', function(e){
						(new NewPost()).open({modal: true});
					}]
				]
			}),
			leftNavButton: (function(){
				if (viewOptions && viewOptions.backButton) {
					return ui.button({
						config: 'backButton',
						properties: {backgroundImage: 'images/back_profile.png', width: 64},
						events: [['click', function(e){ GLOBAL.tabGroup.activeTab.close(self, {animated: true}); }]]
					});
				}
			})(),
			layout: 'vertical'
		}
	});

	var profileHeader = ui.view({
		properties: {
			width: Ti.UI.FILL, height: 66,
			backgroundImage: 'images/profile_top_bg.png'
		},
		content: [
			// profile avatar
			_f.createAvatar( profile.basic ? profile.basic.avatar_url : 'images/avatar.png', {
				width: 48, height: 48, left: 10
			}),
			// profile real name
			ui.label({
				config: 'defaultLabel',
				properties: { left: 66, top: 14, text: profile.basic.name, font: { fontSize: 16, fontWeight:'bold' }, color: '#fff', shadowColor: '#000', shadowOffset: {x:0,y:1} }
			}),
			// profile entity name
			ui.label({
				config: 'defaultLabel',
				properties: { left: 66, top: 34, text: (new URI(profile.core.entity)).host(), font: { fontSize: 14}, color: '#d4d5d7', shadowColor: '#000', shadowOffset: {x:0,y:1} }
			})
		]
	});
	// self.add( profileHeader );

	var
	tableView = ui.table({ config: 'tableView' }),
	nameEntry = ui.textField({
		config: 'profileTextField',
		properties: {hintText: 'Type Your Name', returnKeyType: Ti.UI.RETURNKEY_DONE, value: profile.basic.name},
		events: [['return', function(e){
			saveProfile('basic');
		}]]
	}),
	urlEntry = ui.textField({
		config: 'profileTextField',
		properties: {hintText: 'URL', value: profile.basic.website_url,
			autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
			autocorrect: false,
			returnKeyType: Ti.UI.RETURNKEY_DONE,
			keyboardType: Ti.UI.KEYBOARD_URL
		},
		events: [['return', function(e){
			saveProfile('basic');
		}]]
	}),
	locationEntry = ui.textField({
		config: 'profileTextField',
		properties: {hintText: 'Your Location', returnKeyType: Ti.UI.RETURNKEY_DONE, value: profile.basic.location},
		events: [['return', function(e){
			saveProfile('basic');
		}]]
	}),
	bioEntry = ui.textArea({
		properties: {
			top: 4, bottom: 4, left: 5, right: 5,
			value: profile.basic.bio, returnKeyType: Ti.UI.RETURNKEY_DONE,
			autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES,
			color: '#45474c',
			font: {fontSize:13,fontFamily:'Helvetica'}
		},
		events: [['return', function(e){
			saveProfile('basic');
		}]]
	}),

	tableData = [
		// _f.createTableSection({
		// 	headerView: Ti.UI.createView({height:9}),
		// 	footerView: Ti.UI.createView({height:5})
		// }, [
		// 	{
		// 		config: 'rowWithChild',
		// 		content: [ ui.label({config: 'defaultRowLabel', properties: {text: 'Upload Photo'} }) ]
		// 	}
		// ]),

		_f.createTableSection({
			headerView: Ti.UI.createView({height:7}),
			footerView: Ti.UI.createView({height:2})
		}, [
			{
				config: 'profileEditableRow',
				content: [
					ui.label({config: 'defaultRowLabel', properties: {text: 'Name'} }),
					nameEntry
				],
				events: [['click', function(e){ nameEntry.focus(); }]]
			},
			{
				config: 'profileEditableRow',
				content: [
					ui.label({config: 'defaultRowLabel', properties: {text: 'URL'} }),
					urlEntry
				],
				events: [['click', function(e){ urlEntry.focus(); }]]
			},
			{
				config: 'profileEditableRow',
				content: [
					ui.label({config: 'defaultRowLabel', properties: {text: 'Location'} }),
					locationEntry
				],
				events: [['click', function(e){ locationEntry.focus(); }]]
			}
		]),

		ui.section({
			properties: {
				headerView: Ti.UI.createView({height:3}),
				footerView: Ti.UI.createView({height:5})
			},
			content: [ui.row({
				properties: {
					height: 104,
					backgroundImage: 'images/row_bg_full.png', backgroundTopCap: 8, backgroundLeftCap: 20,
					selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE},
				content: [ bioEntry ],
				events: [['click', function(e){ nameEntry.focus(); }]]
			})]
		})
	];

	tableView.setData( tableData );
	self.add(tableView);

	return self;
}

module.exports = createView;
