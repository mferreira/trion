var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api2'),
	NewPost = require('views/newPost2'),
	_f = require('views/func_repo');

// api.favorites_GET( GLOBAL.entity);

GLOBAL.proccessingData = false;

function getView(config) {

	var _this = this;

	function startProcessingData(dontHideHeader){
		GLOBAL.proccessingData = true;
		if (!dontHideHeader) section.setHeaderView( Ti.UI.createView({height:10}) );
	}
	function stopProcessingData(){
		GLOBAL.proccessingData = false;
		if (cachedPosts && cachedPosts.length) section.setHeaderView( showNewPosts );
		else section.setHeaderView( Ti.UI.createView({height:10}) );

		for (var i=afterProcessingDataQueue.length, q; i--;){
			q = afterProcessingDataQueue[i];
			q.func.apply(_this, q.args);
		}
	}

	var conf = config;
	conf.noCache = conf.noCache || false;
	conf.noEvents = conf.noEvents || false;

	var myStream_cache = (conf.noCache) ? null : Ti.App.Properties.hasProperty(conf.appProperties_cache)? JSON.parse(Ti.App.Properties.getString(conf.appProperties_cache)) : [],
		afterProcessingDataQueue = [];

	var tableData = [],
		row,
		fetchPostsTimer,
		cachedPosts = null,
		hasFocus = null,
		// cache
		mentions_cache = [],
		updatingOlder = false,
		lastRow = null,
		lastRowIndex = null,
		gapRowIndex = null,
		maxPostsToGet = 31,
		maxOldPostsToGet = 10,
		// containingTab = null,
		dontProccessNewPosts = false,
		showMore_active = false,
		sectionHead = null,
		tableIsEmpty = false,
		reachedPostHistoryEnd = false,

	actIndicator = Ti.UI.createActivityIndicator({
		color: '#2f3238',
		font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'},
		message: 'Loading...',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE
	}),
	loadingRow = ui.row({
		config: 'rowWithoutChild_bottom',
		content: [ui.view({
			properties: { height:Ti.UI.SIZE, width:Ti.UI.SIZE },
			content: [actIndicator]
		})]
	}),

	self = ui.win({
		config: 'windowProperties',
		properties: {
			title: conf.title,
			rightNavButton: ui.button({
				config: 'newPostButton',
				events: [ ['click', function(e){
					e.source.enabled = false;
					(new NewPost(null, null, {button: e.source})).open({modal: true});
				}] ]
			}),
			leftNavButton: (function(){
				if (conf.hasBackButton) {
					return ui.button({
						config: 'backButton',
						events: [['click', function(e){ GLOBAL.tabGroup.activeTab.close(self, {animated: true}); }]]
					});
				}
			})(),
			navBarHidden: false
		},
		events: [
			['open', function(e){
				Ti.API.debug(conf.title+' fired event: OPEN');
				// Ti.App.fireEvent('startBgJob');
				// bgPostCheck();
				startBgJobCallback();
			}],
			['focus', function(e){
				hasFocus = true;
				tableView.scrollsToTop = true;
				if (conf.clearNotificationCenterOnFocus) {
					Ti.UI.iPhone.appBadge=1;
					Ti.UI.iPhone.appBadge=0;
					Ti.App.iOS.cancelAllLocalNotifications();
				}
			}],
			['blur', function(e){
				hasFocus = false;
				tableView.scrollsToTop = false;
			}]
		]
	}),

	tableRowTotal = 0,
	pulling = false,
	reloading = false,
	offset = 0,
	lastDistance = 0,


	showNewPosts_label = ui.label({
		text: '', color: '#6B6A67', top: 16,
		width: Ti.UI.SIZE, height: 13, backgroundColor: 'transparent',
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: { fontSize: 13, fontWeight: 'bold' },
		shadowColor:'#fff', shadowOffset:{x:0,y:1}
	}),
	showNewPosts_loading = Ti.UI.createActivityIndicator({
		color: '#6B6A67',
		font: {fontFamily:'Helvetica Neue', fontSize:13, fontWeight:'bold'},
		message: 'Loading...',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE
	}),
	showNewPosts = ui.view({
		properties: { width: Ti.UI.FILL, height: 40, showNewPosts_label: showNewPosts_label, showNewPosts_loading: showNewPosts_loading },
		content: [ showNewPosts_label, showNewPosts_loading ],
		events: [['click', function(e){
			if (GLOBAL.proccessingData){
				showNewPosts.showNewPosts_loading.hide();
				var showNewPosts_label_text = showNewPosts_label.text;
				showNewPosts_label.applyProperties({
					text: L('working_please_wait','Working. Please wait.'),
					width: Ti.UI.SIZE
				});
				setTimeout(function(){
					showNewPosts_label.applyProperties({
						text: showNewPosts_label_text,
						width: Ti.UI.SIZE
					});
				}, 2500);
			}
			else if (cachedPosts.length) {
				dontProccessNewPosts = false;

				showNewPosts.showNewPosts_label.hide();
				showNewPosts.showNewPosts_loading.show();

				// if (containingTab) containingTab.setBadge( null );

				// api[conf.api_setLastKnown]<( cachedPosts[0] );

				// updateTableData({data: cachedPosts});
				on_posts_GET.success({posts: cachedPosts, dontHideHeader: true});
				// cachedPosts = null;
			}
		}]]
	}),

	showMore_label = ui.label({text: 'Show more posts', height: 30}),
	showMore_loading = Ti.UI.createActivityIndicator({
		color: '#2f3238',
		font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'},
		message: 'Loading...',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE
	}),
	showMore_view = ui.view({
		properties: {height: 39, left: 2, right: 2, // backgroundImage: 'images/cache_row.png', backgroundRepeat: true,
			showMore_loading: showMore_loading, showMore_label: showMore_label},
		content: [showMore_label, showMore_loading]
	}),

	tableView_clicking = false,
	tableView = ui.table({
		config: 'tableView',
		properties: {minRowHeight: 72, top: 0},
		events: [
			['longpress', function(e){
				// Ti.API.debug('Row longpress event fired <- '+JSON.stringify(e));
				if (e.rowData && e.rowData.className !== 'noPosts' && e.rowData.className !== 'theEnd')
					api.execIfOnline(function(){
						var menu = _f.createPostQuickMenu(e);
						menu.open();
					});
			}],
			['singletap', function(e){
				// Ti.API.debug('Row singletap event fired <- '+JSON.stringify(e));
				if (e.index >= 0) {
					var rowIndex = e.index,
						postData = e.rowData.postData,
						myEntity = GLOBAL.entity;

					if (e.rowData.className === 'theEnd') return;

					else if (e.rowData.className === 'showMore') {
						if (showMore_active) return;
						showMore_active = true;

						showMore_view.showMore_label.hide(); // hide the label
						showMore_view.showMore_loading.show(); // show the spinner

						gapRowIndex = rowIndex;
						Ti.API.debug('User is asking for more posts. Gap on index '+gapRowIndex);
						var top_post = section.rows[gapRowIndex-1].postData,
							bottom_post = section.rows[gapRowIndex+2].postData;
						Ti.API.debug('Top post: '+JSON.stringify(top_post));
						Ti.API.debug('Bottom post: '+JSON.stringify(bottom_post));
						api[conf.api_GET]({
							limit: maxPostsToGet,
							before_id: top_post.id,
							before_id_entity: top_post.entity,
							until_id: bottom_post.id,
							until_id_entity: bottom_post.entity
						}, {
							onload: function(e){
								var posts = e.posts,
									postsLen = e.posts.length;
								Ti.API.warn('###########################################################');
								Ti.API.info('Matching last cached post with the end of recieved list...');
								Ti.API.debug('New posts tail -> '+posts[postsLen-1].id);
								Ti.API.debug('Section head -> '+section.rows[gapRowIndex+1].postData.id);
								Ti.API.warn('###########################################################');
								// remove the gap row if no more posts are missing in the gap

								if (posts[postsLen-1].id === section.rows[gapRowIndex+1].postData.id || !postsLen) {
									// tableData.splice(gapRowIndex, 1);
									tableView.deleteRow(gapRowIndex);
									posts.splice(-1, 1);
									postsLen--;
								}

								if (postsLen) {
									var proccessShowMore = function (postsData){
										for (i=postsLen; i--;) {
											postData = postsData[i];

											if (postData.type.split('#')[0] === api.postType('Repost') && !postData.original_post) {
												Ti.API.debug('Repost found. Need to grab original post data.');

												var recurData = postsData,
													_proccessShowMore = proccessShowMore;

												postData.original_post = {};
												var mention = postData.mentions[0];
												api.entity_post_GET({
													entity: mention.entity,
													post_id: mention.id,
													callback: {
														onload: function(post){
															Ti.API.debug('Entity post seems to exist. Doing recursive stuff.');
															postData.original_post.post_content = post;
															postData.original_post.profile = api.getCache('discover')[mention.entity];
															postData.content.text = post.content.text;
															recurData[i] = postData;
															_proccessShowMore(recurData);
														},
														onerror: function(resp){
															if (resp.status === 404) {
																Ti.API.warn('Entity post does not exist anymore (404). Removing from list and doing recursive stuff.');
																recurData.splice(i, 1); // remove the the entry since it doesnt exist anymore
															}
															_proccessShowMore(recurData);
														}
													}
												});
												return;
											}
										}

										Ti.API.info('Inserting the new posts in the gap row.');
										var newPosts = [], insertRowAfterIndex = gapRowIndex-1;
										for (var i=0; i<postsLen; i++){
											Ti.API.debug('Adding row after index '+(insertRowAfterIndex+i)+': '+JSON.stringify(posts[i]));
											tableView.insertRowAfter(insertRowAfterIndex+i, new _f.createRowFromPost(posts[i]) );
										}

										showMore_view.showMore_label.show(); // show the label
										showMore_view.showMore_loading.hide(); // hide the spinner
										showMore_active = false;
									};

									if (Ti.Network.online) proccessShowMore( posts );
								}
								else {
									showMore_view.showMore_label.show(); // show the label
									showMore_view.showMore_loading.hide(); // hide the spinner
									showMore_active = false;
								}

								// tableData.splice.apply(tableData, [gapRowIndex, 0].concat(newPosts));
								// tableView.setData(tableData);
							},
							onerror: function(e){
								showMore_view.showMore_label.show(); // show the label
								showMore_view.showMore_loading.hide(); // hide the spinner
								showMore_active = false;
							}
						});
					}
					else {
						if (!tableView_clicking && postData) {
							tableView_clicking = true;
							// Ti.API.debug('Row post data: '+JSON.stringify(postData));
							var PostDetails = require('views/postDetails');
							var postDetails = new PostDetails(e, { backButton: self.title });
							GLOBAL.tabGroup.activeTab.open( postDetails );
							setTimeout(function(){ tableView_clicking = false; }, 1000);
						}
					}
				}
			}],
			['scroll',function(e){
				// do nothing if proccessing data
				if (GLOBAL.proccessingData) return;

				offset = e.contentOffset.y;

				if (pulling && !reloading && offset > -70 && offset < 0){
					pulling = false;
					var unrotate = Ti.UI.create2DMatrix();
					imageArrow.animate({transform:unrotate, duration:180});
					labelStatus.applyProperties({
						text: L('pull_down_to_refresh','Pull down to refresh'),
						width: Ti.UI.SIZE
					});
				} else if (!pulling && !reloading && offset < -70){
					pulling = true;
					var rotate = Ti.UI.create2DMatrix().rotate(180);
					imageArrow.animate({transform:rotate, duration:180});
					labelStatus.applyProperties({
						text: L('release_to_refresh','Release to refresh')+'...',
						width: Ti.UI.SIZE
					});
				}

				if (section.rowCount && !reachedPostHistoryEnd) {
					var total = offset + e.size.height,
						theEnd = e.contentSize.height;
					var distance = theEnd - total;
					// going down is the only time we dynamically load,
					// going up we can safely ignore -- note here that
					// the values will be negative so we do the opposite
					if (distance < lastDistance) {
						// adjust the % of rows scrolled before we decide to start fetching
						var nearEnd = theEnd * 0.9;

						if (!updatingOlder && (total >= nearEnd)) {
							Ti.API.debug('User reached the end of the table. Grabbing older posts.');
							getOlderPosts();
						}
					}
					lastDistance = distance;
				}
			}],
			['dragEnd',function(e){
				if ( !GLOBAL.proccessingData && Ti.Network.online && pulling && !reloading && offset < -70){
					e.source.setContentInsets({top:70}, {animated:true});

					section.setHeaderView( Ti.UI.createView({height:10}) ); // remove the "X new posts"
					pulling = false;
					reloading = true;
					labelStatus.hide();
					imageArrow.hide();
					actInd.show();

					dontProccessNewPosts = false;
					updateTableData();
				}
			}]
		]
	});

	self.add(tableView);


	var tableHeader = Ti.UI.createView({
		// backgroundColor:'#e3ded8',
		backgroundImage: 'images/bg.png',
		backgroundRepeat: true,
		width: 320, height: 40
	});

	var border = Ti.UI.createView({
		backgroundColor:'#1382b9',
		bottom:0,
		height:1
	});
	// tableHeader.add(border);

	var headerContentWrap = Ti.UI.createView({width: 150, height: 25, bottom:10});

	var imageArrow = Ti.UI.createImageView({
		left:0, width:10, height:12,
		image:'images/arrow.png'
	});
	headerContentWrap.add(imageArrow);

	var labelStatus = Ti.UI.createLabel({
		color:'#6b6a67',
		font:{fontSize:13, fontWeight:'bold'},
		text: L('pull_down_to_refresh','Pull down to refresh'),
		textAlign:'center',
		right:0, width:130
	});
	headerContentWrap.add(labelStatus);

	var actInd = Ti.UI.createActivityIndicator({
		color: '#6B6A67',
		font: {fontFamily:'Helvetica Neue', fontSize:13, fontWeight:'bold'},
		message: 'Loading...',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE
	});
	headerContentWrap.add(actInd);
	tableHeader.add(headerContentWrap);

	function initTableView() {
		if (section.rowCount === 0) {
			tableIsEmpty = true;
			var actInd = Ti.UI.createActivityIndicator({
				color: '#2f3238',
				font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'},
				message: 'Loading...',
				style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
				height:Ti.UI.SIZE,
				width:Ti.UI.SIZE
			});
			tableView.setData([
				ui.section({
					properties: { headerView: Ti.UI.createView({height:9}), footerView: Ti.UI.createView({height:5}) },
					content: [ui.row({
						config: 'rowWithoutChild_full',
						content: [ui.view({
							properties: { height:Ti.UI.SIZE, width:Ti.UI.SIZE },
							content: [actInd]
						})]
					})]
				})
			]);
			actInd.show();
			// updateTableData();
		}
	}

	function resetPullHeader(){
		var last_update = moment().fromNow();
		reloading = false;
		actInd.hide();
		imageArrow.transform=Ti.UI.create2DMatrix();
		imageArrow.show();
		labelStatus.show();
		tableView.setContentInsets({top:-1}, {animated:true});
		// store timestamp for future reference
		Ti.App.Properties.setString('myStream_lastupdate', last_update);
	}

	tableView.headerPullView = tableHeader;

	var section = Ti.UI.createTableViewSection({ headerView: Ti.UI.createView({height:1}), footerView: Ti.UI.createView({height:1}) });
	function updateTableData (args, callback){
		Ti.API.log(conf.title, 'updateTableData() called');
		startProcessingData(args ? args.dontHideHeader : null);

		if (!reloading) {
			labelStatus.applyProperties({
				text: L('working_please_wait','Working. Please wait.'),
				width: Ti.UI.SIZE
			});
		}

		showNewPosts.touchEnabled = false;

		// only show activity indicator if now doing background fetch
		// if (!dontProccessNewPosts) ui.showIndicator();

		if (!args || !args.data || !args.data.length) {
			Ti.API.log(conf.title, 'updateTableData() no data given. Downloading...');
			var apiFilters = { limit: maxPostsToGet };
			if (conf.entity) apiFilters.entities = conf.entity; // only grab posts from this entity
			if (section.rowCount > 1 && section.rows[1].postData) { // new way to grab posts SINCE
				Ti.API.debug('Table has posts. Adding filter: since');
				var sincePost = section.rows[1].postData;
				apiFilters.since = sincePost.published_at;
			}
			if (conf.api_filters) _.extend(apiFilters, conf.api_filters);

			api[conf.api_GET](apiFilters, {
				onload: on_posts_GET.success,
				onerror: resetPullHeader
			});
		}
		else {
			Ti.API.log(conf.title, 'updateTableData() Got data input. Processing...');
			cachedPosts = null;
			var argsdata_len = args.data.length;

			if (argsdata_len) {
				// api[conf.api_setLastKnown](args.data[1]);

				var postData, i, j;
				sectionHead = args.data[0];

				///////////////
				// first stage
				///////////////
				if (!args.gotoSecondStage) {
					for (i=args.data.length; i--;) {
						postData = args.data[i];

						if (postData.type.split('#')[0] === api.postType('Repost') && !postData.original_post) {
							Ti.API.debug('Repost found. Need to grab original post data.');

							var recurData = args.data,
								recurArgs = args,
								_updateTableData = updateTableData;

							postData.original_post = {};
							var mention = postData.mentions[0];
							api.entity_post_GET({
								entity: mention.entity,
								post_id: mention.post,
								callback: {
									onload: function(post){
										Ti.API.debug('Entity post seems to exist. Doing recursive stuff.');
										postData.original_post.post_content = post;
										postData.original_post.profile = api.getCache('discover')[mention.entity];
										postData.content.text = post.content.text;
										recurData[i] = postData;
										recurArgs.data = recurData;
										_updateTableData(recurArgs);
									},
									onerror: function(resp){
										if (resp.status === 404) {
											Ti.API.warn('Entity post does not exist anymore (404). Removing from list and doing recursive stuff.');
											recurData.splice(i, 1); // remove the the entry since it doesnt exist anymore
											recurArgs.data = recurData;
										}
										_updateTableData(recurArgs);
									}
								}
							});
							return;
						}
					}
				}


				// TODO: make the post cache limit an option?
				// limiting local cache to maxPostsToGet posts
				if (!conf.noCache && !args.loadingFromCache) {
					// Ti.API.info('-------------- myStream_cache -------------');
					// Ti.API.debug('Saving data to cache: '+JSON.stringify(args.data));
					myStream_cache = args.data.concat(myStream_cache);
					myStream_cache = myStream_cache.slice(0, maxPostsToGet);
					Ti.App.Properties.setString(conf.appProperties_cache, JSON.stringify(myStream_cache));
					// Ti.API.debug(JSON.stringify(myStream_cache));
					// Ti.API.info('-------------- myStream_cache -------------');
				}

				///////////////
				// second stage
				///////////////
				var row, rowProperties = null;
				if (section.rowCount) {
					Ti.API.debug('We have data in the table already - appending rows');
					var new_section = Ti.UI.createTableViewSection({ headerView: Ti.UI.createView({height:1}), footerView: Ti.UI.createView({height:1}) });

					section.rows[0].applyProperties({ backgroundImage: 'images/row_bg_mid.png' });
					for (i=0, j=argsdata_len; i<j; i++) {
						postData = args.data[i];
						if (!postData.content) continue;
						// Storing post data to local cache
						// if (!conf.noCache) myStream_cache.splice(i, 0, postData);
						// Ti.API.debug('postData '+i+': '+JSON.stringify(postData));

						if (!i) { rowProperties = { backgroundImage: 'images/row_bg_top.png', backgroundTopCap: 8, backgroundLeftCap: 20 }; }
						else rowProperties = null;
						row = new _f.createRowFromPost(postData, rowProperties);
						new_section.add(row);
					}

					if (!args.isLastSegment) {
						// if the last post from the payload doesnt match the last post we had: might have more in the gap
						new_section.add(ui.row({
							config: 'genericPostRow',
							properties: { height: 39, className: 'showMore' },
							content: [showMore_view]
						}));
					}

					for (i=0, j=section.rowCount; i<j; i++) {
						new_section.add(section.rows[i]);
					}
					section = new_section;
					tableView.setData([section]);
				}
				else {
					Ti.API.debug('Table is empty - setting data');

					// if (argsdata_len === 1) {
					// 	Ti.API.debug('Only one row to be added. Setting a full background image.');
					// 	rowProperties = { backgroundImage: 'images/row_bg_full.png', backgroundTopCap: 8, backgroundLeftCap: 20 };
					// 	row = new _f.createRowFromPost(args.data[0], rowProperties);
					// 	section.add( row );
					// }
					// else {
					Ti.API.debug('More then one row to be added. Setting a top background image first.');
					for (i=0, j=argsdata_len; i<j; i++) {
						postData = args.data[i];
						if (!postData.content) continue;
						// Storing post data to local cache
						// Ti.API.debug('postData '+i+': '+JSON.stringify(postData));

						if (!i)
							rowProperties = { backgroundImage: 'images/row_bg_top.png', backgroundTopCap: 8, backgroundLeftCap: 20 };
						// else if (argsdata_len-1 === i && argsdata_len < 21)
						// 	rowProperties = {backgroundImage: 'images/row_bg_bottom.png',  backgroundTopCap: 10, backgroundLeftCap: 20};
						else
							rowProperties = null;
						row = new _f.createRowFromPost(postData, rowProperties);
						section.add( row );
						// tableData.push(row);
					}
					// }

					// add the loading indicator at the bottom if we have enough posts
					if (argsdata_len >= 31) {
						section.add(loadingRow);
						actIndicator.show();
					}
					else {
						var theEndRow = ui.row({
							config: 'rowWithoutChild_bottom',
							content: [ui.view({
								properties: { height:Ti.UI.SIZE, width:Ti.UI.SIZE, className: 'theEnd' },
								content: [ui.label({touchEnabled: false, text: '- The End -', width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
									color: '#6B6A67', font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'} })]
							})]
						});
						section.add(theEndRow);
						reachedPostHistoryEnd = true;
					}

					tableData = [ section ];

					// push the data into the tableView
					tableView.setData(tableData);

					// remember the post on the bottom of the table
					lastRow = row;
					// lastRowIndex = tableData.length -1;
					lastRowIndex = section.rowCount -1;
				}

				Ti.API.debug('Table row count: '+section.rowCount);
				// if (!conf.noCache) Ti.API.debug(conf.appProperties_cache+': '+JSON.stringify(myStream_cache));
			}

			showNewPosts.touchEnabled = true;
			resetPullHeader();
			// ui.hideIndicator();

			stopProcessingData();
			section.setHeaderView( Ti.UI.createView({height:10}) );
			labelStatus.applyProperties({
				text: L('pull_down_to_refresh','Pull down to refresh'),
				width: Ti.UI.SIZE
			});

			if (callback) callback();
		}
	}

	function addNewPost(e){
		var row = new _f.createRowFromPost(e.post);
		// tableData.unshift(row);
		tableView.insertRowBefore(0, row);

		// set last known post
		if (conf.api_setLastKnown) api[conf.api_setLastKnown](myStream_cache[0]);

		// update local storage cache
		// var myStream_cache = Ti.App.Properties.getString('myStream_cache', []);
		if (!conf.noCache) {
			myStream_cache.unshift(e.post);
			myStream_cache = myStream_cache.slice(0, maxPostsToGet);
			Ti.App.Properties.setString(conf.appProperties_cache, JSON.stringify(myStream_cache));
		}
	}

	function getOlderPosts() {
		if (Ti.Network.online && !reachedPostHistoryEnd) {
			updatingOlder = true;
			// tableData.push(loadingRow);
			// tableView.appendRow(loadingRow);
			var postData = lastRow.postData;

			var apiFilters = {
				before: postData.published_at,
				// entities: postData.entity,
				limit: maxOldPostsToGet
			};
			if (conf.entity) apiFilters.entity = conf.entity; // only grab posts from this entity

			api[conf.api_posts_OLD]( apiFilters, {
				onload: function(resp){
					appendOlderPosts(resp.posts);
				},
				onerror: function(e){
					Ti.API.error('api.posts_OLD');
				}
			});
		}
	}

	function appendOlderPosts(posts){
		if (!posts.length) {
			var theEndRow = ui.row({
				config: 'rowWithoutChild_bottom',
				content: [ui.view({
					properties: { height:Ti.UI.SIZE, width:Ti.UI.SIZE, className: 'theEnd' },
					content: [ui.label({touchEnabled: false, text: '- The End -', width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
						color: '#6B6A67', font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'} })]
				})]
			});
			tableView.deleteRow(section.rowCount -1);
			tableView.appendRow(theEndRow);
			reachedPostHistoryEnd = true;
		}
		else {
			var postData, i, j;
			for (i=posts.length; i--;) {
				postData = posts[i];

				if (postData.type.split('#')[0] === api.postType('Repost') && !postData.original_post) {
					Ti.API.debug('Repost found. Need to grab original post data.');

					var recurData = posts;

					postData.original_post = {};
					var mention = postData.mentions[0];
					api.entity_post_GET({
						entity: mention.entity,
						post_id: mention.id,
						callback: {
							onload: function(post){
								Ti.API.debug('Entity post seems to exist. Doing recursive stuff.');
								postData.original_post.post_content = post;
								postData.original_post.profile = api.getCache('discover')[mention.entity];
								postData.content.text = post.content.text;
								recurData[i] = postData;
								appendOlderPosts( recurData );
							},
							onerror: function(resp){
								if (resp.status === 404) {
									Ti.API.warn('Entity post does not exist anymore (404). Removing from list and doing recursive stuff.');
									recurData.splice(i, 1); // remove the the entry since it doesnt exist anymore
								}
								appendOlderPosts( recurData );
							}
						}
					});
					return;
				}
			}

			section.remove(loadingRow);

			var row, newRows = [];
			for (i=0, j=posts.length; i<j; i++) {
				postData = posts[i];
				if (!postData.content.text) { continue; }
				row = new _f.createRowFromPost(postData);
				section.add(row);
				// tableView.insertRowBefore(lastRowIndex+i, row);
			}
			section.add(loadingRow);

			// replace the last row "Loading" with the fetched posts
			// tableData.splice.apply(tableData, [tableData.length -1, 1].concat(newRows));
			//


			// tableView.deleteRow( lastRowIndex , {animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
			tableView.setData([section]);

			// remember the post on the bottom of the table
			lastRow = row;
			lastRowIndex = section.rowCount -1;
			// just scroll down a bit to the new rows to bring them into view
			// tableView.scrollToIndex(lastRowIndex-(maxOldPostsToGet-2),{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});
		}

		updatingOlder = false;
		Ti.API.debug('Table row count: '+section.rowCount);
	}

	function seekAndDestroyDeletedPosts(post_list){
		if (!section.rowCount) return;

		Ti.API.info('Checking for deleted posts...');
		var del_indexTable = [], cur_postData, i,j,k,l;
		for (i=0,j=post_list.length; i<j; i++) {
			cur_postData = post_list[i];
			if (cur_postData.type.split('#')[0] === api.postType('Delete')) {
				// Ti.API.warn('POST:DELETE -> '+JSON.stringify(cur_postData));
				// Ti.API.warn('Looking for the deleted post in our table...');

				for (k=0,l=section.rows.length; k<l; k++){
					var curRow = section.rows[k];
					if (!curRow.postData) continue; // skip rows that are not posts

					// Ti.API.warn(curRow.postData.id +' === '+ cur_postData.content.id +' && '+curRow.postData.entity +' === '+ cur_postData.entity);
					if (curRow.postData.id === cur_postData.content.id && curRow.postData.entity === cur_postData.entity) {
						Ti.API.warn('Found post at table index: '+k);
						del_indexTable.push(k);
						break;
					}
				}
			}
		}
		// delete posts from the tableview
		for (i=0, j=del_indexTable.length; i<j; i++) {
			var itable = del_indexTable[i];
			Ti.API.warn('POST:DELETE -> Deleting row at index: '+itable);
			tableView.deleteRow(itable, {animated: true});
			myStream_cache.splice(itable, 1);
			Ti.App.Properties.setString(conf.appProperties_cache, JSON.stringify(myStream_cache));
		}
		// set the top row background image
		section.rows[0].applyProperties({ backgroundImage: section.rows.length > 1 ? 'images/row_bg_top.png' : 'images/row_bg_full.png' });
		// reset our section head (top row)
		sectionHead = section.rows[0].postData;
	}
	////////////////////////////////////////////

	var
	didBackgroundPostSelection = false,
	on_posts_GET = {
		success: function(e){
			Ti.API.log(conf.title, ' ------------------- / -------------------');
			Ti.API.log(conf.title, 'on_posts_GET.success <- '+JSON.stringify(e));
			Ti.API.log(conf.title, ' ------------------- / -------------------');

			startProcessingData(e.dontHideHeader);

			showNewPosts.touchEnabled = true;
			cachedPosts = e.posts;
			var cachedPostsLength = cachedPosts.length, i;

			var afterDelete = function(){
				Ti.API.log(conf.title, 'Running afterDelete() ...');
				Ti.API.log(conf.title, 'cachedPostsLength = '+cachedPostsLength);
				if (dontProccessNewPosts && cachedPostsLength && sectionHead) {

					didBackgroundPostSelection = false;
					// Ti.API.warn('###########################################################');
					// Ti.API.info('Matching last cached post with the end of recieved list...');
					// Ti.API.debug(JSON.stringify(cachedPosts[cachedPostsLength-1]));
					// Ti.API.debug(JSON.stringify(section.rows[0].postData));
					Ti.API.warn('###########################################################');
					Ti.API.info('cachedPostsLength -> '+cachedPostsLength);

					// remove already cached posts
					Ti.API.info('Removing duplicates (already cached posts)...');
					for (i=0,j=cachedPostsLength+1; i<j; i++) {
						// if (i<cachedPostsLength) Ti.API.debug(cachedPosts[i].id +' === '+ sectionHead.id);
						if (i<cachedPostsLength && cachedPosts[i].id === sectionHead.id) { didBackgroundPostSelection = true; break; }
					}
					cachedPosts = cachedPosts.slice(0,i);
					cachedPostsLength = cachedPosts.length;
					Ti.API.warn('###########################################################');

					showNewPosts_label.text = (cachedPostsLength < maxPostsToGet ? cachedPostsLength: '+30') +' new posts';

					if (cachedPostsLength) {
						Ti.API.debug('Not proccessing cached posts but showing notification to the user.');
						// if (!hasFocus && containingTab) containingTab.setBadge( cachedPostsLength );
						showNewPosts.showNewPosts_label.show();
						showNewPosts.showNewPosts_loading.hide();
						section.setHeaderView( showNewPosts );
						Ti.App.fireEvent(conf.notificationEvent, {postsLength: cachedPostsLength});
					}
				}
				else if (cachedPostsLength) {
					var isLastSegment = didBackgroundPostSelection || false;
					if (sectionHead) {
						// remove already cached posts
						for (i=0,j=cachedPostsLength+1; i<j; i++) {
							if (i<cachedPostsLength && cachedPosts[i].id === sectionHead.id) { isLastSegment = true; break; }
						}
						cachedPosts = cachedPosts.slice(0,i);
						cachedPostsLength = cachedPosts.length;

						if (!cachedPostsLength) { resetPullHeader(); return; }

					}
					Ti.API.debug('Calling updateTableData() with:');
					Ti.API.debug('cachedPosts -> '+JSON.stringify(cachedPosts));
					Ti.API.debug('isLastSegment -> '+isLastSegment);
					updateTableData({data: cachedPosts, isLastSegment: isLastSegment, dontHideHeader: e.dontHideHeader});
					cachedPosts = null;
				}
				else {
					Ti.API.log(conf.title, 'afterDelete() -- Setting tableView message to "No Posts"');
					resetPullHeader();
					stopProcessingData();
					labelStatus.applyProperties({
						text: L('pull_down_to_refresh','Pull down to refresh'),
						width: Ti.UI.SIZE
					});

					Ti.API.log(conf.title, 'tableIsEmpty = '+tableIsEmpty);
					if (tableIsEmpty) {
						var noPostSection = Ti.UI.createTableViewSection();
						noPostSection.add(ui.row({
							config: 'rowWithoutChild_full',
							properties: { height: 40, className: 'noPosts' },
							content: [
								ui.view({
									properties: {touchEnabled: false, top: 12, bottom: 12, left: 14, right: 14},
									content: [
										ui.label({touchEnabled: false, text: 'No Posts', width: Ti.UI.FILL, height: Ti.UI.FILL, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
											color: '#6B6A67', font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'} })
									]
								})
							]
						}));
						tableView.setData([noPostSection]);
					}
				}
			};

			// check for deleted posts
			api.deleted_post_GET({limit: 20}, {
				onload: function(e) { seekAndDestroyDeletedPosts(e.posts); afterDelete(); stopProcessingData(); },
				onerror: function(e) { stopProcessingData(); }
			});


		},
		error: function(e){
			ui.hideIndicator();
			alert(e.status +' - '+ e.error);
		}
	},

	on_posts_POST = {
		success: function(e){
			ui.hideIndicator();
		},
		error: function(e){
			ui.hideIndicator();
			alert(e.status +' - '+ e.error);
		}
	},

	on_posts_DELETE = {
		success: function(e){
			tableView.deleteRow(e.rowIndex, {animated: true});
			myStream_cache.splice(e.rowIndex, 1);
			Ti.App.Properties.setString(conf.appProperties_cache, JSON.stringify(myStream_cache));
			ui.hideIndicator();
		},
		error: function(e){
			ui.hideIndicator();
			alert(e.status +' - '+ e.error);
		}
	};

	var bgPostCheck = function(){
		// Ti.API.debug('GLOBAL.proccessingData -> '+GLOBAL.proccessingData);
		if (GLOBAL.proccessingData) return;
		Ti.API.info('Running background check for "'+conf.title+'"...');
		if (Ti.Network.online) {
			dontProccessNewPosts = true;
			updateTableData();
		}
	};

	// Ti.API.debug('-------------- myStream_cache -------------');
	// Ti.API.debug(JSON.stringify(myStream_cache));
	// Ti.API.debug('-------------- myStream_cache -------------');
	if (myStream_cache && myStream_cache.length > 1 && myStream_cache[0].id === myStream_cache[1].id) myStream_cache.splice(0, 1);
	var callbackAfterUpdate = (myStream_cache && myStream_cache.length) ? bgPostCheck : null;
	initTableView();
	updateTableData({data: myStream_cache, loadingFromCache: true, gotoSecondStage: true}, callbackAfterUpdate);

	self.defineTab = function(tab) { containingTab = tab; };

	function startBgJobCallback(){
		stopBgJobCallback();
		Ti.API.info('Starting background job to check for new "'+conf.title+'": '+GLOBAL.appConfig.bgCheck[conf.bgCheck]);
		fetchPostsTimer = setInterval( bgPostCheck, GLOBAL.appConfig.bgCheck[conf.bgCheck] );
	}
	function stopBgJobCallback(){
		if (fetchPostsTimer) clearInterval(fetchPostsTimer);
		Ti.API.info('Stopping background job for: '+conf.title);
	}
	function apiDelete_Callback(e){ on_posts_DELETE[e.callback](e); }

	function startEventListeners(){
		Ti.API.info('---------------------------');
		Ti.API.info('Starting "'+conf.title+'" event listeners.');
		Ti.API.info('---------------------------');
		////////////////////////////////////////////
		// Ti.API.debug('cached_data: '+JSON.stringify(cached_data));
		// Ti.App.addEventListener('api:'+conf.api_events.GET+':success', on_posts_GET.success);
		// Ti.App.addEventListener('api:'+conf.api_events.GET+':error', on_posts_GET.error);
		Ti.App.addEventListener('api:'+conf.api_events.POST+':success', on_posts_POST.success);
		Ti.App.addEventListener('api:'+conf.api_events.POST+':error', on_posts_POST.error);
		Ti.App.addEventListener('api:'+conf.api_events.DELETE, apiDelete_Callback);
		Ti.App.addEventListener('startBgJob', startBgJobCallback);
		Ti.App.addEventListener('stopBgJob', stopBgJobCallback);
	}
	function stopEventListeners(){
		GLOBAL[conf.title+'_postsBgJobs'] = false;
		Ti.API.info('Stopping "'+conf.title+'" event listeners.');
		stopBgJobCallback();
		Ti.App.removeEventListener('api:'+conf.api_events.POST+':success', on_posts_POST.success);
		Ti.App.removeEventListener('api:'+conf.api_events.POST+':error', on_posts_POST.error);
		Ti.App.removeEventListener('api:'+conf.api_events.DELETE, apiDelete_Callback);
		Ti.App.removeEventListener('startBgJob', startBgJobCallback);
		Ti.App.removeEventListener('stopBgJob', stopBgJobCallback);
		Ti.App.removeEventListener('stopEvents', stopEventListeners);
	}

	Ti.API.debug('conf.noEvents: '+conf.noEvents);
	if (!conf.noEvents) {
		GLOBAL[conf.title+'_postsBgJobs'] = true;
		startEventListeners();
		Ti.App.addEventListener('stopEvents', stopEventListeners);
	}

	return self;
}

module.exports = getView;
