var Main, URI, api, ui,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

ui = require('lib/ui_sugar');

URI = require('lib/vendor/jsuri.min');

api = require('lib/tent.api2');

Main = (function() {
  function Main() {
    this.openMain = __bind(this.openMain, this);
    this.syncFollowsAndStart = __bind(this.syncFollowsAndStart, this);
    this.resetAuthButton = __bind(this.resetAuthButton, this);
    this.removeAuthEvents = __bind(this.removeAuthEvents, this);
    this.addAuthEvents = __bind(this.addAuthEvents, this);
    this.registerAppCanceled = __bind(this.registerAppCanceled, this);
    this.registerAppError = __bind(this.registerAppError, this);
    this.grabTokenFailed = __bind(this.grabTokenFailed, this);
    this.grabTokenSuccess = __bind(this.grabTokenSuccess, this);
    this.reAuthApp = __bind(this.reAuthApp, this);
    this.authEntity = __bind(this.authEntity, this);
    this.authenticating = false;
    this.addAuthEvents();
    this.self = ui.win({
      properties: {
        navBarHidden: true,
        backgroundColor: '#029cd2'
      },
      events: [
        [
          'open', (function(_this) {
            return function(e) {
              api.clearAuthCache();
              if (GLOBAL.reAuthApp) {
                _this.entity_input.value = GLOBAL.entity;
                _this.reAuthApp();
              }
              if (!api.isAuthenticated()) {
                _this.entity_input.focus();
              }
              GLOBAL.hasAuthorizeOpen = true;
            };
          })(this)
        ], [
          'focus', (function(_this) {
            return function(e) {
              Ti.API.debug('Authorize has focus.');
              if (!_this.authenticating) {
                _this.resetAuthButton();
                _this.entity_input.value = '';
                _this.entity_input.focus();
              } else if (api.cacheData && !api.cacheData.oauth) {
                _this.resetAuthButton();
              }
            };
          })(this)
        ]
      ]
    });
    this.scrollView = Ti.UI.createScrollView({
      contentWidth: 'auto',
      contentHeight: 'auto',
      height: Ti.UI.FILL,
      width: Ti.UI.FILL,
      showVerticalScrollIndicator: false
    });
    this.wrap = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      layout: 'vertical',
      top: 53
    });
    this.appLogo = Ti.UI.createImageView({
      image: 'images/authorize_logo.png',
      width: 117,
      height: 117,
      top: 0
    });
    this.entity_input = ui.textField({
      properties: {
        hintText: 'Entity URI',
        value: '',
        color: '#009BD4',
        textAlign: 'center',
        top: 40,
        height: 40,
        width: 280,
        borderRadius: 2,
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_NONE,
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        autocorrect: false,
        keyboardType: Ti.UI.KEYBOARD_URL,
        returnKeyType: Ti.UI.RETURNKEY_NEXT,
        backgroundColor: '#E6F5FB'
      },
      events: [
        [
          'change', (function(_this) {
            return function(e) {
              return _this.loginButton.enabled = !!e.source.value.length;
            };
          })(this)
        ], [
          'return', (function(_this) {
            return function(e) {
              var url;
              e.source.blur();
              url = new URI(e.source.value);
              if (!url.protocol()) {
                e.source.value = "https://" + e.source.value;
              }
              if (e.source.value) {
                _this.authEntity();
              }
            };
          })(this)
        ]
      ]
    });
    this.loginButton = ui.button({
      properties: {
        title: L('authenticate', 'Authenticate'),
        top: 15,
        height: 40,
        width: 280,
        enabled: false,
        backgroundImage: 'images/ipad-button-green.png',
        backgroundSelectedImage: 'images/ipad-button-green-pressed.png',
        style: Ti.UI.iPhone.SystemButtonStyle.PLAIN,
        color: 'white',
        shadowColor: 'black',
        shadowOffset: {
          x: 0,
          y: 1
        },
        font: {
          fontWeight: 'bold',
          fontSize: 18
        }
      },
      events: [
        [
          'click', (function(_this) {
            return function(e) {
              var url;
              _this.entity_input.blur();
              url = new URI(_this.entity_input.value);
              if (!url.protocol()) {
                _this.entity_input.value = "https://" + _this.entity_input.value;
              }
              if (_this.entity_input.value) {
                _this.authEntity();
              }
            };
          })(this)
        ]
      ]
    });
    this.wrap.add(this.appLogo);
    this.wrap.add(this.entity_input);
    this.wrap.add(this.loginButton);
    this.scrollView.add(this.wrap);
    this.self.add(this.scrollView);
  }

  Main.prototype.authEntity = function() {
    this.authenticating = true;
    this.entity_input.blur();
    this.loginButton.applyProperties({
      title: 'Discovering service...',
      enabled: false,
      backgroundImage: 'images/ipad-button-grey.png',
      backgroundSelectedImage: 'images/ipad-button-grey-pressed.png'
    });
    api.discover(this.entity_input.value, {
      onload: (function(_this) {
        return function(e) {
          Ti.API.debug('Setting profile avatar and other stuff here.');
          GLOBAL.entity = e.entity;
          GLOBAL.apn_channel = GLOBAL.entity.replace('://', '_').replace(/\./g, '_0_').replace(/\:/g, '_00_');
          Ti.App.Properties.setString('entity', GLOBAL.entity);
          _this.loginButton.title = 'Registering app...';
          Ti.API.debug('Will start app registration now.');
          return api.registerApp(true);
        };
      })(this),
      onerror: (function(_this) {
        return function() {
          _this.resetAuthButton();
          return alert('Service discovery failed.\nPlease try again.');
        };
      })(this),
      thiz: this
    }, true);
  };

  Main.prototype.reAuthApp = function() {
    this.authenticating = true;
    this.entity_input.blur();
    this.loginButton.applyProperties({
      title: 'Re-Authenticating...',
      enabled: false,
      backgroundImage: 'images/ipad-button-grey.png',
      backgroundSelectedImage: 'images/ipad-button-grey-pressed.png'
    });
    return api.registerApp(true);
  };

  Main.prototype.grabTokenSuccess = function(e) {
    Ti.API.debug('Clearing all cache before starting...');
    api.clearDiscoveryCache();
    Ti.App.Properties.removeProperty('myStream_cache');
    Ti.App.Properties.removeProperty('myMentions_cache');
    return this.syncFollowsAndStart();
  };

  Main.prototype.grabTokenFailed = function() {
    alert('Authentication failed.\nPlease try again.');
    return this.resetAuthButton();
  };

  Main.prototype.registerAppError = function(e) {
    this.resetAuthButton();
    return alert('App registration failed.\nPlease try again.');
  };

  Main.prototype.registerAppCanceled = function(e) {
    this.resetAuthButton();
    return alert('App registration canceled.');
  };

  Main.prototype.addAuthEvents = function() {
    Ti.App.addEventListener('api:grabToken:success', this.grabTokenSuccess);
    Ti.App.addEventListener('api:grabToken:failed', this.grabTokenFailed);
    Ti.App.addEventListener('api:registerApp:error', this.registerAppError);
    return Ti.App.addEventListener('api:registerApp:canceled', this.registerAppCanceled);
  };

  Main.prototype.removeAuthEvents = function() {
    Ti.App.removeEventListener('api:registerApp:error', this.registerAppError);
    Ti.App.removeEventListener('api:grabToken:success', this.grabTokenSuccess);
    return Ti.App.removeEventListener('api:grabToken:failed', this.grabTokenFailed);
  };

  Main.prototype.resetAuthButton = function() {
    return this.loginButton.applyProperties({
      title: L('authenticate', 'Authenticate'),
      enabled: true,
      backgroundImage: 'images/ipad-button-green.png',
      backgroundSelectedImage: 'images/ipad-button-green-pressed.png'
    });
  };

  Main.prototype.syncFollowsAndStart = function() {
    this.loginButton.title = 'Updating followings...';
    return api.followings_GET({
      onload: (function(_this) {
        return function() {
          _this.loginButton.title = 'Updating followers...';
          return api.followers_GET({
            onload: function() {
              _this.loginButton.title = 'All done. Welcome!';
              return _this.openMain();
            },
            onerror: function() {
              _this.loginButton.title = 'Server having problems.';
              return _this.openMain();
            }
          });
        };
      })(this),
      onerror: (function(_this) {
        return function() {
          _this.loginButton.title = 'Server having problems.';
          return _this.openMain();
        };
      })(this)
    });
  };

  Main.prototype.openMain = function() {
    var main;
    this.authenticating = false;
    GLOBAL.loggedOut = false;
    main = require('views/main');
    setTimeout((function(_this) {
      return function() {
        main.getView().open({
          transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
        });
        return _this.self.close();
      };
    })(this), 1000);
    return this.removeAuthEvents();
  };

  return Main;

})();

module.exports = (new Main()).self;
