var GLOBAL, Profile, api, mentionsWindow, myStreamWindow, png_name_ext, tabGroup, tab_home, tab_me, tab_mentions, _f;

GLOBAL = require('globals');

api = require('lib/tent.api2');

_f = require('views/func_repo');

myStreamWindow = require('views/myStream');

mentionsWindow = require('views/mentions');

Profile = require('views/profile');

png_name_ext = GLOBAL.isTablet ? '_76' : '';

tab_home = Ti.UI.createTab({
  backgroundImage: "images/stream_tab" + png_name_ext + ".png",
  selectedBackgroundImage: "images/stream_tab_active" + png_name_ext + ".png",
  backgroundImage2: "images/stream_tab2" + png_name_ext + ".png",
  selectedBackgroundImage2: "images/stream_tab_active2" + png_name_ext + ".png",
  tabName: 'Stream',
  window: myStreamWindow
});

tab_mentions = Ti.UI.createTab({
  backgroundImage: "images/mentions_tab" + png_name_ext + ".png",
  selectedBackgroundImage: "images/mentions_tab_active" + png_name_ext + ".png",
  backgroundImage2: "images/mentions_tab2" + png_name_ext + ".png",
  selectedBackgroundImage2: "images/mentions_tab_active2" + png_name_ext + ".png",
  tabName: 'Mentions',
  window: mentionsWindow
});

tab_me = Ti.UI.createTab({
  backgroundImage: "images/profile_tab" + png_name_ext + ".png",
  selectedBackgroundImage: "images/profile_tab_active" + png_name_ext + ".png",
  backgroundImage2: "images/profile_tab2" + png_name_ext + ".png",
  selectedBackgroundImage2: "images/profile_tab_active2" + png_name_ext + ".png",
  tabName: 'Me',
  window: new Profile(GLOBAL.entity)
});

tabGroup = Ti.UI.createTabGroup();

tabGroup.addTab(tab_home);

tabGroup.addTab(tab_mentions);

tabGroup.addTab(tab_me);

tabGroup = _f.overrideTabs(tabGroup, {
  backgroundImage: 'images/tabbar.png',
  backgroundTopCap: 5,
  height: 49
}, {}, {});

GLOBAL.tabGroup = tabGroup;

exports.getView = function() {
  var apn, err;
  api.discover(GLOBAL.entity);
  Ti.API.debug('Starting post timestamp updater job.');
  setInterval(function() {
    var ts, tsCache, tsLabel, _i, _len, _ref, _results;
    if (GLOBAL.timestampCache.length) {
      _ref = GLOBAL.timestampCache;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        tsCache = _ref[_i];
        tsLabel = tsCache[0];
        ts = tsCache[1];
        _results.push(tsLabel.text = moment(ts).fromNow());
      }
      return _results;
    }
  }, 60000);
  if (Ti.Platform.model !== "Simulator") {
    apn = require('lib/apn');
    try {
      if (Ti.Network.remoteNotificationsEnabled) {
        if (!Ti.Network.remoteDeviceUUID) {
          Ti.API.warn('remoteDeviceUUID is not set. Re-registering for Push Notifications.');
          apn.unregisterForPushNotifications();
          apn.registerForPushNotifications();
        } else {
          apn.checkInstallation({
            onload: function(e) {
              if (e.channels.indexOf(GLOBAL.apn_channel) === -1) {
                return apn.subscribeChannel(GLOBAL.apn_channel);
              }
            },
            onerror: function(e) {
              Ti.API.warn('Parse installation check error. Uploading new installation.');
              return apn.uploadParseInstallation();
            }
          });
        }
      } else {
        apn.registerForPushNotifications();
      }
    } catch (_error) {
      err = _error;
      Ti.API.error(err);
      alert(err);
    }
  }
  return tabGroup;
};

exports.updateFollows = function(callback) {
  if (api.isAuthenticated() && Ti.Network.online) {
    Ti.API.info('Updating followings...');
    return api.followings_GET({
      onload: function() {
        Ti.API.info('Updating followers...');
        return api.followers_GET({
          onload: function() {
            return callback();
          },
          onerror: function() {
            Ti.API.warn('Searching followings - Server having problems');
            return callback();
          }
        });
      },
      onerror: function() {
        Ti.API.warn('Searching followers - Server having problems');
        return callback();
      }
    });
  }
};

exports.addTabGroupEvents = function() {
  Ti.App.addEventListener('notify:newPosts', function(e) {
    if (tabGroup.activeTab.tabName !== 'Stream' && e.postsLength) {
      return tabGroup.hasNotification(tabGroup.tabs[0]);
    }
  });
  return Ti.App.addEventListener('notify:newMentions', function(e) {
    if (tabGroup.activeTab.tabName !== 'Mentions' && e.postsLength) {
      return tabGroup.hasNotification(tabGroup.tabs[1]);
    }
  });
};
