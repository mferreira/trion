var GLOBAL = require('globals'),
	api = require('lib/tent.api'),
	_f = require('views/func_repo');


//
var myStreamWindow = require('views/myStream');
var navStream = Ti.UI.iPhone.createNavigationGroup({ window: myStreamWindow });
var tab_home = Ti.UI.createImageView({
	backgroundImage: 'images/stream_tab'+(GLOBAL.isTablet ? '_76':'')+'.png',
	selectedBackgroundImage: 'images/stream_tab_active'+(GLOBAL.isTablet ? '_76':'')+'.png',
	backgroundImage2: 'images/stream_tab2'+(GLOBAL.isTablet ? '_76':'')+'.png',
	selectedBackgroundImage2: 'images/stream_tab_active2'+(GLOBAL.isTablet ? '_76':'')+'.png',
	tabName: 'Stream',
	index: 0,
	window: navStream
});

//
var mentionsWindow = require('views/mentions');
var navMentions = Ti.UI.iPhone.createNavigationGroup({ window: mentionsWindow, visible: false });
var tab_mentions = Ti.UI.createImageView({
	backgroundImage: 'images/mentions_tab'+(GLOBAL.isTablet ? '_76':'')+'.png',
	selectedBackgroundImage: 'images/mentions_tab_active'+(GLOBAL.isTablet ? '_76':'')+'.png',
	backgroundImage2: 'images/mentions_tab2'+(GLOBAL.isTablet ? '_76':'')+'.png',
	selectedBackgroundImage2: 'images/mentions_tab_active2'+(GLOBAL.isTablet ? '_76':'')+'.png',
	tabName: 'Mentions',
	index: 1,
	window: navMentions
});

//
var Profile = require('views/profile');
var navProfile = Ti.UI.iPhone.createNavigationGroup({ window: (new Profile(GLOBAL.entity)), visible: false });
var tab_me = Ti.UI.createImageView({
	backgroundImage: 'images/profile_tab'+(GLOBAL.isTablet ? '_76':'')+'.png',
	selectedBackgroundImage: 'images/profile_tab_active'+(GLOBAL.isTablet ? '_76':'')+'.png',
	backgroundImage2: 'images/profile_tab2'+(GLOBAL.isTablet ? '_76':'')+'.png',
	selectedBackgroundImage2: 'images/profile_tab_active2'+(GLOBAL.isTablet ? '_76':'')+'.png',
	tabName: 'Me',
	index: 2,
	window: navProfile
});

// create tab group
var tabGroup = Ti.UI.createView({bottom: 0, height: 49});
GLOBAL.tabGroup = tabGroup;

// add tabs
tabGroup.tabs = [tab_home, tab_mentions, tab_me];
tabGroup.activeTab = tab_home;

tabGroup = _f.overrideTabs2( tabGroup, {
	backgroundImage: 'images/tabbar.png', backgroundTopCap: 5, height: 49
});

var rootWin = Ti.UI.createWindow();
rootWin.add(navStream);
rootWin.add(navMentions);
rootWin.add(navProfile);
rootWin.add(tabGroup);

/////////////////////////////////
exports.getView = function() {
	// pre-discover our profile data
	api.discover(GLOBAL.entity);
	/////////////////////////////////
	// background jobs
	/////////////////////////////////
	//
	Ti.API.debug('Starting post timestamp updater job.');
	setInterval(function(){
		if (GLOBAL.timestampCache.length) {
			for (var i=GLOBAL.timestampCache.length; i--;){
				var tsLabel = GLOBAL.timestampCache[i][0],
					ts = GLOBAL.timestampCache[i][1];
				tsLabel.text = moment.unix(ts).fromNow();
			}
		}
	}, 60000); // TODO: put this in the config screen
	return rootWin;
};

exports.updateFollows =function(callback) {
	if (api.isAuthenticated() && Ti.Network.online) {
		Ti.API.info('Updating followings...');
		api.followings_GET({
			onload: function(){
				Ti.API.info('Updating followers...');
				api.followers_GET({
					onload: function() { callback(); },
					onerror: function(){
						Ti.API.warn('Searching followings - Server having problems');
						callback();
					}
				});
			},
			onerror: function(){
				Ti.API.warn('Searching followers - Server having problems');
				callback();
			}
		});
	}
};

exports.addTabGroupEvents = function() {
	Ti.App.addEventListener('notify:newPosts', function(e){
		if (tabGroup.activeTab.tabName !== 'Stream' && e.postsLength) {
			tabGroup.tabs[0].applyProperties(tabGroup.tabs[0].deselected2);
			// tabGroup.tabs[0].selected.hide();
		}
	});

	Ti.App.addEventListener('notify:newMentions', function(e){
		if (tabGroup.activeTab.tabName !== 'Mentions' && e.postsLength) {
			tabGroup.tabs[1].applyProperties(tabGroup.tabs[1].deselected2);
			// tabGroup.tabs[1].deselected2.show();
			// tabGroup.tabs[1].selected.hide();
		}
	});
};

Ti.App.addEventListener('resumed', function(){
	Ti.API.info('App resuming with arguments: '+JSON.stringify(Ti.App.getArguments()));
	if (Ti.App.getArguments().url) {
		var URI = require('lib/vendor/jsuri.min'),
			NewPost = require('views/newPost2');

		var uri =  new URI(Ti.App.getArguments().url);
		if (uri.protocol() === 'tentstatus') {
			(new NewPost({
				prefill: {content: {text: decodeURIComponent(uri.host()) }}
			})).open({modal: true});
		}
	}
});
