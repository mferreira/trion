var NewPost, URI, api, createView, favoritePosts, generatorReplyRows, moment, ui, _f;

ui = require('lib/ui_sugar');

api = require('lib/tent.api2');

URI = require('lib/vendor/jsuri.min');

_f = require('views/func_repo');

moment = require('lib/vendor/moment.min');

favoritePosts = require('lib/favoritePosts');

NewPost = require('views/newPost2');

generatorReplyRows = function(postData, tableView, replySection, replyRows, rowCount, windowClosed) {
  var entity_post_GET_onerror, entity_post_GET_onload, hasReply, lastRow, reply, _i, _len, _ref;
  if (windowClosed) {
    return;
  }
  rowCount = rowCount || 0;
  hasReply = false;
  entity_post_GET_onload = function(post) {
    var row;
    Ti.API.debug('Entity post seems to exist. Updating table content.');
    row = new _f.createRowFromPost(post, {});
    if (!rowCount) {
      row.applyProperties({
        backgroundImage: 'images/row_bg_full.png',
        backgroundTopCap: 8,
        backgroundLeftCap: 20
      });
    } else if (rowCount === 1) {
      replyRows[0].applyProperties({
        backgroundImage: 'images/row_bg_top.png',
        backgroundTopCap: 8,
        backgroundLeftCap: 20
      });
    }
    if (replyRows.length) {
      tableView.appendRow(replyRows[replyRows.length - 1]);
    }
    rowCount += 1;
    replyRows.push(row);
    return generatorReplyRows(post, tableView, replySection, replyRows, rowCount);
  };
  entity_post_GET_onerror = function(resp) {
    var post_text;
    post_text = 'Error grabbing post.';
    if (resp.status === 404) {
      Ti.API.warn('Entity post does not exist anymore (404).');
      return post_text = 'Post no longer exists.';
    }
  };
  if (postData.mentions) {
    _ref = postData.mentions;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      reply = _ref[_i];
      if (reply.post) {
        Ti.API.debug("generatorReplyRows rowCount: " + rowCount);
        hasReply = true;
        if (!replySection) {
          replySection = ui.section({
            properties: {
              headerView: Ti.UI.createView({
                height: 1
              }),
              footerView: Ti.UI.createView({
                height: 5
              })
            }
          });
          tableView.appendSection(replySection);
        }
        api.entity_post_GET({
          entity: reply.entity || (postData.original_post ? postData.original_post.post_content.entity : postData.entity),
          post_id: reply.post,
          callback: {
            onload: entity_post_GET_onload,
            onerror: entity_post_GET_onerror
          }
        });
        break;
      }
    }
  }
  if (replyRows.length && !hasReply) {
    lastRow = replyRows[replyRows.length - 1];
    lastRow.applyProperties({
      backgroundImage: replyRows.length === 1 ? 'images/row_bg_full.png' : 'images/row_bg_bottom.png',
      backgroundTopCap: 8,
      backgroundLeftCap: 20
    });
    return tableView.appendRow(lastRow);
  }
};

createView = function(eventData, viewOptions) {
  var Social, hasReplyAll, isActivityViewSupported, isPublic, isRepost, postActionsButtons, postData, replyRows, replySection, rowIndex, self, tableView, tableView_clicking, thisPostActions, thisPostRow, thisPostSection, windowClosed;
  Social = require('dk.napp.social');
  isActivityViewSupported = Social.isActivityViewSupported();
  rowIndex = eventData.index;
  postData = eventData.rowData.postData;
  hasReplyAll = postData.mentions ? postData.mentions.length > 1 || (postData.mentions.length === 1 && postData.mentions[0].entity !== GLOBAL.entity) : false;
  replySection = null;
  replyRows = [];
  windowClosed = false;
  isRepost = postData.type === api.postType('Repost');
  isPublic = postData.permissions && postData.permissions['public'];
  postActionsButtons = [
    ui.button({
      properties: {
        backgroundImage: 'images/post_reply_icon.png',
        left: 0,
        width: 40,
        height: 40
      },
      events: [
        [
          'click', function(e) {
            return (new NewPost({
              originalPost: postData,
              reply: true
            })).open({
              modal: true
            });
          }
        ]
      ]
    }), ui.button({
      properties: {
        enabled: hasReplyAll,
        backgroundImage: 'images/post_replyall_icon.png',
        left: isActivityViewSupported ? 15 : 19,
        width: 40,
        height: 40
      },
      events: [
        [
          'click', function(e) {
            return (new NewPost({
              originalPost: postData,
              reply: true,
              replyAll: true
            })).open({
              modal: true
            });
          }
        ]
      ]
    }), ui.button({
      properties: {
        backgroundImage: favoritePosts.isFavorite(postData.id) ? 'images/post_fav_active_icon.png' : 'images/post_fav_icon.png',
        left: isActivityViewSupported ? 15 : 19,
        width: 40,
        height: 40
      },
      events: [
        [
          'click', function(e) {
            if (favoritePosts.isFavorite(postData.id)) {
              favoritePosts.removeFavorite(postData.id);
              return e.source.backgroundImage = 'images/post_fav_icon.png';
            } else {
              favoritePosts.addFavorite(postData);
              return e.source.backgroundImage = 'images/post_fav_active_icon.png';
            }
          }
        ]
      ]
    }), (function() {
      if (postData.entity === GLOBAL.entity) {
        return ui.button({
          properties: {
            backgroundImage: 'images/post_delete_icon.png',
            left: isActivityViewSupported ? 15 : 19,
            width: 40,
            height: 40
          },
          events: [
            [
              'click', function(e) {
                return ui.alert({
                  properties: {
                    cancel: 1,
                    buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                    message: L('delete_the_post', 'Delete the post') + '?',
                    title: L('confirm', 'Confirm')
                  },
                  events: [
                    [
                      'click', function(e) {
                        if (e.index !== e.cancel) {
                          ui.showIndicator();
                          api.posts_DELETE({
                            id: postData.id,
                            eventData: {
                              rowIndex: rowIndex
                            }
                          });
                          return self.close();
                        }
                      }
                    ]
                  ]
                }).show();
              }
            ]
          ]
        });
      } else {
        return ui.button({
          properties: {
            backgroundImage: 'images/post_repost_icon.png',
            left: isActivityViewSupported ? 15 : 19,
            width: 40,
            height: 40
          },
          events: [
            [
              'click', function(e) {
                return ui.alert({
                  properties: {
                    cancel: 1,
                    buttonNames: [L('yes', 'Yes'), L('no', 'No')],
                    message: L('repost', 'Repost') + ' ' + (new URI(isRepost ? postData.original_post.post_content.entity : postData.entity)).host()(+'?'),
                    title: L('confirm', 'Confirm')
                  },
                  events: [
                    [
                      'click', function(e) {
                        if (e.index !== e.cancel) {
                          ui.showIndicator();
                          return api.posts_POST({
                            "permissions": {
                              "public": true
                            },
                            "type": api.postType('Repost'),
                            "content": {
                              "entity": isRepost ? postData.original_post.post_content.entity : postData.entity,
                              "id": isRepost ? postData.original_post.post_content.id : postData.id
                            },
                            "mentions": [
                              {
                                "entity": isRepost ? postData.original_post.post_content.entity : postData.entity,
                                "post": isRepost ? postData.original_post.post_content.id : postData.id
                              }
                            ]
                          });
                        }
                      }
                    ]
                  ]
                }).show();
              }
            ]
          ]
        });
      }
    })()
  ];
  Ti.API.debug("isActivityViewSupported: " + isActivityViewSupported);
  if (isActivityViewSupported) {
    postActionsButtons.push(ui.button({
      properties: {
        backgroundImage: 'images/post_more_icon.png',
        left: 15,
        width: 40,
        height: 40
      },
      events: [
        [
          'click', function(e) {
            return Social.activityView({
              text: isRepost ? postData.original_post.post_content.content.text : postData.content.text,
              removeIcons: "print,contact,camera"
            });
          }
        ]
      ]
    }));
  }
  self = ui.win({
    config: 'windowProperties',
    properties: {
      title: 'Post',
      rightNavButton: ui.button({
        config: 'newPostButton',
        events: [
          [
            'click', function(e) {
              e.source.enabled = false;
              return (new NewPost(null, null, {
                button: e.source
              })).open({
                modal: true
              });
            }
          ]
        ]
      }),
      leftNavButton: (function() {
        if (viewOptions && viewOptions.backButton) {
          return ui.button({
            config: 'backButton',
            events: [
              [
                'click', function(e) {
                  var er;
                  try {
                    GLOBAL.tabGroup.activeTab.close(self, {
                      animated: true
                    });
                    return windowClosed = true;
                  } catch (_error) {
                    er = _error;
                    return Ti.API.error(er);
                  }
                }
              ]
            ]
          });
        }
      })()
    },
    events: [
      [
        'open', function(e) {
          return generatorReplyRows(postData, tableView, replySection, replyRows, windowClosed);
        }
      ], [
        'close', function(e) {
          var thisPostActions, thisPostRow, thisPostSection;
          postActionsButtons.length = replyRows.length = 0;
          tableView.setData();
          return thisPostRow = thisPostActions = thisPostSection = null;
        }
      ]
    ]
  });
  thisPostRow = new _f.createRowDetailsFromPost(postData, {
    backgroundImage: 'images/row_bg_top.png',
    backgroundTopCap: 8,
    backgroundLeftCap: 20,
    autoLink: true
  });
  thisPostActions = ui.row({
    config: 'postActions',
    content: [
      ui.view({
        properties: {
          left: 2,
          right: 2,
          height: 39,
          top: 0,
          backgroundImage: 'images/post_bottom_bg.png',
          backgroundTopCap: 8,
          backgroundLeftCap: 20
        },
        content: [
          ui.view({
            properties: {
              width: isActivityViewSupported ? 260 : 220,
              height: 39,
              top: 0,
              layout: 'horizontal'
            },
            content: postActionsButtons
          })
        ]
      })
    ]
  });
  tableView_clicking = false;
  tableView = ui.table({
    config: 'tableView',
    events: [
      [
        'longpress', function(e) {
          var menu;
          if (e.index >= 2) {
            menu = _f.createPostQuickMenu(e);
            return menu.open();
          }
        }
      ], [
        'singletap', function(e) {
          var PostDetails, myEntity;
          Ti.API.debug('Row click event fired.');
          if (e.index >= 2) {
            rowIndex = e.index;
            postData = e.rowData.postData;
            myEntity = GLOBAL.entity;
            if (!tableView_clicking) {
              tableView_clicking = true;
              PostDetails = require('views/postDetails');
              GLOBAL.tabGroup.activeTab.open(new PostDetails(e, {
                backButton: self.title
              }));
              return setTimeout(function() {
                return tableView_clicking = false;
              }, 1000);
            }
          }
        }
      ]
    ]
  });
  thisPostSection = ui.section({
    properties: {
      headerView: Ti.UI.createView({
        height: 9
      }),
      footerView: Ti.UI.createView({
        height: 5
      })
    },
    content: [thisPostRow, thisPostActions]
  });
  tableView.setData([thisPostSection]);
  self.add(tableView);
  return self;
};

module.exports = createView;
