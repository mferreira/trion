var ui = require('lib/ui_sugar');

var
globalAlertCounter = null,
menus = {
	current: null,
	alertCounter: 0,
	stream: {
		badgeLabel: ui.label({visible: false, text: '' , right: 15, width: Ti.UI.SIZE, height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: { fontSize: 13 }}),
		alertCounter: 0
	},
	mentions: {
		badgeLabel: ui.label({visible: false, text: '' , right: 15, width: Ti.UI.SIZE, height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: { fontSize: 13 }}),
		alertCounter: 0
	},
	me: {
		badgeLabel: ui.label({visible: false, text: '' , right: 15, width: Ti.UI.SIZE, height: Ti.UI.SIZE, textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER, font: { fontSize: 13 }}),
		alertCounter: 0
	}
};

var appMenu = ui.win({
	properties: {
		navBarHidden: true,
		backgroundColor: '#e1e4e9',
		modal: false,
		left: 0,
		width: 200,
		zIndex: 1
	},
	content: [
		ui.table({
			properties: {
				top: 44,
				scrollsToTop: false,
				backgroundColor: '#e1e4e9',
				separatorStyle:Titanium.UI.iPhone.TableViewSeparatorStyle.NONE,
				// separatorColor: '#c5c6ca',
				// style: Ti.UI.iPhone.TableViewStyle.GROUPED,
				data: [
					ui.row({
						config: 'sideMenuRow',
						content: [
							// ui.label({text: '$', left: 1, height: 12, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#777', font: { fontSize: 12, fontFamily: 'Tentr-symbols' } }),
							ui.label({ config: 'sideMenuLabel', properties: {text: 'Stream'} }),
							menus.stream.badgeLabel
						],
						events: [['click', function(){
							menus.current = 'stream';
							Ti.App.fireEvent('appmenu:stream');
							menus.stream.alertCounter = 0;
							updateGlobalAlertCounterNumber();
							menus.stream.badgeLabel.visible = false;
						} ]]
					}),
					ui.row({
						config: 'sideMenuRow',
						content: [
							// ui.label({text: '$', left: 1, height: 12, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#777', font: { fontSize: 12, fontFamily: 'Tentr-symbols' } }),
							ui.label({ config: 'sideMenuLabel', properties: {text: 'Mentions'} }),
							menus.mentions.badgeLabel
						],
						events: [['click', function(){
							menus.current = 'mentions';
							Ti.App.fireEvent('appmenu:mentions');
							menus.mentions.alertCounter = 0;
							updateGlobalAlertCounterNumber();
							menus.mentions.badgeLabel.visible = false;
						} ]]
					}),
					ui.row({
						config: 'sideMenuRow',
						content: [
							ui.label({ config: 'sideMenuLabel', properties: {text: 'Favorite Posts'} })
						],
						events: [['click', function(){
							menus.current = 'favoritePosts';
							Ti.App.fireEvent('appmenu:favoritePosts');
						} ]]
					}),
					ui.row({
						config: 'sideMenuRow',
						content: [
							ui.label({ config: 'sideMenuLabel', properties: {text: 'Drafts'} })
						],
						events: [['click', function(){
							menus.current = 'drafts';
							Ti.App.fireEvent('appmenu:drafts');
						} ]]
					}),

					ui.row({config: 'sideMenuSeparatorRow'}),

					ui.row({
						config: 'sideMenuRow',
						content: [
							// ui.label({text: '$', left: 1, height: 12, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#777', font: { fontSize: 12, fontFamily: 'Tentr-symbols' } }),
							ui.label({ config: 'sideMenuLabel', properties: {text: 'Me'} }),
							menus.me.badgeLabel
						],
						events: [['click', function(){
							menus.current = 'me';
							Ti.App.fireEvent('appmenu:me');
						} ]]
					}),
					ui.row({
						config: 'sideMenuRow',
						content: [
							// ui.label({text: '$', left: 1, height: 12, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#777', font: { fontSize: 12, fontFamily: 'Tentr-symbols' } }),
							ui.label({ config: 'sideMenuLabel', properties: {text: 'Settings'} }),
							menus.me.badgeLabel
						],
						events: [['click', function(){
							menus.current = 'me';
							Ti.App.fireEvent('appmenu:me');
						} ]]
					})
				]
			}
		})
	]
});

Ti.App.addEventListener('notify:newPosts', function(e){
	if (menus.current !== 'stream') {
		// add the badge number to the menu
		// e.postsLength
		if (e.postsLength){
			menus.stream.alertCounter = e.postsLength;
			menus.stream.badgeLabel.updateLayout({
				text: e.postsLength,
				width: Ti.UI.SIZE
			});
			menus.stream.badgeLabel.show();
		}

		updateGlobalAlertCounterNumber();
	}
});

Ti.App.addEventListener('notify:newMentions', function(e){
	if (menus.current !== 'mentions') {
		// add the badge number to the menu
		// e.postsLength
		if (e.postsLength){
			menus.mentions.alertCounter = e.postsLength;
			menus.mentions.badgeLabel.updateLayout({
				text: e.postsLength,
				width: Ti.UI.SIZE
			});
			menus.mentions.badgeLabel.show();
		}

		updateGlobalAlertCounterNumber();
	}
});

exports.getView = function() { return appMenu; };
exports.setGlobalAlertCounter = function(counter) {
	globalAlertCounter = counter;
};

function updateGlobalAlertCounterNumber() {
	Ti.API.debug('Updating global alert counter...');
	var num = menus.stream.alertCounter + menus.mentions.alertCounter;
	if (num && num > 0) {
		globalAlertCounter.updateLayout({
			text: num,
			width: Ti.UI.SIZE
		});
		globalAlertCounter.width = globalAlertCounter.rect.width +4;
		globalAlertCounter.show();
	}
	else globalAlertCounter.hide();
}

// Stream
// Mentions
// Favorites
// Drafts
// Settings
//  - Update frequency: Never, rarely, ...
//  - Notification
//   * Mentions
//   * Reposts
//   * New Followers
//   * Private Post
//  - About