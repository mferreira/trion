var api, apn, createView, settings, ui, _f;

ui = require('lib/ui_sugar');

api = require('lib/tent.api');

_f = require('views/func_repo');

apn = require('lib/apn');

settings = Ti.App.Properties.hasProperty('settings') ? JSON.parse(Ti.App.Properties.getString('settings')) : {};

createView = function(viewOptions) {
  var ignore_clicks, profile, section1, section2, section3, self, tableData, tableView;
  if (typeof profile === 'string') {
    profile = api.getCache('discover')[profile];
  }
  ignore_clicks = false;
  self = ui.win({
    config: 'windowProperties',
    properties: {
      title: 'Settings'
    }
  });
  if (viewOptions && viewOptions.backButton) {
    self.leftNavButton = ui.button({
      config: 'backButton',
      events: [
        [
          'click', function(e) {
            GLOBAL.tabGroup.activeTab.close(self, {
              animated: true
            });
          }
        ]
      ]
    });
  }
  tableView = ui.table({
    config: 'tableView'
  });
  section1 = _f.createTableSection({
    headerView: Ti.UI.createView({
      height: 9
    }),
    footerView: Ti.UI.createView({
      height: 5
    })
  }, [
    {
      config: 'rowWithoutChild',
      content: [
        ui.label({
          config: 'defaultRowLabel',
          properties: {
            text: 'Clear cache'
          }
        })
      ],
      events: [
        [
          'click', function(e) {
            ui.alert({
              properties: {
                cancel: 1,
                buttonNames: [L('ok', 'Ok'), L('cancel', 'Cancel')],
                message: L('clear_cache_warning', "Some user data might be unavailable until " + Ti.App.name + "\ndownloads it again") + '.',
                title: L('confirm', 'Confirm')
              },
              events: [
                [
                  'click', function(e) {
                    if (e.index !== e.cancel) {
                      api.clearDiscoveryCache();
                      Ti.App.Properties.removeProperty('myStream_cache');
                      Ti.App.Properties.removeProperty('myMentions_cache');
                      Ti.App.Properties.removeProperty('settings');
                      Ti.App.Properties.removeProperty('bonds:following');
                      Ti.App.Properties.removeProperty('bonds:following_ext');
                      Ti.App.Properties.removeProperty('bonds:followers');
                    }
                  }
                ]
              ]
            }).show();
          }
        ]
      ]
    }
  ]);
  section2 = _f.createTableSection({
    headerView: Ti.UI.createView({
      height: 5
    }),
    footerView: Ti.UI.createView({
      height: 5
    })
  }, [
    {
      config: 'rowWithChild',
      content: [
        ui.label({
          config: 'defaultRowLabel',
          properties: {
            text: 'Update Frequency'
          }
        }), Ti.UI.createImageView({
          image: 'images/arrow_child.png',
          right: 0,
          width: 11,
          height: 11
        })
      ],
      events: [
        [
          'click', function(e) {
            var UpdateFreq, updateFreqWin;
            UpdateFreq = require('views/settings_updateFreq');
            updateFreqWin = UpdateFreq(settings.updateFreq || {
              frequently: true,
              rarely: false,
              never: false
            }, {
              onclose: function(updateFreq) {
                Ti.API.debug("settings.updateFreq: " + (JSON.stringify(updateFreq)));
                settings.updateFreq = updateFreq;
                settings.updateFreq.minutes_stream = updateFreq.frequently ? 1.7 : updateFreq.rarely ? 10.7 : 0;
                settings.updateFreq.minutes_mentions = updateFreq.frequently ? 1.2 : updateFreq.rarely ? 10.2 : 0;
                Ti.App.Properties.setString('settings', JSON.stringify(settings));
                GLOBAL.appConfig.bgCheck.posts_timeout = settings.updateFreq.minutes_stream * 60000;
                GLOBAL.appConfig.bgCheck.mentions_timeout = settings.updateFreq.minutes_mentions * 60000;
                Ti.App.fireEvent('startBgJob');
              }
            });
            GLOBAL.tabGroup.activeTab.open(updateFreqWin);
          }
        ]
      ]
    }
  ]);
  section3 = _f.createTableSection({
    headerView: Ti.UI.createView({
      height: 5
    }),
    footerView: Ti.UI.createView({
      height: 5
    })
  }, [
    {
      config: 'rowWithoutChild',
      content: [
        ui.label({
          config: 'defaultRowLabel',
          properties: {
            text: 'Check APN state'
          }
        })
      ],
      events: [
        [
          'click', function(e) {
            var apn_state;
            apn_state = require('views/apn_state').createView();
            GLOBAL.tabGroup.activeTab.open(apn_state);
          }
        ]
      ]
    }
  ]);
  tableData = [section1, section2, section3];
  tableView.setData(tableData);
  self.add(tableView);
  return self;
};

module.exports = createView;
