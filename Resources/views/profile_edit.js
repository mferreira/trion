var NewPost, ProfileEdit, URI, api, ui, _f;

ui = require('lib/ui_sugar');

api = require('lib/tent.api2');

URI = require('lib/vendor/jsuri.min');

_f = require('views/func_repo');

NewPost = require('views/newPost2');

module.exports = ProfileEdit = (function() {
  function ProfileEdit(profile, viewOptions) {
    var bioEntry, birthdateEntry, genderEntry, locationEntry, nameEntry, profileHeader, saveProfile, tableData, tableView, urlEntry;
    this.profile = profile;
    this.viewOptions = viewOptions;
    if (typeof this.profile === 'string') {
      this.profile = api.getCache('discover')[this.profile];
    }
    this.self = ui.win({
      config: 'windowProperties',
      properties: {
        title: 'Edit Profile',
        rightNavButton: ui.button({
          config: 'newPostButton',
          events: [
            [
              'click', function() {
                e.source.enabled = false;
                return (new NewPost(null["null"], {
                  button: e.source
                })).open({
                  modal: true
                });
              }
            ]
          ]
        }),
        leftNavButton: ((function(_this) {
          return function() {
            if (_this.viewOptions && _this.viewOptions.backButton) {
              return ui.button({
                config: 'backButton',
                properties: {
                  backgroundImage: 'images/back_profile.png',
                  width: 64
                },
                events: [
                  [
                    'click', function(e) {
                      saveProfile('basic');
                      tableView.setData();
                      return GLOBAL.tabGroup.activeTab.close(_this.self, {
                        animated: true
                      });
                    }
                  ]
                ]
              });
            }
          };
        })(this))(),
        layout: 'vertical'
      }
    });
    profileHeader = ui.view({
      properties: {
        width: Ti.UI.FILL,
        height: 66,
        backgroundImage: 'images/profile_top_bg.png'
      },
      content: [
        _f.createAvatar(this.profile.avatar || "images/avatar.png", {
          width: 48,
          height: 48,
          left: 10
        }), ui.label({
          config: 'defaultLabel',
          properties: {
            left: 66,
            top: 14,
            text: this.profile.name,
            font: {
              fontSize: 16,
              fontWeight: 'bold'
            },
            color: '#fff',
            shadowColor: '#000',
            shadowOffset: {
              x: 0,
              y: 1
            }
          }
        }), ui.label({
          config: 'defaultLabel',
          properties: {
            left: 66,
            top: 34,
            text: (new URI(this.profile.entity)).host(),
            font: {
              fontSize: 14
            },
            color: '#d4d5d7',
            shadowColor: '#000',
            shadowOffset: {
              x: 0,
              y: 1
            }
          }
        })
      ]
    });
    tableView = ui.table({
      config: 'tableView'
    });
    nameEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        hintText: 'Type Your Name',
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        value: this.profile.name
      }
    });
    urlEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        hintText: 'URL',
        value: this.profile.website,
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE,
        autocorrect: false,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        keyboardType: Ti.UI.KEYBOARD_URL
      }
    });
    birthdateEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        hintText: 'Not set',
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        value: this.profile.birthdate
      }
    });
    genderEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        hintText: 'Not set',
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        value: this.profile.gender
      }
    });
    locationEntry = ui.textField({
      config: 'profileTextField',
      properties: {
        hintText: 'Your Location',
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        value: this.profile.location
      }
    });
    bioEntry = ui.textArea({
      properties: {
        top: 4,
        bottom: 4,
        left: 5,
        right: 5,
        value: this.profile.bio,
        returnKeyType: Ti.UI.RETURNKEY_DONE,
        autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_SENTENCES,
        color: '#45474c',
        font: {
          fontSize: 13,
          fontFamily: 'Helvetica'
        }
      }
    });
    tableData = [
      _f.createTableSection({
        headerView: Ti.UI.createView({
          height: 7
        }),
        footerView: Ti.UI.createView({
          height: 2
        })
      }, [
        {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Name'
              }
            }), nameEntry
          ],
          events: [
            [
              'click', function(e) {
                return nameEntry.focus();
              }
            ]
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Gender'
              }
            }), genderEntry
          ],
          events: [
            [
              'click', function(e) {
                return genderEntry.focus();
              }
            ]
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Birthdate'
              }
            }), birthdateEntry
          ],
          events: [
            [
              'click', function(e) {
                return birthdateEntry.focus();
              }
            ]
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'URL'
              }
            }), urlEntry
          ],
          events: [
            [
              'click', function(e) {
                return urlEntry.focus();
              }
            ]
          ]
        }, {
          config: 'profileEditableRow',
          content: [
            ui.label({
              config: 'defaultRowLabel',
              properties: {
                text: 'Location'
              }
            }), locationEntry
          ],
          events: [
            [
              'click', function(e) {
                return locationEntry.focus();
              }
            ]
          ]
        }
      ]), ui.section({
        properties: {
          headerView: Ti.UI.createView({
            height: 3
          }),
          footerView: Ti.UI.createView({
            height: 5
          })
        },
        content: [
          ui.row({
            properties: {
              height: 104,
              backgroundImage: 'images/row_bg_full.png',
              backgroundTopCap: 8,
              backgroundLeftCap: 20,
              selectionStyle: Ti.UI.iPhone.TableViewCellSelectionStyle.NONE
            },
            content: [bioEntry],
            events: [
              [
                'click', function(e) {
                  return nameEntry.focus();
                }
              ]
            ]
          })
        ]
      })
    ];
    tableView.setData(tableData);
    this.self.add(tableView);
    saveProfile = (function(_this) {
      return function(type) {
        return api.profile_PUT({
          type: type,
          payload: {
            name: nameEntry.value,
            avatar: _this.profile.avatar,
            birthdate: birthdateEntry.value,
            location: locationEntry.value,
            gender: genderEntry.value,
            bio: bioEntry.value,
            website: urlEntry.value
          }
        });
      };
    })(this);
  }

  ProfileEdit.prototype.getView = function() {
    return this.self;
  };

  return ProfileEdit;

})();
