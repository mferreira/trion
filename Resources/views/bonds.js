var ui = require('lib/ui_sugar'),
	api = require('lib/tent.api2'),
	URI = require('lib/vendor/jsuri.min'),
	NewPost = require('views/newPost2');


var lists = {
	'Following': {
		apiCall: 'followings_GET',
		options: ['Unfollow', 'Message', 'Cancel'],
		destructive: 0
	},
	'Followers': {
		apiCall: 'followers_GET',
		options: ['Follow', 'Message', 'Cancel'],
		destructive: -1
	}
},

lastFollow_id = null,

createRow = function(follow, row_config, listType){
	// Ti.API.info('Generating row for: '+JSON.stringify(follow));
	try {

	var
	profile_basic = follow.profile? follow.profile[api.getUri('basic')] : null,
	// isFollowing = api.isFollowing(follow.entity),

	// follow_button = ui.button({
	// 	config: isFollowing ? 'followingButton' : 'followButton',
	// 	properties: {
	// 		right: 0,
	// 		isFollowing: isFollowing,
	// 		isFollowButton: true,
	// 		follow: follow,
	// 		actInd: Ti.UI.createActivityIndicator({
	// 			color: '#2f3238',
	// 			style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
	// 			height:Ti.UI.SIZE,
	// 			width:Ti.UI.SIZE,
	// 			right: 5
	// 		})
	// 	}
	// }),

	entityAvatar = ui.image({
		properties: {
			defaultImage: 'images/avatar.png',
			image: profile_basic? profile_basic.avatar_url : 'images/avatar.png',
			width: 30, height: 30, left: 0, borderRadius: 3
		}
	}),
	entityName = ui.label({
		text: profile_basic ? profile_basic.name : follow.profile ? 'No Profile' : 'Loading...',
		top: 0, left: 40, right: 10, height: 17, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
		color: profile_basic ? '#2f3238' : '#9a9faa', font: { fontSize: 14, fontWeight: 'bold' } }),
	entityURI = ui.label({text: (new URI(follow.entity)).host(), bottom: 0, left: 40, right: 10, height: 12, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#585b60', font: { fontSize: 12 } });

	// check if we have the entity profile
	if (!follow.profile) {
		api.discover(follow.entity, {
			onload: function(profileData, views){
				fillEntityInfo( profileData, views );
			},
			onerror: function(resp) {
				Ti.API.warn('Failed entity profile discovery. Retrying...');
				api.discover(follow.entity, {
					onload: function(profileData, views){
						fillEntityInfo( profileData, views );
					},
					onerror: function(resp, views) {
						views.entityName.applyProperties({
							text: 'Failed discovery',
							color: '#9a9faa'
						});
						Ti.API.warn('Failed entity profile discovery. Giving up.');
					}
				});
			},
			data: {entityAvatar: entityAvatar, entityName: entityName}
		});
	}

	var rowContainer = ui.view({
		properties: { height: 30, left: 12, width: GLOBAL.isTablet ? 568 : 276, top: 10 },
		content: [
			// entity avatar
			entityAvatar,
			// entity real name
			entityName,
			// entity uri
			entityURI
			// follow button
			// follow_button,
			// follow_button.actInd
		]
	}),
	row = ui.row({
		config: row_config || 'rowWithoutChild',
		properties: {
			className: 'entityRow', height: (row_config === 'rowWithoutChild_bottom') ? 54 : 50, follow: follow
		},
		content: [rowContainer]
	});

	function addFollowButton(follow, isFollowing) {
		var follow_button = ui.button({
			config: isFollowing ? 'followingButton' : 'followButton',
			properties: {
				right: 0,
				isFollowing: isFollowing,
				isFollowButton: true,
				follow: follow,
				actInd: Ti.UI.createActivityIndicator({
					color: '#2f3238',
					style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
					height:Ti.UI.SIZE,
					width:Ti.UI.SIZE,
					right: 5
				})
			}
		});
		rowContainer.add(follow_button);
		rowContainer.add(follow_button.actInd);
	}

	if (follow.entity !== GLOBAL.entity) {
		if (listType === 'Followers') {
			api.isFollowing(follow.entity, Ti.Network.online, {
				onload: function(isFollowing) {
					addFollowButton(follow, isFollowing);
				}
			});
		}
		else {
			var isFollowing = api.isFollowing(follow.entity);
			addFollowButton(follow, isFollowing);
		}
	}

	return row;

	} catch (er) {Ti.API.error(er);}
};

function fillEntityInfo (profile, views){
	Ti.API.debug('Executing fillEntityInfo() with: '+JSON.stringify(profile));
	if (profile.basic) {
		if (profile.basic.avatar_url) views.entityAvatar.setImage( profile.basic.avatar_url );
		if (profile.basic.name) views.entityName.applyProperties({
			text: profile.basic.name,
			color: '#2f3238'
		});
	}
	else {
		views.entityName.applyProperties({
			text: 'No Profile',
			color: '#9a9faa'
		});
	}
}


function createView (listType, profile, viewOptions) {

	listType = (listType.toLowerCase() === 'followers') ? 'Followers' : 'Following';

	if (typeof profile === 'string') profile = api.getCache('discover')[profile];

	// contains all XHR id's
	var openCons = [];

	function addMoreBonds (args) {
		Ti.API.debug('section.rowCount -> '+section.rowCount);
		updatingOlder = true;
		// args: entity, before_id
		api[lists[listType].apiCall]({
			onload: function(list){
				var list_length = list.length;
				var lastSegment = (section.rowCount+list_length === maxBonds),
					section_rowCount = section.rowCount,
					newRows = [], last_index = list_length-1;

				for (var i=0, j=list_length; i<j; i++){
					if (selfClosed) break;
					var follow = list[i];
					Ti.API.debug('Inserting entity "'+follow.entity+'" at index '+(section_rowCount-1));
					newRows.push( createRow(follow, (lastSegment && i===last_index) ? 'rowWithoutChild_bottom' : null, listType) );
					section_rowCount++;
				}
				if (lastSegment) {
					// newRows[newRows.length-1].applyProperties({ backgroundImage: 'images/row_bg_bottom.png', backgroundTopCap: 8, backgroundLeftCap: 20 });
					// var bottomRow = createRow(list[list_length-1], 'rowWithoutChild_bottom');
					// tableView.deleteRow(section.rowCount-1);
					// tableView.appendRow( bottomRow );
				}
				tableView.appendRow(newRows);
				// lastBondId = list[list_length-1].id;
				updatingOlder = false;
			},
			onerror: function(e){
				ui.alert({
					properties: {
						title: 'Server Error',
						message: JSON.stringify(e)
					}
				}).show();
			},
			onid: function(id){ openCons.push(id); }
		}, args.entity, {before_id: args.before_id, limit: 10});
	}

	var
	// lastBondId = null,
	maxBonds = parseInt(viewOptions.maxBonds, 10),
	lastRow = ui.row({ config: 'rowWithoutChild_bottom' }),
	updatingOlder = false,
	tableOffset = 0,
	lastDistance = 0,
	selfClosed = false,

	self = ui.win({
		config: 'windowProperties',
		properties: {
			title: listType,
			rightNavButton: ui.button({
				config: 'newPostButton',
				events: [ ['click', function(e){
					e.source.enabled = false;
					(new NewPost(null, null, {button: e.source})).open({modal: true});
				}] ]
			}),
			leftNavButton: (function(){
				if (viewOptions && viewOptions.backButton) {
					return ui.button({
						config: 'backButton',
						properties: {backgroundImage: 'images/back_profile.png', width: 64},
						events: [['click', function(e){
							Ti.API.debug('GLOBAL.cur_tab -> '+JSON.stringify(GLOBAL.cur_tab));
							Ti.API.debug('GLOBAL.tabGroup.activeTab.children -> '+JSON.stringify(GLOBAL.tabGroup.activeTab.children));
							GLOBAL.tabGroup.activeTab.close(self, {animated: true});
						}]]
					});
				}
			})(),
			layout: 'vertical'
		},
		events: [
			['open', function(e){
				// load follow list into the tableview
				api[lists[listType].apiCall]({
					onload: function(list){
						var list_length = list.length;
						var onlyOneBond = (list_length === 1);
						for (var i=0, j=list_length; i<j; i++){
							var follow = list[i];
							section.add(
								createRow(follow,
									onlyOneBond ? 'rowWithoutChild_full' :
										!i ? 'rowWithoutChild_top' : (i===maxBonds-1) ? 'rowWithoutChild_bottom' : 'rowWithoutChild',
									listType)
							);
						}
						// if (!onlyOneBond) {
						// 	lastRow.add(actIndicatorView);
						// 	tableView.appendRow(lastRow);
						// 	actIndicator.show();
						// }
						// lastBondId = list[list_length-1].id;
						tableView.setData( [section] );
					},
					onid: function(id){ openCons.push(id); }
				}, profile.entity);
			}],
			['close', function(e){
				selfClosed = true;
				// cancel all open connections
				api.cancelXHR(openCons);
				openCons.length = 0;
				self.remove(tableView);
				section = tableView = actIndicator = actIndicatorView = null;
			}]
		]
	}),

	section = ui.section({
		properties: { headerView: Ti.UI.createView({height:9}), footerView: Ti.UI.createView({height:1}) }
	}),
	tableView = ui.table({
		config: 'tableView',
		// properties: { rowHeight: 50 },
		events: [
			['click', function(e){
				// Ti.API.debug(JSON.stringify(e.source));

				var source = e.source,
					index = e.index,
					myEntity = api.getMyProfile().core.entity,
					follow;

				if (source.isFollowButton) {
					follow = source.follow;
					source.hide();
					source.actInd.show();
					if (!source.isFollowing) {
						api.followings_POST({
							entity: follow.entity
						}, {
							onload: function(newFollow){
								api.addFollowing(newFollow);
								source.applyProperties(ui.defaultConfig['followingButton']);
								source.actInd.hide();
								source.show();
							},
							onerror: function(e){
								if (e.status === 404){
									api.removeFollowing(follow.entity);
									source.applyProperties(ui.defaultConfig['followButton']);
									source.actInd.hide();
									source.show();
								}
								else if (e.status === 409) { // already following error
									api.followings_GET_ENTITY(follow.entity, {
										onload: function(followData) {
											api.addFollowing(followData);
											source.applyProperties(ui.defaultConfig['followingButton']);
											source.actInd.hide();
											source.show();
										},
										onerror: function(e) {
											source.applyProperties(ui.defaultConfig['followingButton']);
											source.actInd.hide();
											source.show();
										}
									});
								}
								else {
									source.actInd.hide();
									source.show();
								}
							}
						});
					}
					else {
						api.followings_DELETE({
							id: api.getFollowing(follow.entity).id
						}, {
							onload: function(e){
								api.removeFollowing(follow.entity);
								source.applyProperties(ui.defaultConfig['followButton']);
								source.actInd.hide();
								source.show();
							},
							onerror: function(e){
								if (e.status === 404) {
									api.followings_GET_ENTITY(follow.entity, {
										onload: function(followData) {
											api.followings_DELETE({
												id: followData.id
											}, {
												onload: function(e){
													source.applyProperties(ui.defaultConfig['followButton']);
													source.actInd.hide();
													source.show();
												},
												onerror: function(e){
													source.actInd.hide();
													source.show();
												}
											});
										}
									});
								}
								else {
									source.actInd.hide();
									source.show();
								}
							}
						});
					}
				}
				else {
					follow = e.rowData.follow;
					// Ti.API.debug('Row data: '+JSON.stringify(follow));
					if (follow.entity === GLOBAL.entity) GLOBAL.tabGroup.setActiveTab(2);
					else {
						var Profile = require('views/profile');
						GLOBAL.tabGroup.activeTab.open( new Profile(follow.entity, { backButton: self.title }) );
					}
				}
			}],
			['scroll',function(e){
				if (section.rowCount && section.rowCount < maxBonds) {
					tableOffset = e.contentOffset.y;
					var total = tableOffset + e.size.height,
						theEnd = e.contentSize.height;
					var distance = theEnd - total;
					if (distance < lastDistance) {
						// adjust the % of rows scrolled before we decide to start fetching
						var nearEnd = theEnd * 0.9;

						if (!updatingOlder && (total >= nearEnd)) {
							var lastBondId = section.rows[section.rowCount-1].follow.id;
							Ti.API.debug('User reached the end of the table. Grabbing more bonds.');
							Ti.API.warn('Section row count: '+section.rowCount);
							Ti.API.warn('Grabbing bonds before id: '+lastBondId);
							addMoreBonds({
								entity: profile.core.entity,
								before_id: lastBondId
							});
						}
					}
					lastDistance = distance;
				}
			}]
		]
	});

	var actIndicator = Ti.UI.createActivityIndicator({
		color: '#2f3238',
		font: {fontFamily:'Helvetica Neue', fontSize:15, fontWeight:'bold'},
		message: 'Loading...',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.DARK,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE
	}),
	actIndicatorView = ui.view({
		properties: { height:Ti.UI.SIZE, width:Ti.UI.SIZE },
		content: [actIndicator]
	});
	tableView.setData([
		ui.section({
			properties: { headerView: Ti.UI.createView({height:9}), footerView: Ti.UI.createView({height:1}) },
			content: [ui.row({
				config: 'rowWithoutChild_full',
				content: [actIndicatorView]
			})]
		})
	]);
	actIndicator.show();

	self.add(tableView);

	return self;

}

module.exports = createView;
