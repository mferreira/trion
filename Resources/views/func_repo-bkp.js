var ui = require('lib/ui_sugar'),
	GLOBAL = require('globals'),
	api = require('lib/tent.api2'),
	uri = require('lib/vendor/jsuri.min'),
	moment = require('lib/vendor/moment.min'),
	_ = require('lib/vendor/underscore-min'),
	StyledLabel = require('ti.styledlabel');


function createRowFromPost (post, viewProperties){
	// Ti.API.debug('Creating row from post data: '+JSON.stringify(post));

	function fillEntityInfo (profile){
		// Ti.API.debug('post.entity: ', post.entity);
		// Ti.API.debug('profile: ', JSON.stringify(profile));

		if (profile.basic) {
			if (profile.basic.avatar_url) entityAvatar.setImage( profile.basic.avatar_url );
			if (profile.basic.name.length) entityName.setText( profile.basic.name );
		}
		// else {
		// 	entityName.setText( (new uri(post.entity)).host() );
		// }
		// entityLabel.setText( (new URI(profile.core.entity)).host() );
		// bio.setText( profile.basic.bio );
		// location.setText( profile.basic.location || 'Unknown' );
	}

	var
	isBottom = (viewProperties && (viewProperties.backgroundImage === 'images/row_bg_bottom.png' || viewProperties.backgroundImage === 'images/row_bg_full.png')),
	isRepost = (post.type === api.postType('Repost')),
	hasLocation = !!post.content.location,
	postEntity = isRepost ? post.original_post.profile.core.entity : post.entity,

	entityProfile = api.getProfile(post.entity), repostEntityProfile = null,

	wrapView = ui.view({
		properties: {touchEnabled: true, top: 11, bottom: 12, width: GLOBAL.isTablet ? 510 : 210, left: 71 /**, right: 14**/}
	}),

	entityName = ui.label({ touchEnabled: false, text: (new uri(postEntity)).host(), color: '#2E3238',
		top: 0, left: 0, width: 225, height: 13, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: { fontSize: 13, fontWeight: 'bold' } }),
	entityAvatar = ui.image({
		properties: {bubbleParent: false, defaultImage: 'images/avatar.png', image: 'images/avatar.png',
			width: 46, height: 46, top: 13, left: 14, borderRadius: 3},
		events: [['click', function(e){
			if (postEntity === GLOBAL.entity) GLOBAL.tabGroup.setActiveTab(2);
			else {
				var Profile = require('views/profile'),
					profileView = new Profile(postEntity, { backButton: 'back' });

				if (!profileView) alert('Error discovering this entity.');
				else GLOBAL.tabGroup.activeTab.open( profileView );
			}
		}]]
	});


	var postTimestampLabel = ui.label({touchEnabled: false, text: moment(post.published_at).fromNow(), top: 0, right: 0, width: 90, height: 10, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, color: '#909AA2', font: { fontSize: 10 } });
	GLOBAL.timestampCache.push([postTimestampLabel, post.published_at]); // we'll use this to update every timestamp

	var postContent = post.content.text.replace(/\^\[(.+)\]\(\d+\)/gi, "$1"), // replace inline mention for display text
		postInfoView = ui.view({ properties: { touchEnabled: false, bottom: isBottom ? 4:0, height: 28, left: 2, right: 2, layout: 'horizontal',
			backgroundImage: isBottom ? 'images/postinfo_bottom_bg.png' : 'images/postinfo_bg.png' } }),
		rowConfig = 'genericPostRow', className='', hasDetails = true;

	if (hasLocation) {
		postInfoView.add( ui.image({
			properties: {widht: 8, height: 8, top: 11, left: (postInfoView.children.length? 3:69), image: 'images/postinfo_location.png', touchEnabled: false}
		}));
	}

	// TODO: implement diferent row layout for post types
	switch (post.type) {
		case api.postType('Status'):
			className = 'status';
			break;
		case api.postType('Repost'):
			className = 'repost';
			// title shows entity of the original post
			repostEntityProfile = post.original_post.profile;

			// add symbol and information about the repost
			postInfoView.add( ui.image({
				properties: {widht: 9, height: 8, top: 12, left: (postInfoView.children.length? 3:69), image: 'images/repost_icon.png', touchEnabled: false}
			})),
			postInfoView.add( ui.label({
				touchEnabled: false,
				text: 'Repost by '+((entityProfile !== null &&
									entityProfile.basic.name.length) ?
									entityProfile.basic.name : (new uri(post.entity)).host()),
				left: 5, height: 13, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x:0, y:1} }) );
			break;
		case api.postType('Follower'):
			className = 'follower';
			postContent = 'You have a new follower';
			hasDetails = false;
			break;
		default:
			break;
	}

	// check if we have the entity profile
	if (repostEntityProfile) {
		fillEntityInfo( repostEntityProfile );
	}
	// discover the profile if we dont know it yet
	else if (entityProfile) {
		fillEntityInfo( entityProfile );
	}
	else {
		api.discover(post.entity, {
			onload: function(profileData){
				fillEntityInfo( profileData );
			},
			onerror: function(resp) {
				Ti.API.warn('Failed entity profile discovery. Retrying...');
				api.discover(post.entity, {
					onload: function(profileData){
						fillEntityInfo( profileData );
					},
					onerror: function(resp) {
						Ti.API.warn('Failed entity profile discovery. Giving up.');
					}
				});
			}
		});
	}

	var postContentText = (viewProperties && viewProperties.autoLink) ?
		ui.textArea({
			properties: {
				value: postContent,
				color: '#2F3238', font:{fontFamily:'Helvetica',fontSize:13},
				top: 7, bottom: -1, left: -8, right: -9, height: Ti.UI.SIZE,
				textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
				autoLink: Ti.UI.AUTOLINK_ALL,
				editable: false, scrollable: false,
				backgroundColor: 'white',
				bubbleParent: true
			}
		}) :
		ui.label({
			touchEnabled: false,
			text: postContent,
			top: 16, bottom: 0, left: 0, right: 0, height: Ti.UI.SIZE,
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#2F3238', font: { fontSize: 13 }
		});

	var postContentView = ui.view({
		properties: { touchEnabled: true, top: 0, left: 0, right: 0 },
		content: [
			// post content
			postContentText,
			// entity name
			entityName,
			// time 'ago'
			postTimestampLabel
		]
	});
	wrapView.add(postContentView);



	if (post.permissions && !post.permissions['public']) {
		className += 'Private';
		// add symbol and information about privacy
		postInfoView.add( ui.label({
			touchEnabled: false,
			text: '$', left: (postInfoView.children.length? 3:69),
			height: 11, top: 10, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 10, fontFamily: 'Tentr-symbols' } }) );
		postInfoView.add( ui.label({
			touchEnabled: false,
			text: 'Private', left: 2, height: Ti.UI.SIZE, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x:0, y:1} }) );
	}

	var reply = null;
	// run through the mentions to check for any post - it's a reply
	if (post.type !== api.postType('Repost') && post.mentions) {
		for (var i=post.mentions.length; i--;) {
			if (post.mentions[i].post) {
				className += 'Reply';
				reply = post.mentions[i];
				var replyEntityProfile = api.getProfile(reply.entity);

				// its a reply - add visual info about it
				postInfoView.add( ui.image({
					properties: {widht: 9, height: 8, top: 11, left: (postInfoView.children.length? 3:69), image: 'images/reply_icon.png', touchEnabled: false}
				})),
				postInfoView.add( ui.label({
					touchEnabled: false,
					text: 'Reply to '+(replyEntityProfile && replyEntityProfile.basic? replyEntityProfile.basic.name: (new uri(reply.entity)).host()),
					left: 5, height: 13, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x:0, y:1} }) );

				break;
			}
		}
	}

	// hack to set the height to acomodate content
	postContentView.height = Ti.UI.SIZE;
	wrapView.height = Ti.UI.SIZE;

	var rowProperties = { className: className, postData: post, height: Ti.UI.SIZE, hasDetails: hasDetails };
	if (viewProperties) _.extend(rowProperties, viewProperties);

	var rowContent = [ entityAvatar, wrapView ];

	if (postInfoView.children.length) {
		postContentView.bottom = 30; // give some bottom space
		rowContent.push(postInfoView);
		entityAvatar.bottom = 42;
	}
	else {
		entityAvatar.bottom = isBottom ? 18 : 13;
	}

	var row = ui.row({
		config: rowConfig,
		properties: rowProperties,
		content: rowContent
	});

	return row;
}

function createRowDetailsFromPost (post, viewProperties){
	// Ti.API.debug('Creating row from post data: '+JSON.stringify(post));

	var fillEntityInfo = function(profile){
		// Ti.API.debug('post.entity: ', post.entity);
		Ti.API.debug('fillEntityInfo <- ', JSON.stringify(profile));

		if (profile.profile) {
			if (profile.profile.avatar_url) entityAvatar.setImage( profile.basic.avatar_url );
			if (profile.profile.name) entityName.setText( profile.basic.name );
		}
		entityURI.setText( (new uri(profile.entity)).host() );
	},

	isBottom = (viewProperties && (viewProperties.backgroundImage === 'images/row_bg_bottom.png' || viewProperties.backgroundImage === 'images/row_bg_full.png')),
	isRepost = (post.type === api.postType('Repost')),
	hasLocation = !!post.content.location,
	postEntity = isRepost ? post.original_post.profile.core.entity : post.entity,

	_entityURI = new uri(postEntity);

	entityProfile = api.getProfile(post.entity), repostEntityProfile = null,

	wrapView = ui.view({
		properties: {touchEnabled: true, top: 11, width: GLOBAL.isTablet ? 510 : 210, left: 71 /**, right: 14**/}
	}),

	entityName = ui.label({ touchEnabled: false, text: _entityURI.host(), color: '#2E3238',
		top: 7, left: 0, width: 225, height: 15, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: { fontSize: 15, fontWeight: 'bold' } }),
	entityURI = ui.label({ touchEnabled: false, text: '^'+_entityURI.host(), color: '#97989b',
		top: 29, left: 0, width: 225, height: 13, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, font: { fontSize: 13 } }),
	entityAvatar = ui.image({
		properties: {bubbleParent: false, defaultImage: 'images/avatar.png', image: 'images/avatar.png',
			width: 46, height: 46, top: 13, left: 14, borderRadius: 3},
		events: [['click', function(e){
			if (postEntity === GLOBAL.entity) GLOBAL.tabGroup.setActiveTab(2);
			else {
				var Profile = require('views/profile');
				GLOBAL.tabGroup.activeTab.open( new Profile(postEntity, { backButton: 'back' }) );
			}
		}]]
	});


	var postTimestampLabel = ui.label({touchEnabled: false, text: moment(post.published_at).fromNow(), top: 8, right: 0, width: 90, height: 10, textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT, color: '#909AA2', font: { fontSize: 10 } });
	GLOBAL.timestampCache.push([postTimestampLabel, post.published_at]); // we'll use this to update every timestamp

	var postContent = post.content.text,
		styledPostContent, postContentText,
		postInfoView = ui.view({ properties: { touchEnabled: false, bottom: isBottom ? 4:0, height: 28, left: 2, right: 2, layout: 'horizontal',
			backgroundImage: isBottom ? 'images/postinfo_bottom_bg.png' : 'images/postinfo_bg.png' } }),
		rowConfig = 'genericPostRow', className='', hasDetails = true;

	// TODO: implement diferent row layout for post types
	switch (post.type) {
		case api.postType('Status'):
			className = 'status';
			break;
		case api.postType('Repost'):
			className = 'repost';
			// title shows entity of the original post
			repostEntityProfile = post.original_post.profile;

			// add symbol and information about the repost
			postInfoView.add( ui.image({
				properties: {widht: 9, height: 8, top: 12, left: (postInfoView.children.length? 3:69), image: 'images/repost_icon.png', touchEnabled: false}
			})),
			postInfoView.add( ui.label({
				touchEnabled: false,
				text: 'Repost by '+((entityProfile !== null &&
									entityProfile.basic.name.length) ?
									entityProfile.basic.name : (new uri(post.entity)).host()),
				left: 5, height: 13, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x:0, y:1} }) );
			break;
		case api.postType('Follower'):
			className = 'follower';
			postContent = 'You have a new follower';
			hasDetails = false;
			break;
		default:
			break;
	}

	// check if we have the entity profile
	if (repostEntityProfile) {
		fillEntityInfo( repostEntityProfile );
	}
	// discover the profile if we dont know it yet
	else if (entityProfile) {
		fillEntityInfo( entityProfile );
	}
	else {
		api.discover(post.entity, {
			onload: function(profileData){
				fillEntityInfo( profileData );
			},
			onerror: function(resp) {
				Ti.API.warn('Failed entity profile discovery. Retrying...');
				api.discover(post.entity, {
					onload: function(profileData){
						fillEntityInfo( profileData );
					},
					onerror: function(resp) {
						Ti.API.warn('Failed entity profile discovery. Giving up.');
					}
				});
			}
		});
	}

	// should we style the label ?
	if (viewProperties && viewProperties.autoLink) {
		styledPostContent = StyledLabel.createLabel({
			top: 68, bottom: 13, left: 14, right: 14, height: Ti.UI.SIZE,
			html: post2HTML(post)
		});
		styledPostContent.addEventListener('click', function (e) {
			if (e.url) {
				// alert('You clicked ' + e.url);
				// Ti.API.debug('You clicked ' + e.url);
				var entity = e.url.replace(/\/+$/, '');
				if (entity === GLOBAL.entity) GLOBAL.tabGroup.setActiveTab(2);
				else {
					// make sure we have entity discover data in cache
					api.discover(entity, {
						thiz: this,
						onload: function(data) {
							var Profile = require('views/profile');
							GLOBAL.tabGroup.activeTab.open( new Profile(entity, { backButton: 'back' }) );
						},
						onerror: function(e){
							alert('Error retrieving profile data.');
						}
					});
				}
			}
		});
		postContentText = styledPostContent;
	}
	else {
		postContentText = ui.label({
			touchEnabled: false,
			text: postContent,
			top: 68, bottom: 13, left: 14, right: 14, height: Ti.UI.SIZE,
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#2F3238', font: { fontSize: 15 }
		});
	}

	var postContentView = ui.view({
		properties: { touchEnabled: true, top: 0, left: 0, right: 0 },
		content: [
			// entity name
			entityName,
			// entity uri
			entityURI,
			// time 'ago'
			postTimestampLabel
		]
	});
	wrapView.add(postContentView);


	if (post.permissions && !post.permissions['public']) {
		className += 'Private';
		// add symbol and information about privacy
		postInfoView.add( ui.label({
			touchEnabled: false,
			text: '$', left: (postInfoView.children.length? 3:69),
			height: 11, top: 10, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 10, fontFamily: 'Tentr-symbols' } }) );
		postInfoView.add( ui.label({
			touchEnabled: false,
			text: 'Private', left: 2, height: Ti.UI.SIZE, top: 9, textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, color: '#5E6165', font: { fontSize: 11 }, shadowColor: '#fff', shadowOffset: {x:0, y:1} }) );
	}

	var rowContent = [ entityAvatar, wrapView, postContentText ];

	if (postInfoView.children.length) {
		// postContentView.bottom = 30; // give some bottom space
		postContentText.applyProperties({
			bottom: (viewProperties && viewProperties.autoLink) ? 29 : 30,
			height: Ti.UI.SIZE
		});
		rowContent.push(postInfoView);
		entityAvatar.bottom = 42;
	}
	else {
		entityAvatar.bottom = isBottom ? 18 : 13;
	}

	if (hasLocation) {
		var entityLocation = Titanium.Map.createAnnotation({
			latitude: post.content.location.coordinates[0],
			longitude: post.content.location.coordinates[0],
			title: _entityURI.host(),
			// subtitle:'Mountain View, CA',
			pincolor:Titanium.Map.ANNOTATION_RED,
			animate:true,
			myid:1 // Custom property to uniquely identify this annotation.
		});
		Ti.API.debug('postInfoView.children.length -> '+postInfoView.children.length);
		var postInfoView_padding = postInfoView.children.length ? 13 : 0;
		var mapView = ui.view({
			properties: {left: 14, right: 14, height: 60,
				bottom: ((postContent && viewProperties && viewProperties.autoLink) ? 15 : 16) + postInfoView_padding,
				location: {
					latitude: post.content.location.coordinates[0],
					longitude: post.content.location.coordinates[1],
					title: _entityURI.host()
				}
			},
			content: [
				Ti.Map.createView({
					left: 0, right: 0, height: 60, touchEnabled: false,
					mapType: Titanium.Map.STANDARD_TYPE,
					region: {latitude: post.content.location.coordinates[0], longitude: post.content.location.coordinates[1],
							latitudeDelta:1, longitudeDelta:1},
					animate:true,
					regionFit:true,
					userLocation:false,
					annotations:[entityLocation]
				})
			],
			events: [['click', function(e){
				var location = e.source.location;
				Ti.API.debug('location: '+JSON.stringify(location));
				var entityLocation = Titanium.Map.createAnnotation({
					latitude: location.latitude,
					longitude: location.longitude,
					title: location.title,
					// subtitle:'Mountain View, CA',
					pincolor:Titanium.Map.ANNOTATION_RED,
					animate:true,
					myid:1 // Custom property to uniquely identify this annotation.
				});
				var mapViewWin = ui.win({
					config: 'windowProperties',
					properties: {
						leftNavButton: ui.button({
							config: 'backButton',
							events: [['click', function(e){ GLOBAL.tabGroup.activeTab.close(mapViewWin, {animated: true}); }]]
						})
					},
					content: [Ti.Map.createView({
						left: 0, right: 0, top: 0, bottom: 0,
						mapType: Titanium.Map.STANDARD_TYPE,
						region: {latitude: location.latitude, longitude: location.longitude,
								latitudeDelta:0.01, longitudeDelta:0.01},
						animate:true,
						regionFit:true,
						userLocation:false,
						annotations:[entityLocation]
					})]
				});
				GLOBAL.tabGroup.activeTab.open(mapViewWin);
			}]]
		});
		rowContent.push(mapView);
		postContentText.bottom = postContentText.bottom + 68 ;
	}

	// hack to set the height to acomodate content
	postContentView.height = Ti.UI.SIZE;
	wrapView.height = Ti.UI.SIZE;

	var rowProperties = { className: className, postData: post, height: Ti.UI.SIZE, hasDetails: hasDetails };
	if (viewProperties) _.extend(rowProperties, viewProperties);

	var row = ui.row({
		config: rowConfig,
		properties: rowProperties,
		content: rowContent
	});

	return row;
}

function getInlineMentions (postText){
	var re = /\^\[(.+)\]\((\d+)\)/ig,
		match, mentions = [];
	while (match = re.exec(postText)) { mentions.push( {url: match[1], index: match[2]} ); }
	return mentions;
}

function post2HTML (postData) {
	var postText = postData.content.text,
		postMentions = postData.mentions;
	function regex2anchor (foo, p1, p2) { return "<a href='"+postMentions[parseInt(p2,10)].entity+"'>"+p1+"</a>"; }
	return postText.replace(/\^\[(.+)\]\((\d+)\)/ig, regex2anchor);
}

function createWindowTitle (title){
	return Titanium.UI.createLabel({
		color:'#2e89c4',
		width:210, height:18, top:10,
		text: title,
		textAlign:'center',
		font:{fontFamily:'HelveticaNeue-Medium',fontSize:18},
		shadowColor:'#fff',shadowOffset:{x:0,y:1}
	});
}

/**
 * Override a tab group's tab bar on iOS.
 *
 * NOTE: Call this function on a tabGroup AFTER you have added all of your tabs to it! We'll look at the tabs that exist
 * to generate the overriding tab bar view. If your tabs change, call this function again and we'll update the display.
 *
 * @param tabGroup The tab group to override
 * @param backgroundOptions The options for the background view; use properties like backgroundColor, or backgroundImage.
 * @param selectedOptions The options for a selected tab button.
 * @param deselectedOptions The options for a deselected tab button.
 */
exports.overrideTabs = function (tabGroup, backgroundOptions, selectedOptions, deselectedOptions) {
	// a bunch of our options need to default to 0 for everything to position correctly; we'll do it en mass:
	backgroundOptions = _.extend({bottom: 0, height: 49, width: Ti.UI.FILL}, backgroundOptions);
	selectedOptions = _.extend({bottom: 0, height: 49}, selectedOptions);

	// // create the overriding tab bar using the passed in background options
	// backgroundOptions.height = backgroundOptions.height || 49;
	var background = Ti.UI.createView(backgroundOptions);

	// pass all touch events through to the tabs beneath our background
	background.touchEnabled = false;

	// create our individual tab buttons
	var increment = GLOBAL.isTablet ? 76 : 100 / tabGroup.tabs.length;
	selectedOptions.width = GLOBAL.isTablet ? increment : increment + '%';

	var buttonWrap = Ti.UI.createView({left: (Ti.Platform.displayCaps.platformWidth-296) /2});
	background.add(buttonWrap);

	for (var i = 0, l = tabGroup.tabs.length; i < l; i++) {
		var tab = tabGroup.tabs[i];

		// position our views over the tab.
		selectedOptions.left = GLOBAL.isTablet ? (increment*i) + (34*i) : increment * i + '%';

		// customize the selected and deselected based on properties in the tab.
		selectedOptions.title = tab.title;
		if (tab.backgroundImage) {
			selectedOptions.image = tab.backgroundImage;
		}

		tab.ct = Ti.UI.createImageView(selectedOptions);

		if (GLOBAL.isTablet) { buttonWrap.add(tab.ct); }
		else { background.add(tab.ct); }

	}

	tabGroup.selectTab = function(tab){
		tab.ct.image = tab.selectedBackgroundImage;
	};
	tabGroup.unselectTab = function(tab){
		tab.ct.image = tab.backgroundImage;
	};
	tabGroup.hasNotification = function(tab){
		tab.ct.image = tab.backgroundImage2;
	};

	// update the tab group, removing any old overrides
	if (tabGroup.overrideTabs) { tabGroup.remove(tabGroup.overrideTabs); }
	else { tabGroup.addEventListener('focus', overrideFocusTab); }

	tabGroup.add(background);
	tabGroup.overrideTabs = background;

	Titanium.Gesture.addEventListener('orientationchange', function(){
		buttonWrap.left = (Ti.Platform.displayCaps.platformWidth-296) /2 ;
	});

	return tabGroup;
};

function overrideFocusTab(e) {
	Ti.API.debug('tab focus - overrideFocusTab called');
	if (e.previousIndex >= 0) e.source.unselectTab(e.source.tabs[e.previousIndex]);
	e.source.selectTab(e.tab);
}

exports.createAvatar = function(image, properties, borderWidth){
	return ui.view({
		properties: _.extend({backgroundImage: 'images/avatar_bg.png', width: 46, height: 46}, properties),
		content: [ui.image({
			properties: {
				image: image, borderRadius: parseInt( borderWidth ? borderWidth/2 : 3, 10), top: parseInt( borderWidth ? borderWidth/2 : 4, 10),
				width: properties.width- (borderWidth || 9), height: properties.height- (borderWidth || 9)
			}
		})]
	});
};

exports.createTableSection = function(properties, data) {
	var section = Ti.UI.createTableViewSection(properties);

	if (data.length === 1) {
		section.add(ui.row({
			config: data[0].config+'_full',
			properties: data[0].properties,
			content: [ ui.view({
				config: 'rowContainerView',
				properties: {height: 40, top: 1},
				content: data[0].content
			}) ],
			events: data[0].events
		}));
	}
	else {
		for (var i=0, j=data.length; i<j; i++){
			var cur_data = data[i], config;
			if (i===0) {
				config = cur_data.config+'_top';
				properties = {height: 40, bottom: 0};
			}
			else if (i===j-1) {
				config = cur_data.config+'_bottom';
				properties = {height: 40, top: 0};
			}
			else {
				config = cur_data.config;
				properties = {height: 40, top: 0};
			}

			section.add(ui.row({
				config: config,
				properties: cur_data.properties,
				content: [ ui.view({
					config: 'rowContainerView',
					properties: properties,
					content: cur_data.content
				}) ],
				events: cur_data.events
			}));
		}
	}

	return section;
};

exports.createPostQuickMenu = function(eventData) {

	var NewPost = require('views/newPost2'),
		favoritePosts = require('lib/favoritePosts'),

		rowIndex = eventData.index,
		postData = eventData.rowData.postData,
		myEntity = GLOBAL.entity;

	var replyAll = false, myPost = false,
		isRepost = (postData.type === api.postType('Repost'));
		isFavorite = favoritePosts.isFavorite(postData.id);

	// no one was yet mentioned or only I was mentioned = no option to reply everyone
	if (postData.mentions.length < 2) {
		if (postData.mentions.length && postData.mentions[0].entity !== myEntity) { replyAll = true; }
	}
	// entities found in mentions = show option to reply everyone
	else { replyAll = true; }

	if (postData.entity === myEntity) myPost = true;


	var wrapperWin = ui.win({
		properties: {backgroundColor: 'rgba(0,0,0, 0.3)'},
		events: [
			['click', function(e){ closeMenu(); }],
			['open', function(e){ menuWrap.animate(slide_menu_in); }]
		]
	}),

	menuWrap = ui.view({
		properties: {
			backgroundImage: GLOBAL.isTablet ? 'images/postMenu_bg_ipad.png' : 'images/postMenu_bg.png',
			backgroundLeftCap: GLOBAL.isTablet ? 6 : 0,
			backgroundTopCap: GLOBAL.isTablet ? 6 : 0,
			backgroundRepeat: GLOBAL.isTablet ? false : true,
			height: GLOBAL.isTablet ? 246 : 240, // shadow is 6px high
			bottom: GLOBAL.isTablet ? -246 : -240,
			width: GLOBAL.isTablet ? 320 : Ti.UI.FILL
		},
		content: [
			// Ti.UI.createView({backgroundImage: 'images/postMenu_dropshadow.png', height: 12, width: Ti.UI.FILL, top: 0}),
			ui.view({
				properties: {
					height: 234, bottom: GLOBAL.isTablet ? 6 : 0,
					bubbleParent: false
				},
				content: [
					Ti.UI.createView({backgroundImage: 'images/postMenu_div.png', height: 150, width: 150, top: 46}), // buttons div
					ui.button({
						properties: {top: 23, left: 46, backgroundImage: 'images/postMenu_reply.png', width: 90, height: 70},
						events: [['click', function(e){
							(new NewPost({ originalPost: postData, reply: true })).open({modal: true});
							closeMenu();
						}]]
					}),
					ui.button({
						properties: {enabled: replyAll, top: 23, right: 46, backgroundImage: 'images/postMenu_replyall.png', width: 90, height: 70},
						events: [['click', function(e){
							(new NewPost({originalPost: postData, reply: true, replyAll: true})).open({modal: true});
							closeMenu();
						}]]
					}),
					(function(){
						if (myPost)
							return ui.button({
								properties: {bottom: 23, left: 46, backgroundImage: 'images/postMenu_delete.png', width: 90, height: 70},
								events: [['click', function(e){
									// only delete with user confirmation
									ui.alert({
										properties: {
											cancel: 1,
											buttonNames: [L('yes', 'Yes'), L('no', 'No')],
											message: L('delete_the_post','Delete the post')+'?',
											title: L('confirm','Confirm')
										},
										events: [
											['click', function(e){
												if (e.index != e.cancel) {
													ui.showIndicator();
													api.posts_DELETE({
														id: postData.id,
														eventData: { rowIndex: rowIndex }
													});
												}
											}]
										]
									}).show();
									closeMenu();
								}]]
							});
						else
							return ui.button({
								properties: {bottom: 23, left: 46, backgroundImage: 'images/postMenu_repost.png', width: 90, height: 70},
								events: [['click', function(e){
									// only repost with user confirmation
									ui.alert({
										properties: {
											cancel: 1,
											buttonNames: [L('yes', 'Yes'), L('no', 'No')],
											message: L('repost','Repost')+' '+(new uri(isRepost ? postData.original_post.post_content.entity : postData.entity)).host() +'?',
											title: L('confirm','Confirm')
										},
										events: [
											['click', function(e){
												if (e.index != e.cancel) {
													ui.showIndicator();
													api.posts_POST({
														"permissions": {"public":true},
														"type": api.postType('Repost'),
														"content":{
															"entity": isRepost ? postData.original_post.post_content.entity : postData.entity,
															"id": isRepost ? postData.original_post.post_content.id : postData.id},
														"mentions":[{
															"entity": isRepost ? postData.original_post.post_content.entity : postData.entity,
															"post": isRepost ? postData.original_post.post_content.id : postData.id}]
													});
												}
											}]
										]
									}).show();
									closeMenu();
								}]]
							});
					})(),
					ui.button({
						properties: {bottom: 23, right: 46,
							backgroundImage: isFavorite ? 'images/postMenu_unfav.png' : 'images/postMenu_fav.png', width: 90, height: 70},
						events: [['click', function(e){
							if (isFavorite) { favoritePosts.removeFavorite( postData.id ); e.source.backgroundImage = 'images/postMenu_unfav.png'; }
							else { favoritePosts.addFavorite( postData ); e.source.backgroundImage = 'images/postMenu_fav.png'; }
							closeMenu();
						}]]
					})
				]
			})
		]
	}),

	slide_menu_in =  Titanium.UI.createAnimation({bottom: GLOBAL.isTablet ? (Ti.Platform.displayCaps.platformHeught / 2) - 123 : 0, duration: 200}),
	slide_menu_out =  Titanium.UI.createAnimation({bottom: -246, duration: 200}),

	cancelButton = ui.button({
		properties: {
			width: 280, height: 44, bottom: 20, title: 'Cancel'
		},
		events: [
			['click', function(e){
				setTimeout(function(){ wrapperWin.close(); }, 205);
				menuWrap.animate(slide_menu_out);
			}]
		]
	});

	// menu.add(cancelButton);
	wrapperWin.add(menuWrap);

	function closeMenu() {
		setTimeout(function(){ wrapperWin.close(); }, 205);
		menuWrap.animate(slide_menu_out);
	}

	return wrapperWin;
};

exports.cropImage = function(original, w, h){
	// Here's our base (full-size) image.
	// It's never displayed as-is.
	var baseImage = Titanium.UI.createImageView({
		image: original,
		width: 'auto', height: 'auto'
	});

	// Here's the view we'll use to do the cropping.
	// It's never displayed either.
	var cropView = Titanium.UI.createView({
		width: w, height: h
	});

	// Add the image to the crop-view.
	// Position it left and above origin.
	cropView.add(baseImage);
	baseImage.left= -(baseImage.size.width/2 - cropView.width/2);
	baseImage.top= -(baseImage.size.height/2 - cropView.height/2);

	// now convert the crop-view to an image Blob and return an imageView
	return Titanium.UI.createImageView({
		image: cropView.toImage(),
		width: w, height: h
	});
};

exports.createRowFromPost = createRowFromPost;
exports.createRowDetailsFromPost = createRowDetailsFromPost;
exports.createWindowTitle = createWindowTitle;
